package com.b2infosoft_jaipur.chakhle.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.pojo.OfferListPojo;

import java.util.ArrayList;

/**
 * Created by Microsoft on 6/20/2017.
 */

public class OfferListAdapter extends RecyclerView.Adapter<OfferListAdapter.OfferListHolder> {

    Context mContext;
    ArrayList<OfferListPojo> mList = new ArrayList<>();

    public OfferListAdapter(Context ctx, ArrayList<OfferListPojo> resturentListPojos) {

        mContext = ctx;
        mList = resturentListPojos;
    }


    @Override
    public OfferListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_offer_list_row, parent, false);
        return new OfferListHolder(view);
    }

    @Override
    public void onBindViewHolder(OfferListHolder holder, final int position) {
        if (mList.get(position).type.equals("percentage")) {
            holder.tvCoupanCode.setText(mList.get(position).Offer_CuopanCode);
            holder.tvOffers.setText(mList.get(position).value + "% on " + mList.get(position).discount_title);
        } else if (mList.get(position).type.equals("price")) {
            holder.tvCoupanCode.setText(mList.get(position).Offer_CuopanCode);
            holder.tvOffers.setText("Rs " + mList.get(position).value + " " + mList.get(position).discount_title);
        }


       /* if (position % 2 == 0) {
                     holder.card_view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_red));
        } else {

            holder.card_view.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.holo_green_dark));
        }*/


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class OfferListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvCoupanCodeTxt, tvCoupanCode, tvOffers, tvKnowMore;
        CardView card_view;

        public OfferListHolder(View itemView) {
            super(itemView);
            tvCoupanCodeTxt = (TextView) itemView.findViewById(R.id.tvCoupanCodeTxt);
            tvCoupanCode = (TextView) itemView.findViewById(R.id.tvCoupanCode);
            tvOffers = (TextView) itemView.findViewById(R.id.tvOffers);
            tvKnowMore = (TextView) itemView.findViewById(R.id.tvKnowMore);
            card_view = (CardView) itemView.findViewById(R.id.card_view);

            tvCoupanCode.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
            tvCoupanCodeTxt.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tvOffers.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tvKnowMore.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();

            switch (view.getId()) {
               /* case R.id.iconArrow:
                    AppGlobal.OrderID = mList.get(pos).OrderID;
                    mContext.startActivity(new Intent(mContext, OrderStatusActivity.class));
                    break;*/
            }
        }
    }
}
