package com.b2infosoft_jaipur.chakhle.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.activities.RestaurentDetailsActivity;
import com.b2infosoft_jaipur.chakhle.activities.ReviewOrderActivity;
import com.b2infosoft_jaipur.chakhle.fragments.CartValueInterface;
import com.b2infosoft_jaipur.chakhle.fragments.NewCartDataFill;
import com.b2infosoft_jaipur.chakhle.fragments.NotifyAdapter;
import com.b2infosoft_jaipur.chakhle.pojo.RestaurentDetailsPojo;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Microsoft on 6/14/2017.
 */

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.ViewHolder> {

    private ArrayList<RestaurentDetailsPojo> itemsData;
    Context mContext;
    int count = 0;
    CartValueInterface callFragment;
    int cartValue = 0;
    Dialog dialog;
    int totQty = 0;
    NotifyAdapter notifyAdapter;
    ViewHolder recommendedVh;

    public SearchListAdapter(Context mContext, ArrayList<RestaurentDetailsPojo> itemsData) {
        this.itemsData = itemsData;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_product_list_row, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        recommendedVh = viewHolder;
        callFragment = (CartValueInterface) mContext;
        recommendedVh.setIsRecyclable(false);

        //final RestaurentDetailsPojo restaurentDetailsPojo = AppGlobal.RestaurentProductList.get(position);
        totQty = AppGlobal.RestaurentProductList.get(position).halfQty + AppGlobal.RestaurentProductList.get(position).fullQty + AppGlobal.RestaurentProductList.get(position).mediumQty + AppGlobal.RestaurentProductList.get(position).largeQty;
        viewHolder.tvQuantity.setText("" + totQty);
        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {


                if (!AppGlobal.RestaurentProductList.get(i).in_time.equals("") && !AppGlobal.RestaurentProductList.get(i).out_time.equals("")) {
                    String in_time = AppGlobal.RestaurentProductList.get(i).in_time;
                    String out_time = AppGlobal.RestaurentProductList.get(i).out_time;
                    try {
                        Date mToday = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
                        String curTime = sdf.format(mToday);
                        Date start = sdf.parse(in_time);
                        Date end = sdf.parse(out_time);
                        Date userDate = sdf.parse(curTime);

                        if (end.before(start)) {
                            Calendar mCal = Calendar.getInstance();
                            mCal.setTime(end);
                            mCal.add(Calendar.DAY_OF_YEAR, 1);
                            end.setTime(mCal.getTimeInMillis());
                        }
                        Log.d("curTime", userDate.toString());
                        Log.d("start", start.toString());
                        Log.d("end", end.toString());
                        if (userDate.after(start) && userDate.before(end)) {
                            Log.d("result", "falls between start and end , go to screen 1 ");
                            Log.d("Amar>>>", "Yes");
                            viewHolder.LayoutPlusDummy.setVisibility(View.GONE);
                            viewHolder.tvQuantity.setVisibility(View.GONE);

                        } else {
                            Log.d("Amar>>>", "No");
                            Log.d("result", "does not fall between start and end , go to screen 2 ");
                            viewHolder.LayoutPlusDummy.setVisibility(View.VISIBLE);
                            viewHolder.tvQuantity.setVisibility(View.GONE);
                        }
                    } catch (ParseException e) {
                        viewHolder.LayoutPlusDummy.setVisibility(View.VISIBLE);
                        viewHolder.tvQuantity.setVisibility(View.GONE);
                        // Invalid date was entered
                    }
                } else {
                    viewHolder.LayoutPlusDummy.setVisibility(View.VISIBLE);
                    viewHolder.tvQuantity.setVisibility(View.GONE);

                }


            }

        }


        viewHolder.tvFoodName.setText(itemsData.get(position).Product_name);

        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                if (!AppGlobal.RestaurentProductList.get(i).Product_description.equals("")) {
                    viewHolder.tvFoodDesc.setVisibility(View.VISIBLE);
                    viewHolder.div2.setVisibility(View.VISIBLE);
                    viewHolder.tvFoodDesc.setText(AppGlobal.RestaurentProductList.get(i).Product_description);
                }
            }
        }
        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                if (AppGlobal.RestaurentProductList.get(i).Product_vegproduct.equals("veg")) {
                    viewHolder.tvFoodType.setBackgroundResource(R.drawable.veg_icon);

                } else {
                    viewHolder.tvFoodType.setBackgroundResource(R.drawable.non_veg_icon);
                }
            }
        }

        viewHolder.tvItemRs.setText(" Rs. " + itemsData.get(position).half);
        if (!AppGlobal.RestDiscount.equals("")) {
            // viewHolder.tvItemRs.setPaintFlags(viewHolder.tvItemRs.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            int discount = Integer.parseInt(AppGlobal.RestDiscount);
            int actualPrice = Integer.parseInt(AppGlobal.RestaurentProductList.get(position).Product_price);
            int discountRs = discount * actualPrice;
            int totalDiscount = discountRs / 100;
            int initialPrice = actualPrice - totalDiscount;
            Log.d("afterdiscount>>>", "" + initialPrice);
            Log.d("afterdiscount>>>", "" + initialPrice);
            AppGlobal.RestaurentProductList.get(position).setAfterDiscountValue(initialPrice);
            viewHolder.tvDiscountRs.setVisibility(View.GONE);
            viewHolder.tvDiscountRs.setText("Rs. " + initialPrice);
        } else {
            AppGlobal.RestaurentProductList.get(position).setAfterDiscountValue(Integer.parseInt(AppGlobal.RestaurentProductList.get(position).Product_price));
        }
        count = AppGlobal.RestaurentProductList.get(position).getCount();
        Log.d("count>>>", "" + AppGlobal.RestaurentProductList.get(position).getCount());
        viewHolder.tvItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).getCount());
        viewHolder.imgMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count = AppGlobal.RestaurentProductList.get(position).getCount();
                if (count == 0) {
                    count = 0;
                    cartValue = 0;
                    callFragment.setCartData(cartValue, "check", AppGlobal.RestaurentCategoryID);
                    viewHolder.tvItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).getCount());
                    if (count == 0) {
                        viewHolder.imgMines.setImageResource(R.drawable.disable_mines);
                        AppGlobal.RestaurentProductList.get(position).setCount(count);
                    }
                } else {
                    callFragment.setCartData(cartValue, "-", AppGlobal.RestaurentCategoryID);
                    count = count - 1;
                    AppGlobal.RestaurentProductList.get(position).setCount(count);
                    viewHolder.tvItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).getCount());
                    cartValue = cartValue - 1;
                    if (count == 0) {
                        viewHolder.imgMines.setImageResource(R.drawable.disable_mines);
                    } else {
                        viewHolder.imgMines.setImageResource(R.drawable.enable_mines);
                    }
                }
                if (count == 0) {
                    callFragment.setCartData(cartValue, "check", AppGlobal.RestaurentCategoryID);
                }
                Log.d("CartValue>>>", "" + cartValue);
            }
        });

        viewHolder.imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (AppGlobal.mainCartList.isEmpty()) {
                    count = AppGlobal.RestaurentProductList.get(position).getCount();
                    count = count + 1;
                    cartValue = cartValue + 1;
                    AppGlobal.RestaurentProductList.get(position).setCount(count);
                    viewHolder.tvItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).getCount());
                    if (count == 0) {
                        viewHolder.imgMines.setImageResource(R.drawable.disable_mines);
                    } else {
                        viewHolder.imgMines.setImageResource(R.drawable.enable_mines);
                    }
                    count = 0;
                    Log.d("CartValue>>>", "" + cartValue);
                    AppGlobal.ProductIDList.add(AppGlobal.RestaurentCategoryID);
                    callFragment.setCartData(cartValue, "+", AppGlobal.RestaurentCategoryID);
                } else {
                    if (AppGlobal.RestaurentProductList.get(position).restaurant_id.equals(AppGlobal.RestaurentCategoryID)) {
                        AppGlobal.mainCartList.clear();
                        count = AppGlobal.RestaurentProductList.get(position).getCount();
                        count = count + 1;
                        cartValue = cartValue + 1;
                        AppGlobal.RestaurentProductList.get(position).setCount(count);
                        viewHolder.tvItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).getCount());
                        if (count == 0) {
                            viewHolder.imgMines.setImageResource(R.drawable.disable_mines);
                        } else {
                            viewHolder.imgMines.setImageResource(R.drawable.enable_mines);
                        }
                        count = 0;
                        Log.d("CartValue>>>", "" + cartValue);
                        AppGlobal.ProductIDList.add(AppGlobal.RestaurentCategoryID);
                        callFragment.setCartData(cartValue, "+", AppGlobal.RestaurentCategoryID);
                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                        alertDialogBuilder.setMessage("Your cart contains already dishes, Are You want to clear them and add another dishes.");
                        alertDialogBuilder.setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        // Toast.makeText(mContext, "You clicked yes  button", Toast.LENGTH_LONG).show();
                                        AppGlobal.mainCartList.clear();
                                        count = AppGlobal.RestaurentProductList.get(position).getCount();
                                        count = count + 1;
                                        cartValue = cartValue + 1;
                                        AppGlobal.RestaurentProductList.get(position).setCount(count);
                                        viewHolder.tvItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).getCount());
                                        if (count == 0) {
                                            viewHolder.imgMines.setImageResource(R.drawable.disable_mines);
                                        } else {
                                            viewHolder.imgMines.setImageResource(R.drawable.enable_mines);
                                        }
                                        count = 0;
                                        Log.d("CartValue>>>", "" + cartValue);
                                        AppGlobal.ProductIDList.add(AppGlobal.RestaurentCategoryID);
                                        callFragment.setCartData(cartValue, "+", AppGlobal.RestaurentCategoryID);
                                    }
                                });

                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mContext.startActivity(new Intent(mContext, ReviewOrderActivity.class));
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                }
            }
        });

        if (itemsData.size() - 1 == position) {
            viewHolder.blankSpace.setVisibility(View.VISIBLE);
        } else {
            viewHolder.blankSpace.setVisibility(View.GONE);
        }

        viewHolder.imgPlusDummy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // ArrayList<ProductListingDialogAdapterRowPojo> productListingDialogAdapterRowPojos1 = new ArrayList<>();
                // productListingDialogAdapterRowPojos1.add(new ProductListingDialogAdapterRowPojo(restaurentProductList.get(position).half, restaurentProductList.get(position).half_label, "0", "0"));
                // productListingDialogAdapterRowPojos1.add(new ProductListingDialogAdapterRowPojo(restaurentProductList.get(position).medium, restaurentProductList.get(position).medium_label, "0", "0"));
                // productListingDialogAdapterRowPojos1.add(new ProductListingDialogAdapterRowPojo(restaurentProductList.get(position).full, restaurentProductList.get(position).full_label, "0", "0"));
                // productListingDialogAdapterRowPojos1.add(new ProductListingDialogAdapterRowPojo(restaurentProductList.get(position).large, restaurentProductList.get(position).large_label, "0", "0"));
                generateItemReport(viewHolder, position);
            }
        });

    }


    private void generateItemReport(final ViewHolder recommendedVh, final int position) {
        totQty = 0;
        Button btnConfirm, btnCancel;
        RecyclerView recycler_productList_Dialog;
        final TextView tvHalfPrice, tvHalfLabel, tvHalfItemCount, tvMediumPrice, tvMediumLabel, tvMediumItemCount, tvFullPrice, tvFullLabel, tvFullItemCount, tvLargePrice, tvLargeLabel, tvLargeItemCount;
        ImageView imgHalfMines, imgHalfPlus, imgMediumMines, imgMediumPlus, imgFullMines, imgFullPlus, imgLargeMines, imgLargePlus;
        LinearLayout layoutHalf, layoutMedium, layoutFull, layoutLarge;
        View div3, div4, div2, div1;

        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_orders);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        recycler_productList_Dialog = (RecyclerView) dialog.findViewById(R.id.recycler_productList_Dialog);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recycler_productList_Dialog.setLayoutManager(linearLayoutManager);
        recycler_productList_Dialog.setHasFixedSize(true);
        dialog.show();

        final int halfQty_old = AppGlobal.RestaurentProductList.get(position).halfQty;
        final int mediumQty_old = AppGlobal.RestaurentProductList.get(position).mediumQty;
        final int fullQty_old = AppGlobal.RestaurentProductList.get(position).fullQty;
        final int largeQty_old = AppGlobal.RestaurentProductList.get(position).largeQty;

        Log.d("halfQty_old", "" + halfQty_old);
        Log.d("mediumQty_old", "" + mediumQty_old);
        Log.d("fullQty_old", "" + fullQty_old);
        Log.d("largeQty_old", "" + largeQty_old);

        layoutHalf = (LinearLayout) dialog.findViewById(R.id.layoutHalf);
        layoutMedium = (LinearLayout) dialog.findViewById(R.id.layoutMedium);
        layoutFull = (LinearLayout) dialog.findViewById(R.id.layoutFull);
        layoutLarge = (LinearLayout) dialog.findViewById(R.id.layoutLarge);

        div1 = (View) dialog.findViewById(R.id.div1);
        div2 = (View) dialog.findViewById(R.id.div2);
        div3 = (View) dialog.findViewById(R.id.div3);
        div4 = (View) dialog.findViewById(R.id.div4);

        tvHalfPrice = (TextView) dialog.findViewById(R.id.tvHalfPrice);
        tvHalfLabel = (TextView) dialog.findViewById(R.id.tvHalfLabel);
        tvHalfItemCount = (TextView) dialog.findViewById(R.id.tvHalfItemCount);
        tvMediumPrice = (TextView) dialog.findViewById(R.id.tvMediumPrice);
        tvMediumLabel = (TextView) dialog.findViewById(R.id.tvMediumLabel);
        tvMediumItemCount = (TextView) dialog.findViewById(R.id.tvMediumItemCount);
        tvFullItemCount = (TextView) dialog.findViewById(R.id.tvFullItemCount);
        tvLargeLabel = (TextView) dialog.findViewById(R.id.tvLargeLabel);
        tvLargeItemCount = (TextView) dialog.findViewById(R.id.tvLargeItemCount);
        tvLargePrice = (TextView) dialog.findViewById(R.id.tvLargePrice);
        tvFullPrice = (TextView) dialog.findViewById(R.id.tvFullPrice);
        tvFullLabel = (TextView) dialog.findViewById(R.id.tvFullLabel);

        if (!itemsData.get(position).half.equals("0") || !itemsData.get(position).half_label.equals("")) {
            layoutHalf.setVisibility(View.VISIBLE);
            div1.setVisibility(View.VISIBLE);

            for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                    tvHalfItemCount.setText("" + AppGlobal.RestaurentProductList.get(i).halfQty);
                }
            }
        }

        if (!itemsData.get(position).medium.equals("0") || !itemsData.get(position).medium_label.equals("")) {
            layoutMedium.setVisibility(View.VISIBLE);
            div2.setVisibility(View.VISIBLE);

            for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                    tvMediumItemCount.setText("" + AppGlobal.RestaurentProductList.get(i).mediumQty);
                }
            }
        }

        if (!itemsData.get(position).full.equals("0") || !itemsData.get(position).full_label.equals("")) {
            layoutFull.setVisibility(View.VISIBLE);
            div3.setVisibility(View.VISIBLE);

            for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                    tvFullItemCount.setText("" + AppGlobal.RestaurentProductList.get(i).fullQty);
                }
            }
        }
        if (!itemsData.get(position).large.equals("0") || !itemsData.get(position).large_label.equals("")) {
            layoutLarge.setVisibility(View.VISIBLE);
            div4.setVisibility(View.VISIBLE);

            for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                    tvLargeItemCount.setText("" + AppGlobal.RestaurentProductList.get(i).largeQty);
                }
            }
        }

        tvHalfPrice.setText("Rs. " + itemsData.get(position).half);
        tvHalfLabel.setText(itemsData.get(position).half_label);
        // tvHalfItemCount.setText("" + itemsData.get(position).halfQty);

        tvMediumPrice.setText("Rs. " + itemsData.get(position).medium);
        tvMediumLabel.setText("" + itemsData.get(position).medium_label);
        // tvMediumItemCount.setText("" + itemsData.get(position).mediumQty);

        tvLargeLabel.setText("" + itemsData.get(position).large_label);
        tvLargePrice.setText("Rs. " + itemsData.get(position).large);
        //  tvLargeItemCount.setText("" + itemsData.get(position).largeQty);

        tvFullPrice.setText("Rs. " + itemsData.get(position).full);
        tvFullLabel.setText("" + itemsData.get(position).full_label);
        //   tvFullItemCount.setText("" + itemsData.get(position).fullQty);

        imgHalfMines = (ImageView) dialog.findViewById(R.id.imgHalfMines);
        imgHalfPlus = (ImageView) dialog.findViewById(R.id.imgHalfPlus);
        imgMediumMines = (ImageView) dialog.findViewById(R.id.imgMediumMines);
        imgMediumPlus = (ImageView) dialog.findViewById(R.id.imgMediumPlus);
        imgFullMines = (ImageView) dialog.findViewById(R.id.imgFullMines);
        imgFullPlus = (ImageView) dialog.findViewById(R.id.imgFullPlus);
        imgLargeMines = (ImageView) dialog.findViewById(R.id.imgLargeMines);
        imgLargePlus = (ImageView) dialog.findViewById(R.id.imgLargePlus);

        imgHalfMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int halfQty = 0;

                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        Log.d("Product_id>>>>", AppGlobal.RestaurentProductList.get(i).Product_id);
                        Log.d("Product_id>>>>", itemsData.get(position).Product_id);
                        halfQty = AppGlobal.RestaurentProductList.get(i).halfQty;
                        Log.d("halfQty>>>", "" + halfQty);
                        if (halfQty == 0) {
                            AppGlobal.RestaurentProductList.get(i).halfQty = 0;
                        } else {
                            halfQty = halfQty - 1;
                            AppGlobal.RestaurentProductList.get(i).halfQty = halfQty;
                        }
                        //        totQty = totQty + halfQty;

                        Log.d("fdfdf", AppGlobal.RestaurentProductList.get(i).halfQty + "  halfQty ");
                        Log.d("fdfdf", AppGlobal.RestaurentProductList.get(i).mediumQty + "  halfQty ");
                        Log.d("fdfdf", AppGlobal.RestaurentProductList.get(i).fullQty + "  halfQty ");
                        Log.d("fdfdf", AppGlobal.RestaurentProductList.get(i).largeQty + "  halfQty ");
                    }


                }
                tvHalfItemCount.setText("" + halfQty);
                // notifyAdapter = (RestaurentDetailsActivity) mContext;
                //  notifyAdapter.notifyData(AppGlobal.cateID);
            }
        });
        imgHalfPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int halfQty = 0;

                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        halfQty = AppGlobal.RestaurentProductList.get(i).halfQty;
                        Log.d("halfQty>>>", "" + halfQty);
                        halfQty = halfQty + 1;
                        //   totQty = totQty + halfQty;
                        AppGlobal.RestaurentProductList.get(i).halfQty = halfQty;
                    }
                }

                tvHalfItemCount.setText("" + halfQty);
                // notifyAdapter = (RestaurentDetailsActivity) mContext;
                // notifyAdapter.notifyData(AppGlobal.cateID);
            }
        });
        imgMediumMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int mediumQty = 0;

                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        Log.d("Product_id>>>>", AppGlobal.RestaurentProductList.get(i).Product_id);
                        Log.d("Product_id>>>>", itemsData.get(position).Product_id);
                        mediumQty = AppGlobal.RestaurentProductList.get(i).mediumQty;
                        Log.d("mediumQty>>>", "" + mediumQty);
                        if (mediumQty == 0) {
                            AppGlobal.RestaurentProductList.get(i).mediumQty = 0;
                        } else {
                            mediumQty = mediumQty - 1;
                            AppGlobal.RestaurentProductList.get(i).mediumQty = mediumQty;
                        }
                        //        totQty = totQty + halfQty;
                    }
                }

                tvMediumItemCount.setText("" + mediumQty);
                //  notifyAdapter = (RestaurentDetailsActivity) mContext;
                // notifyAdapter.notifyData(AppGlobal.cateID);
            }
        });
        imgMediumPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                int mediumQty = 0;

                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        mediumQty = AppGlobal.RestaurentProductList.get(i).mediumQty;
                        Log.d("mediumQty>>>", "" + mediumQty);
                        mediumQty = mediumQty + 1;
                        //   totQty = totQty + halfQty;
                        AppGlobal.RestaurentProductList.get(i).mediumQty = mediumQty;
                    }
                }

                tvMediumItemCount.setText("" + mediumQty);
                //   notifyAdapter = (RestaurentDetailsActivity) mContext;
                //   notifyAdapter.notifyData(AppGlobal.cateID);

            }
        });
        imgFullMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                int fullQty = 0;

                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        Log.d("Product_id>>>>", AppGlobal.RestaurentProductList.get(i).Product_id);
                        Log.d("Product_id>>>>", itemsData.get(position).Product_id);
                        fullQty = AppGlobal.RestaurentProductList.get(i).fullQty;
                        Log.d("fullQty>>>", "" + fullQty);
                        if (fullQty == 0) {
                            AppGlobal.RestaurentProductList.get(i).fullQty = 0;
                        } else {
                            fullQty = fullQty - 1;
                            AppGlobal.RestaurentProductList.get(i).fullQty = fullQty;
                        }
                        //        totQty = totQty + halfQty;
                    }
                }

                // totQty = totQty + fullQty;
                tvFullItemCount.setText("" + fullQty);
                // notifyAdapter = (RestaurentDetailsActivity) mContext;
                // notifyAdapter.notifyData(AppGlobal.cateID);

            }
        });
        imgFullPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int fullQty = 0;

                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        fullQty = AppGlobal.RestaurentProductList.get(i).fullQty;
                        Log.d("fullQty>>>", "" + fullQty);
                        fullQty = fullQty + 1;
                        //   totQty = totQty + halfQty;
                        AppGlobal.RestaurentProductList.get(i).fullQty = fullQty;
                    }
                }


                tvFullItemCount.setText("" + fullQty);
                //  notifyAdapter = (RestaurentDetailsActivity) mContext;
                //  notifyAdapter.notifyData(AppGlobal.cateID);
                //   totQty = totQty + fullQty;
            }
        });

        imgLargeMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int largeQty = 0;

                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        Log.d("Product_id>>>>", AppGlobal.RestaurentProductList.get(i).Product_id);
                        Log.d("Product_id>>>>", itemsData.get(position).Product_id);
                        largeQty = AppGlobal.RestaurentProductList.get(i).largeQty;
                        Log.d("largeQty>>>", "" + largeQty);
                        if (largeQty == 0) {
                            AppGlobal.RestaurentProductList.get(i).largeQty = 0;
                        } else {
                            largeQty = largeQty - 1;
                            AppGlobal.RestaurentProductList.get(i).largeQty = largeQty;
                        }
                        //        totQty = totQty + halfQty;
                    }

                }

                //totQty = totQty + largeQty;
                tvLargeItemCount.setText("" + largeQty);
                //notifyAdapter = (RestaurentDetailsActivity) mContext;
                //notifyAdapter.notifyData(AppGlobal.cateID);

            }
        });
        imgLargePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                int largeQty = 0;

                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        largeQty = AppGlobal.RestaurentProductList.get(i).largeQty;
                        Log.d("largeQty>>>", "" + largeQty);
                        largeQty = largeQty + 1;
                        //   totQty = totQty + halfQty;
                        AppGlobal.RestaurentProductList.get(i).largeQty = largeQty;
                    }
                }


                tvLargeItemCount.setText("" + largeQty);
                //  notifyAdapter = (RestaurentDetailsActivity) mContext;
                // notifyAdapter.notifyData(AppGlobal.cateID);
                //   totQty = totQty + largeQty;
            }
        });
        //   ProductListingDialogAdapter productListingDialogAdapter = new ProductListingDialogAdapter(mContext, AppGlobal.RestaurentProductList, productListingDialogAdapterRowPojos1);
        //  recycler_productList_Dialog.setAdapter(productListingDialogAdapter);
        btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppGlobal.ProductIDList.isEmpty()) {
                    NewCartDataFill newCartDataFill = (NewCartDataFill) mContext;
                    newCartDataFill.newCartFillData(AppGlobal.RestaurentProductList);
                    dialog.dismiss();
                    Log.d("Confirm>>", "Confirm");
                    totQty = 0;
                    totQty = AppGlobal.RestaurentProductList.get(position).halfQty + AppGlobal.RestaurentProductList.get(position).fullQty + AppGlobal.RestaurentProductList.get(position).mediumQty + AppGlobal.RestaurentProductList.get(position).largeQty;
                    recommendedVh.tvQuantity.setText("" + totQty);
                } else if (AppGlobal.ProductIDList.get(0).equals(AppGlobal.RestaurentCategoryID)) {
                    AppGlobal.ProductIDList.add(AppGlobal.RestaurentCategoryID);
                    {
                        NewCartDataFill newCartDataFill = (NewCartDataFill) mContext;
                        newCartDataFill.newCartFillData(AppGlobal.RestaurentProductList);
                        dialog.dismiss();
                        Log.d("Confirm>>", "Confirm");
                        totQty = 0;
                        totQty = AppGlobal.RestaurentProductList.get(position).halfQty + AppGlobal.RestaurentProductList.get(position).fullQty + AppGlobal.RestaurentProductList.get(position).mediumQty + AppGlobal.RestaurentProductList.get(position).largeQty;
                        recommendedVh.tvQuantity.setText("" + totQty);
                    }

                } else {
                    {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                        alertDialogBuilder.setMessage("Your cart contains already dishes, Are You want to clear them and add another dishes.");
                        alertDialogBuilder.setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        // Toast.makeText(mContext, "You clicked yes  button", Toast.LENGTH_LONG).show();
                                        AppGlobal.ProductIDList.clear();

                                        NewCartDataFill newCartDataFill = (NewCartDataFill) mContext;
                                        newCartDataFill.newCartFillData(AppGlobal.RestaurentProductList);
                                        dialog.dismiss();
                                        Log.d("Confirm>>", "Confirm");
                                        totQty = 0;
                                        totQty = AppGlobal.RestaurentProductList.get(position).halfQty + AppGlobal.RestaurentProductList.get(position).fullQty + AppGlobal.RestaurentProductList.get(position).mediumQty + AppGlobal.RestaurentProductList.get(position).largeQty;
                                        recommendedVh.tvQuantity.setText("" + totQty);
                                        notifyAdapter = (RestaurentDetailsActivity) mContext;
                                        notifyAdapter.notifyData(AppGlobal.cateID);
                                    }
                                });

                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mContext.startActivity(new Intent(mContext, ReviewOrderActivity.class));
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                }
            }
        });


        btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AppGlobal.RestaurentProductList.get(position).halfQty = halfQty_old;
                AppGlobal.RestaurentProductList.get(position).mediumQty = mediumQty_old;
                AppGlobal.RestaurentProductList.get(position).fullQty = fullQty_old;
                AppGlobal.RestaurentProductList.get(position).largeQty = largeQty_old;


                if (AppGlobal.ProductIDList.isEmpty()) {
                    NewCartDataFill newCartDataFill = (NewCartDataFill) mContext;
                    newCartDataFill.newCartFillData(AppGlobal.RestaurentProductList);
                    dialog.dismiss();
                    Log.d("Confirm>>", "Confirm");
                    totQty = 0;
                    totQty = AppGlobal.RestaurentProductList.get(position).halfQty + AppGlobal.RestaurentProductList.get(position).fullQty + AppGlobal.RestaurentProductList.get(position).mediumQty + AppGlobal.RestaurentProductList.get(position).largeQty;
                    recommendedVh.tvQuantity.setText("" + totQty);
                } else if (AppGlobal.ProductIDList.get(0).equals(AppGlobal.RestaurentCategoryID)) {
                    AppGlobal.ProductIDList.add(AppGlobal.RestaurentCategoryID);
                    {
                        NewCartDataFill newCartDataFill = (NewCartDataFill) mContext;
                        newCartDataFill.newCartFillData(AppGlobal.RestaurentProductList);
                        dialog.dismiss();
                        Log.d("Confirm>>", "Confirm");
                        totQty = 0;
                        totQty = AppGlobal.RestaurentProductList.get(position).halfQty + AppGlobal.RestaurentProductList.get(position).fullQty + AppGlobal.RestaurentProductList.get(position).mediumQty + AppGlobal.RestaurentProductList.get(position).largeQty;
                        recommendedVh.tvQuantity.setText("" + totQty);
                    }

                } else {
                    {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                        alertDialogBuilder.setMessage("Your cart contains already dishes, Are You want to clear them and add another dishes.");
                        alertDialogBuilder.setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        // Toast.makeText(mContext, "You clicked yes  button", Toast.LENGTH_LONG).show();
                                        AppGlobal.ProductIDList.clear();

                                        NewCartDataFill newCartDataFill = (NewCartDataFill) mContext;
                                        newCartDataFill.newCartFillData(AppGlobal.RestaurentProductList);
                                        dialog.dismiss();
                                        Log.d("Confirm>>", "Confirm");
                                        totQty = 0;
                                        totQty = AppGlobal.RestaurentProductList.get(position).halfQty + AppGlobal.RestaurentProductList.get(position).fullQty + AppGlobal.RestaurentProductList.get(position).mediumQty + AppGlobal.RestaurentProductList.get(position).largeQty;
                                        recommendedVh.tvQuantity.setText("" + totQty);
                                        notifyAdapter = (RestaurentDetailsActivity) mContext;
                                        notifyAdapter.notifyData(AppGlobal.cateID);
                                    }
                                });

                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mContext.startActivity(new Intent(mContext, ReviewOrderActivity.class));
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                }
            }
        });

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvFoodName, tvFoodDesc, tvItemRs, tvItemCount, tvDiscountRs, blankSpace;
        ImageView imgMines, imgPlus, tvFoodType, imgPlusDummy;
        View div2;
        LinearLayout LayoutPlusDummy, layoutPlusMinus;
        TextView tvQuantity;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tvFoodName = (TextView) itemLayoutView.findViewById(R.id.tvFoodName);
            tvFoodType = (ImageView) itemView.findViewById(R.id.tvFoodType);
            tvFoodDesc = (TextView) itemLayoutView.findViewById(R.id.tvFoodDesc);
            tvItemRs = (TextView) itemLayoutView.findViewById(R.id.tvActualRs);
            tvItemCount = (TextView) itemLayoutView.findViewById(R.id.tvItemCount);
            imgMines = (ImageView) itemLayoutView.findViewById(R.id.imgMines);
            blankSpace = (TextView) itemLayoutView.findViewById(R.id.blankSpace);
            tvQuantity = (TextView) itemView.findViewById(R.id.tvHalfQty);

            imgPlus = (ImageView) itemLayoutView.findViewById(R.id.imgPlus);
            div2 = (View) itemLayoutView.findViewById(R.id.div2);
            tvFoodName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
            tvFoodDesc.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            //  tvItemRs.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
            // tvItemCount.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tvDiscountRs = (TextView) itemView.findViewById(R.id.tvDiscountRs);
            // tvDiscountRs.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
            tvFoodDesc.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));

            layoutPlusMinus = (LinearLayout) itemView.findViewById(R.id.layoutPlusMinus);
            LayoutPlusDummy = (LinearLayout) itemView.findViewById(R.id.LayoutPlusDummy);
            imgPlusDummy = (ImageView) itemView.findViewById(R.id.imgPlusDummy);
            tvQuantity.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            Log.d("pos>>>", "" + pos);
            for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                if (itemsData.get(pos).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                    if (!AppGlobal.RestaurentProductList.get(i).in_time.equals("") && !AppGlobal.RestaurentProductList.get(i).out_time.equals("")) {
                        String in_time = AppGlobal.RestaurentProductList.get(i).in_time;
                        String out_time = AppGlobal.RestaurentProductList.get(i).out_time;
                        try {
                            Date mToday = new Date();
                            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
                            String curTime = sdf.format(mToday);
                            Date start = sdf.parse(in_time);
                            Date end = sdf.parse(out_time);
                            Date userDate = sdf.parse(curTime);
                            if (end.before(start)) {
                                Calendar mCal = Calendar.getInstance();
                                mCal.setTime(end);
                                mCal.add(Calendar.DAY_OF_YEAR, 1);
                                end.setTime(mCal.getTimeInMillis());
                            }
                            Log.d("curTime", userDate.toString());
                            Log.d("start", start.toString());
                            Log.d("end", end.toString());
                            if (userDate.after(start) && userDate.before(end)) {
                                Log.d("result", "falls between start and end , go to screen 1 ");
                                Log.d("Amar>>>", "Yes");
                                UtilityMethod.showAlertBox(mContext, "This product is available after " + out_time);
                            } else {
                                Log.d("Amar>>>", "No");
                                Log.d("result", "does not fall between start and end , go to screen 2 ");
                                generateItemReport(recommendedVh, pos);
                            }
                        } catch (ParseException e) {
                            generateItemReport(recommendedVh, pos);
                            // Invalid date was entered
                        }
                    } else {
                        generateItemReport(recommendedVh, pos);

                    }
                }
            }
            //generateItemReport(recommendedVh, pos);
        }
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }
}
