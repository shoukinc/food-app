package com.b2infosoft_jaipur.chakhle.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.activities.RestaurantListActivity;
import com.b2infosoft_jaipur.chakhle.fragments.CallFragment;
import com.b2infosoft_jaipur.chakhle.pojo.SavedAddressPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Microsoft on 5/11/2017.
 */
public class SavedAddressAdapter extends RecyclerView.Adapter<SavedAddressAdapter.SavedAddressHolder> {

    Context mContext;
    ArrayList<SavedAddressPojo> mList;
    SessionManager sessionManager;
    String ClassName = "";

    public SavedAddressAdapter(Context ctx, ArrayList<SavedAddressPojo> list,String className) {
        mContext = ctx;
        mList = list;
        ClassName = className;

    }

    @Override
    public SavedAddressHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_saved_address_row, parent, false);
        return new SavedAddressHolder(view);
    }

    @Override
    public void onBindViewHolder(final SavedAddressHolder holder, final int position) {

        holder.tv_address.setText(mList.get(position).flat_number + " , " + mList.get(position).landmark + " , " + mList.get(position).locality);
        holder.tv_type.setText(mList.get(position).tag_address);

        if (mList.get(position).tag_address.equals("Home")) {
            holder.img_Icon.setBackgroundResource(R.drawable.home_icon);
        } else if (mList.get(position).tag_address.equals("Office")) {
            holder.img_Icon.setBackgroundResource(R.drawable.office_icon);
        } else if (mList.get(position).tag_address.equals("Other")) {
            holder.img_Icon.setBackgroundResource(R.drawable.other_icon);
        }
        if (ClassName.equals("SearchRestaurent")){
            holder.iconEdit.setVisibility(View.GONE);
            holder.iconDelete.setVisibility(View.GONE);
        }
        else{
            holder.iconEdit.setVisibility(View.VISIBLE);
            holder.iconDelete.setVisibility(View.VISIBLE);
        }
        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, RestaurantListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("ClassName", "SearchRestaurentActivity");
                bundle.putString("locationAddress", holder.tv_address.getText().toString());
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mContext.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class SavedAddressHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RelativeLayout parent;
        ImageView img_Icon, iconEdit, iconDelete;
        TextView tv_address, tv_type;
        View view;
        CallFragment callFragment;

        public SavedAddressHolder(View itemView) {
            super(itemView);
            sessionManager = new SessionManager(mContext);
            callFragment = (CallFragment) mContext;
            parent = (RelativeLayout) itemView.findViewById(R.id.parent);
            img_Icon = (ImageView) itemView.findViewById(R.id.img_Icon);
            iconDelete = (ImageView) itemView.findViewById(R.id.iconDelete);
            iconEdit = (ImageView) itemView.findViewById(R.id.iconEdit);
            tv_address = (TextView) itemView.findViewById(R.id.tv_address);
            tv_type = (TextView) itemView.findViewById(R.id.tv_type);
            //view = (View) itemView.findViewById(R.id.view);
           // tv_address.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tv_type.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_medium.ttf"));
            itemView.setOnClickListener(this);
            iconEdit.setOnClickListener(this);
            iconDelete.setOnClickListener(this);


        }


        @Override
        public void onClick(View v) {
            final int pos = getLayoutPosition();
            //String Id = mList.get(pos).NewsId;
            /*Log.d("Id",Id);
            AppGlobal.NewsID = Id;
            mContext.startActivity(new Intent(mContext, NewsDetailsActivity.class));*/

            switch (v.getId()) {
                case R.id.iconEdit:
                    String AddressId = mList.get(pos).AddressId;
                    callFragment.setFragment(AddressId);
                    break;

                case R.id.iconDelete:
                    WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, AppGlobal.ApiCallWaitMsg, true) {
                        @Override
                        public void handleResponse(String response) {
                            Log.d("response", response);
                            try {
                                JSONObject mainObj = new JSONObject(response);
                                //{"data":[],"message":"Register successfull.","success":true,"status":200}
                                if (mainObj.getString("success").equals("true")) {
                                    mList.remove(pos);
                                    notifyDataSetChanged();
                                    UtilityMethod.showAlertBox(mContext, "Deleted successfull");
                                    // UtilityMethod.showAlertBox(mContext, "Password Updated successfull");
                                } else {
                                    UtilityMethod.showAlertBox(mContext, "Deleting Failed");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    serviceCaller.addNameValuePair("address_id", mList.get(pos).AddressId);
                    serviceCaller.addNameValuePair("user_id", sessionManager.getValuesSession(SessionManager.Key_UserID));
                    String url = AppGlobal.DeleteAddress;
                    Log.d("url>>", url);
                    serviceCaller.execute(url);
                    break;
            }

        }
    }
}
