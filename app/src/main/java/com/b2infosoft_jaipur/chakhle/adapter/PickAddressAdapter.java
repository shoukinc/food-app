package com.b2infosoft_jaipur.chakhle.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.activities.ManageAddressActivity;
import com.b2infosoft_jaipur.chakhle.fragments.SendData;
import com.b2infosoft_jaipur.chakhle.pojo.SavedAddressPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.util.ArrayList;

/**
 * Created by Microsoft on 6/3/2017.
 */

public class PickAddressAdapter extends RecyclerView.Adapter<PickAddressAdapter.PickAddressHolder> {

    Context mContext;
    ArrayList<SavedAddressPojo> mList;
    SessionManager sessionManager;
    private int rbPosoition = 0;
    private RadioButton rbChecked = null;
    public int mSelectedItem = -1;

    public PickAddressAdapter(Context ctx, ArrayList<SavedAddressPojo> list) {
        mContext = ctx;
        mList = list;

    }

    @Override
    public PickAddressHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_pick_address_row, parent, false);
        return new PickAddressHolder(view);
    }

    @Override
    public void onBindViewHolder(final PickAddressHolder holder, final int position) {
        holder.tv_address.setText(mList.get(position).flat_number + " , " + mList.get(position).landmark + " , " + mList.get(position).locality);
        holder.tv_type.setText(mList.get(position).tag_address);
        if (mList.get(position).tag_address.equals("Home")) {
            holder.img_Icon.setBackgroundResource(R.drawable.home_icon);
        } else if (mList.get(position).tag_address.equals("Office")) {
            holder.img_Icon.setBackgroundResource(R.drawable.office_icon);
        } else if (mList.get(position).tag_address.equals("Other")) {
            holder.img_Icon.setBackgroundResource(R.drawable.other_icon);
        }
        holder.radioButton.setChecked(position == mSelectedItem);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class PickAddressHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView img_Icon, iconEdit;
        TextView tv_address, tv_type;
        View view;
        SendData sendData;
        RadioButton radioButton;

        public PickAddressHolder(View itemView) {
            super(itemView);
            sessionManager = new SessionManager(mContext);
            sendData = (SendData) mContext;
            img_Icon =  itemView.findViewById(R.id.img_Icon);
            radioButton =   itemView.findViewById(R.id.radioButton);
            iconEdit =   itemView.findViewById(R.id.iconEdit);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_type =  itemView.findViewById(R.id.tv_type);
            //view = (View) itemView.findViewById(R.id.view);
            // tv_address.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tv_type.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_medium.ttf"));
           /* itemView.setOnClickListener(this);*/
            iconEdit.setOnClickListener(this);


            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    AppGlobal.OrderLocatioID = mList.get(mSelectedItem).AddressId;
                    AppGlobal.OrderMobileNumber = mList.get(mSelectedItem).mobile_no;
                    AppGlobal.OrderDeliverAddress = mList.get(mSelectedItem).flat_number + " , " + mList.get(mSelectedItem).landmark + " , " + mList.get(mSelectedItem).locality;
                    Log.d("Address>>", mList.get(mSelectedItem).flat_number + " , " + mList.get(mSelectedItem).landmark + " , " + mList.get(mSelectedItem).locality);
                    sendData.sendDataToActivity(mList.get(mSelectedItem).name, mList.get(mSelectedItem).mobile_no, "");
                    //Toast.makeText(mContext, "" + mList.get(mSelectedItem).flat_number, Toast.LENGTH_SHORT).show();
                    notifyDataSetChanged();
                    // notifyItemRangeChanged(0, mList.size());
                }
            };
            itemView.setOnClickListener(clickListener);
            radioButton.setOnClickListener(clickListener);

        }


        @Override
        public void onClick(View v) {
            final int pos = getLayoutPosition();
            //String Id = mList.get(pos).NewsId;

            switch (v.getId()) {
                case R.id.iconEdit:
                    String AddressId = mList.get(pos).AddressId;
                    Intent intent = new Intent(mContext, ManageAddressActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("FromWhere", "PickAddressActivity");
                    bundle.putString("AddressID", AddressId);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                    break;


            }

        }
    }
}
