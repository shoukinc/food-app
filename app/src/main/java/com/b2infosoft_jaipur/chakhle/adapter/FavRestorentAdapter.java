package com.b2infosoft_jaipur.chakhle.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.activities.RestaurentDetailsActivity;
import com.b2infosoft_jaipur.chakhle.fragments.CallFragment;
import com.b2infosoft_jaipur.chakhle.pojo.ResturentListPojo;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by Microsoft on 5/29/2017.
 */

public class FavRestorentAdapter extends RecyclerView.Adapter<FavRestorentAdapter.FavRestorentHolder> {

    Context mContext;
    ArrayList<ResturentListPojo> mList = new ArrayList<>();
    ArrayList<ResturentListPojo> DiscountList = new ArrayList<>();

    public FavRestorentAdapter(Context ctx, ArrayList<ResturentListPojo> resturentListPojos, ArrayList<ResturentListPojo> discountList) {
        mContext = ctx;
        mList = resturentListPojos;
        DiscountList = discountList;

    }


    @Override
    public FavRestorentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_restaurent_list_row, parent, false);
        return new FavRestorentHolder(view);
    }

    @Override
    public void onBindViewHolder(FavRestorentHolder holder, final int position) {

        holder.tvRestaurentName.setText(mList.get(position).name);
        holder.tvRestaurentType.setText(mList.get(position).address);
        Glide.with(mContext).load(mList.get(position).logo_image)
                .into(holder.img_restaurent);
        holder.tvTimes.setText(mList.get(position).delivery_time);

        holder.restaurentRs.setText("Minimum Purchase: "+mList.get(position).minimum_purchase + " Rs.");
        holder.tvOpen_Close.setText("Open Today");

        for (int i = 0; i < DiscountList.size(); i++) {

            if (mList.get(position).id.equals(DiscountList.get(i).DiscountOffer_restaurant_id)) {
                if (!DiscountList.get(i).DiscountOffer_type.equals("") && !DiscountList.get(i).DiscountOffer_value.equals("")) {
                   /* if (DiscountList.get(i).DiscountOffer_type.equals("price")) {
                        holder.tvDiscount.setVisibility(View.VISIBLE);
                        holder.tvDiscount.setText(DiscountList.get(position).DiscountOffer_value + " Rs.");
                    } else */
                    if (DiscountList.get(i).DiscountOffer_type.equals("percentage")) {
                        holder.imgDiscount.setVisibility(View.VISIBLE);
                        holder.tvDiscount.setVisibility(View.VISIBLE);
                        holder.tvDiscount.setText(DiscountList.get(i).DiscountOffer_value + "%" + " OFF");
                    }
                }
            }
        }


        /*if (mList.get(position).chakhle_assured.equals("1")) {
            holder.tv_address.setVisibility(View.VISIBLE);

        } else {
            holder.tv_address.setVisibility(View.GONE);
        }*/
        Log.d("rating>>>", "" + mList.get(position).TotalRating);
        if (!mList.get(position).TotalRating.equals("0")) {
            Log.d("rating>>>", "if condition");
            holder.tvRating.setText(mList.get(position).TotalReview);
            // holder.tvRating.setText(ReviewList.get(position).TotalRating.substring(0, 3));

            try {
                float ratingBar = Float.parseFloat(mList.get(position).TotalRating);
                if (ratingBar < 2) {
                    holder.rat1.setImageResource(R.drawable.fill_star);

                } else if (ratingBar < 3) {
                    holder.rat1.setImageResource(R.drawable.fill_star);
                    holder.rat2.setImageResource(R.drawable.fill_star);

                } else if (ratingBar < 4) {
                    holder.rat1.setImageResource(R.drawable.fill_star);
                    holder.rat2.setImageResource(R.drawable.fill_star);
                    holder.rat3.setImageResource(R.drawable.fill_star);

                } else if (ratingBar < 5) {
                    holder.rat1.setImageResource(R.drawable.fill_star);
                    holder.rat2.setImageResource(R.drawable.fill_star);
                    holder.rat3.setImageResource(R.drawable.fill_star);
                    holder.rat4.setImageResource(R.drawable.fill_star);

                } else if (ratingBar < 6) {
                    holder.rat1.setImageResource(R.drawable.fill_star);
                    holder.rat2.setImageResource(R.drawable.fill_star);
                    holder.rat3.setImageResource(R.drawable.fill_star);
                    holder.rat4.setImageResource(R.drawable.fill_star);
                    holder.rat5.setImageResource(R.drawable.fill_star);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class FavRestorentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView img_restaurent, rat1, rat2, rat3, rat4, rat5, imgDiscount;
        TextView tvRestaurentName, restaurentRs, tvRestaurentType, tvRating, tvTimes, tv_address, tvOutlet, tvMins, tvDiscount, tvOpen_Close;
        CallFragment callFragment;

        public FavRestorentHolder(View itemView) {
            super(itemView);
//            callFragment = (CallFragment) mContext;
            img_restaurent = (ImageView) itemView.findViewById(R.id.img_restaurent);
            rat1 = (ImageView) itemView.findViewById(R.id.rat1);
            rat2 = (ImageView) itemView.findViewById(R.id.rat2);
            rat3 = (ImageView) itemView.findViewById(R.id.rat3);
            rat4 = (ImageView) itemView.findViewById(R.id.rat4);
            rat5 = (ImageView) itemView.findViewById(R.id.rat5);
            imgDiscount = (ImageView) itemView.findViewById(R.id.imgDiscount);
            tvOutlet = (TextView) itemView.findViewById(R.id.tvOutlet);
            restaurentRs = (TextView) itemView.findViewById(R.id.restaurentRs);
            tvRestaurentName = (TextView) itemView.findViewById(R.id.tvRestaurentName);
            tvRestaurentType = (TextView) itemView.findViewById(R.id.tvRestaurentType);
            tvRating = (TextView) itemView.findViewById(R.id.tvRating);
            tvTimes = (TextView) itemView.findViewById(R.id.tvTimes);
            tv_address = (TextView) itemView.findViewById(R.id.tv_address);
            tvMins = (TextView) itemView.findViewById(R.id.tvMins);
            tvDiscount = (TextView) itemView.findViewById(R.id.tvDiscount);
            tvOpen_Close = (TextView) itemView.findViewById(R.id.tvOpen_Close);
           // tvDiscount.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
            tvOpen_Close.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
           // tvRestaurentType.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
           // tvRating.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_light.ttf"));
            //tvMins.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_light.ttf"));
            tvOutlet.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
          //  tv_address.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            //tvTimes.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_light.ttf"));
            tvRestaurentName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_medium.ttf"));
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            AppGlobal.RestDiscount = "";
            AppGlobal.RestaurentCategoryID = "";
            AppGlobal.RestaurentName = "";
            AppGlobal.RestarentImage = "";
            AppGlobal.RestarentAddress = "";
            AppGlobal.RestRating = "";
            AppGlobal.RestBanner = "";
            AppGlobal.RestaurentCategoryID = mList.get(pos).id;
            AppGlobal.RestaurentName = mList.get(pos).name;
            AppGlobal.RestarentImage = mList.get(pos).logo_image;
            AppGlobal.RestarentAddress = mList.get(pos).address;
            AppGlobal.RestRating = mList.get(pos).TotalRating;
            AppGlobal.RestBanner = mList.get(pos).banner_image;

            for (int i = 0; i < DiscountList.size(); i++) {

                if (mList.get(pos).id.equals(DiscountList.get(i).DiscountOffer_restaurant_id)) {
                    if (!DiscountList.get(i).DiscountOffer_type.equals("") && !DiscountList.get(i).DiscountOffer_value.equals("")) {
                   /* if (DiscountList.get(i).DiscountOffer_type.equals("price")) {
                        holder.tvDiscount.setVisibility(View.VISIBLE);
                        holder.tvDiscount.setText(DiscountList.get(position).DiscountOffer_value + " Rs.");
                    } else */
                        if (DiscountList.get(i).DiscountOffer_type.equals("percentage")) {
                            AppGlobal.RestDiscount = DiscountList.get(i).DiscountOffer_value;
                        }
                    }
                }
            }
            Log.d("RestID>>>", AppGlobal.RestaurentCategoryID);
          //  callFragment.setFragment("RestaurentDetailsFragment");
            AppGlobal.IamFrom = "FavRestoActivity";

            mContext.startActivity(new Intent(mContext, RestaurentDetailsActivity.class));
        }
    }
}
