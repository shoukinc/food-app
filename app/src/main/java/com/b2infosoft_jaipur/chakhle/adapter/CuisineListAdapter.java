package com.b2infosoft_jaipur.chakhle.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;

import java.util.ArrayList;

/**
 * Created by Microsoft on 6/19/2017.
 */

public class CuisineListAdapter extends RecyclerView.Adapter<CuisineListAdapter.CuisineListHolder> {

    Context mContext;
    ArrayList<String> mList;

    public CuisineListAdapter(Context ctx, ArrayList<String> list) {

        mContext = ctx;
        mList = list;
    }

    @Override
    public CuisineListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_cuisines_list_row, parent, false);
        return new CuisineListHolder(view);
    }

    @Override
    public void onBindViewHolder(CuisineListHolder holder, final int position) {
        //holder.img_Icon.setImageResource(mList.get(position).foodImages);
        holder.tvCuisinesName.setText(mList.get(position));
        if (mList.size() - 1 == position) {
            holder.div.setVisibility(View.GONE);
         //   holder.tvBlank.setVisibility(View.VISIBLE);
        } else {
            holder.div.setVisibility(View.VISIBLE);
           // holder.tvBlank.setVisibility(View.GONE);

        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class CuisineListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CheckBox checkboxCuisine;
        TextView tvCuisinesName,tvBlank;
        View div;

        public CuisineListHolder(View itemView) {
            super(itemView);
            checkboxCuisine = (CheckBox) itemView.findViewById(R.id.checkboxCuisine);
            tvCuisinesName = (TextView) itemView.findViewById(R.id.tvCuisinesName);
           // tvBlank = (TextView) itemView.findViewById(R.id.tvBlank);
            div = (View) itemView.findViewById(R.id.div);
            tvCuisinesName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
           /* int pos = getLayoutPosition();
            String Id = mList.get(pos).NewsId;*/
            /*Log.d("Id",Id);
            AppGlobal.NewsID = Id;
            mContext.startActivity(new Intent(mContext, NewsDetailsActivity.class));*/
        }
    }
}
