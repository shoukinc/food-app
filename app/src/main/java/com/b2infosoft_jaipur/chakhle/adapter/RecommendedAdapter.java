package com.b2infosoft_jaipur.chakhle.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.activities.RestaurentDetailsActivity;
import com.b2infosoft_jaipur.chakhle.activities.ReviewOrderActivity;
import com.b2infosoft_jaipur.chakhle.fragments.CartValueInterface;
import com.b2infosoft_jaipur.chakhle.fragments.NewCartDataFill;
import com.b2infosoft_jaipur.chakhle.fragments.NotifyAdapter;
import com.b2infosoft_jaipur.chakhle.fragments.ProductListFragment;
import com.b2infosoft_jaipur.chakhle.pojo.RestaurentDetailsPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Microsoft on 5/20/2017.
 */

public class RecommendedAdapter extends RecyclerView.Adapter<RecommendedAdapter.RecommendedVh> {


    private ArrayList<RestaurentDetailsPojo> restaurentProductList = new ArrayList<>();
/*
    private ArrayList<ProductListingDialogAdapterRowPojo> productListingDialogAdapterRowPojos = new ArrayList<>();
*/

    CartValueInterface callFragment;
    int cartValue = 0;
    private Context mContext;
    AppCompatActivity appCompatActivity;
    int count = 0;
    int quantity = 0;
    SessionManager sessionManager;
    RestaurentDetailsPojo restaurentDetailsPojo = null;
    ProductListFragment productListFragment;
    NotifyAdapter notifyAdapter;
    Dialog dialog;
    int totQty = 0;
    int Quantity = 0;
    int TotalVariantPrice = 0;
    int VariantPrice = 0;
    String Variant = "";
    RecommendedVh recommendedVh;

    public RecommendedAdapter(Context context, ArrayList<RestaurentDetailsPojo> restaurentProductList) {
        this.mContext = context;
        appCompatActivity = (AppCompatActivity) mContext;
        this.restaurentProductList = restaurentProductList;
        AppGlobal.MainProductList = restaurentProductList;
        sessionManager = new SessionManager(mContext);
        callFragment = (CartValueInterface) appCompatActivity;

        for (int i = 0; i < restaurentProductList.size(); i++) {
            count = count + restaurentProductList.get(i).count;
        }
        Log.d("amar>>", "" + count);
        if (count != 0) {
            callFragment.setCartData(count, "visibleCart", AppGlobal.RestaurentCategoryID);
        }
    }

    @Override
    public RecommendedVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.layout_restasrent_details_firstfrg, parent, false);
        return new RecommendedVh(view);
    }

    @Override
    public void onBindViewHolder(final RecommendedVh holder, final int position) {

      /*  holder.LayoutPlusDummy.setVisibility(View.VISIBLE);*/
        recommendedVh = holder;
        holder.setIsRecyclable(false);
        totQty = AppGlobal.RestaurentProductList.get(position).halfQty + AppGlobal.RestaurentProductList.get(position).fullQty + AppGlobal.RestaurentProductList.get(position).mediumQty + AppGlobal.RestaurentProductList.get(position).largeQty;
        holder.tvQuantity.setText("" + totQty);
        if (!restaurentProductList.get(position).in_time.equals("") && !restaurentProductList.get(position).out_time.equals("")) {
            String in_time = restaurentProductList.get(position).in_time;
            String out_time = restaurentProductList.get(position).out_time;
            try {
                Date mToday = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
                String curTime = sdf.format(mToday);
                Date start = sdf.parse(in_time);
                Date end = sdf.parse(out_time);
                Date userDate = sdf.parse(curTime);

                if (end.before(start)) {
                    Calendar mCal = Calendar.getInstance();
                    mCal.setTime(end);
                    mCal.add(Calendar.DAY_OF_YEAR, 1);
                    end.setTime(mCal.getTimeInMillis());
                }
                Log.d("curTime", userDate.toString());
                Log.d("start", start.toString());
                Log.d("end", end.toString());
                if (userDate.after(start) && userDate.before(end)) {
                    Log.d("result", "falls between start and end , go to screen 1 ");
                    Log.d("Amar>>>", "Yes");
                    holder.LayoutPlusDummy.setVisibility(View.GONE);
                    holder.tvQuantity.setVisibility(View.GONE);


                } else {
                    Log.d("Amar>>>", "No");
                    Log.d("result", "does not fall between start and end , go to screen 2 ");
                    holder.LayoutPlusDummy.setVisibility(View.VISIBLE);
                    holder.tvQuantity.setVisibility(View.GONE);
                }
            } catch (ParseException e) {
                holder.LayoutPlusDummy.setVisibility(View.VISIBLE);
                holder.tvQuantity.setVisibility(View.GONE);
                // Invalid date was entered
            }
        } else {
            holder.LayoutPlusDummy.setVisibility(View.VISIBLE);
            holder.tvQuantity.setVisibility(View.GONE);
        }
        restaurentDetailsPojo = AppGlobal.RestaurentProductList.get(position);
        if (!restaurentProductList.get(position).Product_image1.equals("No Image")) {
            holder.imgFood.setVisibility(View.VISIBLE);
        } else {
            holder.imgFood.setVisibility(View.GONE);
        }

        Glide.with(mContext).load(restaurentProductList.get(position).Product_image1).into(holder.imgFood);
        holder.tvFoodName.setText(restaurentProductList.get(position).Product_name);
        holder.tvItemRs.setText("Rs. " + restaurentProductList.get(position).half);
        holder.tvItemCount.setText("" + restaurentProductList.get(position).count);


        Log.d("des",restaurentProductList.get(position).Product_name+"  "+restaurentProductList.get(position).Product_description+" rfgfgf ");

        if (!restaurentProductList.get(position).Product_description.equals("")) {
            holder.tvFoodDesc.setVisibility(View.VISIBLE);
            holder.div2.setVisibility(View.VISIBLE);
            holder.tvFoodDesc.setText(restaurentProductList.get(position).Product_description);
        }
        if (restaurentProductList.get(position).Product_vegproduct.equals("veg")) {
            holder.tvFoodType.setBackgroundResource(R.drawable.veg_icon);
        } else {
            holder.tvFoodType.setBackgroundResource(R.drawable.non_veg_icon);
        }
        if (!restaurentProductList.get(position).discount.equals("")) {
            // holder.tvItemRs.setPaintFlags(holder.tvItemRs.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            int discount = Integer.parseInt(restaurentProductList.get(position).discount);
            int actualPrice = Integer.parseInt(restaurentDetailsPojo.Product_price);
            int discountRs = discount * actualPrice;
            int totalDiscount = discountRs / 100;
            int initialPrice = actualPrice - totalDiscount;
            Log.d("afterdiscount>>>", "" + initialPrice);
            AppGlobal.RestaurentProductList.get(position).setAfterDiscountValue(initialPrice);
            holder.tvDiscountRs.setVisibility(View.GONE);
            holder.tvDiscountRs.setText("Rs. " + initialPrice);
        } else {
            AppGlobal.RestaurentProductList.get(position).setAfterDiscountValue(Integer.parseInt(AppGlobal.RestaurentProductList.get(position).Product_price));
        }
        count = AppGlobal.RestaurentProductList.get(position).getCount();
        Log.d("count>>>", "" + AppGlobal.RestaurentProductList.get(position).getCount());
        holder.tvItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).getCount());


        /********************Simple Quentity***********************/
        holder.imgMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count = AppGlobal.RestaurentProductList.get(position).getCount();
                if (count == 0) {
                    count = 0;
                    cartValue = 0;
                    holder.tvItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).getCount());
                    if (count == 0) {
                        //AppGlobal.MainProductList.clear();
                        holder.imgMines.setImageResource(R.drawable.disable_mines);
                        AppGlobal.RestaurentProductList.get(position).setCount(count);
                    }
                } else {
                    //  AppGlobal.MainProductList.remove(position);
                    AppGlobal.ProductIDList.remove(0);
                    callFragment.setCartData(cartValue, "-", AppGlobal.RestaurentCategoryID);
                    count = count - 1;
                    cartValue = cartValue - 1;
                    AppGlobal.CartValue = AppGlobal.CartValue - 1;
                    AppGlobal.RestaurentProductList.get(position).setCount(count);
                    holder.tvItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).getCount());
                    if (count == 0) {
                        holder.imgMines.setImageResource(R.drawable.disable_mines);
                    } else {
                        holder.imgMines.setImageResource(R.drawable.enable_mines);
                    }
                }
                if (count == 0) {
//                    AppGlobal.ProductIDList.remove(position);
                    callFragment.setCartData(cartValue, "check", AppGlobal.RestaurentCategoryID);
                }
                notifyAdapter = (RestaurentDetailsActivity) mContext;
                notifyAdapter.notifyData(AppGlobal.cateID);
            }
        });
        holder.imgPlus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {

                //  if (AppGlobal.ProductIDList.isEmpty()) {
                if (AppGlobal.ProductIDList.isEmpty()) {
                    count = AppGlobal.RestaurentProductList.get(position).getCount();
                    count = count + 1;
                    cartValue = cartValue + 1;
                    AppGlobal.CartValue = 1 + AppGlobal.CartValue;
                    AppGlobal.RestaurentProductList.get(position).setCount(count);
                    holder.tvItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).getCount());
                    if (count == 0) {
                        holder.imgMines.setImageResource(R.drawable.disable_mines);
                    } else {
                        holder.imgMines.setImageResource(R.drawable.enable_mines);
                    }
                    count = 0;
                    Log.d("CartValue>>>", "" + cartValue);
                    callFragment.setCartData(cartValue, "+", AppGlobal.RestaurentCategoryID);
                    notifyAdapter = (RestaurentDetailsActivity) mContext;
                    notifyAdapter.notifyData(AppGlobal.cateID);

                } else {
                    if (AppGlobal.ProductIDList.get(0).equals(AppGlobal.RestaurentCategoryID)) {
                        AppGlobal.ProductIDList.add(AppGlobal.RestaurentCategoryID);
                        //if (AppGlobal.mainCartList.get(position).Product_id.equals(restaurentDetailsPojo.Product_id))
                        //Toast.makeText(mContext, "Same", Toast.LENGTH_SHORT).show();
                        count = AppGlobal.RestaurentProductList.get(position).getCount();
                        count = count + 1;
                        cartValue = cartValue + 1;
                        AppGlobal.CartValue = 1 + AppGlobal.CartValue;
                        AppGlobal.RestaurentProductList.get(position).setCount(count);
                        holder.tvItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).getCount());
                        if (count == 0) {
                            holder.imgMines.setImageResource(R.drawable.disable_mines);
                        } else {
                            holder.imgMines.setImageResource(R.drawable.enable_mines);
                        }
                        count = 0;
                        Log.d("CartValue>>>", "" + cartValue);
                        // AppGlobal.ProductIDList.add(AppGlobal.RestaurentCategoryID);
                        callFragment.setCartData(cartValue, "+", AppGlobal.RestaurentCategoryID);
                        notifyAdapter = (RestaurentDetailsActivity) mContext;
                        notifyAdapter.notifyData(AppGlobal.cateID);
                    } else {
                        // Toast.makeText(mContext, "Diff", Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                        alertDialogBuilder.setMessage("Your cart contains already dishes, Are You want to clear them and add another dishes.");
                        alertDialogBuilder.setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        // Toast.makeText(mContext, "You clicked yes  button", Toast.LENGTH_LONG).show();
                                        AppGlobal.ProductIDList.clear();
                                        count = AppGlobal.RestaurentProductList.get(position).getCount();
                                        count = count + 1;
                                        cartValue = cartValue + 1;
                                        AppGlobal.CartValue = 1 + AppGlobal.CartValue;
                                        AppGlobal.RestaurentProductList.get(position).setCount(count);
                                        if (count == 0) {
                                            holder.imgMines.setImageResource(R.drawable.disable_mines);
                                        } else {
                                            holder.imgMines.setImageResource(R.drawable.enable_mines);
                                        }
                                        count = 0;
                                        Log.d("CartValue>>>", "" + cartValue);
                                        AppGlobal.ProductIDList.add(AppGlobal.RestaurentCategoryID);
                                        holder.tvItemCount.setText("" + cartValue);
                                        callFragment.setCartData(cartValue, "+", AppGlobal.RestaurentCategoryID);
                                        notifyAdapter = (RestaurentDetailsActivity) mContext;
                                        notifyAdapter.notifyData(AppGlobal.cateID);
                                    }
                                });

                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mContext.startActivity(new Intent(mContext, ReviewOrderActivity.class));
                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                }

            }
        });

        /******************************************************/


        holder.imgPlusDummy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // ArrayList<ProductListingDialogAdapterRowPojo> productListingDialogAdapterRowPojos1 = new ArrayList<>();
                // productListingDialogAdapterRowPojos1.add(new ProductListingDialogAdapterRowPojo(restaurentProductList.get(position).half, restaurentProductList.get(position).half_label, "0", "0"));
                // productListingDialogAdapterRowPojos1.add(new ProductListingDialogAdapterRowPojo(restaurentProductList.get(position).medium, restaurentProductList.get(position).medium_label, "0", "0"));
                // productListingDialogAdapterRowPojos1.add(new ProductListingDialogAdapterRowPojo(restaurentProductList.get(position).full, restaurentProductList.get(position).full_label, "0", "0"));
                // productListingDialogAdapterRowPojos1.add(new ProductListingDialogAdapterRowPojo(restaurentProductList.get(position).large, restaurentProductList.get(position).large_label, "0", "0"));
                generateItemReport(holder, position);
            }
        });
        if (restaurentProductList.size() - 1 == position) {
            holder.blankSpace.setVisibility(View.VISIBLE);
        } else {
            holder.blankSpace.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return restaurentProductList.size();
    }

    public class RecommendedVh extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imgFood, imgMines, imgPlus, tvFoodType, imgPlusDummy;
        TextView tvFoodName, tvFoodDesc;
        TextView tvItemRs, blankSpace;
        TextView tvItemCount, tvDiscountRs;
        TextView tvQuantity;
        View div2;
        LinearLayout LayoutPlusDummy, layoutPlusMinus;

        public RecommendedVh(View itemView) {
            super(itemView);
            tvFoodName = (TextView) itemView.findViewById(R.id.tvFoodName);
            tvFoodType = (ImageView) itemView.findViewById(R.id.tvFoodType);
            tvItemRs = (TextView) itemView.findViewById(R.id.tvActualRs);
            blankSpace = (TextView) itemView.findViewById(R.id.blankSpace);
            tvFoodDesc = (TextView) itemView.findViewById(R.id.tvFoodDesc);
            div2 = (View) itemView.findViewById(R.id.div2);
            tvItemCount = (TextView) itemView.findViewById(R.id.tvItemCount);
            layoutPlusMinus = (LinearLayout) itemView.findViewById(R.id.layoutPlusMinus);
            imgMines = (ImageView) itemView.findViewById(R.id.imgMines);
            imgPlus = (ImageView) itemView.findViewById(R.id.imgPlus2);
            imgFood = (ImageView) itemView.findViewById(R.id.imgFood);
            tvQuantity = (TextView) itemView.findViewById(R.id.tvHalfQty);
            LayoutPlusDummy = (LinearLayout) itemView.findViewById(R.id.LayoutPlusDummy);

            imgPlusDummy = (ImageView) itemView.findViewById(R.id.imgPlusDummy);
            tvFoodName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
            //tvItemRs.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
            tvDiscountRs = (TextView) itemView.findViewById(R.id.tvDiscountRs);
            //tvDiscountRs.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
            //tvItemCount.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tvFoodDesc.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tvQuantity.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {

            int pos = getAdapterPosition();
            Log.d("pos>>>", "" + pos);
            if (!restaurentProductList.get(pos).in_time.equals("") && !restaurentProductList.get(pos).out_time.equals("")) {
                String in_time = restaurentProductList.get(pos).in_time;
                String out_time = restaurentProductList.get(pos).out_time;
                try {
                    Date mToday = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
                    String curTime = sdf.format(mToday);
                    Date start = sdf.parse(in_time);
                    Date end = sdf.parse(out_time);
                    Date userDate = sdf.parse(curTime);

                    if (end.before(start)) {
                        Calendar mCal = Calendar.getInstance();
                        mCal.setTime(end);
                        mCal.add(Calendar.DAY_OF_YEAR, 1);
                        end.setTime(mCal.getTimeInMillis());
                    }
                    Log.d("curTime", userDate.toString());
                    Log.d("start", start.toString());
                    Log.d("end", end.toString());
                    if (userDate.after(start) && userDate.before(end)) {
                        Log.d("result", "falls between start and end , go to screen 1 ");
                        Log.d("Amar>>>", "Yes");
                        //  generateItemReport(recommendedVh, pos);
                        UtilityMethod.showAlertBox(mContext, "This product is available after " + out_time);

                    } else {
                        generateItemReport(recommendedVh, pos);
                    }
                } catch (ParseException e) {
                    generateItemReport(recommendedVh, pos);
                    // Invalid date was entered
                }
            } else {
                generateItemReport(recommendedVh, pos);

            }
        }
    }

    private void generateItemReport(final RecommendedVh recommendedVh, final int position) {
        totQty = 0;
        Button btnConfirm,btnCancel;
        RecyclerView recycler_productList_Dialog;
        LinearLayout layoutHalf, layoutMedium, layoutFull, layoutLarge;
        View div3, div4, div2, div1;
        final TextView tvHalfPrice, tvHalfLabel, tvHalfItemCount, tvMediumPrice, tvMediumLabel, tvMediumItemCount, tvFullPrice, tvFullLabel, tvFullItemCount, tvLargePrice, tvLargeLabel, tvLargeItemCount;
        ImageView imgHalfMines, imgHalfPlus, imgMediumMines, imgMediumPlus, imgFullMines, imgFullPlus, imgLargeMines, imgLargePlus;

        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_orders);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        recycler_productList_Dialog = (RecyclerView) dialog.findViewById(R.id.recycler_productList_Dialog);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recycler_productList_Dialog.setLayoutManager(linearLayoutManager);
        recycler_productList_Dialog.setHasFixedSize(true);
        dialog.show();

        final int halfQty_old=AppGlobal.RestaurentProductList.get(position).halfQty;
        final int mediumQty_old=AppGlobal.RestaurentProductList.get(position).mediumQty;
        final int fullQty_old=AppGlobal.RestaurentProductList.get(position).fullQty;
        final int largeQty_old=AppGlobal.RestaurentProductList.get(position).largeQty;

        Log.d("halfQty_old",""+halfQty_old);
        Log.d("mediumQty_old",""+mediumQty_old);
        Log.d("fullQty_old",""+fullQty_old);
        Log.d("largeQty_old",""+largeQty_old);


        layoutHalf = (LinearLayout) dialog.findViewById(R.id.layoutHalf);
        layoutMedium = (LinearLayout) dialog.findViewById(R.id.layoutMedium);
        layoutFull = (LinearLayout) dialog.findViewById(R.id.layoutFull);
        layoutLarge = (LinearLayout) dialog.findViewById(R.id.layoutLarge);
        div1 = (View) dialog.findViewById(R.id.div1);
        div2 = (View) dialog.findViewById(R.id.div2);
        div3 = (View) dialog.findViewById(R.id.div3);
        div4 = (View) dialog.findViewById(R.id.div4);

        tvHalfPrice = (TextView) dialog.findViewById(R.id.tvHalfPrice);
        tvHalfLabel = (TextView) dialog.findViewById(R.id.tvHalfLabel);
        tvHalfItemCount = (TextView) dialog.findViewById(R.id.tvHalfItemCount);
        tvMediumPrice = (TextView) dialog.findViewById(R.id.tvMediumPrice);
        tvMediumLabel = (TextView) dialog.findViewById(R.id.tvMediumLabel);
        tvMediumItemCount = (TextView) dialog.findViewById(R.id.tvMediumItemCount);
        tvFullItemCount = (TextView) dialog.findViewById(R.id.tvFullItemCount);
        tvLargeLabel = (TextView) dialog.findViewById(R.id.tvLargeLabel);
        tvLargeItemCount = (TextView) dialog.findViewById(R.id.tvLargeItemCount);
        tvLargePrice = (TextView) dialog.findViewById(R.id.tvLargePrice);
        tvFullPrice = (TextView) dialog.findViewById(R.id.tvFullPrice);
        tvFullLabel = (TextView) dialog.findViewById(R.id.tvFullLabel);

        if (!AppGlobal.RestaurentProductList.get(position).half.equals("0") || !AppGlobal.RestaurentProductList.get(position).half_label.equals("")) {
            layoutHalf.setVisibility(View.VISIBLE);
            div1.setVisibility(View.VISIBLE);
        }
        if (!AppGlobal.RestaurentProductList.get(position).medium.equals("0") || !AppGlobal.RestaurentProductList.get(position).medium_label.equals("")) {
            layoutMedium.setVisibility(View.VISIBLE);
            div2.setVisibility(View.VISIBLE);
        }
        if (!AppGlobal.RestaurentProductList.get(position).full.equals("0") || !AppGlobal.RestaurentProductList.get(position).full_label.equals("")) {
            layoutFull.setVisibility(View.VISIBLE);
            div3.setVisibility(View.VISIBLE);
        }
        if (!AppGlobal.RestaurentProductList.get(position).large.equals("0") || !AppGlobal.RestaurentProductList.get(position).large_label.equals("")) {
            layoutLarge.setVisibility(View.VISIBLE);
            div4.setVisibility(View.VISIBLE);
        }

        tvHalfPrice.setText("Rs. " + AppGlobal.RestaurentProductList.get(position).half);
        tvHalfLabel.setText(AppGlobal.RestaurentProductList.get(position).half_label);
        tvHalfItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).halfQty);
        tvMediumPrice.setText("Rs. " + AppGlobal.RestaurentProductList.get(position).medium);
        tvMediumLabel.setText("" + AppGlobal.RestaurentProductList.get(position).medium_label);
        tvMediumItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).mediumQty);
        tvLargeLabel.setText("" + AppGlobal.RestaurentProductList.get(position).large_label);
        tvLargeItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).largeQty);
        tvLargePrice.setText("Rs. " + AppGlobal.RestaurentProductList.get(position).large);
        tvFullPrice.setText("Rs. " + AppGlobal.RestaurentProductList.get(position).full);
        tvFullLabel.setText("" + AppGlobal.RestaurentProductList.get(position).full_label);
        tvFullItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).fullQty);

        imgHalfMines = (ImageView) dialog.findViewById(R.id.imgHalfMines);
        imgHalfPlus = (ImageView) dialog.findViewById(R.id.imgHalfPlus);
        imgMediumMines = (ImageView) dialog.findViewById(R.id.imgMediumMines);
        imgMediumPlus = (ImageView) dialog.findViewById(R.id.imgMediumPlus);
        imgFullMines = (ImageView) dialog.findViewById(R.id.imgFullMines);
        imgFullPlus = (ImageView) dialog.findViewById(R.id.imgFullPlus);
        imgLargeMines = (ImageView) dialog.findViewById(R.id.imgLargeMines);
        imgLargePlus = (ImageView) dialog.findViewById(R.id.imgLargePlus);

        imgHalfMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int halfQty = AppGlobal.RestaurentProductList.get(position).halfQty;
                Log.d("halfQty>>>", "" + halfQty);
                if (halfQty == 0) {
                    AppGlobal.RestaurentProductList.get(position).halfQty = 0;
                } else {
                    halfQty = halfQty - 1;
                    AppGlobal.RestaurentProductList.get(position).halfQty = halfQty;
                }
                //        totQty = totQty + halfQty;
                tvHalfItemCount.setText("" + halfQty);
                // notifyAdapter = (RestaurentDetailsActivity) mContext;
                //  notifyAdapter.notifyData(AppGlobal.cateID);
            }
        });
        imgHalfPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int halfQty = AppGlobal.RestaurentProductList.get(position).halfQty;
                Log.d("halfQty>>>", "" + halfQty);
                halfQty = halfQty + 1;
                //   totQty = totQty + halfQty;
                AppGlobal.RestaurentProductList.get(position).halfQty = halfQty;
                tvHalfItemCount.setText("" + halfQty);
                // notifyAdapter = (RestaurentDetailsActivity) mContext;
                // notifyAdapter.notifyData(AppGlobal.cateID);
            }
        });
        imgMediumMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int mediumQty = AppGlobal.RestaurentProductList.get(position).mediumQty;
                Log.d("mediumQty>>>", "" + mediumQty);
                if (mediumQty == 0) {
                    AppGlobal.RestaurentProductList.get(position).mediumQty = 0;
                } else {
                    mediumQty = mediumQty - 1;
                    AppGlobal.RestaurentProductList.get(position).mediumQty = mediumQty;
                }
                //   totQty = totQty + mediumQty;
                tvMediumItemCount.setText("" + mediumQty);
                //  notifyAdapter = (RestaurentDetailsActivity) mContext;
                // notifyAdapter.notifyData(AppGlobal.cateID);
            }
        });
        imgMediumPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int mediumQty = AppGlobal.RestaurentProductList.get(position).mediumQty;
                mediumQty = mediumQty + 1;
                AppGlobal.RestaurentProductList.get(position).mediumQty = mediumQty;
                //  totQty = totQty + mediumQty;
                tvMediumItemCount.setText("" + mediumQty);
                //   notifyAdapter = (RestaurentDetailsActivity) mContext;
                //   notifyAdapter.notifyData(AppGlobal.cateID);

            }
        });
        imgFullMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int fullQty = AppGlobal.RestaurentProductList.get(position).fullQty;
                Log.d("fullQty>>>", "" + fullQty);
                if (fullQty == 0) {
                    AppGlobal.RestaurentProductList.get(position).fullQty = 0;
                } else {
                    fullQty = fullQty - 1;
                    AppGlobal.RestaurentProductList.get(position).fullQty = fullQty;
                }
                // totQty = totQty + fullQty;
                tvFullItemCount.setText("" + fullQty);
                // notifyAdapter = (RestaurentDetailsActivity) mContext;
                // notifyAdapter.notifyData(AppGlobal.cateID);

            }
        });
        imgFullPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int fullQty = AppGlobal.RestaurentProductList.get(position).fullQty;
                fullQty = fullQty + 1;
                AppGlobal.RestaurentProductList.get(position).fullQty = fullQty;
                tvFullItemCount.setText("" + fullQty);
                //  notifyAdapter = (RestaurentDetailsActivity) mContext;
                //  notifyAdapter.notifyData(AppGlobal.cateID);
                //   totQty = totQty + fullQty;
            }
        });
        imgLargeMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int largeQty = AppGlobal.RestaurentProductList.get(position).largeQty;
                Log.d("largeQty>>>", "" + largeQty);
                if (largeQty == 0) {
                    AppGlobal.RestaurentProductList.get(position).largeQty = 0;
                } else {
                    largeQty = largeQty - 1;
                    AppGlobal.RestaurentProductList.get(position).largeQty = largeQty;
                }
                //   totQty = totQty + largeQty;
                tvLargeItemCount.setText("" + largeQty);
                //  notifyAdapter = (RestaurentDetailsActivity) mContext;
                //  notifyAdapter.notifyData(AppGlobal.cateID);

            }
        });
        imgLargePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int largeQty = AppGlobal.RestaurentProductList.get(position).largeQty;
                largeQty = largeQty + 1;
                AppGlobal.RestaurentProductList.get(position).largeQty = largeQty;
                tvLargeItemCount.setText("" + largeQty);
                //  notifyAdapter = (RestaurentDetailsActivity) mContext;
                // notifyAdapter.notifyData(AppGlobal.cateID);
                //   totQty = totQty + largeQty;
            }
        });


        //   ProductListingDialogAdapter productListingDialogAdapter = new ProductListingDialogAdapter(mContext, AppGlobal.RestaurentProductList, productListingDialogAdapterRowPojos1);
        //  recycler_productList_Dialog.setAdapter(productListingDialogAdapter);

        btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppGlobal.ProductIDList.isEmpty()) {
                    NewCartDataFill newCartDataFill = (NewCartDataFill) mContext;
                    newCartDataFill.newCartFillData(AppGlobal.RestaurentProductList);
                    dialog.dismiss();
                    Log.d("Confirm>>", "Confirm");
                    totQty = 0;
                    totQty = AppGlobal.RestaurentProductList.get(position).halfQty + AppGlobal.RestaurentProductList.get(position).fullQty + AppGlobal.RestaurentProductList.get(position).mediumQty + AppGlobal.RestaurentProductList.get(position).largeQty;
                    recommendedVh.tvQuantity.setText("" + totQty);
                } else if (AppGlobal.ProductIDList.get(0).equals(AppGlobal.RestaurentCategoryID)) {
                    AppGlobal.ProductIDList.add(AppGlobal.RestaurentCategoryID);
                    {
                        NewCartDataFill newCartDataFill = (NewCartDataFill) mContext;
                        newCartDataFill.newCartFillData(AppGlobal.RestaurentProductList);
                        dialog.dismiss();
                        Log.d("Confirm>>", "Confirm");
                        totQty = 0;
                        totQty = AppGlobal.RestaurentProductList.get(position).halfQty + AppGlobal.RestaurentProductList.get(position).fullQty + AppGlobal.RestaurentProductList.get(position).mediumQty + AppGlobal.RestaurentProductList.get(position).largeQty;
                        recommendedVh.tvQuantity.setText("" + totQty);
                    }

                } else {
                    {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                        alertDialogBuilder.setMessage("Your cart contains already dishes, Are You want to clear them and add another dishes.");
                        alertDialogBuilder.setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        // Toast.makeText(mContext, "You clicked yes  button", Toast.LENGTH_LONG).show();
                                        AppGlobal.ProductIDList.clear();

                                        NewCartDataFill newCartDataFill = (NewCartDataFill) mContext;
                                        newCartDataFill.newCartFillData(AppGlobal.RestaurentProductList);
                                        dialog.dismiss();
                                        Log.d("Confirm>>", "Confirm");
                                        totQty = 0;
                                        totQty = AppGlobal.RestaurentProductList.get(position).halfQty + AppGlobal.RestaurentProductList.get(position).fullQty + AppGlobal.RestaurentProductList.get(position).mediumQty + AppGlobal.RestaurentProductList.get(position).largeQty;
                                        recommendedVh.tvQuantity.setText("" + totQty);
                                        notifyAdapter = (RestaurentDetailsActivity) mContext;
                                        notifyAdapter.notifyData(AppGlobal.cateID);
                                    }
                                });

                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mContext.startActivity(new Intent(mContext, ReviewOrderActivity.class));
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                }
            }
        });

        btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppGlobal.RestaurentProductList.get(position).halfQty = halfQty_old;
                AppGlobal.RestaurentProductList.get(position).mediumQty = mediumQty_old;
                AppGlobal.RestaurentProductList.get(position).fullQty = fullQty_old;
                AppGlobal.RestaurentProductList.get(position).largeQty = largeQty_old;

                if (AppGlobal.ProductIDList.isEmpty()) {
                    NewCartDataFill newCartDataFill = (NewCartDataFill) mContext;
                    newCartDataFill.newCartFillData(AppGlobal.RestaurentProductList);
                    dialog.dismiss();
                    Log.d("Confirm>>", "Confirm");
                    totQty = 0;
                    totQty = AppGlobal.RestaurentProductList.get(position).halfQty + AppGlobal.RestaurentProductList.get(position).fullQty + AppGlobal.RestaurentProductList.get(position).mediumQty + AppGlobal.RestaurentProductList.get(position).largeQty;
                    recommendedVh.tvQuantity.setText("" + totQty);
                } else if (AppGlobal.ProductIDList.get(0).equals(AppGlobal.RestaurentCategoryID)) {
                    AppGlobal.ProductIDList.add(AppGlobal.RestaurentCategoryID);
                    {
                        NewCartDataFill newCartDataFill = (NewCartDataFill) mContext;
                        newCartDataFill.newCartFillData(AppGlobal.RestaurentProductList);
                        dialog.dismiss();
                        Log.d("Confirm>>", "Confirm");
                        totQty = 0;
                        totQty = AppGlobal.RestaurentProductList.get(position).halfQty + AppGlobal.RestaurentProductList.get(position).fullQty + AppGlobal.RestaurentProductList.get(position).mediumQty + AppGlobal.RestaurentProductList.get(position).largeQty;
                        recommendedVh.tvQuantity.setText("" + totQty);
                    }

                } else {
                    {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                        alertDialogBuilder.setMessage("Your cart contains already dishes, Are You want to clear them and add another dishes.");
                        alertDialogBuilder.setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        // Toast.makeText(mContext, "You clicked yes  button", Toast.LENGTH_LONG).show();
                                        AppGlobal.ProductIDList.clear();

                                        NewCartDataFill newCartDataFill = (NewCartDataFill) mContext;
                                        newCartDataFill.newCartFillData(AppGlobal.RestaurentProductList);
                                        dialog.dismiss();
                                        Log.d("Confirm>>", "Confirm");
                                        totQty = 0;
                                        totQty = AppGlobal.RestaurentProductList.get(position).halfQty + AppGlobal.RestaurentProductList.get(position).fullQty + AppGlobal.RestaurentProductList.get(position).mediumQty + AppGlobal.RestaurentProductList.get(position).largeQty;
                                        recommendedVh.tvQuantity.setText("" + totQty);
                                        notifyAdapter = (RestaurentDetailsActivity) mContext;
                                        notifyAdapter.notifyData(AppGlobal.cateID);
                                    }
                                });

                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mContext.startActivity(new Intent(mContext, ReviewOrderActivity.class));
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                }
            }
        });


    }
}
