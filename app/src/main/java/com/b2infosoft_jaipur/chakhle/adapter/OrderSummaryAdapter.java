package com.b2infosoft_jaipur.chakhle.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.pojo.MyOrderPojo;

import java.util.ArrayList;

/**
 * Created by Microsoft on 6/9/2017.
 */

public class OrderSummaryAdapter extends RecyclerView.Adapter<OrderSummaryAdapter.OrderSummaryHolder> {

    Context mContext;
    ArrayList<MyOrderPojo> mList = new ArrayList<>();

    public OrderSummaryAdapter(Context ctx, ArrayList<MyOrderPojo> resturentListPojos) {

        mContext = ctx;
        mList = resturentListPojos;
    }


    @Override
    public OrderSummaryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_order_summary_row, parent, false);
        return new OrderSummaryHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderSummaryHolder holder, final int position) {
//tvMealName, tvQuantity, tvCost;


        Log.d("MealName>>>", mList.get(position).name);

        if (!mList.get(position).half_qty.equals("0")) {
            holder.layoutHalf.setVisibility(View.VISIBLE);
            holder.tvMealName.setText(mList.get(position).name);
            holder.tvHalfQty.setText("\t\t\t" + mList.get(position).half_qty);
            holder.tvHalfPrice.setText(mList.get(position).half_price + " /-");
            holder.tvHalfLabel.setText(mList.get(position).half_label);
            holder.tvContainerCharge.setVisibility(View.GONE);
            holder.tvContainerCharge.setText("Container Charge  " + mList.get(position).container_charge + " /-");
        }
        if (!mList.get(position).medium_qty.equals("0")) {
            holder.layoutMedium.setVisibility(View.VISIBLE);
            holder.tvMediumMealName.setText(mList.get(position).name);
            holder.tvMediumQty.setText("\t\t\t" + mList.get(position).medium_qty);
            holder.tvMediumPrice.setText(mList.get(position).medium_price + " /-");
            holder.tvMediumLabel.setText(mList.get(position).medium_label);
            holder.tvContainerCharge.setVisibility(View.GONE);
            holder.tvContainerCharge.setText("Container Charge  " + mList.get(position).container_charge + " /-");

        }
        if (!mList.get(position).full_qty.equals("0")) {
            holder.layoutFull.setVisibility(View.VISIBLE);
            holder.tvFullMealName.setText(mList.get(position).name);
            holder.tvFullQty.setText("\t\t\t" + mList.get(position).full_qty);
            holder.tvFullPrice.setText(mList.get(position).full_price + " /-");
            holder.tvFullLabel.setText(mList.get(position).full_label);
            holder.tvContainerCharge.setVisibility(View.GONE);
            holder.tvContainerCharge.setText("Container Charge  " + mList.get(position).container_charge + " /-");

        }
        if (!mList.get(position).large_qty.equals("0")) {
            holder.layoutLarge.setVisibility(View.VISIBLE);
            holder.tvLargeMealName.setText(mList.get(position).name);
            holder.tvLargeQty.setText("\t\t\t" + mList.get(position).large_qty);
            holder.tvLargePrice.setText(mList.get(position).large_price + " /-");
            holder.tvLargeLabel.setText(mList.get(position).large_label);
            holder.tvContainerCharge.setVisibility(View.GONE);
            holder.tvContainerCharge.setText("Container Charge  " + mList.get(position).container_charge + " /-");
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class OrderSummaryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvMealName, tvHalfQty, tvHalfPrice, tvHalfLabel, tvMediumLabel, tvMediumQty, tvMediumPrice, tvFullLabel, tvFullQty, tvFullPrice,
                tvLargeLabel, tvLargeQty, tvLargePrice, tvContainerCharge, tvContainerChargeTxt;
        TextView tvMediumMealName, tvFullMealName, tvLargeMealName;
        LinearLayout layoutHalf, layoutMedium, layoutFull, layoutLarge;


        public OrderSummaryHolder(View itemView) {
            super(itemView);
            layoutHalf = (LinearLayout) itemView.findViewById(R.id.layoutHalf);
            tvHalfQty = (TextView) itemView.findViewById(R.id.tvHalfQty);
            tvContainerChargeTxt = (TextView) itemView.findViewById(R.id.tvContainerChargeTxt);
            tvMealName = (TextView) itemView.findViewById(R.id.tvHalfMealName);
            tvHalfPrice = (TextView) itemView.findViewById(R.id.tvHalfPrice);
            tvHalfLabel = (TextView) itemView.findViewById(R.id.tvHalfLabel);
            tvContainerCharge = (TextView) itemView.findViewById(R.id.tvContainerCharge);
            // tvCost.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tvMediumMealName = (TextView) itemView.findViewById(R.id.tvMediumMealName);
            tvFullMealName = (TextView) itemView.findViewById(R.id.tvFullMealName);
            tvLargeMealName = (TextView) itemView.findViewById(R.id.tvLargeMealName);

            tvLargeMealName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tvFullMealName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tvMediumMealName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tvHalfLabel.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tvContainerChargeTxt.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            layoutMedium = (LinearLayout) itemView.findViewById(R.id.layoutMedium);
            tvMediumQty = (TextView) itemView.findViewById(R.id.tvMediumQty);
            tvMealName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));

            tvMediumPrice = (TextView) itemView.findViewById(R.id.tvMediumPrice);
            tvMediumLabel = (TextView) itemView.findViewById(R.id.tvMediumLabel);
            tvMediumLabel.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));

            layoutFull = (LinearLayout) itemView.findViewById(R.id.layoutFull);
            tvFullQty = (TextView) itemView.findViewById(R.id.tvFullQty);
            tvFullPrice = (TextView) itemView.findViewById(R.id.tvFullPrice);
            tvFullLabel = (TextView) itemView.findViewById(R.id.tvFullLabel);
            tvFullLabel.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));

            layoutLarge = (LinearLayout) itemView.findViewById(R.id.layoutLarge);
            tvLargeQty = (TextView) itemView.findViewById(R.id.tvLargeQty);
            tvLargePrice = (TextView) itemView.findViewById(R.id.tvLargePrice);
            tvLargeLabel = (TextView) itemView.findViewById(R.id.tvLargeLabel);
            tvLargeLabel.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();

            switch (view.getId()) {
               /* case R.id.iconArrow:
                    AppGlobal.OrderID = mList.get(pos).OrderID;
                    mContext.startActivity(new Intent(mContext, OrderStatusActivity.class));
                    break;*/
            }
        }
    }
}
