package com.b2infosoft_jaipur.chakhle.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.fragments.CartFillInterface;
import com.b2infosoft_jaipur.chakhle.fragments.CartValueInterface;
import com.b2infosoft_jaipur.chakhle.pojo.CartFillPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.util.ArrayList;

/**
 * Created by Microsoft on 6/3/2017.
 */

public class ReviewOrderAdapter extends RecyclerView.Adapter<ReviewOrderAdapter.ReviewOrderHolder> {

    ArrayList<CartFillPojo> mList;
    SessionManager sessionManager;
    CartValueInterface callFragment;
    int cartValue = 0;
    private Context mContext;
    AppCompatActivity appCompatActivity;
    int count = 0;
    CartFillInterface cartFillInterface;

    public ReviewOrderAdapter(Context ctx, ArrayList<CartFillPojo> list) {
        mContext = ctx;
        mList = list;
        appCompatActivity = (AppCompatActivity) mContext;
    }

    @Override
    public ReviewOrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_review_order_row, parent, false);
        return new ReviewOrderHolder(view);
    }

    @Override
    public void onBindViewHolder(final ReviewOrderHolder holder, final int position) {
        final CartFillPojo cartFillPojo = mList.get(position);
        callFragment = (CartValueInterface) appCompatActivity;
        count = mList.get(position).quantity;
        if (!cartFillPojo.Discount.equals("")) {
            holder.tvActualRsTxt.setVisibility(View.GONE);
            holder.tvActualRs.setVisibility(View.GONE);
            holder.tvActualRs.setText("" + mList.get(position).totalProductPrice);
            holder.tvActualRsTxt.setPaintFlags(holder.tvActualRsTxt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tvActualRs.setPaintFlags(holder.tvActualRs.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.tvActualRsTxt.setVisibility(View.GONE);
            holder.tvActualRs.setVisibility(View.GONE);
        }
        holder.tv_itemName.setText("" + mList.get(position).Product_name);
        holder.tv_afterDiscountPrice.setText("" + mList.get(position).ProductTotalPrice);
        holder.tvItemCount.setText("" + mList.get(position).quantity);
        if (mList.get(position).product_slug.equals("veg")) {
            holder.img_Icon.setImageResource(R.drawable.veg_icon);
        } else {
            holder.img_Icon.setImageResource(R.drawable.non_veg_icon);
        }
        if (mList.size() - 1 == position) {
            holder.view.setVisibility(View.GONE);
        }
        count = cartFillPojo.getQuantity();
        Log.d("count>>>", "" + cartFillPojo.getQuantity());
        holder.tvItemCount.setText("" + cartFillPojo.getQuantity());

        holder.iconMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int remainingValue = 0;
                count = cartFillPojo.getQuantity();
                if (count == 0) {
                    //AppGlobal.MainProductList.clear();
                    holder.iconMines.setImageResource(R.drawable.disable_mines);
                    cartFillPojo.TotalContainerCharge = "0";
                    //restaurentProductList.get(position).setCount(count);
                }
                if (count == 1) {
                    cartValue = 0;
                    if (count == 1) {
                        count = 0;
                        holder.tv_afterDiscountPrice.setText("" + 0);
                        holder.tvActualRs.setText("" + 0);
                        holder.iconMines.setImageResource(R.drawable.disable_mines);
                        cartFillPojo.setQuantity(count);
                        holder.tvItemCount.setText("" + cartFillPojo.getQuantity());

                        if (!cartFillPojo.Discount.equals("")) {
                            callFragment.setCartData(mList.get(position).afterDiscountValue, "-", mList.get(position).Product_price);
                        } else {
                            callFragment.setCartData(Integer.parseInt(mList.get(position).Product_price), "-", mList.get(position).Product_price);
                        }
                    }
                } else if (count != 0) {
                    int amount = Integer.parseInt(holder.tv_afterDiscountPrice.getText().toString());
                    int totalProductPrice = Integer.parseInt(mList.get(position).Product_price);
                    count = count - 1;
                    cartValue = cartValue - 1;
                    AppGlobal.CartValue = AppGlobal.CartValue - 1;
                    cartFillPojo.setQuantity(count);
                    holder.tvItemCount.setText("" + cartFillPojo.getQuantity());
                    remainingValue = amount - mList.get(position).afterDiscountValue;
                    if (!cartFillPojo.Discount.equals("")) {
                        int totalAmount = Integer.parseInt(holder.tvActualRs.getText().toString());
                        int remainingActualValue = totalAmount - totalProductPrice;
                        callFragment.setCartData(mList.get(position).afterDiscountValue, "-", mList.get(position).Product_price);
                        holder.tvActualRs.setText("" + remainingActualValue);
                    } else {
                        callFragment.setCartData(Integer.parseInt(mList.get(position).Product_price), "-", mList.get(position).Product_price);
                    }
                    holder.tv_afterDiscountPrice.setText("" + remainingValue);
                    if (count == 0) {
                        holder.tvItemCount.setText("" + cartFillPojo.getQuantity());
                        holder.iconMines.setImageResource(R.drawable.disable_mines);
                    } else {
                        holder.iconMines.setImageResource(R.drawable.enable_mines);
                    }
                }
                count = cartFillPojo.getQuantity();
                double containerCharge = Double.parseDouble(cartFillPojo.PerProductContainerCharge);
                containerCharge = containerCharge * count;
                mList.get(position).TotalContainerCharge = String.valueOf(containerCharge);
                Log.d("containerCharge>>>", "" + containerCharge);
            }
        });
        holder.iconPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int totalAmount = 0;
                int remainingValue = 0;
                count = cartFillPojo.getQuantity();
                count = count + 1;
                cartValue = cartValue + 1;
                AppGlobal.CartValue = 1 + AppGlobal.CartValue;
                cartFillPojo.setQuantity(count);
                holder.tvItemCount.setText("" + cartFillPojo.getQuantity());
                /*int remainingValue = mList.get(position).ProductTotalPrice + mList.get(position).afterDiscountValue;
                holder.tv_itemRs.setText("" + remainingValue);*/
                if (count == 1) {
                    remainingValue = mList.get(position).afterDiscountValue;
                    int totalProductPrice = Integer.parseInt(mList.get(position).Product_price);
                    holder.tv_afterDiscountPrice.setText("" + remainingValue);
                    holder.tvActualRs.setText("" + totalProductPrice);
                    holder.iconMines.setImageResource(R.drawable.disable_mines);
                    callFragment.setCartData(mList.get(position).afterDiscountValue, "+", mList.get(position).Product_price);

                } else {
                    int amount = Integer.parseInt(holder.tv_afterDiscountPrice.getText().toString());
                    remainingValue = amount + mList.get(position).afterDiscountValue;
                    int totalProductPrice = Integer.parseInt(mList.get(position).Product_price);
                    holder.tv_afterDiscountPrice.setText("" + remainingValue);
                    holder.iconMines.setImageResource(R.drawable.enable_mines);
                    if (!cartFillPojo.Discount.equals("")) {
                        totalAmount = Integer.parseInt(holder.tvActualRs.getText().toString());
                        totalAmount = totalAmount + totalProductPrice;
                        holder.tvActualRs.setText("" + totalAmount);
                        Log.d("CartValue>>>", "" + cartValue);
                        callFragment.setCartData(mList.get(position).afterDiscountValue, "+", mList.get(position).Product_price);
                    } else {
                        Log.d("CartValue>>>", "" + cartValue);
                        callFragment.setCartData(Integer.parseInt(mList.get(position).Product_price), "+", mList.get(position).Product_price);
                    }


                }
                count = 0;
                count = cartFillPojo.getQuantity();
                double containerCharge = Double.parseDouble(cartFillPojo.PerProductContainerCharge);
                containerCharge = containerCharge * count;
                mList.get(position).TotalContainerCharge = String.valueOf(containerCharge);
                Log.d("containerCharge>>>", "" + containerCharge);
            }
        });


        /*************************************************************/
        cartFillInterface = (CartFillInterface) mContext;
        if (mList.get(position).HalfQty != 0) {
            holder.layoutHalf.setVisibility(View.VISIBLE);
            holder.div1.setVisibility(View.VISIBLE);
            holder.tvHalfLabel.setText(mList.get(position).HalfLabel);
            holder.tvHalfItemCount.setText("" + mList.get(position).HalfQty);
            double price = mList.get(position).HalfPrice * mList.get(position).HalfQty;
            holder.tvHalfPrice.setText("" + price);
        }
        if (mList.get(position).MediumQty != 0) {
            holder.layoutMedium.setVisibility(View.VISIBLE);
            holder.div2.setVisibility(View.VISIBLE);
            holder.tvMediumLabel.setText(mList.get(position).MediumLabel);
            holder.tvMediumItemCount.setText("" + mList.get(position).MediumQty);
            double price = mList.get(position).MediumPrice * mList.get(position).MediumQty;
            holder.tvMediumPrice.setText("" + price);
        }
        if (mList.get(position).FullQty != 0) {
            holder.layoutFull.setVisibility(View.VISIBLE);
            holder.div3.setVisibility(View.VISIBLE);
            holder.tvFullLabel.setText(mList.get(position).FullLabel);
            holder.tvFullItemCount.setText("" + mList.get(position).FullQty);
            double price = mList.get(position).FullPrice * mList.get(position).FullQty;
            holder.tvFullPrice.setText("" + price);
        }
        if (mList.get(position).LargeQty != 0) {
            holder.layoutLarge.setVisibility(View.VISIBLE);
            holder.div4.setVisibility(View.VISIBLE);
            holder.tvLargeLabel.setText(mList.get(position).LargeLabel);
            holder.tvLargeItemCount.setText("" + mList.get(position).LargeQty);
            double price = mList.get(position).LargePrice * mList.get(position).LargeQty;
            holder.tvLargePrice.setText("" + price);
        }


        holder.imgHalfMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int halfQty = mList.get(position).HalfQty;
                Log.d("halfQty>>>", "" + halfQty);
                if (halfQty == 0) {
                    mList.get(position).HalfQty = 0;
                } else {
                    halfQty = halfQty - 1;
                    mList.get(position).HalfQty = halfQty;
                }
                holder.tvHalfItemCount.setText("" + halfQty);
                double price = mList.get(position).HalfPrice * mList.get(position).HalfQty;
                holder.tvHalfPrice.setText("" + price);
                cartFillInterface.newCartFillData(mList);




            }
        });
        holder.imgHalfPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int halfQty = mList.get(position).HalfQty;
                Log.d("halfQty>>>", "" + halfQty);
                halfQty = halfQty + 1;
                mList.get(position).HalfQty = halfQty;
                holder.tvHalfItemCount.setText("" + halfQty);
                double price = mList.get(position).HalfPrice * mList.get(position).HalfQty;
                holder.tvHalfPrice.setText("" + price);
                cartFillInterface.newCartFillData(mList);

            }
        });
        holder.imgMediumMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int mediumQty = mList.get(position).MediumQty;
                Log.d("mediumQty>>>", "" + mediumQty);
                if (mediumQty == 0) {
                    mList.get(position).MediumQty = 0;
                } else {
                    mediumQty = mediumQty - 1;
                    mList.get(position).MediumQty = mediumQty;
                }
                holder.tvMediumItemCount.setText("" + mediumQty);
                double price = mList.get(position).MediumPrice * mList.get(position).MediumQty;
                holder.tvMediumPrice.setText("" + price);
                cartFillInterface.newCartFillData(mList);

            }
        });
        holder.imgMediumPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int mediumQty = mList.get(position).MediumQty;
                mediumQty = mediumQty + 1;
                mList.get(position).MediumQty = mediumQty;
                holder.tvMediumItemCount.setText("" + mediumQty);
                double price = mList.get(position).MediumPrice * mList.get(position).MediumQty;
                holder.tvMediumPrice.setText("" + price);
                cartFillInterface.newCartFillData(mList);


            }
        });
        holder.imgFullMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int fullQty = mList.get(position).FullQty;
                Log.d("fullQty>>>", "" + fullQty);
                if (fullQty == 0) {
                    mList.get(position).FullQty = 0;
                } else {
                    fullQty = fullQty - 1;
                    mList.get(position).FullQty = fullQty;
                }
                holder.tvFullItemCount.setText("" + fullQty);
                double price = mList.get(position).FullPrice * mList.get(position).FullQty;
                holder.tvFullPrice.setText("" + price);
                cartFillInterface.newCartFillData(mList);

            }
        });
        holder.imgFullPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int fullQty = mList.get(position).FullQty;
                fullQty = fullQty + 1;
                mList.get(position).FullQty = fullQty;
                holder.tvFullItemCount.setText("" + fullQty);
                double price = mList.get(position).FullPrice * mList.get(position).FullQty;
                holder.tvFullPrice.setText("" + price);
                cartFillInterface.newCartFillData(mList);

            }
        });
        holder.imgLargeMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int largeQty = mList.get(position).LargeQty;
                Log.d("largeQty>>>", "" + largeQty);
                if (largeQty == 0) {
                    mList.get(position).LargeQty = 0;
                } else {
                    largeQty = largeQty - 1;
                    mList.get(position).LargeQty = largeQty;
                }
                holder.tvLargeItemCount.setText("" + largeQty);
                double price = mList.get(position).LargePrice * mList.get(position).LargeQty;
                holder.tvLargePrice.setText("" + price);
                cartFillInterface.newCartFillData(mList);

            }
        });
        holder.imgLargePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int largeQty = mList.get(position).LargeQty;
                largeQty = largeQty + 1;
                mList.get(position).LargeQty = largeQty;
                holder.tvLargeItemCount.setText("" + largeQty);
                double price = mList.get(position).LargePrice * mList.get(position).LargeQty;
                holder.tvLargePrice.setText("" + price);
                cartFillInterface.newCartFillData(mList);

            }
        });


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ReviewOrderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView img_Icon, iconPlus, iconMines;
        TextView tv_itemName, tv_afterDiscountPrice, tvItemCount, tvActualRs, tv_Rs, tvActualRsTxt;
        View view, div1, div2, div3, div4;
        TextView tvHalfPrice, tvHalfLabel, tvHalfItemCount, tvMediumPrice, tvMediumLabel, tvMediumItemCount, tvFullPrice, tvFullLabel, tvFullItemCount, tvLargePrice, tvLargeLabel, tvLargeItemCount;
        ImageView imgHalfMines, imgHalfPlus, imgMediumMines, imgMediumPlus, imgFullMines, imgFullPlus, imgLargeMines, imgLargePlus;
        LinearLayout layoutHalf, layoutMedium, layoutFull, layoutLarge;

        public ReviewOrderHolder(View itemView) {
            super(itemView);
            sessionManager = new SessionManager(mContext);
            // callFragment = new ManageAddressActivity();
            img_Icon = (ImageView) itemView.findViewById(R.id.img_Icon);
            iconPlus = (ImageView) itemView.findViewById(R.id.iconPlus);
            iconMines = (ImageView) itemView.findViewById(R.id.iconMines);
            tvActualRs = (TextView) itemView.findViewById(R.id.tvActualRs);
            tv_Rs = (TextView) itemView.findViewById(R.id.tv_Rs);
            tvActualRsTxt = (TextView) itemView.findViewById(R.id.tvActualRsTxt);
            tv_itemName = (TextView) itemView.findViewById(R.id.tv_itemName);
            tv_afterDiscountPrice = (TextView) itemView.findViewById(R.id.tv_afterDiscountPrice);
            view = (View) itemView.findViewById(R.id.dic);
            tv_itemName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tvActualRsTxt.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tv_Rs.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tvItemCount = (TextView) itemView.findViewById(R.id.tvItemCount);
            // tvItemCount.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));


            tvHalfPrice = (TextView) itemView.findViewById(R.id.tvHalfPrice);
            tvHalfLabel = (TextView) itemView.findViewById(R.id.tvHalfLabel);
            tvHalfItemCount = (TextView) itemView.findViewById(R.id.tvHalfItemCount);
            tvMediumPrice = (TextView) itemView.findViewById(R.id.tvMediumPrice);
            tvMediumLabel = (TextView) itemView.findViewById(R.id.tvMediumLabel);
            tvMediumItemCount = (TextView) itemView.findViewById(R.id.tvMediumItemCount);
            tvFullItemCount = (TextView) itemView.findViewById(R.id.tvFullItemCount);
            tvLargeLabel = (TextView) itemView.findViewById(R.id.tvLargeLabel);
            tvLargeItemCount = (TextView) itemView.findViewById(R.id.tvLargeItemCount);
            tvLargePrice = (TextView) itemView.findViewById(R.id.tvLargePrice);
            tvFullPrice = (TextView) itemView.findViewById(R.id.tvFullPrice);
            tvFullLabel = (TextView) itemView.findViewById(R.id.tvFullLabel);

            layoutHalf = (LinearLayout) itemView.findViewById(R.id.layoutHalf);
            layoutMedium = (LinearLayout) itemView.findViewById(R.id.layoutMedium);
            layoutFull = (LinearLayout) itemView.findViewById(R.id.layoutFull);
            layoutLarge = (LinearLayout) itemView.findViewById(R.id.layoutLarge);

            div1 = (View) itemView.findViewById(R.id.div1);
            div2 = (View) itemView.findViewById(R.id.div2);
            div3 = (View) itemView.findViewById(R.id.div3);
            div4 = (View) itemView.findViewById(R.id.div4);


           /* tvHalfPrice.setText("Rs. " + AppGlobal.RestaurentProductList.get(position).half);
            tvHalfLabel.setText(AppGlobal.RestaurentProductList.get(position).half_label);
            tvHalfItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).halfQty);
            tvMediumPrice.setText("Rs. " + AppGlobal.RestaurentProductList.get(position).medium);
            tvMediumLabel.setText("" + AppGlobal.RestaurentProductList.get(position).medium_label);
            tvMediumItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).mediumQty);
            tvLargeLabel.setText("" + AppGlobal.RestaurentProductList.get(position).large_label);
            tvLargeItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).largeQty);
            tvLargePrice.setText("Rs. " + AppGlobal.RestaurentProductList.get(position).large);
            tvFullPrice.setText("Rs. " + AppGlobal.RestaurentProductList.get(position).full);
            tvFullLabel.setText("" + AppGlobal.RestaurentProductList.get(position).full_label);
            tvFullItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).fullQty);*/

            imgHalfMines = (ImageView) itemView.findViewById(R.id.imgHalfMines);
            imgHalfPlus = (ImageView) itemView.findViewById(R.id.imgHalfPlus);
            imgMediumMines = (ImageView) itemView.findViewById(R.id.imgMediumMines);
            imgMediumPlus = (ImageView) itemView.findViewById(R.id.imgMediumPlus);
            imgFullMines = (ImageView) itemView.findViewById(R.id.imgFullMines);
            imgFullPlus = (ImageView) itemView.findViewById(R.id.imgFullPlus);
            imgLargeMines = (ImageView) itemView.findViewById(R.id.imgLargeMines);
            imgLargePlus = (ImageView) itemView.findViewById(R.id.imgLargePlus);


        }


        @Override
        public void onClick(View v) {
            final int pos = getLayoutPosition();
            //String Id = mList.get(pos).NewsId;
            /*Log.d("Id",Id);
            AppGlobal.NewsID = Id;
            mContext.startActivity(new Intent(mContext, NewsDetailsActivity.class));*/

            switch (v.getId()) {
                //  case R.id.iconEdit:
                /*    String AddressId = mList.get(pos).AddressId;
                    // callFragment.setFragment(AddressId);
                    break;*/


            }

        }
    }
}
