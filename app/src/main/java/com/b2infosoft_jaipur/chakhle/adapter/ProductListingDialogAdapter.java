package com.b2infosoft_jaipur.chakhle.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.fragments.CartValueInterface;
import com.b2infosoft_jaipur.chakhle.fragments.NotifyAdapter;
import com.b2infosoft_jaipur.chakhle.fragments.ProductListFragment;
import com.b2infosoft_jaipur.chakhle.pojo.ProductListingDialogAdapterRowPojo;
import com.b2infosoft_jaipur.chakhle.pojo.RestaurentDetailsPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.util.ArrayList;

/**
 * Created by Microsoft on 03-Oct-17.
 */

public class ProductListingDialogAdapter extends RecyclerView.Adapter<ProductListingDialogAdapter.ProductListing> {


    private ArrayList<RestaurentDetailsPojo> restaurentProductList = new ArrayList<>();
    public ArrayList<ProductListingDialogAdapterRowPojo> productListingDialogAdapterRowPojos = new ArrayList<>();
    CartValueInterface callFragment;
    int cartValue = 0;
    private Context mContext;
    AppCompatActivity appCompatActivity;
    int count = 0;
    int quantity = 0;
    int Quantity = 0;
    int TotalVariantPrice = 0;
    SessionManager sessionManager;
    RestaurentDetailsPojo restaurentDetailsPojo = null;
    ProductListFragment productListFragment;
    NotifyAdapter notifyAdapter;
    Dialog dialog;

    public ProductListingDialogAdapter(Context context, ArrayList<RestaurentDetailsPojo> restaurentProductList, ArrayList<ProductListingDialogAdapterRowPojo> productListingDialogAdapterRowPojos) {
        this.mContext = context;
        appCompatActivity = (AppCompatActivity) mContext;
        this.restaurentProductList = restaurentProductList;
        AppGlobal.MainProductList = restaurentProductList;
        this.productListingDialogAdapterRowPojos = productListingDialogAdapterRowPojos;
        sessionManager = new SessionManager(mContext);
        callFragment = (CartValueInterface) appCompatActivity;
        for (int i = 0; i < restaurentProductList.size(); i++) {
            count = count + restaurentProductList.get(i).count;
        }
        Log.d("amar>>", "" + count);
        if (count != 0) {
            callFragment.setCartData(count, "visibleCart", AppGlobal.RestaurentCategoryID);
        }
    }

    @Override
    public ProductListingDialogAdapter.ProductListing onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.layout_product_listing_dialog_adapter_row, parent, false);
        return new ProductListingDialogAdapter.ProductListing(view);
    }

    @Override
    public void onBindViewHolder(final ProductListingDialogAdapter.ProductListing holder, final int position) {

        holder.tvProductPrice.setText("Rs." + productListingDialogAdapterRowPojos.get(position).VariantPrice + "/-");
        holder.tvProductTitle.setText("" + productListingDialogAdapterRowPojos.get(position).Variant);
        holder.tvItemCount.setText("" + productListingDialogAdapterRowPojos.get(position).Quantity);
        holder.imgMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Quantity = Integer.parseInt(productListingDialogAdapterRowPojos.get(position).Quantity);
                TotalVariantPrice = Integer.parseInt(productListingDialogAdapterRowPojos.get(position).TotalVariantPrice);
                if (Quantity == 0) {
                    holder.tvItemCount.setText("" + 0);
                    holder.imgMines.setImageResource(R.drawable.disable_mines);
                } else {
                    Quantity--;
                    int totalVariantPrice = TotalVariantPrice - Integer.parseInt(productListingDialogAdapterRowPojos.get(position).VariantPrice);
                    productListingDialogAdapterRowPojos.get(position).TotalVariantPrice = String.valueOf(totalVariantPrice);
                    holder.tvItemCount.setText("" + Quantity);
                    holder.imgMines.setImageResource(R.drawable.enable_mines);
                    productListingDialogAdapterRowPojos.get(position).Quantity = String.valueOf(Quantity);
                    Log.d("TotalVariantPrice>>>", "" + productListingDialogAdapterRowPojos.get(position).Quantity + " " + productListingDialogAdapterRowPojos.get(position).TotalVariantPrice);
                }
            }
        });
        holder.imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Quantity = Integer.parseInt(productListingDialogAdapterRowPojos.get(position).Quantity);
                TotalVariantPrice = Integer.parseInt(productListingDialogAdapterRowPojos.get(position).TotalVariantPrice);

                Quantity++;
                if (holder.tvItemCount.getText().toString().equals("0")) {
                    holder.imgMines.setImageResource(R.drawable.disable_mines);
                } else {
                    holder.imgMines.setImageResource(R.drawable.enable_mines);
                }

                int totalVariantPrice = TotalVariantPrice + Integer.parseInt(productListingDialogAdapterRowPojos.get(position).VariantPrice);
                productListingDialogAdapterRowPojos.get(position).TotalVariantPrice = String.valueOf(totalVariantPrice);
                holder.tvItemCount.setText("" + Quantity);
                productListingDialogAdapterRowPojos.get(position).Quantity = String.valueOf(Quantity);
                Log.d("TotalVariantPrice>>>", "" + productListingDialogAdapterRowPojos.get(position).Quantity + " " + productListingDialogAdapterRowPojos.get(position).TotalVariantPrice);


            }
        });

    }


    @Override
    public int getItemCount() {
        return productListingDialogAdapterRowPojos.size();
    }

    public class ProductListing extends RecyclerView.ViewHolder {

        ImageView imgMines, imgPlus;
        TextView tvProductPrice, tvProductTitle, tvItemCount;

        public ProductListing(View itemView) {
            super(itemView);
            tvProductPrice = (TextView) itemView.findViewById(R.id.tvProductPrice);
            tvProductTitle = (TextView) itemView.findViewById(R.id.tvProductTitle);

            tvItemCount = (TextView) itemView.findViewById(R.id.tvItemCount);
            imgMines = (ImageView) itemView.findViewById(R.id.imgMines);
            imgPlus = (ImageView) itemView.findViewById(R.id.imgPlus);


           /* tvProductPrice.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));*/
            tvProductTitle.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));

        }

    }


}