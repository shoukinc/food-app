package com.b2infosoft_jaipur.chakhle.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;

/**
 * Created by Microsoft on 5/17/2017.
 */

public class MenuAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final String[] menu_text;
    private final Integer[] menu_icons;

    public MenuAdapter(Context context,
                       String[] web, Integer[] imageId) {
        super(context, R.layout.menu_row, web);
        this.context = context;
        this.menu_text = web;
        this.menu_icons = imageId;

    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View rowView = inflater.inflate(R.layout.menu_row, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        txtTitle.setText(menu_text[position]);
        imageView.setImageResource(menu_icons[position]);
        txtTitle.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/raleway_light.ttf"));
        return rowView;
    }
}