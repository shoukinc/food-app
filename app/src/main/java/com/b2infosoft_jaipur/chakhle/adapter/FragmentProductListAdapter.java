package com.b2infosoft_jaipur.chakhle.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.activities.RestaurentDetailsActivity;
import com.b2infosoft_jaipur.chakhle.activities.ReviewOrderActivity;
import com.b2infosoft_jaipur.chakhle.fragments.CartValueInterface;
import com.b2infosoft_jaipur.chakhle.fragments.NewCartDataFill;
import com.b2infosoft_jaipur.chakhle.fragments.NotifyAdapter;
import com.b2infosoft_jaipur.chakhle.pojo.RestaurentDetailsPojo;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Microsoft on 5/22/2017.
 */

public class FragmentProductListAdapter extends RecyclerView.Adapter<FragmentProductListAdapter.ViewHolder> {

    private ArrayList<RestaurentDetailsPojo> itemsData;
    Context mContext;
    int count = 0;
    CartValueInterface callFragment;
    int cartValue = 0;
    NotifyAdapter notifyAdapter;
    Dialog dialog;
    int totQty = 0;
    ViewHolder recommendedVh;

    public FragmentProductListAdapter(Context mContext, ArrayList<RestaurentDetailsPojo> itemsData) {
        this.itemsData = itemsData;
        this.mContext = mContext;
        AppGlobal.ItemDataList = itemsData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_product_list_row, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        recommendedVh = viewHolder;
        recommendedVh.setIsRecyclable(false);

        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                totQty = AppGlobal.RestaurentProductList.get(i).halfQty + AppGlobal.RestaurentProductList.get(i).fullQty + AppGlobal.RestaurentProductList.get(i).mediumQty + AppGlobal.RestaurentProductList.get(i).largeQty;
                viewHolder.tvQuantity.setText("" + totQty);
            }
        }

        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                if (!itemsData.get(position).in_time.equals("") && !itemsData.get(position).out_time.equals("")) {
                    String in_time = itemsData.get(position).in_time;
                    String out_time = itemsData.get(position).out_time;
                    try {
                        Date mToday = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
                        String curTime = sdf.format(mToday);
                        Date start = sdf.parse(in_time);
                        Date end = sdf.parse(out_time);
                        Date userDate = sdf.parse(curTime);

                        if (end.before(start)) {
                            Calendar mCal = Calendar.getInstance();
                            mCal.setTime(end);
                            mCal.add(Calendar.DAY_OF_YEAR, 1);
                            end.setTime(mCal.getTimeInMillis());
                        }
                        Log.d("curTime", userDate.toString());
                        Log.d("start", start.toString());
                        Log.d("end", end.toString());
                        if (userDate.after(start) && userDate.before(end)) {
                            Log.d("result", "falls between start and end , go to screen 1 ");
                            Log.d("Amar>>>", "Yes");
                            viewHolder.LayoutPlusDummy.setVisibility(View.GONE);
                            viewHolder.tvQuantity.setVisibility(View.GONE);
                        } else {
                            Log.d("Amar>>>", "No");
                            Log.d("result", "does not fall between start and end , go to screen 2 ");
                            /*viewHolder.layoutPlusMinus.setVisibility(View.GONE);*/
                            viewHolder.LayoutPlusDummy.setVisibility(View.VISIBLE);
                            viewHolder.tvQuantity.setVisibility(View.GONE);
                        }
                    } catch (ParseException e) {
                        /*viewHolder.layoutPlusMinus.setVisibility(View.GONE);*/
                        viewHolder.LayoutPlusDummy.setVisibility(View.VISIBLE);
                        viewHolder.tvQuantity.setVisibility(View.GONE);
                        // Invalid date was entered
                    }
                }
            } /*else {
          *//*  viewHolder.layoutPlusMinus.setVisibility(View.GONE);*//*
                viewHolder.LayoutPlusDummy.setVisibility(View.GONE);
                viewHolder.tvQuantity.setVisibility(View.GONE);
            }*/
        }

        final RestaurentDetailsPojo restaurentDetailsPojo = itemsData.get(position);
        //final RestaurentDetailsPojo mainPojo = itemsData.get(position);
        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                restaurentDetailsPojo.setCount(AppGlobal.RestaurentProductList.get(i).count);
                viewHolder.tvItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).getCount());
            }
        }
        //viewHolder.tvItemCount.setText("" + AppGlobal.RestaurentProductList.get(position).getCount());
        viewHolder.tvFoodName.setText(itemsData.get(position).Product_name);
        if (!itemsData.get(position).Product_description.equals("")) {
            viewHolder.tvFoodDesc.setVisibility(View.VISIBLE);
            viewHolder.div2.setVisibility(View.VISIBLE);
            viewHolder.tvFoodDesc.setText(itemsData.get(position).Product_description);
        }

        if (itemsData.get(position).Product_vegproduct.equals("veg")) {
            viewHolder.tvFoodType.setBackgroundResource(R.drawable.veg_icon);
        } else {
            viewHolder.tvFoodType.setBackgroundResource(R.drawable.non_veg_icon);
        }

        viewHolder.tvItemRs.setText(" Rs. " + itemsData.get(position).half);
        if (!AppGlobal.RestaurentProductList.get(position).discount.equals("")) {
            // viewHolder.tvItemRs.setPaintFlags(viewHolder.tvItemRs.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            int discount = Integer.parseInt(AppGlobal.RestaurentProductList.get(position).discount);
            int actualPrice = Integer.parseInt(itemsData.get(position).Product_price);
            int discountRs = discount * actualPrice;
            int totalDiscount = discountRs / 100;
            int initialPrice = actualPrice - totalDiscount;
            Log.d("afterdiscount>>>", "" + initialPrice);
            Log.d("afterdiscount>>>", "" + initialPrice);
            restaurentDetailsPojo.setAfterDiscountValue(initialPrice);
            viewHolder.tvDiscountRs.setVisibility(View.GONE);
            viewHolder.tvDiscountRs.setText("Rs. " + initialPrice);
            for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                    //restaurentDetailsPojo.setCount(AppGlobal.RestaurentProductList.get(i).count);
                    AppGlobal.RestaurentProductList.get(i).setAfterDiscountValue(initialPrice);
                }
            }
        } else {
            restaurentDetailsPojo.setAfterDiscountValue(Integer.parseInt(itemsData.get(position).Product_price));
            for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                    // restaurentDetailsPojo.setCount(AppGlobal.RestaurentProductList.get(i).count);
                    AppGlobal.RestaurentProductList.get(i).setAfterDiscountValue(Integer.parseInt(itemsData.get(position).Product_price));
                }
            }
        }
        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                //restaurentDetailsPojo.setCount(AppGlobal.RestaurentProductList.get(i).count);
                //AppGlobal.RestaurentProductList.get(i).setAfterDiscountValue(Integer.parseInt(itemsData.get(position).Product_price));
                count = AppGlobal.RestaurentProductList.get(i).getCount();

            }
        }
        Log.d("count>>>", "" + count);
        viewHolder.tvItemCount.setText("" + restaurentDetailsPojo.getCount());
        viewHolder.imgMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        count = AppGlobal.RestaurentProductList.get(i).getCount();

                    }
                }
                // count = restaurentDetailsPojo.getCount();
                if (count == 0) {
                    count = 0;
                    cartValue = 0;
                    callFragment.setCartData(cartValue, "check", AppGlobal.RestaurentCategoryID);
                    viewHolder.tvItemCount.setText("" + count);

                    if (count == 0) {
                        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                            if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                                AppGlobal.RestaurentProductList.get(i).setCount(count);
                            }
                        }
                        viewHolder.imgMines.setImageResource(R.drawable.disable_mines);
                        // restaurentDetailsPojo.setCount(count);
                    }
                } else {
                    callFragment.setCartData(cartValue, "-", AppGlobal.RestaurentCategoryID);
                    count = count - 1;
                    for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                        if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                            AppGlobal.RestaurentProductList.get(i).setCount(count);
                        }
                    }
                    //  restaurentDetailsPojo.setCount(count);
                    viewHolder.tvItemCount.setText("" + count);
                    cartValue = cartValue - 1;
                    if (count == 0) {
                        viewHolder.imgMines.setImageResource(R.drawable.disable_mines);
                    } else {
                        viewHolder.imgMines.setImageResource(R.drawable.enable_mines);
                    }
                }
                if (count == 0) {
                    callFragment.setCartData(cartValue, "check", AppGlobal.RestaurentCategoryID);
                }
                Log.d("CartValue>>>", "" + cartValue);
                notifyAdapter = (RestaurentDetailsActivity) mContext;
                notifyAdapter.notifyData(AppGlobal.cateID);
            }
        });

        viewHolder.imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (AppGlobal.ProductIDList.isEmpty()) {
                    for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                        if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                            count = AppGlobal.RestaurentProductList.get(i).getCount();

                        }
                    }
                    // count = restaurentDetailsPojo.getCount();
                    count = count + 1;
                    cartValue = cartValue + 1;
                    for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                        if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                            AppGlobal.RestaurentProductList.get(i).setCount(count);
                        }
                    }
                    //restaurentDetailsPojo.setCount(count);
                    viewHolder.tvItemCount.setText("" + count);
                    if (count == 0) {
                        viewHolder.imgMines.setImageResource(R.drawable.disable_mines);
                    } else {
                        viewHolder.imgMines.setImageResource(R.drawable.enable_mines);
                    }
                    count = 0;
                    Log.d("CartValue>>>", "" + cartValue);
                    AppGlobal.ProductIDList.add(AppGlobal.RestaurentCategoryID);
                    callFragment.setCartData(cartValue, "+", AppGlobal.RestaurentCategoryID);
                    notifyAdapter = (RestaurentDetailsActivity) mContext;
                    notifyAdapter.notifyData(AppGlobal.cateID);
                } else {
                    if (AppGlobal.ProductIDList.get(0).equals(AppGlobal.RestaurentCategoryID)) {
                        AppGlobal.ProductIDList.add(AppGlobal.RestaurentCategoryID);
                        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                            if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                                count = AppGlobal.RestaurentProductList.get(i).getCount();

                            }
                        }
                        // count = restaurentDetailsPojo.getCount();
                        count = count + 1;
                        cartValue = cartValue + 1;
                        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                            if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                                AppGlobal.RestaurentProductList.get(i).setCount(count);
                            }
                        }
                        //restaurentDetailsPojo.setCount(count);
                        viewHolder.tvItemCount.setText("" + count);
                        if (count == 0) {
                            viewHolder.imgMines.setImageResource(R.drawable.disable_mines);
                        } else {
                            viewHolder.imgMines.setImageResource(R.drawable.enable_mines);
                        }
                        count = 0;
                        Log.d("CartValue>>>", "" + cartValue);
                        AppGlobal.ProductIDList.add(AppGlobal.RestaurentCategoryID);
                        callFragment.setCartData(count, "+", AppGlobal.RestaurentCategoryID);
                        notifyAdapter = (RestaurentDetailsActivity) mContext;
                        notifyAdapter.notifyData(AppGlobal.cateID);
                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                        alertDialogBuilder.setMessage("Your cart contains already dishes, Are You want to clear them and add another dishes.");
                        alertDialogBuilder.setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        // Toast.makeText(mContext, "You clicked yes  button", Toast.LENGTH_LONG).show();
                                        AppGlobal.ProductIDList.clear();
                                        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                                            if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                                                count = AppGlobal.RestaurentProductList.get(i).getCount();

                                            }
                                        }
                                        //  count = restaurentDetailsPojo.getCount();
                                        count = count + 1;
                                        cartValue = cartValue + 1;
                                        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                                            if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                                                AppGlobal.RestaurentProductList.get(i).setCount(count);
                                            }
                                        }
                                        // restaurentDetailsPojo.setCount(count);
                                        viewHolder.tvItemCount.setText("" + count);
                                        if (count == 0) {
                                            viewHolder.imgMines.setImageResource(R.drawable.disable_mines);
                                        } else {
                                            viewHolder.imgMines.setImageResource(R.drawable.enable_mines);
                                        }
                                        count = 0;
                                        Log.d("CartValue>>>", "" + cartValue);
                                        AppGlobal.ProductIDList.add(AppGlobal.RestaurentCategoryID);
                                        callFragment.setCartData(cartValue, "+", AppGlobal.RestaurentCategoryID);
                                        notifyAdapter = (RestaurentDetailsActivity) mContext;
                                        notifyAdapter.notifyData(AppGlobal.cateID);
                                    }
                                });

                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mContext.startActivity(new Intent(mContext, ReviewOrderActivity.class));
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                }
                notifyAdapter = (RestaurentDetailsActivity) mContext;
                notifyAdapter.notifyData(AppGlobal.cateID);
            }
        });

        viewHolder.imgPlusDummy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*ArrayList<ProductListingDialogAdapterRowPojo> productListingDialogAdapterRowPojos1 = new ArrayList<>();
                productListingDialogAdapterRowPojos1.add(new ProductListingDialogAdapterRowPojo(itemsData.get(position).half, itemsData.get(position).half_label, "0", "0"));
                productListingDialogAdapterRowPojos1.add(new ProductListingDialogAdapterRowPojo(itemsData.get(position).medium, itemsData.get(position).medium_label, "0", "0"));
                productListingDialogAdapterRowPojos1.add(new ProductListingDialogAdapterRowPojo(itemsData.get(position).full, itemsData.get(position).full_label, "0", "0"));
                productListingDialogAdapterRowPojos1.add(new ProductListingDialogAdapterRowPojo(itemsData.get(position).large, itemsData.get(position).large_label, "0", "0"));*/
                generateItemReport(viewHolder, position);
            }
        });

        if (itemsData.size() - 1 == position) {
            viewHolder.blankSpace.setVisibility(View.VISIBLE);
        } else {
            viewHolder.blankSpace.setVisibility(View.GONE);

        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvFoodName, tvFoodDesc, tvItemRs, tvItemCount, tvDiscountRs, blankSpace;
        ImageView imgMines, imgPlus, tvFoodType, imgPlusDummy;
        View div2;

        LinearLayout LayoutPlusDummy, layoutPlusMinus;
        TextView tvQuantity;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tvFoodName = (TextView) itemLayoutView.findViewById(R.id.tvFoodName);
            tvFoodType = (ImageView) itemView.findViewById(R.id.tvFoodType);
            tvFoodDesc = (TextView) itemLayoutView.findViewById(R.id.tvFoodDesc);
            tvItemRs = (TextView) itemLayoutView.findViewById(R.id.tvActualRs);
            tvItemCount = (TextView) itemLayoutView.findViewById(R.id.tvItemCount);
            layoutPlusMinus = (LinearLayout) itemLayoutView.findViewById(R.id.layoutPlusMinus);
            imgMines = (ImageView) itemLayoutView.findViewById(R.id.imgMines);
            blankSpace = (TextView) itemLayoutView.findViewById(R.id.blankSpace);
            imgPlus = (ImageView) itemLayoutView.findViewById(R.id.imgPlus);
            div2 = (View) itemLayoutView.findViewById(R.id.div2);
            tvQuantity = (TextView) itemView.findViewById(R.id.tvHalfQty);
            LayoutPlusDummy = (LinearLayout) itemView.findViewById(R.id.LayoutPlusDummy);
            imgPlusDummy = (ImageView) itemView.findViewById(R.id.imgPlusDummy);
            tvFoodName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
            tvFoodDesc.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            //  tvItemRs.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
            // tvItemCount.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tvDiscountRs = (TextView) itemView.findViewById(R.id.tvDiscountRs);
            // tvDiscountRs.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
            tvFoodDesc.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
            tvQuantity.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            Log.d("pos>>>", "" + pos);
            for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                if (itemsData.get(pos).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                    if (!itemsData.get(pos).in_time.equals("") && !itemsData.get(pos).out_time.equals("")) {
                        String in_time = itemsData.get(pos).in_time;
                        String out_time = itemsData.get(pos).out_time;
                        try {
                            Date mToday = new Date();
                            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
                            String curTime = sdf.format(mToday);
                            Date start = sdf.parse(in_time);
                            Date end = sdf.parse(out_time);
                            Date userDate = sdf.parse(curTime);

                            if (end.before(start)) {
                                Calendar mCal = Calendar.getInstance();
                                mCal.setTime(end);
                                mCal.add(Calendar.DAY_OF_YEAR, 1);
                                end.setTime(mCal.getTimeInMillis());
                            }
                            Log.d("curTime", userDate.toString());
                            Log.d("start", start.toString());
                            Log.d("end", end.toString());
                            if (userDate.after(start) && userDate.before(end)) {
                                UtilityMethod.showAlertBox(mContext, "This product is available after " + out_time);
                            } else {
                                generateItemReport(recommendedVh, pos);
                            }
                        } catch (ParseException e) {
                            generateItemReport(recommendedVh, pos);

                        }
                    }
                }
            }
        }
    }

    private void generateItemReport(final ViewHolder viewHolder, final int position) {
        Button btnConfirm, btnCancel;
        totQty = 0;

        RecyclerView recycler_productList_Dialog;
        final TextView tvHalfPrice, tvHalfLabel, tvHalfItemCount, tvMediumPrice, tvMediumLabel, tvMediumItemCount, tvFullPrice, tvFullLabel, tvFullItemCount, tvLargePrice, tvLargeLabel, tvLargeItemCount;
        ImageView imgHalfMines, imgHalfPlus, imgMediumMines, imgMediumPlus, imgFullMines, imgFullPlus, imgLargeMines, imgLargePlus;

        LinearLayout layoutHalf, layoutMedium, layoutFull, layoutLarge;
        View div3, div4, div2, div1;

        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_orders);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        recycler_productList_Dialog = (RecyclerView) dialog.findViewById(R.id.recycler_productList_Dialog);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recycler_productList_Dialog.setLayoutManager(linearLayoutManager);
        recycler_productList_Dialog.setHasFixedSize(true);
        dialog.show();

        int halfQty_old = 0;
        int mediumQty_old = 0;
        int fullQty_old = 0;
        int largeQty_old = 0;

        tvHalfPrice = (TextView) dialog.findViewById(R.id.tvHalfPrice);
        tvHalfLabel = (TextView) dialog.findViewById(R.id.tvHalfLabel);
        tvHalfItemCount = (TextView) dialog.findViewById(R.id.tvHalfItemCount);
        tvMediumPrice = (TextView) dialog.findViewById(R.id.tvMediumPrice);
        tvMediumLabel = (TextView) dialog.findViewById(R.id.tvMediumLabel);
        tvMediumItemCount = (TextView) dialog.findViewById(R.id.tvMediumItemCount);
        tvFullItemCount = (TextView) dialog.findViewById(R.id.tvFullItemCount);
        tvLargeLabel = (TextView) dialog.findViewById(R.id.tvLargeLabel);
        tvLargeItemCount = (TextView) dialog.findViewById(R.id.tvLargeItemCount);
        tvLargePrice = (TextView) dialog.findViewById(R.id.tvLargePrice);
        tvFullPrice = (TextView) dialog.findViewById(R.id.tvFullPrice);
        tvFullLabel = (TextView) dialog.findViewById(R.id.tvFullLabel);


        layoutHalf = (LinearLayout) dialog.findViewById(R.id.layoutHalf);
        layoutMedium = (LinearLayout) dialog.findViewById(R.id.layoutMedium);
        layoutFull = (LinearLayout) dialog.findViewById(R.id.layoutFull);
        layoutLarge = (LinearLayout) dialog.findViewById(R.id.layoutLarge);
        div1 = (View) dialog.findViewById(R.id.div1);
        div2 = (View) dialog.findViewById(R.id.div2);
        div3 = (View) dialog.findViewById(R.id.div3);
        div4 = (View) dialog.findViewById(R.id.div4);

        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {

                if (!AppGlobal.RestaurentProductList.get(i).half.equals("0") || !AppGlobal.RestaurentProductList.get(i).half_label.equals("")) {
                    tvHalfPrice.setText("Rs. " + AppGlobal.RestaurentProductList.get(i).half);
                    tvHalfLabel.setText(AppGlobal.RestaurentProductList.get(i).half_label);
                    tvHalfItemCount.setText("" + AppGlobal.RestaurentProductList.get(i).halfQty);
                    layoutHalf.setVisibility(View.VISIBLE);
                    div1.setVisibility(View.VISIBLE);
                    halfQty_old = AppGlobal.RestaurentProductList.get(i).halfQty;
                }

                if (!AppGlobal.RestaurentProductList.get(i).medium.equals("0") || !AppGlobal.RestaurentProductList.get(i).medium_label.equals("")) {
                    tvMediumPrice.setText("Rs. " + AppGlobal.RestaurentProductList.get(i).medium);
                    tvMediumLabel.setText("" + AppGlobal.RestaurentProductList.get(i).medium_label);
                    tvMediumItemCount.setText("" + AppGlobal.RestaurentProductList.get(i).mediumQty);
                    layoutMedium.setVisibility(View.VISIBLE);
                    div2.setVisibility(View.VISIBLE);
                    mediumQty_old = AppGlobal.RestaurentProductList.get(i).mediumQty;
                }

                if (!AppGlobal.RestaurentProductList.get(i).large.equals("0") || !AppGlobal.RestaurentProductList.get(i).large_label.equals("")) {
                    tvLargeLabel.setText("" + AppGlobal.RestaurentProductList.get(i).large_label);
                    tvLargeItemCount.setText("" + AppGlobal.RestaurentProductList.get(i).largeQty);
                    tvLargePrice.setText("Rs. " + AppGlobal.RestaurentProductList.get(i).large);
                    layoutLarge.setVisibility(View.VISIBLE);
                    div4.setVisibility(View.VISIBLE);
                    largeQty_old = AppGlobal.RestaurentProductList.get(i).largeQty;
                }

                if (!AppGlobal.RestaurentProductList.get(i).full.equals("0") || !AppGlobal.RestaurentProductList.get(i).full_label.equals("")) {
                    tvFullPrice.setText("Rs. " + AppGlobal.RestaurentProductList.get(i).full);
                    tvFullLabel.setText("" + AppGlobal.RestaurentProductList.get(i).full_label);
                    tvFullItemCount.setText("" + AppGlobal.RestaurentProductList.get(i).fullQty);
                    layoutFull.setVisibility(View.VISIBLE);
                    div3.setVisibility(View.VISIBLE);
                    fullQty_old = AppGlobal.RestaurentProductList.get(i).fullQty;
                }


            }
        }
        Log.d("halfQty_old", "" + halfQty_old);
        Log.d("mediumQty_old", "" + mediumQty_old);
        Log.d("fullQty_old", "" + fullQty_old);
        Log.d("largeQty_old", "" + largeQty_old);

        imgHalfMines = (ImageView) dialog.findViewById(R.id.imgHalfMines);
        imgHalfPlus = (ImageView) dialog.findViewById(R.id.imgHalfPlus);
        imgMediumMines = (ImageView) dialog.findViewById(R.id.imgMediumMines);
        imgMediumPlus = (ImageView) dialog.findViewById(R.id.imgMediumPlus);
        imgFullMines = (ImageView) dialog.findViewById(R.id.imgFullMines);
        imgFullPlus = (ImageView) dialog.findViewById(R.id.imgFullPlus);
        imgLargeMines = (ImageView) dialog.findViewById(R.id.imgLargeMines);
        imgLargePlus = (ImageView) dialog.findViewById(R.id.imgLargePlus);

        imgHalfMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        int halfQty = AppGlobal.RestaurentProductList.get(i).halfQty;
                        Log.d("halfQty>>>", "" + halfQty);
                        if (halfQty == 0) {
                            AppGlobal.RestaurentProductList.get(i).halfQty = 0;
                        } else {
                            halfQty = halfQty - 1;
                            AppGlobal.RestaurentProductList.get(i).halfQty = halfQty;
                        }
                        tvHalfItemCount.setText("" + halfQty);
                    }
                }
            /*    notifyAdapter = (RestaurentDetailsActivity) mContext;
                notifyAdapter.notifyData(AppGlobal.cateID);*/
            }
        });
        imgHalfPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        int halfQty = AppGlobal.RestaurentProductList.get(i).halfQty;
                        halfQty = halfQty + 1;
                        Log.d("halfQty>>>", "" + halfQty);
                        AppGlobal.RestaurentProductList.get(i).halfQty = halfQty;
                        tvHalfItemCount.setText("" + halfQty);
                    }
                }
           /*     notifyAdapter = (RestaurentDetailsActivity) mContext;
                notifyAdapter.notifyData(AppGlobal.cateID);*/
            }
        });
        imgMediumMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        int mediumQty = AppGlobal.RestaurentProductList.get(i).mediumQty;
                        Log.d("mediumQty>>>", "" + mediumQty);
                        if (mediumQty == 0) {
                            AppGlobal.RestaurentProductList.get(i).mediumQty = 0;
                        } else {
                            mediumQty = mediumQty - 1;
                            AppGlobal.RestaurentProductList.get(i).mediumQty = mediumQty;
                        }
                        tvMediumItemCount.setText("" + mediumQty);
                    }
                }
             /*   notifyAdapter = (RestaurentDetailsActivity) mContext;
                notifyAdapter.notifyData(AppGlobal.cateID);*/
            }
        });
        imgMediumPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        int mediumQty = AppGlobal.RestaurentProductList.get(i).mediumQty;
                        mediumQty = mediumQty + 1;
                        AppGlobal.RestaurentProductList.get(i).mediumQty = mediumQty;
                        tvMediumItemCount.setText("" + mediumQty);
                    }
                }
        /*        notifyAdapter = (RestaurentDetailsActivity) mContext;
                notifyAdapter.notifyData(AppGlobal.cateID);*/
            }
        });
        imgFullMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        int fullQty = AppGlobal.RestaurentProductList.get(i).fullQty;
                        Log.d("fullQty>>>", "" + fullQty);
                        if (fullQty == 0) {
                            AppGlobal.RestaurentProductList.get(i).fullQty = 0;
                        } else {
                            fullQty = fullQty - 1;
                            AppGlobal.RestaurentProductList.get(i).fullQty = fullQty;
                        }
                        tvFullItemCount.setText("" + fullQty);

                    }
                }
          /*      notifyAdapter = (RestaurentDetailsActivity) mContext;
                notifyAdapter.notifyData(AppGlobal.cateID);*/
            }
        });
        imgFullPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        int fullQty = AppGlobal.RestaurentProductList.get(i).fullQty;
                        fullQty = fullQty + 1;
                        AppGlobal.RestaurentProductList.get(i).fullQty = fullQty;
                        tvFullItemCount.setText("" + fullQty);

                    }
                }
                /*notifyAdapter = (RestaurentDetailsActivity) mContext;
                notifyAdapter.notifyData(AppGlobal.cateID);*/
            }
        });
        imgLargeMines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        int largeQty = AppGlobal.RestaurentProductList.get(i).largeQty;
                        Log.d("largeQty>>>", "" + largeQty);
                        if (largeQty == 0) {
                            AppGlobal.RestaurentProductList.get(i).largeQty = 0;
                        } else {
                            largeQty = largeQty - 1;
                            AppGlobal.RestaurentProductList.get(i).largeQty = largeQty;
                        }
                        tvLargeItemCount.setText("" + largeQty);

                    }
                }
               /* notifyAdapter = (RestaurentDetailsActivity) mContext;
                notifyAdapter.notifyData(AppGlobal.cateID);*/
            }
        });
        imgLargePlus.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        int largeQty = AppGlobal.RestaurentProductList.get(i).largeQty;
                        largeQty = largeQty + 1;
                        AppGlobal.RestaurentProductList.get(i).largeQty = largeQty;
                        tvLargeItemCount.setText("" + largeQty);
                    }
                }
           /*     notifyAdapter = (RestaurentDetailsActivity) mContext;
                notifyAdapter.notifyData(AppGlobal.cateID);*/
            }
        });

        btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewCartDataFill newCartDataFill = (NewCartDataFill) mContext;
                newCartDataFill.newCartFillData(AppGlobal.RestaurentProductList);
                dialog.dismiss();
                Log.d("Confirm>>", "Confirm");
                totQty = 0;
                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {
                        totQty = AppGlobal.RestaurentProductList.get(i).halfQty + AppGlobal.RestaurentProductList.get(i).fullQty + AppGlobal.RestaurentProductList.get(i).mediumQty + AppGlobal.RestaurentProductList.get(i).largeQty;
                        viewHolder.tvQuantity.setText("" + totQty);
                    }
                }
            }
        });

        btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        final int finalHalfQty_old = halfQty_old;
        final int finalMediumQty_old = mediumQty_old;
        final int finalFullQty_old = fullQty_old;
        final int finalLargeQty_old = largeQty_old;
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                Log.d("cancel>>", "cancel");
                totQty = 0;
                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    if (itemsData.get(position).Product_id.equals(AppGlobal.RestaurentProductList.get(i).Product_id)) {


                        itemsData.get(position).halfQty = finalHalfQty_old;
                        itemsData.get(position).mediumQty = finalMediumQty_old;
                        itemsData.get(position).fullQty = finalFullQty_old;
                        itemsData.get(position).largeQty = finalLargeQty_old;

                        AppGlobal.RestaurentProductList.get(i).halfQty = finalHalfQty_old;
                        AppGlobal.RestaurentProductList.get(i).mediumQty = finalMediumQty_old;
                        AppGlobal.RestaurentProductList.get(i).fullQty = finalFullQty_old;
                        AppGlobal.RestaurentProductList.get(i).largeQty = finalLargeQty_old;

                        Log.d("halfQty_old", "" + finalHalfQty_old);
                        Log.d("mediumQty_old", "" + finalMediumQty_old);
                        Log.d("fullQty_old", "" + finalFullQty_old);
                        Log.d("largeQty_old", "" + finalLargeQty_old);

                        totQty = finalHalfQty_old + finalMediumQty_old + finalFullQty_old + finalLargeQty_old;

                    } else {
                        Log.d("halfQty_oldhhhhhhhhhh", "" + AppGlobal.RestaurentProductList.get(i).halfQty);
                        Log.d("mediumQty_old", "" + AppGlobal.RestaurentProductList.get(i).mediumQty);
                        Log.d("fullQty_old", "" + AppGlobal.RestaurentProductList.get(i).fullQty);
                        Log.d("largeQty_old", "" + AppGlobal.RestaurentProductList.get(i).largeQty);
                        totQty = AppGlobal.RestaurentProductList.get(i).halfQty + AppGlobal.RestaurentProductList.get(i).fullQty + AppGlobal.RestaurentProductList.get(i).mediumQty + AppGlobal.RestaurentProductList.get(i).largeQty;

                    }
                }

                NewCartDataFill newCartDataFill = (NewCartDataFill) mContext;
                newCartDataFill.newCartFillData(AppGlobal.RestaurentProductList);

                viewHolder.tvQuantity.setText("" + totQty);
                notifyAdapter = (RestaurentDetailsActivity) mContext;
                notifyAdapter.notifyData(AppGlobal.cateID);
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }
}
