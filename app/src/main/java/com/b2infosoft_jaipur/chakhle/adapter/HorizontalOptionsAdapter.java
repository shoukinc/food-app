package com.b2infosoft_jaipur.chakhle.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.pojo.HorizontalAdvPojo;
import com.b2infosoft_jaipur.chakhle.pojo.ResturentListPojo;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

/**
 * Created by Microsoft on 5/11/2017.
 */
public class HorizontalOptionsAdapter extends RecyclerView.Adapter<HorizontalOptionsAdapter.HorizontalOptionsHolder> {

    Context mContext;
    ArrayList<HorizontalAdvPojo> mList;
    View mainView;
    String CategaryId = "";

    public HorizontalOptionsAdapter(Context ctx, ArrayList<HorizontalAdvPojo> list, View mainView) {

        mContext = ctx;
        mList = list;
        this.mainView = mainView;
    }

    @Override
    public HorizontalOptionsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_horizontal_adv_options, parent, false);
        return new HorizontalOptionsHolder(view);
    }

    @Override
    public void onBindViewHolder(HorizontalOptionsHolder holder, final int position) {
        CategaryId = mList.get(position).Categaryid;
       /* holder.img_Icon.setImageResource(Integer.parseInt(mList.get(position).foodImages));*/


      // Log.d("img",mList.get(position).foodImages+ "Empty");

        holder.tvFoodType.setText(mList.get(position).FoodType);

      if (mList.get(position).foodImages.equals("")) {
          Log.d("img",mList.get(position).foodImages+ " Empty " +position);

          holder.img_Icon.setImageResource(R.drawable.img_a);


      }else{

          Log.d("img",mList.get(position).foodImages+ " full " +position);

          Glide.with(mContext).load("http://chakhle.in/admin/img/category/" + mList.get(position).foodImages)
                  .thumbnail(0.5f)
                  .crossFade()
                  .diskCacheStrategy(DiskCacheStrategy.ALL)
                  .into(holder.img_Icon);
      }
        holder.img_Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResturentListPojo.getResturentList(mainView, mContext, AppGlobal.LocationAddress, "RestFragment", mList.get(position).Categaryid);
            }
        });
        holder.tvFoodType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResturentListPojo.getResturentList(mainView, mContext, AppGlobal.LocationAddress, "RestFragment", mList.get(position).Categaryid);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class HorizontalOptionsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView img_Icon;
        TextView tvFoodType;

        public HorizontalOptionsHolder(View itemView) {
            super(itemView);
            img_Icon = (ImageView) itemView.findViewById(R.id.img);
            tvFoodType = (TextView) itemView.findViewById(R.id.tvFoodType);
            tvFoodType.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_medium.ttf"));
            //itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int pos = getLayoutPosition();
            Log.d("Category>>", mList.get(pos).Categaryid);

            ResturentListPojo.getResturentList(itemView, mContext, AppGlobal.LocationAddress, "RestFragment", mList.get(pos).Categaryid);

        }
    }
}
