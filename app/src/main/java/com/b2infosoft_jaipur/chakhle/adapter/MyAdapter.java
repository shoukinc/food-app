package com.b2infosoft_jaipur.chakhle.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.fragments.CallFragment;
import com.b2infosoft_jaipur.chakhle.pojo.AdvertiesPojo;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.bumptech.glide.Glide;
import com.github.demono.adapter.InfinitePagerAdapter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by u on 26-Dec-17.
 */

public class MyAdapter extends InfinitePagerAdapter {

    ArrayList<AdvertiesPojo> advertiesPojosList;
    Context mContext;
    CallFragment callFragment;
    public MyAdapter(Context ctx,ArrayList<AdvertiesPojo> advertiesPojosList) {
        this.advertiesPojosList = advertiesPojosList;
        mContext = ctx;
        callFragment = (CallFragment) mContext;
    }

    @Override
    public int getItemCount() {
        return advertiesPojosList == null ? 0 : advertiesPojosList.size();
    }

    @Override
    public View getItemView(final int currentPage, View convertView, ViewGroup container) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View layout = (View) inflater.inflate(R.layout.layout_restaurent_adverties_row, container, false);
        ImageView imageView = (ImageView) layout.findViewById(R.id.imageView);
        // int pageId = Integer.parseInt(pageIDsArray[position]);
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.right_to_left);
        layout.startAnimation(animation);

        Glide.with(mContext)
                .load(advertiesPojosList.get(currentPage).image)
                .crossFade()
                .into(imageView);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                {
                    if (advertiesPojosList.size()>0){
                        if (advertiesPojosList.get(currentPage).off_time.equals("")||advertiesPojosList.get(currentPage).off_time.equals("0")) {
                            UtilityMethod.showAlertBox(mContext, "Time not updated by restaurant admin.");
                        }else{
                            String in_time = advertiesPojosList.get(currentPage).off_time;
                            String out_time = advertiesPojosList.get(currentPage).off_time_1;
                            try {
                                Date mToday = new Date();
                                java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm:ss");
                                String curTime = sdf.format(mToday);
                                Date start = sdf.parse(in_time);
                                Date end = sdf.parse(out_time);
                                Date userDate = sdf.parse(curTime);
                                if (end.before(start)) {
                                    Calendar mCal = Calendar.getInstance();
                                    mCal.setTime(end);
                                    mCal.add(Calendar.DAY_OF_YEAR, 1);
                                    end.setTime(mCal.getTimeInMillis());
                                }
                                Log.d("curTime", userDate.toString());
                                Log.d("start", start.toString());
                                Log.d("end", end.toString());
                                if (userDate.after(start) && userDate.before(end)) {
                                    Log.d("currentPage>>>", "" + currentPage);
                                    AppGlobal.RestDiscount = "";
                                    AppGlobal.RestaurentCategoryID = "";
                                    AppGlobal.RestaurentName = "";
                                    AppGlobal.RestarentImage = "";
                                    AppGlobal.RestarentAddress = "";
                                    AppGlobal.RestRating = "";
                                    AppGlobal.RestBanner = "";
                                    AppGlobal.RestaurentCategoryID = advertiesPojosList.get(currentPage).restaurant_id;
                                    AppGlobal.RestaurentName = advertiesPojosList.get(currentPage).name;
                                    AppGlobal.RestarentImage = advertiesPojosList.get(currentPage).image;
                                    AppGlobal.RestBanner = advertiesPojosList.get(currentPage).hotel_banner;
                                    AppGlobal.RestarentAddress = advertiesPojosList.get(currentPage).link;
                                    Log.d("link>>>", advertiesPojosList.get(currentPage).hotel_banner);
                                    //AppGlobal.RestRating = advertiesPojosList.get(position).TotalRating;
                                    // AppGlobal.RestBanner = advertiesPojosList.get(position).banner_image;
                                    Log.d("RestID>>>", AppGlobal.RestaurentCategoryID);
                                    Log.d("Name>>>", AppGlobal.RestaurentName);
                                    callFragment.setFragment("RestaurentDetailsFragment");
                                    //mContext.startActivity(new Intent(mContext, RestaurentDetailsActivity.class));
                                } else {
                                    java.text.SimpleDateFormat _12HourSDF = new java.text.SimpleDateFormat("hh:mm a");
                                    UtilityMethod.showAlertBox(mContext, "This Restaurent is open only " +_12HourSDF.format(start)+ " to "+_12HourSDF.format(end));
                                }
                            } catch (ParseException e) {
                                // Invalid date was entered
                            }
                        }
                    }
                }
            }
        });

        return layout;
    }

}
