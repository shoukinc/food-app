package com.b2infosoft_jaipur.chakhle.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.b2infosoft_jaipur.chakhle.fragments.ProductListFragment;
import com.b2infosoft_jaipur.chakhle.fragments.RecommendedFragment;
import com.b2infosoft_jaipur.chakhle.pojo.RestaurentDetailsPojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Microsoft on 5/20/2017.
 */

public class RestaruentDetailViewPager extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    ArrayList<RestaurentDetailsPojo> restaurantCategoryList = new ArrayList<>();
    ArrayList<RestaurentDetailsPojo> restaurentProductList = new ArrayList<>();
    int mNumOfTabs;

    int tabCount;

    //Constructor to the class
    public RestaruentDetailViewPager(FragmentManager fm, int tabCount, ArrayList<RestaurentDetailsPojo> restaurantCategoryList, final ArrayList<RestaurentDetailsPojo> restaurentProductList) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
        this.restaurantCategoryList = restaurantCategoryList;
        this.restaurentProductList = restaurentProductList;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs

        if (position == 0) {
            return new RecommendedFragment();
        } else {
            String categoryID = restaurantCategoryList.get(position).category_id;
            return new ProductListFragment(categoryID);
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public int getItemPosition(Object object) {
        return FragmentStatePagerAdapter.POSITION_NONE;

    }
}
