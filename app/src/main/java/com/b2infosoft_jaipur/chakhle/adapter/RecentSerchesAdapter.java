package com.b2infosoft_jaipur.chakhle.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.activities.RestaurantListActivity;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Microsoft on 5/11/2017.
 */
public class RecentSerchesAdapter extends RecyclerView.Adapter<RecentSerchesAdapter.RecentSerchesHolder> {

    Context mContext;
    List<String> mList;

    public RecentSerchesAdapter(Context ctx, List<String> list) {

        mContext = ctx;
        mList = list;
    }

    @Override
    public RecentSerchesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_recent_search_row, parent, false);
        return new RecentSerchesHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecentSerchesHolder holder, final int position) {

        holder.tv_address.setText(mList.get(position));
        if (mList.size()-1 == position){
            holder.view.setVisibility(View.GONE);
        }
        holder.tv_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String str = (String) holder.tv_address.getText().toString();

                String kept = str.substring(0, str.indexOf(","));
                String remainder = str.substring(str.indexOf(",") + 1, str.length());
                Log.d("reminder>>>", remainder);
                Log.d("kept>>>", kept);
                AppGlobal.City = kept;
                SessionManager sessionManager = new SessionManager(mContext);
                if (sessionManager.isLoggedIn()) {
                    sessionManager.saveLocationList(mContext, str);
                }
                Intent intent = new Intent(mContext, RestaurantListActivity.class);
                //intent.putExtra("locationAddress", str);
                //intent.putExtra("address", address);
                Bundle bundle = new Bundle();
                bundle.putString("ClassName", "SearchRestaurentActivity");
                //Add your data from getFactualResults method to bundle

                AppGlobal.Addresses=str;
                AppGlobal.LocationAddress = str;
                AppGlobal.City = str;
                bundle.putString("locationAddress", str);
                bundle.putString("address", str);
                bundle.putString("city", kept);

                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mContext.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class RecentSerchesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // ImageView img_Icon;
        TextView tv_address;
        View view;


        public RecentSerchesHolder(View itemView) {
            super(itemView);
            // img_Icon = (ImageView) itemView.findViewById(R.id.img_Icon);
            tv_address = (TextView) itemView.findViewById(R.id.tv_address);
            view = (View)itemView.findViewById(R.id.view);
            //itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
           /* int pos = getLayoutPosition();
            String Id = mList.get(pos).NewsId;*/
            /*Log.d("Id",Id);
            AppGlobal.NewsID = Id;
            mContext.startActivity(new Intent(mContext, NewsDetailsActivity.class));*/
        }
    }
}
