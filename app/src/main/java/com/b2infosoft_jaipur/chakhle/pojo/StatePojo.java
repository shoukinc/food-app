package com.b2infosoft_jaipur.chakhle.pojo;

import android.content.Context;
import android.util.Log;

import com.b2infosoft_jaipur.chakhle.activities.ManageAddressActivity;
import com.b2infosoft_jaipur.chakhle.activities.PartnerWithUsActivity;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Microsoft on 5/31/2017.
 */

public class StatePojo {

    public String StateID = "";
    public String StateName = "";

    public StatePojo(String stateID, String stateName) {
        StateID = stateID;
        StateName = stateName;
    }

    public static void getStateList(final Context mContext, final String From) {

        final ArrayList<StatePojo> list = new ArrayList<>();
        WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject mainObject = new JSONObject(response);
                    JSONArray stateArray = mainObject.getJSONArray("State");
                    for (int i = 0; i < stateArray.length(); i++) {
                        JSONObject dataObject = stateArray.getJSONObject(i);
                        list.add(new StatePojo(dataObject.getString("id"), dataObject.getString("name")));
                    }
                    if (!list.isEmpty() && From.equals("addNewAddress")) {
                        ((ManageAddressActivity) mContext).setStateAdapter(list);
                    } else if (!list.isEmpty() && From.equals("partnerWithUs")) {
                        ((PartnerWithUsActivity) mContext).setStateAdapter(list);
                    }

                } catch (JSONException e) {
                    if (From.equals("addNewAddress")) {
                        ((ManageAddressActivity) mContext).setStateAdapter(list);
                        e.printStackTrace();
                    } else if (From.equals("partnerWithUs")) {
                        ((PartnerWithUsActivity) mContext).setStateAdapter(list);
                        e.printStackTrace();
                    }
                }

            }
        };

        String url = AppGlobal.GetStates;
        Log.d("url>>", url);
        webServiceCaller.execute(url);

    }


}
