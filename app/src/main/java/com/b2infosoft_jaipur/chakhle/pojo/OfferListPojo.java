package com.b2infosoft_jaipur.chakhle.pojo;

import android.content.Context;
import android.util.Log;

import com.b2infosoft_jaipur.chakhle.activities.OfferActivity;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Microsoft on 6/20/2017.
 */

public class OfferListPojo {
    public String id = "";
    public String discount_title = "";
    public String Offer_CuopanCode = "";
    public String start_date = "";
    public String expiry_date = "";
    public String minimum_purchase = "";
    public String type = "";
    public String value = "";
    public String status = "";
    public String restaurant_id = "";

    public OfferListPojo(String id, String discount_title, String offer_CuopanCode, String start_date, String expiry_date, String minimum_purchase, String type, String value, String status, String restaurant_id) {
        this.id = id;
        this.discount_title = discount_title;
        Offer_CuopanCode = offer_CuopanCode;
        this.start_date = start_date;
        this.expiry_date = expiry_date;
        this.minimum_purchase = minimum_purchase;
        this.type = type;
        this.value = value;
        this.status = status;
        this.restaurant_id = restaurant_id;
    }

    public OfferListPojo(String offerTitle, String offer_CuopanCode) {
        discount_title = offerTitle;
        Offer_CuopanCode = offer_CuopanCode;
    }

    public static void getOfferList(final Context mContext) {

        final ArrayList<OfferListPojo> list = new ArrayList<>();
        WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject mainObject = new JSONObject(response);
                    JSONArray dataArray = mainObject.getJSONArray("data");
                    for (int i = 0; i < dataArray.length(); i++) {
                        JSONObject dataObject = dataArray.getJSONObject(i);

                        JSONObject DiscountOffer = dataObject.getJSONObject("DiscountOffer");
                        JSONObject DiscountOfferRestaurant = dataObject.getJSONObject("DiscountOfferRestaurant");
                        list.add(new OfferListPojo(
                                DiscountOffer.getString("id"),
                                DiscountOffer.getString("discount_title"),
                                "No Coupan Code Available",
                                DiscountOffer.getString("start_date"),
                                DiscountOffer.getString("expiry_date"),
                                DiscountOffer.getString("minimum_purchase"),
                                DiscountOffer.getString("type"),
                                DiscountOffer.getString("value"),
                                DiscountOffer.getString("status"),
                                DiscountOfferRestaurant.getString("restaurant_id")
                        ));
                    }
                    if (!list.isEmpty()) {
                        ((OfferActivity) mContext).setOfferListAdapter(list);
                    }

                } catch (JSONException e) {
                    ((OfferActivity) mContext).setOfferListAdapter(list);
                    e.printStackTrace();
                }
            }
        };

        String url = AppGlobal.DiscountOffer;
        Log.d("url>>", url);
        webServiceCaller.execute(url);
    }

}
