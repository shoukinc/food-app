package com.b2infosoft_jaipur.chakhle.pojo;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import com.b2infosoft_jaipur.chakhle.activities.FavRestorentActivity;
import com.b2infosoft_jaipur.chakhle.activities.SearchActivity;
import com.b2infosoft_jaipur.chakhle.fragments.RestaurentFragment;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Microsoft on 5/12/2017.
 */
public class ResturentListPojo {

    public String id = "";
    public String name = "";
    public String slug = "";
    public String moblie = "";
    public String email = "";
    public String logo_image = "";
    public String description = "";
    public String address = "";
    public String minimum_purchase = "";
    public String delivery_time = "";
    public String chakhle_assured = "";
    public String pure_veg_dishes = "";
    public String status = "";
    public String distance = "";
    public String TotalVote = "";
    public String TotalRating = "";
    public String cuisine = "";
    public String TotalReview = "";
    public String banner_image = "";
    public String open_time = "";
    public String close_time = "";
    public String DiscountOffer_restaurant_id = "";
    public String ReviewRestaurent_id = "";
    public String discount_title = "";
    public String DiscountOffer_minimum_purchase = "";
    public String DiscountOffer_type = "";
    public String DiscountOffer_value = "";
    public ArrayList<ResturentListPojo> DiscountArrayList;
    public ArrayList<ResturentListPojo> ReviewArrayList;

    public ResturentListPojo(String discountOffer_restaurant_id, String discount_title, String DiscountOffer_minimum_purchase, String DiscountOffer_type, String DiscountOffer_value) {
        DiscountOffer_restaurant_id = discountOffer_restaurant_id;
        this.discount_title = discount_title;
        this.DiscountOffer_minimum_purchase = DiscountOffer_minimum_purchase;
        this.DiscountOffer_type = DiscountOffer_type;
        this.DiscountOffer_value = DiscountOffer_value;
    }

    public ResturentListPojo(String id, String name, String slug, String moblie, String email, String logo_image, String description, String address, String minimum_purchase, String delivery_time, String chakhle_assured, String pure_veg_dishes, String status, String distance, String cuisine, String rating, String TotalReview, String banner, String openTime, String closeTime) {
        this.id = id;
        this.name = name;
        this.slug = slug;
        this.moblie = moblie;
        this.email = email;
        this.logo_image = logo_image;
        this.description = description;
        this.address = address;
        this.minimum_purchase = minimum_purchase;
        this.delivery_time = delivery_time;
        this.chakhle_assured = chakhle_assured;
        this.pure_veg_dishes = pure_veg_dishes;
        this.status = status;
        this.distance = distance;
        this.cuisine = cuisine;
        this.TotalRating = rating;
        this.TotalReview = TotalReview;
        this.banner_image = banner;
        this.open_time = openTime;
        this.close_time = closeTime;
    }

    public static void getResturentList(final View v, final Context context, final String location, final String ClassName, final String categoryId) {
        final ArrayList<ResturentListPojo> list = new ArrayList<>();
        final ArrayList<ResturentListPojo> reviewList = new ArrayList<>();
        final ArrayList<ResturentListPojo> discountList = new ArrayList<>();

        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, context, "Please wait....", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response res", response);
                try {

                    try{
                        JSONObject    mainObj = new JSONObject(response);

                        if (mainObj.getBoolean("success") == true) {
                            String totalRating = "";
                            String totalVotes = "";
                            String discount_title = "";
                            String minimum_purchase = "";
                            String type = "";
                            String value = "";

                            JSONArray mainArrayJson = mainObj.getJSONArray("data");
                            for (int i = 0; i < mainArrayJson.length(); i++) {
                                JSONObject dataObject = mainArrayJson.getJSONObject(i);
                                JSONObject resObject = dataObject.getJSONObject("Restaurant");
                                try {
                                    JSONArray DiscountOfferRestaurantArray = dataObject.getJSONArray("DiscountOfferRestaurant");
                                    if (DiscountOfferRestaurantArray.length() != 0) {
                                        for (int l = 0; l < DiscountOfferRestaurantArray.length(); l++) {
                                            JSONObject discountOfferObject = DiscountOfferRestaurantArray.getJSONObject(l);
                                            String RestID = discountOfferObject.getString("restaurant_id");
                                            JSONObject discountDataObject = discountOfferObject.getJSONObject("DiscountOffer");
                                            discountList.add(new ResturentListPojo(
                                                    RestID,
                                                    discountDataObject.getString("discount_title"),
                                                    discountDataObject.getString("minimum_purchase"),
                                                    discountDataObject.getString("type"),
                                                    discountDataObject.getString("value")
                                            ));
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                list.add(new ResturentListPojo(
                                        resObject.getString("id"),
                                        resObject.getString("name"),
                                        resObject.getString("slug"),
                                        resObject.getString("moblie"),
                                        resObject.getString("email"),
                                        resObject.getString("logo_image"),
                                        resObject.getString("description"),
                                        resObject.getString("address"),
                                        resObject.getString("minimum_purchase"),
                                        resObject.getString("delivery_time"),
                                        resObject.getString("chakhle_assured"),
                                        resObject.getString("pure_veg_dishes"),
                                        resObject.getString("status"),
                                        resObject.getString("distance"),
                                        resObject.getString("cuisine"),
                                        resObject.getString("rating"),
                                        resObject.getString("totalreview"),
                                        resObject.getString("banner_image"),
                                        resObject.getString("off_time"),
                                        resObject.getString("off_time_1")
                                ));

                            }
                            if (list != null) {
                                if (ClassName.equals("RestFragment")) {
                                    RestaurentFragment restaurentFragment = new RestaurentFragment();
                                    restaurentFragment.setAdapterData(v, context, list, discountList);
                                } else if (ClassName.equals("RestaurentFilterActivity")) {
                                    RestaurentFragment restaurentFragment = new RestaurentFragment();
                                    restaurentFragment.setAdapterData(v, context, list, discountList);
                                } else if (ClassName.equals("SearchActivity")) {
                                    ((SearchActivity) context).setRestaurentList(list, discountList);
                                }
                                //  ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer2, new RestaurentFragment(list, discountList)).addToBackStack(null).commit();
                            }

                        } else {
                            UtilityMethod.showAlertBox(context, "Opps! No Restaurant Found");
                        }

                    }catch (JSONException e)
                    {
                        e.printStackTrace();
                        getResturentList(v,context,location,ClassName,categoryId);

                    }

                } catch (Exception e) {
                    RestaurentFragment restaurentFragment = new RestaurentFragment();
                    restaurentFragment.setAdapterData(v, context, list, discountList);
                    //((AppCompatActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer2, new RestaurentFragment(list, discountList)).addToBackStack(null).commit();
                    e.printStackTrace();
                }
            }
        };
        //  serviceCaller.addNameValuePair("location", "67/167, Pratap Nagar, Sanganer, Pratap Nagar, Jaipur, Rajasthan 302029, India");

        Log.d("location",location+" hhhh ");
        Log.d("categoryId",categoryId+" hhhh ");
        serviceCaller.addNameValuePair("location", location);

        if (ClassName.equals("RestaurentFilterActivity")){

            String chakhle_assured=AppGlobal.Chakhle_assured;
            String pure_veg_dishes=AppGlobal.Pure_veg_dishes;
            String rating=AppGlobal.Rating;
            String offers=AppGlobal.Offers;

            serviceCaller.addNameValuePair("catogory", "1");
            serviceCaller.addNameValuePair("chakhle_assured", chakhle_assured);
            serviceCaller.addNameValuePair("pure_veg_dishes", pure_veg_dishes);
            serviceCaller.addNameValuePair("rating", rating);
            serviceCaller.addNameValuePair("offers", offers);

        }else{
            serviceCaller.addNameValuePair("catogory", categoryId);
        }

        String url = AppGlobal.SearchResturents;
        Log.d("url>>", url);
        serviceCaller.execute(url);
    }

    public static void getFavRestaurant(final Context mContext, String UserID) {

        final ArrayList<ResturentListPojo> list = new ArrayList<>();
        final ArrayList<ResturentListPojo> discountList = new ArrayList<>();

        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait....", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getBoolean("success") == true) {

                        JSONArray mainArrayJson = mainObj.getJSONArray("data");
                        for (int i = 0; i < mainArrayJson.length(); i++) {
                            JSONObject dataObject = mainArrayJson.getJSONObject(i);
                            JSONObject resObject = dataObject.getJSONObject("Restaurant");

                            try {
                                JSONArray DiscountOfferRestaurantArray = dataObject.getJSONArray("DiscountOfferRestaurant");
                                if (DiscountOfferRestaurantArray.length() != 0) {
                                    for (int l = 0; l < DiscountOfferRestaurantArray.length(); l++) {

                                        JSONObject discountOfferObject = DiscountOfferRestaurantArray.getJSONObject(l);
                                        String RestID = discountOfferObject.getString("restaurant_id");
                                        JSONObject discountDataObject = discountOfferObject.getJSONObject("DiscountOffer");
                                        discountList.add(new ResturentListPojo(
                                                RestID,
                                                discountDataObject.getString("discount_title"),
                                                discountDataObject.getString("minimum_purchase"),
                                                discountDataObject.getString("type"),
                                                discountDataObject.getString("value")
                                        ));
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            list.add(new ResturentListPojo(
                                    resObject.getString("id"),
                                    resObject.getString("name"),
                                    resObject.getString("slug"),
                                    resObject.getString("moblie"),
                                    resObject.getString("email"),
                                    resObject.getString("logo_image"),
                                    resObject.getString("description"),
                                    resObject.getString("address"),
                                    resObject.getString("minimum_purchase"),
                                    resObject.getString("delivery_time"),
                                    resObject.getString("chakhle_assured"),
                                    resObject.getString("pure_veg_dishes"),
                                    resObject.getString("status"),
                                    "",
                                    resObject.getString("cuisine"),
                                    resObject.getString("rating"),
                                    resObject.getString("totalreview"),
                                    resObject.getString("banner_image"),
                                    resObject.getString("open_time"),
                                    resObject.getString("close_time")

                            ));
                        }

                        if (list != null) {
                            ((FavRestorentActivity) mContext).setFavRestorantList(list, discountList);
                        }

                    } else {
                        UtilityMethod.showAlertBox(mContext, "Opps! No Favrouite Restaurant Found");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    ((FavRestorentActivity) mContext).setFavRestorantList(list, discountList);

                }

            }
        };

        //  serviceCaller.addNameValuePair("location", "67/167, Pratap Nagar, Sanganer, Pratap Nagar, Jaipur, Rajasthan 302029, India");
        serviceCaller.addNameValuePair("user_id", UserID);
        String url = AppGlobal.GetFavoriteRest;
        Log.d("url>>", url);
        serviceCaller.execute(url);
    }

}
