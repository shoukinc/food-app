package com.b2infosoft_jaipur.chakhle.pojo;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.b2infosoft_jaipur.chakhle.fragments.RestaurentFragment;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Microsoft on 5/16/2017.
 */

public class HorizontalAdvPojo {
    Context mContext;
    public String Categaryid = "";
    public String foodImages;
    public String FoodType = "";


   /* public HorizontalAdvPojo(Context mContext, String foodImages, String foodType) {
        this.mContext = mContext;
        this.foodImages = foodImages;
        FoodType = foodType;
    }*/

    public HorizontalAdvPojo(Context mContext, String Categaryid, String foodImages, String foodType) {
        this.mContext = mContext;
        this.Categaryid = Categaryid;
        this.foodImages = foodImages;
        this.FoodType = foodType;
    }

    public static void getCategoriesWithImageList(final View v,final Context mContext) {
        final ArrayList<HorizontalAdvPojo> horizontalAdvPojos1 = new ArrayList<>();

        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait....", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Category");
                        if (!jsonObject1.getString("image").equals("")){
                            horizontalAdvPojos1.add(new HorizontalAdvPojo(mContext,jsonObject1.getString("id"),
                                    jsonObject1.getString("image"),
                                    jsonObject1.getString("name")));
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (horizontalAdvPojos1!=null){
                    RestaurentFragment restaurentFragment = new RestaurentFragment();
                    restaurentFragment.setCategoryListWithImage(v, mContext,horizontalAdvPojos1);
                }


            }
        };
        String url = AppGlobal.CategaryImage;
        Log.d("url>>", url);
        serviceCaller.execute(url);

    }

}
