package com.b2infosoft_jaipur.chakhle.pojo;

/**
 * Created by Microsoft on 5/27/2017.
 */

public class NotificationPojo {

    public String NotificationTitle = "";
    public String NotificationDesc = "";
    public String NotificationTiming = "";

    public String order_id="";

    public NotificationPojo(String notificationTitle, String notificationDesc,String Timing,String order_id) {
        this.NotificationTitle = notificationTitle;
        this.NotificationDesc = notificationDesc;
        this.NotificationTiming = Timing;
        this.order_id = order_id;
    }

}
