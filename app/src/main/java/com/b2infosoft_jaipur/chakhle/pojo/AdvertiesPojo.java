package com.b2infosoft_jaipur.chakhle.pojo;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Microsoft on 5/11/2017.
 */
public class AdvertiesPojo {

    public String id = "";
    public String name = "";
    public String restaurant_id = "";
    public String link = "";
    public String image = "";
    public String off_time = "";
    public String off_time_1 = "";
    public String hotel_banner="";

    public AdvertiesPojo(String id, String name, String restaurant_id, String link, String image, String off_time, String off_time_1, String hotel_banner) {
        this.id = id;
        this.name = name;
        this.restaurant_id = restaurant_id;
        this.link = link;
        this.image = image;
        this.off_time = off_time;
        this.off_time_1 = off_time_1;
        this.hotel_banner = hotel_banner;
    }

    public static void getAdvt_List(View v, final Context context, String City) {

        final ArrayList<AdvertiesPojo> list = new ArrayList<>();
        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, context, "Please wait....", false) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response+" j");
                try {
                    JSONObject mainObj = new JSONObject(response);

                    AppGlobal.AdvertiseList.clear();
                    if (mainObj.getBoolean("success") == true) {
                        JSONArray mainArrayJson = mainObj.getJSONArray("data");
                        for (int i = 0; i < mainArrayJson.length(); i++) {
                            JSONObject dataObject = mainArrayJson.getJSONObject(i);
                            JSONObject resObject = dataObject.getJSONObject("Advertisement");

                            //for (int j = 0; j < resObject.length(); j++) {
                           /* Log.d("Objectdata>>>", resObject.getString("id"));
                            Log.d("Objectdata>>>", resObject.getString("name"));
                            */

                            list.add(new AdvertiesPojo(
                                    resObject.getString("id"),
                                    resObject.getString("name"),
                                    resObject.getString("restaurant_id"),
                                    resObject.getString("link"),
                                    resObject.getString("image"),
                                    resObject.getString("off_time"),
                                    resObject.getString("off_time_1"),
                                    resObject.getString("hotel_banner")
                            ));

                        }
                        if (list != null) {
                            AppGlobal.AdvertiseList = list;
                            //RestaurentFragment restaurentFragment = new RestaurentFragment(list);
                            //((AppCompatActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new RestaurentFragment(list, Advlist)).addToBackStack(null).commit();
                        }

                    }
                } catch (JSONException e) {
                    AppGlobal.AdvertiseList = list;

                    Log.d("Empty","Empty");

                    // ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new RestaurentFragment(list, Advlist)).addToBackStack(null).commit();
                    e.printStackTrace();
                }
            }
        };

        //  serviceCaller.addNameValuePair("city", "jaipur");
        Log.d("city>>>", "" + City);
        serviceCaller.addNameValuePair("city", City);
        String url = AppGlobal.AdvtList;
        Log.d("url>>", url);
        serviceCaller.execute(url);

    }

}
