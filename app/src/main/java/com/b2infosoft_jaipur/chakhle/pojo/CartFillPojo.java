package com.b2infosoft_jaipur.chakhle.pojo;

/**
 * Created by Microsoft on 6/6/2017.
 */

public class CartFillPojo {
    public String Product_id = "";
    public String Product_name = "";
    public String product_slug = "";
    public String Product_price = "";
    public String restaurent_id = "";
    public String user_id = "";
    public String RestaurentName = "";
    public String Discount = "";
    public String TotalContainerCharge = "";
    public String PerProductContainerCharge = "";
    public String MinimumPurchase = "";
    public String ExtraCharge = "";
    public int HalfQty;
    public double HalfPrice;
    public double HalfTotalPrice;
    public int FullQty;
    public double FullPrice;
    public double FullTotalPrice;
    public int LargeQty;
    public double LargePrice;
    public double LargeTotalPrice;
    public int MediumQty;
    public double MediumPrice;
    public double MediumTotalPrice;
    public String DiscountType;
    public String HalfLabel;
    public String MediumLabel;
    public String FullLabel;
    public String LargeLabel;
    public String let_nite_charge = "";
    public String tax_percentage = "";
    public String RestInTime = "";
    public String RestOffTime = "";


    public double getProductTotalPrice() {
        return ProductTotalPrice;
    }

    public void setProductTotalPrice(int productTotalPrice) {
        ProductTotalPrice = productTotalPrice;
    }

    public double ProductTotalPrice;
    public int quantity = 0;
    public int afterDiscountValue = 0;

    public int getTotalProductPrice() {
        return totalProductPrice;
    }

    public void setTotalProductPrice(int totalProductPrice) {
        this.totalProductPrice = totalProductPrice;
    }

    public int totalProductPrice = 0;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public CartFillPojo(String product_id, int count, String product_name, String product_price, double ProductTotalPrice, String product_category_id, String userID, String slugType, int afterDiscountValue, int totalProductPrice, String restName, String Discount,
                        String TotalContainerCharge, String MinimumPurchase, String ExtraCharge, String PerProductContainerCharge,
                        int HalfQty,
                        double HalfPrice,
                        double HalfTotalPrice,
                        int FullQty,
                        double FullPrice,
                        double FullTotalPrice,
                        int LargeQty,
                        double LargePrice,
                        double LargeTotalPrice,
                        int MediumQty,
                        double MediumPrice,
                        double MediumTotalPrice, String DiscountType,
                        String halfLabel, String mediumLabel, String fullLabel, String largeLabel,
                        String let_nite_charge, String tax_percentage, String restIn, String restOff
    ) {
        Product_id = product_id;
        Product_name = product_name;
        Product_price = product_price;
        restaurent_id = product_category_id;
        this.quantity = count;
        this.user_id = userID;
        this.ProductTotalPrice = ProductTotalPrice;
        product_slug = slugType;
        this.afterDiscountValue = afterDiscountValue;
        this.totalProductPrice = totalProductPrice;
        this.RestaurentName = restName;
        this.Discount = Discount;
        this.TotalContainerCharge = TotalContainerCharge;
        this.MinimumPurchase = MinimumPurchase;
        this.ExtraCharge = ExtraCharge;
        this.PerProductContainerCharge = PerProductContainerCharge;
        this.HalfQty = HalfQty;
        this.HalfPrice = HalfPrice;
        this.HalfTotalPrice = HalfTotalPrice;
        this.FullQty = FullQty;
        this.FullPrice = FullPrice;
        this.FullTotalPrice = FullTotalPrice;
        this.LargeQty = LargeQty;
        this.LargePrice = LargePrice;
        this.LargeTotalPrice = LargeTotalPrice;
        this.MediumQty = MediumQty;
        this.MediumPrice = MediumPrice;
        this.MediumTotalPrice = MediumTotalPrice;
        this.DiscountType = DiscountType;

        this.HalfLabel = halfLabel;
        this.MediumLabel = mediumLabel;
        this.FullLabel = fullLabel;
        this.LargeLabel = largeLabel;
        this.let_nite_charge = let_nite_charge;
        this.tax_percentage = tax_percentage;
        this.RestInTime = restIn;
        this.RestOffTime = restOff;
    }

}
