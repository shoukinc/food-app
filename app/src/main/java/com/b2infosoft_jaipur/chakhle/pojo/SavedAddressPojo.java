package com.b2infosoft_jaipur.chakhle.pojo;

import android.content.Context;
import android.util.Log;

import com.b2infosoft_jaipur.chakhle.activities.ManageAddressActivity;
import com.b2infosoft_jaipur.chakhle.activities.PickAddressActivity;
import com.b2infosoft_jaipur.chakhle.activities.SearchRestaurentActivity;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Microsoft on 5/11/2017.
 */
public class SavedAddressPojo {

    public String AddressId = "";
    public String user_id = "";
    public String locality = "";
    public String flat_number = "";
    public String landmark = "";
    public String tag_address = "";
    public String name = "";
    public String mobile_no = "";
    public String pincode = "";
    public String state_id = "";
    public String city_id = "";


    public SavedAddressPojo(String addressId, String user_id, String locality, String flat_number, String landmark, String tag_address, String name, String mobile_no, String pincode, String state_id, String city_id) {
        AddressId = addressId;
        this.user_id = user_id;
        this.locality = locality;
        this.flat_number = flat_number;
        this.landmark = landmark;
        this.tag_address = tag_address;
        this.name = name;
        this.mobile_no = mobile_no;
        this.pincode = pincode;
        this.state_id = state_id;
        this.city_id = city_id;


    }

    public static void getAddressList(final Context context, String UserID, final String FromWhere, final String AddressID, final String ClassName) {

        final ArrayList<SavedAddressPojo> list = new ArrayList<>();
        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, context, "Please wait....", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getBoolean("success") == true) {
                        JSONObject dataObjectt = mainObj.getJSONObject("data");
                        // for (int i = 0; i < dataObjectt.length(); i++) {
                        JSONArray userAddressArray = dataObjectt.getJSONArray("UserAddress");
                        for (int i = 0; i < userAddressArray.length(); i++) {
                            JSONObject addressObject = userAddressArray.optJSONObject(i);
                            list.add(new SavedAddressPojo(
                                    addressObject.getString("id"),
                                    addressObject.getString("user_id"),
                                    addressObject.getString("locality"),
                                    addressObject.getString("flat_number"),
                                    addressObject.getString("landmark"),
                                    addressObject.getString("tag_address"),
                                    addressObject.getString("name"),
                                    addressObject.getString("mobile_no"),
                                    addressObject.getString("pincode"),
                                    addressObject.getString("state_id"),
                                    addressObject.getString("city_id")
                            ));
                        }
                    }
                    if (list != null) {
                        if (FromWhere.equals("Activity") && ClassName.equals("ManageAddressActivity")) {
                            ((ManageAddressActivity) context).setAddressList(list, AddressID);
                        } else if (FromWhere.equals("Activity") && ClassName.equals("PickAddressActivity")) {
                            ((PickAddressActivity) context).setAddressList(list, AddressID);
                        } else if (FromWhere.equals("Activity") && ClassName.equals("SearchResturentActivity")) {
                            ((SearchRestaurentActivity) context).setAddressList(list);
                        }
                    }
                    // }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        //  serviceCaller.addNameValuePair("city", "jaipur");
        serviceCaller.addNameValuePair("user_id", UserID);
        String url = AppGlobal.GetUserAddressList;
        Log.d("url>>", url);
        serviceCaller.execute(url);
    }
}
