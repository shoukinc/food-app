package com.b2infosoft_jaipur.chakhle.pojo;

import android.content.Context;
import android.util.Log;

import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Microsoft on 5/30/2017.
 */

public class FavRestaurantsPojo {

    public String id = "";
    public String name = "";
    public String slug = "";
    public String moblie = "";
    public String email = "";
    public String logo_image = "";
    public String description = "";
    public String address = "";
    public String minimum_purchase = "";
    public String delivery_time = "";
    public String chakhle_assured = "";
    public String pure_veg_dishes = "";
    public String status = "";
    public String distance = "";
    public String TotalVote = "";
    public String TotalRating = "";
    public String cuisine = "";
    public String TotalReview = "";
    public String banner_image = "";
    public String open_time = "";
    public String close_time = "";


    public FavRestaurantsPojo(String id, String name, String slug, String moblie, String email, String logo_image, String description, String address, String minimum_purchase, String delivery_time, String chakhle_assured, String pure_veg_dishes, String status, String distance, String cuisine, String rating, String TotalReview, String banner, String openTime, String closeTime) {
        this.id = id;
        this.name = name;
        this.slug = slug;
        this.moblie = moblie;
        this.email = email;
        this.logo_image = logo_image;
        this.description = description;
        this.address = address;
        this.minimum_purchase = minimum_purchase;
        this.delivery_time = delivery_time;
        this.chakhle_assured = chakhle_assured;
        this.pure_veg_dishes = pure_veg_dishes;
        this.status = status;
        this.distance = distance;
        this.cuisine = cuisine;
        this.TotalRating = rating;
        this.TotalReview = TotalReview;
        this.banner_image = banner;
        this.open_time = openTime;
        this.close_time = closeTime;
    }

    public static void getFavRestaurants(final Context mContext, String UserID) {

        final ArrayList<FavRestaurantsPojo> list = new ArrayList<>();

        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait....", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getBoolean("success") == true) {


                        JSONArray mainArrayJson = mainObj.getJSONArray("data");
                        for (int i = 0; i < mainArrayJson.length(); i++) {
                            JSONObject dataObject = mainArrayJson.getJSONObject(i);
                            JSONObject resObject = dataObject.getJSONObject("Restaurant");
                            list.add(new FavRestaurantsPojo(
                                    resObject.getString("id"),
                                    resObject.getString("name"),
                                    resObject.getString("slug"),
                                    resObject.getString("moblie"),
                                    resObject.getString("email"),
                                    resObject.getString("logo_image"),
                                    resObject.getString("description"),
                                    resObject.getString("address"),
                                    resObject.getString("minimum_purchase"),
                                    resObject.getString("delivery_time"),
                                    resObject.getString("chakhle_assured"),
                                    resObject.getString("pure_veg_dishes"),
                                    resObject.getString("status"),
                                    resObject.getString("distance"),
                                    resObject.getString("cuisine"),
                                    resObject.getString("rating"),
                                    resObject.getString("totalreview"),
                                    resObject.getString("banner_image"),
                                    resObject.getString("open_time"),
                                    resObject.getString("close_time")

                            ));
                        }

                        if (list != null) {
                         //   ((FavRestorentActivity)mContext).setFavRestorantList(list);
                        }

                    } else {
                        UtilityMethod.showAlertBox(mContext, "Opps! No Favrouite Restaurant Found");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                  //  ((FavRestorentActivity)mContext).setFavRestorantList(list);

                }

            }
        };

        //  serviceCaller.addNameValuePair("location", "67/167, Pratap Nagar, Sanganer, Pratap Nagar, Jaipur, Rajasthan 302029, India");
        serviceCaller.addNameValuePair("user_id", UserID);
        String url = AppGlobal.GetFavoriteRest;
        Log.d("url>>", url);
        serviceCaller.execute(url);

    }
}

