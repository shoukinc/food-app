package com.b2infosoft_jaipur.chakhle.pojo;

/**
 * Created by Microsoft on 04-Oct-17.
 */

public class ProductListingDialogAdapterRowPojo {

    public String VariantPrice = "";
    public String Variant = "";
    public String Quantity = "";
    public String TotalVariantPrice = "";

    public ProductListingDialogAdapterRowPojo(String VariantPrice, String Variant, String Quantity, String TotalVariantPrice) {
        this.VariantPrice = VariantPrice;
        this.Variant = Variant;
        this.Quantity = Quantity;
        this.TotalVariantPrice = TotalVariantPrice;

    }
}
