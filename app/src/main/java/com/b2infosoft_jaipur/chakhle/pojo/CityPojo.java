package com.b2infosoft_jaipur.chakhle.pojo;

import android.content.Context;
import android.util.Log;

import com.b2infosoft_jaipur.chakhle.activities.ManageAddressActivity;
import com.b2infosoft_jaipur.chakhle.activities.PartnerWithUsActivity;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Microsoft on 5/31/2017.
 */

public class CityPojo {
    public String CityID = "";
    public String CityName = "";

    public CityPojo(String stateID, String stateName) {
        CityID = stateID;
        CityName = stateName;
    }

    public static void getCityList(final Context mContext, String StateID, final String From) {

        final ArrayList<CityPojo> list = new ArrayList<>();
        WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", false) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject mainObject = new JSONObject(response);
                    JSONArray stateArray = mainObject.getJSONArray("City");
                    for (int i = 0; i < stateArray.length(); i++) {
                        JSONObject dataObject = stateArray.getJSONObject(i);
                        list.add(new CityPojo(dataObject.getString("id"), dataObject.getString("name")));
                    }
                    if (!list.isEmpty() && From.equals("addNewAddress")) {
                        ((ManageAddressActivity) mContext).setCityAdapter(list);
                    } else if (!list.isEmpty() && From.equals("partnerWithUs")) {
                        ((PartnerWithUsActivity) mContext).setCityAdapter(list);
                    }

                } catch (JSONException e) {
                    if (From.equals("addNewAddress")) {
                        ((ManageAddressActivity) mContext).setCityAdapter(list);
                        e.printStackTrace();
                    } else if (From.equals("partnerWithUs")) {
                        ((PartnerWithUsActivity) mContext).setCityAdapter(list);
                        e.printStackTrace();
                    }

                }

            }
        };

        webServiceCaller.addNameValuePair("state_id", StateID);
        String url = AppGlobal.GetCities;
        Log.d("url>>", url);
        webServiceCaller.execute(url);

    }

}
