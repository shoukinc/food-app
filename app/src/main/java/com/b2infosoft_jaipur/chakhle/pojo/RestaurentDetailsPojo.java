package com.b2infosoft_jaipur.chakhle.pojo;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.b2infosoft_jaipur.chakhle.activities.RestaurentDetailsActivity;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Microsoft on 5/19/2017.
 */

public class RestaurentDetailsPojo {

    public String id = "";
    public String name = "";
    public String slug = "";
    public String moblie = "";
    public String email = "";
    public String logo_image = "";
    public String description = "";
    public String address = "";
    public String minimum_purchase = "";
    public String delivery_time = "";
    public String chakhle_assured = "";
    public String pure_veg_dishes = "";
    public String status = "";
    public String distance = "";
    public String TotalVote = "";
    public String TotalRating = "";
    public String cuisine = "";
    public String TotalReview = "";
    public String banner_image = "";
    public String open_time = "";
    public String close_time = "";
    public String half = "";
    public String medium = "";
    public String full = "";
    public String large = "";
    public String half_label = "";
    public String full_label = "";
    public String medium_label = "";
    public String large_label = "";
    public String in_time = "";
    public String out_time = "";
    public String extra_charge = "";
    public String container_charge = "";
    public String Restaddress = "";

    public String let_nite_charge = "";
    public String tax_percentage = "";
    public String RestInTime = "";
    public String Restoff_time_1 = "";

    public int halfQty;
    public int mediumQty;
    public int fullQty;
    public int largeQty;


    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String discount = "";

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String discountType = "";

    public RestaurentDetailsPojo(String id, String name, String slug, String moblie, String email, String logo_image, String description, String address, String minimum_purchase, String delivery_time, String chakhle_assured, String pure_veg_dishes, String status, String distance, String cuisine, String rating, String TotalReview, String banner, String openTime, String closeTime) {
        this.id = id;
        this.name = name;
        this.slug = slug;
        this.moblie = moblie;
        this.email = email;
        this.logo_image = logo_image;
        this.description = description;
        this.address = address;
        this.minimum_purchase = minimum_purchase;
        this.delivery_time = delivery_time;
        this.chakhle_assured = chakhle_assured;
        this.pure_veg_dishes = pure_veg_dishes;
        this.status = status;
        this.distance = distance;
        this.cuisine = cuisine;
        this.TotalRating = rating;
        this.TotalReview = TotalReview;
        this.banner_image = banner;
        this.open_time = openTime;
        this.close_time = closeTime;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int count;

    public int getAfterDiscountValue() {
        return afterDiscountValue;
    }

    public void setAfterDiscountValue(int afterDiscountValue) {
        this.afterDiscountValue = afterDiscountValue;
    }

    public int afterDiscountValue;

    public int getProductTotalValue() {
        return productTotalValue;
    }

    public void setProductTotalValue(int productTotalValue) {
        this.productTotalValue = productTotalValue;
    }

    public int productTotalValue;
    public String restaurant_id = "";
    public String is_favorite = "";
    public String category_id = "";
    public String Category_name = "";
    public String Category_slug = "";
    public String Product_id = "";
    public String Product_name = "";
    public String Product_description = "";
    public String Product_image1 = "";
    public String Product_vegproduct = "";
    public String Product_slug = "";
    public String Product_status = "";
    public String Product_price = "";
    public String Product_category_id = "";

    public RestaurentDetailsPojo(String restName, String isFav, String RestID, String product_id, String product_name, String product_description, String product_image1, String product_vegproduct, String product_slug, String product_status, String product_price, String product_category_id, int count, int afterDiscountValue, int productTotalvalue, String dscount, String half, String medium, String full, String large, String half_label, String medium_label, String full_label, String large_label, String in_time, String out_time, String minimum_purchase, String extra_charge, String container_charge,
                                 int halfQty, int mediumQty, int fullQty, int largeQty,String discountType,String Restaddress,String let_nite_charge,
                                 String tax_percentage,String restoff_time,String restoff_time_1) {
        name = restName;
        is_favorite = isFav;
        restaurant_id = RestID;
        Product_id = product_id;
        Product_name = product_name;
        Product_description = product_description;
        Product_image1 = product_image1;
        Product_vegproduct = product_vegproduct;
        Product_slug = product_slug;
        Product_status = product_status;
        Product_price = product_price;
        Product_category_id = product_category_id;
        this.count = count;
        this.afterDiscountValue = afterDiscountValue;
        this.productTotalValue = productTotalvalue;
        this.discount = dscount;
        this.half = half;
        this.medium = medium;
        this.full = full;
        this.large = large;
        this.half_label = half_label;
        this.medium_label = medium_label;
        this.full_label = full_label;
        this.large_label = large_label;
        this.in_time = in_time;
        this.out_time = out_time;
        this.minimum_purchase = minimum_purchase;
        this.extra_charge = extra_charge;
        this.container_charge = container_charge;
        this.halfQty = halfQty;
        this.mediumQty = mediumQty;
        this.fullQty = fullQty;
        this.largeQty = largeQty;
        this.discountType = discountType;
        this.Restaddress = Restaddress;
        this.let_nite_charge = let_nite_charge;
        this.tax_percentage = tax_percentage;
        this.RestInTime = restoff_time;
        this.Restoff_time_1 = restoff_time_1;


    }

    public RestaurentDetailsPojo(String product_id, String product_name, String product_description, String product_image1, String product_vegproduct, String product_slug, String product_status, String product_price, String product_category_id, int count) {
        Product_id = product_id;
        Product_name = product_name;
        Product_description = product_description;
        Product_image1 = product_image1;
        Product_vegproduct = product_vegproduct;
        Product_slug = product_slug;
        Product_status = product_status;
        Product_price = product_price;
        Product_category_id = product_category_id;
        this.count = count;
    }


    public RestaurentDetailsPojo(String restaurant_id, String category_id, String name, String slug) {
        this.restaurant_id = restaurant_id;
        this.category_id = category_id;
        Category_name = name;
        Category_slug = slug;


    }


    public static void getRestaurentDetails(final View view, final Context mContext, String RestaurentID, String userID) {

        final ArrayList<RestaurentDetailsPojo> list = new ArrayList<>();
        final ArrayList<RestaurentDetailsPojo> restaurentProductList = new ArrayList<>();
        final ArrayList<RestaurentDetailsPojo> RestaurantCategoryList = new ArrayList<>();
        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait....", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                String isFav = "";

                try {
                    String restID = "", CateID = "", restroName = "", minimum = "", extra = "",address="",let_night_charge="",tax_percentage = "",
                    restOff ="",restOff1 = "";
                    int intDiscount;
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getBoolean("success") == true) {
                        JSONObject dataJson = mainObj.getJSONObject("data");
                        JSONObject resObj = dataJson.getJSONObject("Restaurant");
                        isFav = resObj.getString("is_favorite");
                        restroName = resObj.getString("name");
                        minimum = resObj.getString("minimum_purchase");
                        extra = resObj.getString("extra_charge");
                        address = resObj.getString("address");
                        let_night_charge = resObj.getString("let_night_charge");
                        tax_percentage = resObj.getString("tax_percentage");
                        restOff = resObj.getString("off_time");
                        restOff1 = resObj.getString("off_time_1");
                       /* Discount = resObj.getString("discount");
                        String Discount2[] = Discount.split(",");
                        String discount1 = Discount2[1];
                        String actualDiscount = discount1.replace("%", " ").trim();
                        //int intDiscount = Integer.parseInt(actualDiscount);
                        mainDiscount = actualDiscount.substring(0, actualDiscount.indexOf("."));
                        intDiscount = Integer.parseInt(mainDiscount);
                        Log.d("Discount>>", "" + mainDiscount);*/

                        JSONArray RestaurantCategoryArray = dataJson.getJSONArray("RestaurantCategory");
                        RestaurantCategoryList.add(new RestaurentDetailsPojo("0", "0", "RECOMMENDED", "RECOMMENDED"));
                        for (int j = 0; j < RestaurantCategoryArray.length(); j++) {
                            JSONObject RestaurantCategoryObject = RestaurantCategoryArray.getJSONObject(j);
                            restID = RestaurantCategoryObject.getString("restaurant_id");
                            CateID = RestaurantCategoryObject.getString("category_id");
                            JSONObject jObject = RestaurantCategoryObject.getJSONObject("Category");
                            RestaurantCategoryList.add(new RestaurentDetailsPojo(
                                    restID, CateID, jObject.getString("name"), jObject.getString("slug")
                            ));
                        }
                        JSONArray mainProductArray = mainObj.getJSONArray("products");
                        // restaurentProductList.add(new RestaurentDetailsPojo("","","","","","","","","","",0));
                        for (int j = 0; j < mainProductArray.length(); j++) {
                            JSONObject productObject = mainProductArray.getJSONObject(j);
                            JSONObject dataProductObject = productObject.getJSONObject("Product");
                            restaurentProductList.add(new RestaurentDetailsPojo(
                                    restroName,
                                    isFav,
                                    restID,
                                    dataProductObject.getString("id"),
                                    dataProductObject.getString("name"),
                                    dataProductObject.getString("description"),
                                    dataProductObject.getString("image1"),
                                    dataProductObject.getString("vegproduct"),
                                    dataProductObject.getString("slug"),
                                    dataProductObject.getString("status"),
                                    dataProductObject.getString("price"),
                                    dataProductObject.getString("category_id"),
                                    0, 0, 0
                                    ,
                                    "",
                                    dataProductObject.getString("half"),
                                    dataProductObject.getString("medium"),
                                    dataProductObject.getString("full"),
                                    dataProductObject.getString("large"),
                                    dataProductObject.getString("half_label"),
                                    dataProductObject.getString("medium_label"),
                                    dataProductObject.getString("full_label"),
                                    dataProductObject.getString("large_label"),
                                    dataProductObject.getString("off_time"),
                                    dataProductObject.getString("off_time_1"),
                                    minimum, extra,
                                    dataProductObject.getString("container_charge"),
                                    0, 0, 0, 0,"",address,let_night_charge,tax_percentage,restOff,restOff1

                            ));
                        }
                        if (!RestaurantCategoryList.isEmpty() && !restaurentProductList.isEmpty()) {
                            SessionManager sessionManager;
                            ArrayList<CartFillPojo> mainSessonCartList;
                            sessionManager = new SessionManager(mContext);
                            mainSessonCartList = sessionManager.getCartList(mContext);
                            if (mainSessonCartList != null) {
                                int count = 0;
                                for (int i = 0; i < mainSessonCartList.size(); i++) {
                                    for (int j = 0; j < restaurentProductList.size(); j++) {
                                        if (mainSessonCartList.get(i).restaurent_id.equals(restaurentProductList.get(j).restaurant_id)) {
                                            if (mainSessonCartList.get(i).Product_id.equals(restaurentProductList.get(j).Product_id)) {
                                                //   count = mainSessonCartList.get(i).quantity;
                                                //   restaurentProductList.get(j).setCount(count);
                                                restaurentProductList.get(j).halfQty = mainSessonCartList.get(i).HalfQty;
                                                restaurentProductList.get(j).fullQty = mainSessonCartList.get(i).FullQty;
                                                restaurentProductList.get(j).largeQty = mainSessonCartList.get(i).LargeQty;
                                                restaurentProductList.get(j).mediumQty = mainSessonCartList.get(i).MediumQty;
                                                AppGlobal.ProductIDList.add(mainSessonCartList.get(i).restaurent_id);
                                            }
                                        }
                                    }
                                }
                            }
                            for (int position = 0; position < restaurentProductList.size(); position++) {
                                for (int i = 0; i < AppGlobal.DiscountList.size(); i++) {
                                    if (restaurentProductList.get(position).restaurant_id.equals(AppGlobal.DiscountList.get(i).DiscountOffer_restaurant_id)) {
                                        if (!AppGlobal.DiscountList.get(i).DiscountOffer_type.equals("") && !AppGlobal.DiscountList.get(i).DiscountOffer_value.equals("")) {
                                            Log.d("Discount>>>", AppGlobal.DiscountList.get(i).DiscountOffer_value);
                                            restaurentProductList.get(position).setDiscount(AppGlobal.DiscountList.get(i).DiscountOffer_value);
                                            restaurentProductList.get(position).setDiscountType(AppGlobal.DiscountList.get(i).DiscountOffer_type);

                                        }
                                    }
                                }
                            }
                            Log.d("RestaurantCategoryList>>>", "" + RestaurantCategoryList.size());
                            Log.d("RestaurantCategoryList>>>", "" + restaurentProductList.size());
                           /* RestaurentDetailsFragment restaurentDetailsFragment = new RestaurentDetailsFragment();
                            restaurentDetailsFragment.setProductList(view,mContext,RestaurantCategoryList, restaurentProductList);*/
                            ((RestaurentDetailsActivity) mContext).setProductList(RestaurantCategoryList, restaurentProductList);
                        }
                    }
                } catch (JSONException e) {
                    if (!RestaurantCategoryList.isEmpty() && !restaurentProductList.isEmpty()) {
                        SessionManager sessionManager;
                        ArrayList<CartFillPojo> mainSessonCartList;
                        sessionManager = new SessionManager(mContext);
                        mainSessonCartList = sessionManager.getCartList(mContext);
                        if (mainSessonCartList == null) {
                            int count = 0;
                            for (int i = 0; i < mainSessonCartList.size(); i++) {
                                for (int j = 0; j < restaurentProductList.size(); j++) {
                                    if (mainSessonCartList.get(i).restaurent_id.equals(restaurentProductList.get(j).restaurant_id)) {
                                        if (mainSessonCartList.get(i).Product_id.equals(restaurentProductList.get(j).Product_id)) {
                                            restaurentProductList.get(j).halfQty = mainSessonCartList.get(i).HalfQty;
                                            restaurentProductList.get(j).fullQty = mainSessonCartList.get(i).FullQty;
                                            restaurentProductList.get(j).largeQty = mainSessonCartList.get(i).LargeQty;
                                            restaurentProductList.get(j).mediumQty = mainSessonCartList.get(i).MediumQty;
                                            AppGlobal.ProductIDList.add(mainSessonCartList.get(i).restaurent_id);

                                        }
                                    }
                                }
                            }
                        }
                        Log.d("RestaurantCategoryList>>>", "" + RestaurantCategoryList.size());
                        Log.d("RestaurantCategoryList>>>", "" + restaurentProductList.size());
                       /* RestaurentDetailsFragment restaurentDetailsFragment = new RestaurentDetailsFragment();
                        restaurentDetailsFragment.setProductList(view,mContext,RestaurantCategoryList, restaurentProductList);*/
                        ((RestaurentDetailsActivity) mContext).setProductList(RestaurantCategoryList, restaurentProductList);
                    }
                    //((AppCompatActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new RestaurentFragment(list, reviewList, discountList)).addToBackStack(null).commit();
                    e.printStackTrace();
                }
            }
        };
        //  serviceCaller.addNameValuePair("location", "67/167, Pratap Nagar, Sanganer, Pratap Nagar, Jaipur, Rajasthan 302029, India");
        Log.d("AAAAAA", "gfg b "+ RestaurentID+ " userIDvv "+ userID);
        serviceCaller.addNameValuePair("restaurants_id", RestaurentID);
        serviceCaller.addNameValuePair("user_id", userID);
        String url = AppGlobal.RestaurentDetails;
        Log.d("url>>", url);
        if (RestaurentID.equals("")){

        }else {
            serviceCaller.execute(url);
        }


    }

}
