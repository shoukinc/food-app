package com.b2infosoft_jaipur.chakhle.pojo;

/**
 * Created by Microsoft on 6/3/2017.
 */

public class ReviewOrderPojo {

    public String orderType = "";
    public String ProductName = "";

    public ReviewOrderPojo(String orderType, String productName, String productPrice) {
        this.orderType = orderType;
        ProductName = productName;
        ProductPrice = productPrice;
    }

    public String ProductPrice = "";

}
