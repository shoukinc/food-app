package com.b2infosoft_jaipur.chakhle.services;

/**
 * Created by Pankaj Jangid on 17-Oct-16.
 */

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.activities.MyOrdersActivity;
import com.b2infosoft_jaipur.chakhle.activities.NotificationActivity;
import com.b2infosoft_jaipur.chakhle.activities.OrderStatusActivity;
import com.b2infosoft_jaipur.chakhle.activities.RestaurantListActivity;
import com.b2infosoft_jaipur.chakhle.activities.UserProfileActivity;
import com.b2infosoft_jaipur.chakhle.pojo.NotificationPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyFirebaseMsgService extends FirebaseMessagingService {
    private static final String TAG = "MyAndroidFCMService";
    Map<String, String> data;
    //  ArrayList<NotificationPojo> notificationPojos = new ArrayList<>();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {

            Log.d("remoteMessage>>", remoteMessage.toString());
            if (remoteMessage.getNotification() != null) {
                Log.d(TAG, "Message data payload: " + remoteMessage.getNotification());
                data = new HashMap<String, String>();
                data.put("title", remoteMessage.getNotification().getTitle());
                //data.put("is_background",remoteMessage.getData().get("is_background"));
                data.put("message", remoteMessage.getNotification().getBody());
                data.put("image", remoteMessage.getData().get("image"));
                // data.put("payload",remoteMessage.getData().get("payload"));
                // data.put("timestamp",remoteMessage.getData().get("timestamp"));

                // createNotification(remoteMessage.getData().get("message"), remoteMessage.getData().get("title"));
                createNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle(), "");
            }
            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0) {
                Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

                try {
                    JSONObject json = new JSONObject(remoteMessage.getData().toString());
                    Log.d("JSONmain>>>", "" + json);
                    JSONObject jsonObject1 = json.getJSONObject("data");
                    Log.d("JsonObject1>>>", "" + jsonObject1);

                    //notificationPojos.add(new NotificationPojo(jsonObject1.getString("title"), jsonObject1.getString("message"), jsonObject1.getString("time")));
                    /*String image = jsonObject1.getString("image");
                    String notification_id = jsonObject1.getString("notification_id");
                    String title = jsonObject1.getString("title");
                    String message = jsonObject1.getString("message");
                    String type = jsonObject1.getString("type");
                    String time = jsonObject1.getString("time");*/

                    if (jsonObject1.getString("type").equals("order")) {
                        AppGlobal.OrderID = jsonObject1.getString("order_id");
                        AppGlobal.OrderStatus = "OrderStatus";
                    }
                    createNotification(jsonObject1.getString("message"), jsonObject1.getString("title"), jsonObject1.getString("type"));

                    //  handleDataMessage(json);
                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }

            // createNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
            // Check if message contains a notification payload.
         /*   if (remoteMessage.getNotification() != null) {
                Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            }*/


            // Log.d(TAG, "From: " + remoteMessage.getFrom());
            //  Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
            //  Log.i(TAG, "PAYLOAD MESSAGE: " + customData);
            //create notification
            //

            //   createNotification(remoteMessage.getData().get("message"), remoteMessage.getData().get("title"));
            /*if (remoteMessage.getNotification().getBody() != null) {
                Log.i(TAG, "RECEIVED MESSAGE: " + remoteMessage.getNotification().getBody());
            } else {
                Log.i(TAG, "RECEIVED MESSAGE: " + remoteMessage.getData().get("message"));
            }*/
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressLint("LongLogTag")
    private void createNotification(String messageBody, String title, String type) {
        PendingIntent resultIntent = null;
        Intent intent;
        try {
            if (messageBody == null || messageBody == "") {
                messageBody = "empty or null message";
            }
            if (title == null || title == "") {
                title = "empty or null title";
            }

            if (type.equals("login")) {
                intent = new Intent(this, UserProfileActivity.class);
                intent.putExtra("messageBody", messageBody);
                intent.putExtra("messageTitle", title);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                resultIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            }
            if (type.equals("order")) {
                intent = new Intent(this, OrderStatusActivity.class);
                intent.putExtra("messageBody", messageBody);
                intent.putExtra("messageTitle", title);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                resultIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            }
            if (type.equals("offer")) {

                intent = new Intent(this, RestaurantListActivity.class);
                intent.putExtra("messageBody", messageBody);
                intent.putExtra("messageTitle", title);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                resultIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            }
            if (type.equals("All Notification")) {

                intent = new Intent(this, NotificationActivity.class);
                intent.putExtra("messageBody", messageBody);
                intent.putExtra("messageTitle", title);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                resultIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            }
            // AppGlobal.notificationList.add(new NotificationPojo(title, messageBody, ""));
            // AppGlobal.NotificationCount++;
            SessionManager sessionManager = new SessionManager(getApplicationContext());
            String data_list = sessionManager.getKey_NotificationList_();

            if (data_list != null) {
                if (data_list.equals("") || data_list.equals("0")) {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("title", title);
                    jsonObject.put("messageBody", messageBody);
                    jsonObject.put("time", "");

                    if (type.equals("order")) {
                        jsonObject.put("order_id", AppGlobal.OrderID);
                    } else {
                        jsonObject.put("order_id", "");
                    }

                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(jsonObject);

                    JSONObject jsonObject_ = new JSONObject();
                    jsonObject_.put("results", jsonArray);
                    Log.d("jsonObject_", jsonObject_ + " ff");

                    sessionManager.setKey_NotificationList_(jsonObject_ + "");

                } else {

                    JSONObject jsonObject = new JSONObject(data_list);
                    JSONArray jsonArray = jsonObject.getJSONArray("results");
                    JSONObject jsonObject12 = new JSONObject();
                    jsonObject12.put("title", title);
                    jsonObject12.put("messageBody", messageBody);
                    jsonObject12.put("time", "");
                    if (type.equals("order")) {
                        jsonObject12.put("order_id", AppGlobal.OrderID);
                    } else {
                        jsonObject12.put("order_id", "");
                    }
                    jsonArray.put(jsonObject12);

                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("results", jsonArray);

                    sessionManager.setKey_NotificationList_(jsonObject1 + "");
                    Log.d("jsonObject1", jsonObject1 + " ff");
                }

            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("title", title);
                jsonObject.put("messageBody", messageBody);
                jsonObject.put("time", "");
                if (type.equals("order")) {
                    jsonObject.put("order_id", AppGlobal.OrderID);
                } else {
                    jsonObject.put("order_id", "");
                }
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(jsonObject);

                JSONObject jsonObject_ = new JSONObject();
                jsonObject_.put("results", jsonArray);
                Log.d("jsonObject_", jsonObject_ + " ff");

                sessionManager.setKey_NotificationList_(jsonObject_ + "");
            }

            String count = sessionManager.getKey_NotificationList_count();

            if (count != null) {
                if (!count.equals("0")) {
                    int count_int = Integer.parseInt(count);
                    count_int = count_int + 1;
                    count = count_int + "";
                } else {
                    count = "1";
                }
            } else {
                count = "1";
            }

            sessionManager.setKey_NotificationList_count(count);


            Log.d("isAppIsInBackground", "TRUE");
            Log.d("isAppIsInBackground Main", "" + isAppIsInBackground(this));
            /*if (isAppIsInBackground(this)) {
                intent = new Intent(this, SplashActivity.class);
                intent.putExtra("status", "ok");
                intent.putExtra("messageBody", messageBody);
                Log.d("isAppIsInBackground", "TRUE");
            } else {
                intent = new Intent(this, NotificationActivity.class);
                intent.putExtra("status", "ok");
                intent.putExtra("messageBody", messageBody);
                Log.d("isAppIsInBackground", "FALSE");
            }*/
            //notification sound
            final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + getApplicationContext().getPackageName() + "/raw/notification");


            Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


            NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this);
            mNotificationBuilder.setSmallIcon(getNotificationIcon(mNotificationBuilder));
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mNotificationBuilder.setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.app_icon));
            } else {
                mNotificationBuilder.setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.app_icon));
            }
            mNotificationBuilder.setContentTitle(title);
            mNotificationBuilder.setContentText(messageBody);
            mNotificationBuilder.setAutoCancel(true);
            mNotificationBuilder.setDefaults(Notification.DEFAULT_SOUND);
            mNotificationBuilder.setContentIntent(resultIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(Config.NOTIFICATION_ID, mNotificationBuilder.build());
            //notification sound
            //  NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            //  notificationUtils.playNotificationSound();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private int getNotificationIcon(NotificationCompat.Builder notificationBuilder) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setColor(getResources().getColor(R.color.colorAccent));
            return R.drawable.img_app;

        }
        return R.drawable.app_icon_round;
    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }
        return isInBackground;
    }

}