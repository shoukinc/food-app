package com.b2infosoft_jaipur.chakhle.services;

/**
 * Created by Pankaj Jangid on 17-Oct-16.
 */

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyAndroidFCMIIDService";

    @SuppressLint("LongLogTag")
    @Override
    public void onTokenRefresh() {
        //Get hold of the registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        Log.d(TAG + " refreshedToken", "" + refreshedToken);
        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

    }

    @SuppressLint("LongLogTag")
    private void storeRegIdInPref(String refreshedToken) {
        Log.d(TAG + " storeRegIdInPref", "" + refreshedToken);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", refreshedToken);
        editor.apply();
    }

    private void sendRegistrationToServer(String token) {
        //Implement this method if you want to store the token on your server

        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);


    }
}