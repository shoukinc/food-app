package com.b2infosoft_jaipur.chakhle.webservice;


import android.content.Context;

import com.b2infosoft_jaipur.chakhle.pojo.AdvertiesPojo;
import com.b2infosoft_jaipur.chakhle.pojo.CartFillPojo;
import com.b2infosoft_jaipur.chakhle.pojo.MyOrderPojo;
import com.b2infosoft_jaipur.chakhle.pojo.NotificationPojo;
import com.b2infosoft_jaipur.chakhle.pojo.ProductListingDialogAdapterRowPojo;
import com.b2infosoft_jaipur.chakhle.pojo.RestaurentDetailsPojo;
import com.b2infosoft_jaipur.chakhle.pojo.ResturentListPojo;

import java.util.ArrayList;


/**
 * Created by Amar on 12-May-16.
 */
public class AppGlobal {
    public static CustomDialog customDialog = null;
    public static String uname = "";
    public static String LocalDbName = "Cataloging";
    public static int LocalDbVersion = 1;
    public static String ApiErrorMsg = "Ops! Something Went Wrong, Please Try Again Later";
    public static String ApiLowAmountMsg = "Enter Amount should be between @Min and @Max";
    public static String ApiCallWaitMsg = "Please Wait...";
    public static String ApiCallFailMsg = "Either there was network issue or some error occurred in submitting request, please try again..";
    public static String ApiUrl = "http://chakhle.in/api/";
    // public static String ApiUrl = "http://b2infosoft-pc/swiggy/api/";
    public static String SearchResturents = ApiUrl + "searches/search_restaurants";
    public static String AdvtList = ApiUrl + "searches/list_advertisement";
    public static String RestaurentDetails = ApiUrl + "Restaurants/restaurants_detail/";

    public static String CheckExistingUser = ApiUrl + "users/exist_user";
    public static String UserLogin = ApiUrl + "users/login";
    public static String SignUpUser = ApiUrl + "users/register";
    public static String OtpVerification = ApiUrl + "users/otp_check";
    public static String OtpResend = ApiUrl + "users/send_otp";
    public static String AddtoFavoriteRest = ApiUrl + "searches/add_to_favorites";
    public static String RemoveFavoriteRest = ApiUrl + "searches/remove_from_favorites";
    public static String GetFavoriteRest = ApiUrl + "searches/favorites_restaurants";
    public static String AddUserAddress = ApiUrl + "users/add_address";
    public static String GetUserAddressList = ApiUrl + "users/user_address_list";
    public static String GetStates = ApiUrl + "States/get_states";
    public static String DiscountOffer = ApiUrl + "discount_offers/index";
    public static String GetCities = ApiUrl + "Cities/get_cities";
    public static String UpdateUserAddress = ApiUrl + "users/edit_address";
    public static String ChangePassword = ApiUrl + "users/change_password";
    public static String DeleteAddress = ApiUrl + "users/remove_address_user";
    public static String EditProfile = ApiUrl + "users/edit_profile";
    public static String AddOrder = ApiUrl + "Orders/add_to_order";
    public static String GetOrdersList = ApiUrl + "Orders/orderlist";
    public static String GetBirthday = ApiUrl + "users/getbirthDay/";
    public static String SaveFeedback = ApiUrl + "feedbacks/saveFeedback/";
    public static String SaveReview = ApiUrl + "reviews/add_review";
    public static String SendOtp = ApiUrl + "users/forgot_password";
    public static String ResetPassword = ApiUrl + "users/forgot_check_otp";
    //b2infosoft.in/chakhle/api/reviews/add_review
    public static String CategaryImage = ApiUrl + "categories/category_list";
    public static String PartnerWithUs = ApiUrl + "users/partner_with_us/";

    public static Context mContext = null;
    public static ArrayList<Integer> resturentPojoList = new ArrayList<Integer>();
    public static String MobileNo = "";
    public static String UserID = "";
    public static ArrayList<AdvertiesPojo> AdvertiseList = new ArrayList<>();
    public static String LocationAddress = "";
    public static String Addresses = "";
    public static String Chakhle_assured = "0";
    public static String Offers = "0";
    public static String Pure_veg_dishes = "0";
    public static String Rating = "0";
    public static String City = "";

    public static ArrayList<RestaurentDetailsPojo> RestaurentProductList = new ArrayList<>();
    public static ArrayList<ProductListingDialogAdapterRowPojo> productListingDialogAdapterRowPojos = new ArrayList<>();
    public static ArrayList<ProductListingDialogAdapterRowPojo> MainproductListingDialogAdapterRowPojos = new ArrayList<>();

    public static String CategoryID = "";
    public static String RestaurentCategoryID = "";

    public static String RestaurentName = "";
    public static String RestarentImage = "";
    public static String RestarentAddress = "";
    public static String RestDiscount = "";
    public static String RestRating = "";
    public static String AddedToFav = null;
    public static int CartValue;
    public static String RestBanner = "";
    public static String FavFrom = "";
    public static String GetAddresses = "";
    public static String GetLocationAddress = "";
    public static String GetCity = "";
    public static ArrayList<RestaurentDetailsPojo> MainProductList = new ArrayList<>();
    public static ArrayList<String> ProductIDList = new ArrayList<>();
    public static ArrayList<CartFillPojo> mainCartList = new ArrayList<>();
    public static ArrayList<CartFillPojo> GlobalCartList = new ArrayList<>();

    public static String IamFrom = "";
    public static ArrayList<RestaurentDetailsPojo> ItemDataList = new ArrayList<>();
    public static ArrayList<MyOrderPojo> OrderproductList = new ArrayList<>();
    public static String TotalCost = "";
    public static String OrderLocatioID = "";
    public static String OrderDeliverAddress = "";
    public static String TaxAmt = "";
    public static String OrderMobileNumber = "";
    public static String OrderID = "";
    public static String Restaurant_id = "";
    public static String OrderStatus = "";
    public static ArrayList<MyOrderPojo> UserOrderList = new ArrayList<>();
    public static String OrderBooked = "";
    public static int searchCartValue = 0;
    public static ArrayList<NotificationPojo> notificationList = new ArrayList<>();
    public static String cateID = "";
    public static ArrayList<ResturentListPojo> DiscountList = new ArrayList<>();
    public static String SuggestionText = "";
    public static String NotificationTitle = "";
    public static String NotificationBody = "";
    public static int NotificationCount;
    public static String ExtraCharge = "";
    public static String MinimumPurchase = "";
    public static double ItemsAmount=0;
    public static double AfterGstAmt=0;
    public static double AfterDiscountTotal=0;
    public static double discountPrice=0;
    public static double gstPrice=0;
}

