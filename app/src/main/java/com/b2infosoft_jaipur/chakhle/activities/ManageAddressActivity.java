package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.SavedAddressAdapter;
import com.b2infosoft_jaipur.chakhle.fragments.CallFragment;
import com.b2infosoft_jaipur.chakhle.pojo.CityPojo;
import com.b2infosoft_jaipur.chakhle.pojo.SavedAddressPojo;
import com.b2infosoft_jaipur.chakhle.pojo.StatePojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.ConnectionDetector;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.useful.Validations;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ManageAddressActivity extends AppCompatActivity implements CallFragment {

    RecyclerView recycler_addressList;
    TextView tvSavedAddress, tvAddress, tvAddNew;
    Context mContext;
    FrameLayout layoutContainer;
    EditText edtFullName, edtMobileNo, edtPincode, edtFlateNo, edtColony, edtLandmark;
    Spinner spinState, spinCity, spinAddressType;
    String inflate = "No";
    View inflatedLayout;
    TextView tvSave;
    SessionManager sessionManager;
    ConnectionDetector connectionDetector;
    String StateID = "";
    String CityID = "";
    String AddressType = "";
    String addressID = "";
    ArrayList<SavedAddressPojo> editList = new ArrayList<>();
    ImageView locIcon, iconAdd;
    RelativeLayout cityLayout;
    Bundle bundle;
    String FromWhere = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_address);
        mContext = ManageAddressActivity.this;
        sessionManager = new SessionManager(mContext);
        connectionDetector = new ConnectionDetector(mContext);
        initView();
        bundle = getIntent().getExtras();
        try {
            if (bundle != null) {
                FromWhere = bundle.getString("FromWhere");
                addressID = bundle.getString("AddressID");
                if (FromWhere.equals("RestaurentList")) {
                    //if (sessionManager.isLoggedIn()) {
                    SavedAddressPojo.getAddressList(mContext, sessionManager.getValuesSession(SessionManager.Key_UserID), "Activity", "", "ManageAddressActivity");
                    //  }
                } else if (FromWhere.equals("UserProfile")) {
                    SavedAddressPojo.getAddressList(mContext, sessionManager.getValuesSession(SessionManager.Key_UserID), "Activity", "", "ManageAddressActivity");

                } else if (FromWhere.equals("PickAddressActivity")) {
                    if (addressID == null) {
                        addNewAddress();
                    } else {
                        SavedAddressPojo.getAddressList(mContext, sessionManager.getValuesSession(SessionManager.Key_UserID), "Activity", addressID, "ManageAddressActivity");
                    }
                }
            } else {
                addNewAddress();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        locIcon = (ImageView) findViewById(R.id.locIcon);
        recycler_addressList = (RecyclerView) findViewById(R.id.recycler_addressList);
        tvSavedAddress = (TextView) findViewById(R.id.tvSavedAddress);
        tvAddress = (TextView) findViewById(R.id.tvAddress);
        iconAdd = (ImageView) findViewById(R.id.iconAdd);
        layoutContainer = (FrameLayout) findViewById(R.id.layoutContainer);
        tvAddress.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_bold.ttf"));
        tvSavedAddress.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        //tvAddNew.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        iconAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inflate = "Yes";
                addNewAddress();
            }
        });
        locIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (inflate.equals("Yes") && FromWhere.equals("PickAddressActivity")) {
                    Intent intent = new Intent(mContext, ManageAddressActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                else{
                    Intent intent = new Intent(mContext, RestaurantListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        });
    }

    private void addNewAddress() {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        inflatedLayout = inflater.inflate(R.layout.layout_add_new_address, null, false);
        tvSavedAddress.setText("Add New Address");
        iconAdd.setVisibility(View.GONE);
        recycler_addressList.setVisibility(View.GONE);
        StatePojo.getStateList(mContext,"addNewAddress");

        //edtFullName, edtMobileNo, edtPincode, edtFlateNo, edtColony, edtLandmark
        //spinState, spinCity, spinAddressType
        cityLayout = (RelativeLayout) inflatedLayout.findViewById(R.id.cityLayout);
        edtFullName = (EditText) inflatedLayout.findViewById(R.id.edtFullName);
        edtMobileNo = (EditText) inflatedLayout.findViewById(R.id.edtMobileNo);
        edtPincode = (EditText) inflatedLayout.findViewById(R.id.edtPincode);
        edtFlateNo = (EditText) inflatedLayout.findViewById(R.id.edtFlateNo);
        edtColony = (EditText) inflatedLayout.findViewById(R.id.edtColony);
        edtLandmark = (EditText) inflatedLayout.findViewById(R.id.edtLandmark);
        spinState = (Spinner) inflatedLayout.findViewById(R.id.spinState);
        spinCity = (Spinner) inflatedLayout.findViewById(R.id.spinCity);
        spinAddressType = (Spinner) inflatedLayout.findViewById(R.id.spinAddressType);
        tvSave = (TextView) inflatedLayout.findViewById(R.id.tvSave);
        tvSave.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_light.ttf"));
        edtFullName.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        //edtMobileNo.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        //  edtPincode.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtFlateNo.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtColony.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtLandmark.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        if (editList.size() != 0) {
            tvSave.setText("UPDATE");
            tvSavedAddress.setText("Update Address");
            edtFlateNo.setText(editList.get(0).flat_number);
            edtColony.setText(editList.get(0).locality);
            edtLandmark.setText(editList.get(0).landmark);
            edtMobileNo.setText(editList.get(0).mobile_no);
            edtFullName.setText(editList.get(0).name);
            edtPincode.setText(editList.get(0).pincode);
        }
        final List<String> addressTypeList = new ArrayList<String>();
        addressTypeList.add("Select An Address Type");
        addressTypeList.add("Home");
        addressTypeList.add("Office");
        addressTypeList.add("Other");
        ArrayAdapter<String> addressTypeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, addressTypeList);
        addressTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinAddressType.setAdapter(addressTypeAdapter);
        try {
            if (!addressID.equals("")) {
                for (int j = 0; j < addressTypeList.size(); j++) {
                    if (addressTypeList.get(j).equals(editList.get(0).tag_address)) {
                        spinAddressType.setSelection(addressTypeAdapter.getPosition(addressTypeList.get(j)));
                        AddressType = addressTypeList.get(j).toString();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        spinAddressType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String txt = adapterView.getItemAtPosition(i).toString();
                if (!txt.equals("Select An Address Type")) {
                    AddressType = addressTypeList.get(i).toString();
                    Log.d("AddressType>>>", AddressType);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (connectionDetector.isConnectingToInternet()) {
                    if (!StateID.equals("") && !CityID.equals("") && !edtFullName.getText().toString().equals("") && !edtMobileNo.getText().toString().equals("")) {
                        if (Validations.isValidMobile(edtMobileNo, "Please enter valid Mobile number")) {
                            saveUserAddress();
                        }
                    } else {
                        UtilityMethod.showAlertBox(mContext, "Please fill all fields");
                    }
                } else {
                    connectionDetector.showAlertDialog(mContext, "No Internet Connection", "You don't have internet connection.", true);

                }
            }
        });
        layoutContainer.addView(inflatedLayout);
    }

    private void saveUserAddress() {
        WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject mainObject = new JSONObject(response);
                    if (mainObject.getString("success").equals("true")) {
                        if (FromWhere.equals("PickAddressActivity")) {
                            UtilityMethod.showAlertBoxwithIntent(mContext, "Added Successfully", PickAddressActivity.class);
                        }
                       /* if (FromWhere.equals("PickAddressActivity")) {
                            UtilityMethod.showAlertBoxwithIntent(mContext, "Added Successfully", PickAddressActivity.class);
                        }*/
                        else {
                            UtilityMethod.showAlertBoxwithIntent(mContext, "Added Successfully", ManageAddressActivity.class);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        Log.d("StateID>>>", StateID);
        Log.d("CityID>>>", CityID);
        webServiceCaller.addNameValuePair("user_id", sessionManager.getValuesSession(SessionManager.Key_UserID));
        webServiceCaller.addNameValuePair("locality", edtColony.getText().toString());
        webServiceCaller.addNameValuePair("flat_number", edtFlateNo.getText().toString());
        webServiceCaller.addNameValuePair("landmark", edtLandmark.getText().toString());
        webServiceCaller.addNameValuePair("tag_address", AddressType);
        webServiceCaller.addNameValuePair("name", edtFullName.getText().toString());
        webServiceCaller.addNameValuePair("mobile_no", edtMobileNo.getText().toString());
        webServiceCaller.addNameValuePair("pincode", edtPincode.getText().toString());
        webServiceCaller.addNameValuePair("state_id", StateID);
        webServiceCaller.addNameValuePair("city_id", CityID);
        String url = "";
        if (tvSave.getText().toString().equals("SAVE")) {
            Log.d("btnType>>", "save Button");
            url = AppGlobal.AddUserAddress;
            Log.d("url>>", url);
        } else {
            Log.d("btnType>>", "update Button");
            webServiceCaller.addNameValuePair("address_id", addressID);
            url = AppGlobal.UpdateUserAddress;
            Log.d("url>>", url);
        }
        webServiceCaller.execute(url);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (inflate.equals("Yes") && FromWhere.equals("PickAddressActivity")) {
            Intent intent = new Intent(mContext, ManageAddressActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        } /*else if (inflate.equals("No")) {
            Intent intent = new Intent(mContext, RestaurantListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else{
            Intent intent = new Intent(mContext, UserProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }*/
    }

    public void setAddressList(ArrayList<SavedAddressPojo> list, String aID) {
        addressID = aID;
        try {
            if (addressID.equals("")) {
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
                recycler_addressList.setLayoutManager(linearLayoutManager);
                recycler_addressList.setHasFixedSize(true);
                SavedAddressAdapter adapter = new SavedAddressAdapter(mContext, list, "ManageAddress");
                recycler_addressList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
                for (int i = 0; i < list.size(); i++) {
                    if (addressID.equals(list.get(i).AddressId)) {
                        editList.add(new SavedAddressPojo(
                                list.get(i).AddressId,
                                list.get(i).user_id,
                                list.get(i).locality,
                                list.get(i).flat_number,
                                list.get(i).landmark,
                                list.get(i).tag_address,
                                list.get(i).name,
                                list.get(i).mobile_no,
                                list.get(i).pincode,
                                list.get(i).state_id,
                                list.get(i).city_id
                        ));
                    }
                }
                addNewAddress();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setStateAdapter(final ArrayList<StatePojo> statePojoList) {
        List<String> stateList = new ArrayList<String>();
        stateList.add("Select State");
        for (int i = 0; i < statePojoList.size(); i++) {
            stateList.add(statePojoList.get(i).StateName);
        }
        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stateList);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinState.setAdapter(stateAdapter);
        try {
            if (!addressID.equals("")) {
                for (int j = 0; j < statePojoList.size(); j++) {
                    if (statePojoList.get(j).StateID.equals(editList.get(0).state_id)) {
                        spinState.setSelection(stateAdapter.getPosition(statePojoList.get(j).StateName));
                        StateID = statePojoList.get(j).StateID;
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        spinState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String txt = adapterView.getItemAtPosition(i).toString();
                if (!txt.equals("Select State")) {
                    StateID = statePojoList.get(i - 1).StateID;
                    Log.d("StateID>>>", StateID);
                    CityPojo.getCityList(mContext, StateID,"addNewAddress");

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setCityAdapter(final ArrayList<CityPojo> cityPojoList) {
        List<String> cityList = new ArrayList<String>();
        cityList.add("Select City");
        for (int i = 0; i < cityPojoList.size(); i++) {
            cityList.add(cityPojoList.get(i).CityName);
        }
        ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cityList);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinCity.setAdapter(cityAdapter);
        try {
            if (!addressID.equals("")) {
                for (int j = 0; j < cityPojoList.size(); j++) {
                    if (cityPojoList.get(j).CityID.equals(editList.get(0).city_id)) {
                        spinCity.setSelection(cityAdapter.getPosition(cityPojoList.get(j).CityName));
                        CityID = cityPojoList.get(j).CityID;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        spinCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String txt = adapterView.getItemAtPosition(i).toString();
                if (!txt.equals("Select City")) {
                    CityID = cityPojoList.get(i - 1).CityID;
                    Log.d("CityID>>>", CityID);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void setFragment(String fragmentName) {
        SessionManager sessionManager = new SessionManager(this);
        addressID = fragmentName;
        Log.d("AddressID>>", addressID);
        if (!addressID.equals("")) {
            inflate = "Yes";
            SavedAddressPojo.getAddressList(mContext, sessionManager.getValuesSession(SessionManager.Key_UserID), "Activity", addressID, "ManageAddressActivity");

        }
    }

    @Override
    public void setCartValue(int value) {

    }
}
