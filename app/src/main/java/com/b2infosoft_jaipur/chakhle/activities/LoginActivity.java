package com.b2infosoft_jaipur.chakhle.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.services.Config;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.ConnectionDetector;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.useful.Validations;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvSignOrReg, tvProceed, tvEnterPhno, tvEnterMobile, tvSignIn, tvResetPassword, tvForgotPassword, tvPhoneNumber, tvSignOrReg_layout;
    FrameLayout linearParentLayout;
    EditText edtMobileNo, edtPassword;
    Context mContext;
    ConnectionDetector connectionDetector;
    SessionManager sessionManager;
    Typeface typeFace;
    String UserID = "", UserMobile = "", UserFirstName = "", UserLastName = "", UserEmail = "", UserCountryName = "", UserStateName = "";
    String UserCityName = "", UserLocation = "", UserReferalCode = "";
    ImageView locIcon, locIcon2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = LoginActivity.this;
        initView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //  UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);
    }

    private void initView() {
        connectionDetector = new ConnectionDetector(mContext);
        sessionManager = new SessionManager(mContext);
        tvProceed = (TextView) findViewById(R.id.tvProceed);
        tvSignOrReg = (TextView) findViewById(R.id.tvSignOrReg);
        tvEnterPhno = (TextView) findViewById(R.id.tvEnterPhno);
        edtMobileNo = (EditText) findViewById(R.id.edtMobileNo);
        locIcon = (ImageView) findViewById(R.id.locIcon);
        linearParentLayout = (FrameLayout) findViewById(R.id.linearParentLayout);
        typeFace = Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf");
        tvSignOrReg.setTypeface(typeFace);
        //edtMobileNo.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        tvProceed.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_light.ttf"));
        tvEnterPhno.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        tvProceed.setOnClickListener(this);
        locIcon.setOnClickListener(this);

        Intent intent = getIntent();
        try {
            if (intent != null) {
                if (intent.getStringExtra("layoutSecond").equals("layout2")) {
                    edtMobileNo.setText(AppGlobal.MobileNo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View v) {
        TextView tvSignIn;
        switch (v.getId()) {
            case R.id.tvProceed:

            /*    setContentView(R.layout.layout_login_2);
                tvSignIn = (TextView) findViewById(R.id.tvSignIn);
                tvSignIn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                    }
                });*/


                if (connectionDetector.isConnectingToInternet()) {
                    if (!edtMobileNo.getText().toString().equals("")) {
                        if (Validations.isValidMobile(edtMobileNo, "Invalid Mobile Number")) {
                            // UtilityMethod.showAlertBox(mContext, "valid");
                            checkExsitingUser();
                        }
                    } else {
                        UtilityMethod.showAlertBox(mContext, "Please Enter Mobile Number");
                    }
                } else {
                    connectionDetector.showAlertDialog(mContext, "No Internet Connection", "You don't have internet connection.", true);
                }

                break;

            case R.id.locIcon:
               /* Intent intent = new Intent(mContext, RestaurantListActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/
                onBackPressed();
                break;
        }

    }

    private void checkExsitingUser() {

        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, AppGlobal.ApiCallWaitMsg, true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getString("success").equals("false")) {
                        //New User
                        AppGlobal.MobileNo = edtMobileNo.getText().toString();
                        startActivity(new Intent(mContext, RegisterActivity.class));

                    } else {


                        String status = mainObj.getString("status");

                        if (status.equals("inactive")) {

                            JSONObject data = mainObj.getJSONObject("data");
                            JSONObject User = data.getJSONObject("User");

                            AppGlobal.UserID = User.getString("id");
                            Log.d("UserID>>>", AppGlobal.UserID);
                            UtilityMethod.showAlertBoxwithIntentClearHistory(mContext, "Please Verify OTP", OtpActivity.class);

                        } else {
                            AppGlobal.MobileNo = edtMobileNo.getText().toString();
                            //Existing User
                            setContentView(R.layout.layout_login_2);
                            //tvPhoneNumber tvEnterMobile,tvSignIn,tvResetPassword,tvForgotPassword;
                            //edtPassword
                            locIcon2 = (ImageView) findViewById(R.id.locIcon2);
                            tvSignOrReg_layout = (TextView) findViewById(R.id.tvSignOrReg2);
                            tvPhoneNumber = (TextView) findViewById(R.id.tvPhoneNumber);
                            tvEnterMobile = (TextView) findViewById(R.id.tvEnterMobile);
                            tvSignIn = (TextView) findViewById(R.id.tvSignIn);
                            tvResetPassword = (TextView) findViewById(R.id.tvResetPassword);
                            tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);
                            tvSignOrReg_layout.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
                            tvSignIn.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_light.ttf"));
                            tvEnterMobile.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
                            tvPhoneNumber.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
                            edtPassword = (EditText) findViewById(R.id.edtPassword);
                            edtPassword.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
                            tvResetPassword.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_light.ttf"));
                            tvForgotPassword.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_light.ttf"));
                            tvPhoneNumber.setText(AppGlobal.MobileNo);

                            tvResetPassword.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, AppGlobal.ApiCallWaitMsg, true) {
                                        @Override
                                        public void handleResponse(String response) {
                                            Log.d("response", response);
                                            try {
                                                JSONObject mainObj = new JSONObject(response);
                                                if (mainObj.getString("success").equals("true")) {

                                                    openResetPwdDialog();
                                                    //UtilityMethod.showAlertBoxwithIntentClearHistory(mContext, "Please Verify OTP", OtpActivity.class);

                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    };

                                    serviceCaller.addNameValuePair("mobile", AppGlobal.MobileNo);
                                    String url = AppGlobal.SendOtp;
                                    Log.d("url>>", url);
                                    serviceCaller.execute(url);

                                }
                            });
                            tvSignIn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!edtPassword.getText().toString().equals("")) {
                                        doLogin();
                                    }
                                }
                            });
                            locIcon2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(mContext, LoginActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.putExtra("layoutSecond", "login2");
                                    startActivity(intent);
                                }
                            });
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };


        serviceCaller.addNameValuePair("mobile", edtMobileNo.getText().toString());
        String url = AppGlobal.CheckExistingUser;
        Log.d("url>>", url);
        serviceCaller.execute(url);


    }

    private void openResetPwdDialog() {
        final EditText edtOtp, edtPwd;
        TextView tvSave;
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_reset_pwd);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        // set the custom dialog components - text, image and button
        tvSave = (TextView) dialog.findViewById(R.id.tvSave);
        edtOtp = (EditText) dialog.findViewById(R.id.edtOtp);
        edtPwd = (EditText) dialog.findViewById(R.id.edtPwd);
        tvSave.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        edtOtp.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        edtPwd.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!edtOtp.getText().toString().equals("") && !edtPwd.getText().toString().equals("")) {
                    WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, AppGlobal.ApiCallWaitMsg, true) {
                        @Override
                        public void handleResponse(String response) {
                            Log.d("response", response);
                            try {
                                JSONObject mainObj = new JSONObject(response);
                                if (mainObj.getString("success").equals("true")) {
                                    dialog.dismiss();
                                    UtilityMethod.showAlertBoxwithIntentClearHistory(mContext, mainObj.getString("message"), LoginActivity.class);
                                } else {
                                    UtilityMethod.showAlertBox(mContext, mainObj.getString("message"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    serviceCaller.addNameValuePair("otp", edtOtp.getText().toString());
                    serviceCaller.addNameValuePair("password", edtPwd.getText().toString());
                    String url = AppGlobal.ResetPassword;
                    Log.d("url>>", url);
                    serviceCaller.execute(url);
                } else {
                    UtilityMethod.showAlertBox(mContext, "Please fill all field");
                }
            }
        });
        dialog.show();
    }

    private void doLogin() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        //  DeviceID= pref.getString("regId", null);
        Log.e("FirebaseRegIDLogin>>>", "" + regId);
        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, AppGlobal.ApiCallWaitMsg, true) {
            @Override
            public void handleResponse(String response) {
                Log.d("responseLogin", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getString("success").equals("true")) {

                        JSONObject dataObject = mainObj.getJSONObject("data");
                        JSONObject userObject = dataObject.getJSONObject("User");
                        UserID = userObject.getString("id");
                        UserMobile = userObject.getString("mobile");
                        UserFirstName = userObject.getString("firstname");
                        UserLastName = userObject.getString("lastname");
                        userObject.getString("lastname2");
                        UserEmail = userObject.getString("email");
                        userObject.getString("lastname2");
                        JSONObject CountryObject = dataObject.getJSONObject("Country");
                        CountryObject.getString("id");
                        UserCountryName = CountryObject.getString("name");
                        JSONObject StateObject = dataObject.getJSONObject("State");
                        StateObject.getString("id");
                        UserStateName = StateObject.getString("name");
                        JSONObject CityObject = dataObject.getJSONObject("City");
                        CityObject.getString("id");
                        UserCityName = CityObject.getString("name");
                        JSONObject LocationObject = dataObject.getJSONObject("Location");
                        LocationObject.getString("id");
                        UserLocation = LocationObject.getString("name");
                        sessionManager.createLoginSession(UserID, UserFirstName, UserLastName, UserMobile, UserEmail, UserCountryName, UserStateName, UserCityName, UserLocation, "", "", "");
                        if (AppGlobal.FavFrom.equals("RestaurentDetails")) {
                            Bundle bundle = new Bundle();
                            AppGlobal.MobileNo = edtMobileNo.getText().toString();
                            Intent intent = new Intent(mContext, RestaurentDetailsActivity.class);
                            bundle.putString("FromWhere", "LogingActivity");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtras(bundle);
                            startActivity(intent);

                        } else if (AppGlobal.FavFrom.equals("RestaurentList")) {
                            AppGlobal.MobileNo = edtMobileNo.getText().toString();
                            Intent intent = new Intent(mContext, RestaurantListActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else {
                            UtilityMethod.showAlertBoxwithIntent(mContext, "Login Successfully", RestaurantListActivity.class);
                        }
                    } else {
                        UtilityMethod.showAlertBox(mContext, "Username & password doesn't matched.");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        /*Log.d("regId",regId);
        Log.d("AppGlobal.MobileNo",AppGlobal.MobileNo);
        Log.d("edtPassword.getText().toString().trim()", edtPassword.getText().toString().trim());
*/
        serviceCaller.addNameValuePair("token", regId);
        serviceCaller.addNameValuePair("mobile", AppGlobal.MobileNo);
        serviceCaller.addNameValuePair("password", edtPassword.getText().toString().trim());
        String url = AppGlobal.UserLogin;
        Log.d("url>>", url);
        serviceCaller.execute(url);
    }
}
