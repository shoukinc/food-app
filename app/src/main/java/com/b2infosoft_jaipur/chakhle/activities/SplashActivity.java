package com.b2infosoft_jaipur.chakhle.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.ConnectionDetector;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    TextView splashLogo;
    Context mContext;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splashLogo = (TextView) findViewById(R.id.splashLogo);
        mContext = SplashActivity.this;
        sessionManager = new SessionManager(mContext);
        /*if (sessionManager.isLoggedIn()){
            displayFirebaseRegId();
        }*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }
        splashLogo.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        /*Animation spinin = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.zoom_out);
        splashLogo.startAnimation(spinin);*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                ConnectionDetector connectionDetector = new ConnectionDetector(getApplicationContext());
                if (connectionDetector.isConnectingToInternet()) {
                    startActivity(new Intent(SplashActivity.this, RestaurantListActivity.class));
                } else {
                    showDailog();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    public void showDailog() {
        Dialog pDlg = new Dialog(SplashActivity.this);
        pDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pDlg.setContentView(R.layout.custom_splash_dialog);
        TextView splash_msg = (TextView) pDlg.findViewById(R.id.splash_dialog_msg);
        Button splash_btn_retry = (Button) pDlg.findViewById(R.id.splash_btn_retry);
        Button splash_btn_exit = (Button) pDlg.findViewById(R.id.splash_btn_exit);
        splash_msg.setText("Please check your internet connection & then start app again. Thank You!");
        //splash_btn_retry.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        //splash_btn_exit.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        //splash_msg.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));

        splash_btn_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();
                startActivity(new Intent(SplashActivity.this, SplashActivity.class));
            }
        });
        splash_btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        pDlg.setCancelable(false);
        pDlg.show();
        /*if (isConnect) {
            pDlg.dismiss();
        }*/
    }

}
