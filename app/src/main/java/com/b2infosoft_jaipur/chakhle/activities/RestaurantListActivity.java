package com.b2infosoft_jaipur.chakhle.activities;


import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.MenuAdapter;
import com.b2infosoft_jaipur.chakhle.fragments.CallFragment;
import com.b2infosoft_jaipur.chakhle.fragments.RecommendedFragment;
import com.b2infosoft_jaipur.chakhle.fragments.RestaurentFragment;
import com.b2infosoft_jaipur.chakhle.pojo.CartFillPojo;
import com.b2infosoft_jaipur.chakhle.pojo.RestaurentDetailsPojo;
import com.b2infosoft_jaipur.chakhle.services.Config;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.sessions.SessionNew;
import com.b2infosoft_jaipur.chakhle.useful.ConnectionDetector;
import com.b2infosoft_jaipur.chakhle.useful.GPSTracker;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;
import com.fastaccess.permission.base.PermissionHelper;
import com.fastaccess.permission.base.callback.OnPermissionCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RestaurantListActivity extends AppCompatActivity implements View.OnClickListener, OnPermissionCallback, CallFragment {

    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    FrameLayout fragmentContainer;
    RelativeLayout relativeHeaderbar;
    Context mContext;
    Intent intent;
    String locationAddress = "", address = "", City = "";
    ImageView iconFav, imgFilter;
    TextView tvCity, tvArea, tvCall, tvFav, tvOrder, tvCart, tvNotification, tvNotificationCount, tvCartCount;
    LinearLayout layoutCall, layoutFav, layoutCart, layoutOrder, layoutNotification;
    Bundle bundle;
    private FloatingActionButton fab;
    private Animation fab_open, fab_close;
    private Boolean isFabOpen = false;
    Dialog dialog = null;
    TextView edtSearch;
    SessionManager sessionManager;
    boolean isFromSession = false;
    ConnectionDetector connectionDetector;
    ListView list;
    FrameLayout revealView, layoutButtons;
    Animation alphaAnimation;
    float pixelDensity;
    boolean flag = true;
    GPSTracker gps;


    LocationManager locationManager;
    boolean GpsStatus;


    private final static String[] MULTI_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    PermissionHelper permissionHelper;
    ImageView imgSlideUp;
    LinearLayout linearBottom;
    Animation slide_up, slide_down;
    boolean doubleBackToExitPressedOnce = false;
    String addToFav = null;

    public String from = "one";
    SharedPreferences permissionStatus;
    String cartValue = "";
    ArrayList<CartFillPojo> mainSessonCartList = new ArrayList<>();
    TextView tvBirthdayTxt, tvNote, tvNoteDetail, tvOk;
    private boolean sentToSettings = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_demo);
        mContext = RestaurantListActivity.this;
        initView();
        //intent = getIntent();

    }

    private void showBirthdayDialog(String title, String note, String description) {
        final Dialog dialog_birthday = new Dialog(mContext);
        dialog_birthday.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_birthday.setContentView(R.layout.dialog_birthday);
        dialog_birthday.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog_birthday.setCancelable(false);
        tvBirthdayTxt = (TextView) dialog_birthday.findViewById(R.id.tvBirthdayTxt);
        tvNote = (TextView) dialog_birthday.findViewById(R.id.tvNote);
        tvNoteDetail = (TextView) dialog_birthday.findViewById(R.id.tvNoteDetail);
        tvOk = (TextView) dialog_birthday.findViewById(R.id.tvOk);
        tvBirthdayTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_bold.ttf"));
        tvNote.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvNoteDetail.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvOk.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvBirthdayTxt.setText(title);
        tvNote.setText(note);
        tvNoteDetail.setText(description);
        dialog_birthday.show();
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setValuesSession(SessionManager.Key_UserBday, "true");
                dialog_birthday.dismiss();
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = event.getActionMasked();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                // Toast.makeText(mContext, "Action was DOWN", Toast.LENGTH_SHORT).show();
                break;
            case MotionEvent.ACTION_UP:
                //  Toast.makeText(mContext, "Action was UP", Toast.LENGTH_SHORT).show();
        }
        return super.onTouchEvent(event);
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        //  DeviceID= pref.getString("regId", null);
        Log.e("FirebaseRegID>>>", "" + regId);
    }

    private void initView() {


        sessionManager = new SessionManager(mContext);
        connectionDetector = new ConnectionDetector(mContext);
        permissionHelper = PermissionHelper.getInstance(this);
        //if (sessionManager.isLoggedIn()) {
        displayFirebaseRegId();


        // }
        tvCity = (TextView) findViewById(R.id.tvCity);
        tvArea = (TextView) findViewById(R.id.tvArea);
        tvCall = (TextView) findViewById(R.id.tvCall);
        tvFav = (TextView) findViewById(R.id.tvFav);
        tvOrder = (TextView) findViewById(R.id.tvOrder);
        tvCart = (TextView) findViewById(R.id.tvCart);
        iconFav = (ImageView) findViewById(R.id.iconFav);
        imgFilter = (ImageView) findViewById(R.id.imgFilter);
        tvNotification = (TextView) findViewById(R.id.tvNotification);
        tvNotificationCount = (TextView) findViewById(R.id.tvNotificationCount);
        tvCartCount = (TextView) findViewById(R.id.tvCartCount);
        edtSearch = (TextView) findViewById(R.id.edtSearch);
        imgSlideUp = (ImageView) findViewById(R.id.imgSlideUp);
        tvCartCount.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));

        tvNotificationCount.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));

        SessionNew sessionNew = new SessionNew(mContext);

        ArrayList<CartFillPojo> sessionNewCartList = sessionNew.getCartList(mContext);

        if (sessionNewCartList != null) {
            if (!sessionNewCartList.isEmpty()) {
                sessionManager.clearArrayList(mContext);
                sessionManager.saveCartList(mContext, sessionNewCartList);
            } else {
                Log.d("empty", "empty");
            }
        } else {
            Log.d("null", "null hh");
        }

        mainSessonCartList = sessionManager.getCartList(mContext);
        String count = sessionManager.getKey_NotificationList_count();

        try {
            if (count.equals("0")) {
                tvNotificationCount.setVisibility(View.GONE);
            } else {
                tvNotificationCount.setVisibility(View.VISIBLE);
                tvNotificationCount.setText(count + "");
            }
        } catch (Exception e) {
            e.printStackTrace();
            tvNotificationCount.setVisibility(View.GONE);
        }
        // tvArea.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        // tvCity.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtSearch.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        tvCall.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvFav.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvOrder.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvCart.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvNotification.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        layoutCall = (LinearLayout) findViewById(R.id.layoutCall);
        layoutFav = (LinearLayout) findViewById(R.id.layoutFav);
        layoutCart = (LinearLayout) findViewById(R.id.layoutCart);
        layoutOrder = (LinearLayout) findViewById(R.id.layoutOrder);
        layoutNotification = (LinearLayout) findViewById(R.id.layoutNotification);
        imgSlideUp.setOnClickListener(this);
        imgFilter.setOnClickListener(this);
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, SearchActivity.class);
                intent.putExtra("FromWhere", "RestaurentList");
                startActivity(intent);
            }
        });
        isFromSession = sessionManager.isLoggedIn();
        if (isFromSession) {
            //showBirthdayDialog();
            // sessionManager.setValuesSession(SessionManager.Key_UserBday, "false");
            WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", false) {
                @Override
                public void handleResponse(String response) {
                    Log.d("response", response);
                    try {
                        // sessionManager.setValuesSession(SessionManager.Key_UserBday, "false");

                        JSONObject mainObject = new JSONObject(response);
                        String BdayValue = sessionManager.getValuesSession(SessionManager.Key_UserBday);
                        if (mainObject.getString("success").equals("true") && BdayValue.equals("false")) {
                            JSONObject data = mainObject.getJSONObject("data");
                            showBirthdayDialog(data.getString("title"), data.getString("note"), data.getString("description"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            };
            webServiceCaller.addNameValuePair("user_id", sessionManager.getValuesSession(SessionManager.Key_UserID));
            String url = AppGlobal.GetBirthday;
            Log.d("url>>", url);
            webServiceCaller.execute(url);
        }

        fragmentContainer = (FrameLayout) findViewById(R.id.fragmentContainer);
        relativeHeaderbar = (RelativeLayout) findViewById(R.id.RelativeHeaderbar);
        relativeHeaderbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, SearchRestaurentActivity.class));
            }
        });
        layoutCall.setOnClickListener(this);
        layoutFav.setOnClickListener(this);
        layoutCart.setOnClickListener(this);
        layoutOrder.setOnClickListener(this);
        layoutNotification.setOnClickListener(this);
        linearBottom = (LinearLayout) findViewById(R.id.linearBottom);
        pixelDensity = getResources().getDisplayMetrics().density;
        alphaAnimation = AnimationUtils.loadAnimation(this, R.anim.circular_reval_anim);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final View dialogView = View.inflate(RestaurantListActivity.this, R.layout.fab_dialog, null);

                dialog = new Dialog(RestaurantListActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(dialogView);
                WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
                wmlp.gravity = Gravity.BOTTOM | Gravity.LEFT;
                wmlp.x = R.dimen.x_axsis;   //x position
                // wmlp.y = 800;   //y position

                // dialog.getWindow().getAttributes().windowAnimations = R.anim.circular_reval_anim;
//               dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                int width = (int) getResources().getDimension(R.dimen.dialog_width);
                dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        revealShow(dialogView, true, null);
                    }
                });
                dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                        if (i == KeyEvent.KEYCODE_BACK) {
                            revealShow(dialogView, false, dialog);
                            return true;
                        }
                        return false;
                    }
                });
                String[] menu_text = {
                        "Profile",
                        "Address",
                        "Orders",
                        "Call Us",
                        "Invite and Earn",
                        "Partner with Us",
                        "Feedback"
                };
                Integer[] menu_images = {
                        R.drawable.fab_icon,
                        R.drawable.location_white,
                        R.drawable.order_larg,
                        R.drawable.call_menu,
                        R.drawable.invite_earn,
                        R.drawable.handshake_a,
                        R.drawable.feedbacks
                };
                ListView dialog_ListView = (ListView) dialog.findViewById(R.id.listView);
                dialog_ListView.setAdapter(new MenuAdapter(mContext, menu_text, menu_images));
                dialog_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String text = parent.getItemAtPosition(position).toString();
                        Log.d("text>>>", text);

                        dialog.dismiss();
                        switch (text.toString()) {
                            case "Profile":
                                if (!isFromSession) {
                                    startActivity(new Intent(mContext, LoginActivity.class));
                                } else {
                                    startActivity(new Intent(mContext, UserProfileActivity.class));
                                }
                                break;
                            case "Orders":
                                dialog.hide();
                                if (!isFromSession) {
                                    startActivity(new Intent(mContext, LoginActivity.class));
                                } else {
                                    startActivity(new Intent(mContext, MyOrdersActivity.class));
                                }
                                break;
                            case "Partner with Us":
                               /* Toast.makeText(mContext,"Partener With Us",Toast.LENGTH_SHORT).show();*/
                                startActivity(new Intent(mContext, PartnerWithUsActivity.class));
                                break;
                            case "Invite and Earn":
                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                String shareBody = "Install Chakle App And Get " + "@string/Rs" + "250/- Hurry Up https://play.google.com/store/apps/details?id=com.b2infosoft_jaipur.chakhle";
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Super Restaurant");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                                break;
                            case "Address":
                                if (!isFromSession) {
                                    startActivity(new Intent(mContext, LoginActivity.class));
                                } else {
                                    Intent intent = new Intent(mContext, ManageAddressActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("FromWhere", "RestaurentList");
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                }
                                break;
                            case "Call Us":
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:9017811811"));
                                startActivity(intent);
                                break;
                            case "Feedback":
                                startActivity(new Intent(mContext, FeedbackActivity.class));
                                break;
                        }
                    }
                });
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();
            }
        });


        bundle = getIntent().getExtras();
        try {
            if (bundle != null) {
                if (bundle.getString("ClassName").equals("RestaurentDetailsActivity")) {
                    int countValue = 0;
                    addToFav = bundle.getString("AddToFav");

                    /*if (!AppGlobal.mainCartList.isEmpty()) {
                        for (int i = 0; i < AppGlobal.mainCartList.size(); i++) {
                            countValue = countValue + AppGlobal.mainCartList.get(i).HalfQty + AppGlobal.mainCartList.get(i).MediumQty + AppGlobal.mainCartList.get(i).FullQty + AppGlobal.mainCartList.get(i).LargeQty;
                        }
                        tvCartCount.setVisibility(View.VISIBLE);
                        tvCartCount.setText("" + countValue);
                    } else {
                        if (mainSessonCartList != null) {
                            Log.d("mainSessonCartList>>>", "" + mainSessonCartList.size());
                            for (int i = 0; i < mainSessonCartList.size(); i++) {
                                countValue = countValue + mainSessonCartList.get(i).HalfQty + mainSessonCartList.get(i).MediumQty + mainSessonCartList.get(i).FullQty + mainSessonCartList.get(i).LargeQty;
                            }
                            //  AppGlobal.mainCartList = mainSessonCartList;
                            // AppGlobal.mainCartList = (ArrayList<CartFillPojo>)mainSessonCartList.clone();
                            tvCartCount.setVisibility(View.VISIBLE);
                            tvCartCount.setText("" + countValue);
                        }
                    }*/

                    Log.d("RestaurentDetailsActivity", "RestaurentDetailsActivity");

                    tvCity.setText(AppGlobal.Addresses);
                    tvArea.setText(AppGlobal.LocationAddress);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragmentContainer2, new RestaurentFragment())
                            .addToBackStack(null).commit();
                } else if (bundle.getString("ClassName").equals("RestaurentFilterActivity")) {
                    int countValue = 0;
                    addToFav = bundle.getString("AddToFav");

                    from = bundle.getString("ClassName");
                    Log.d("RestaurentFilterActivity", "RestaurentFilterActivity");

                    tvCity.setText(AppGlobal.Addresses);
                    tvArea.setText(AppGlobal.LocationAddress);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragmentContainer2, new RestaurentFragment())
                            .addToBackStack(null).commit();
                } else if (bundle.getString("ClassName").equals("SearchRestaurentActivity")) {
                    locationAddress = bundle.getString("locationAddress");
                    address = bundle.getString("address");
                    City = bundle.getString("city");
                    Log.d("locationAddress", "gg " + locationAddress);
                    Log.d("locationAddress", "" + address);
                    AppGlobal.LocationAddress = locationAddress;
                    AppGlobal.Addresses = address;
                    AppGlobal.City = City;
                    tvCity.setText(AppGlobal.Addresses);
                    tvArea.setText(AppGlobal.LocationAddress);
                    if (!locationAddress.equals("")) {
                        Log.d("inner>>", "in");
                        //  AdvertiesPojo.getAdvt_List(mContext, City);
                        //  ResturentListPojo.getResturentList(RestaurantListActivity.this, locationAddress);
                        //  ReviewPojo.getReviewRest(mContext,locationAddress);
                        //  RestaurentDetailsPojo.getDiscountOffer(mContext,locationAddress);

                    }

                    Log.d("SearchRestaurentActivity", "SearchRestaurentActivity");

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer2, new RestaurentFragment()).addToBackStack(null).commit();
                }
            } else {
                /*int countValue = 0;
                if (!AppGlobal.mainCartList.isEmpty()) {
                    for (int i = 0; i < AppGlobal.mainCartList.size(); i++) {
                        // countValue = countValue + AppGlobal.mainCartList.get(i).quantity;
                        countValue = countValue + AppGlobal.mainCartList.get(i).HalfQty + AppGlobal.mainCartList.get(i).MediumQty + AppGlobal.mainCartList.get(i).FullQty + AppGlobal.mainCartList.get(i).LargeQty;
                    }
                    tvCartCount.setVisibility(View.VISIBLE);
                    tvCartCount.setText("" + countValue);
                } else {
                    if (mainSessonCartList == null) {
                        Log.d("mainSessonCartList>>>", "EmptyList");
                    } else {
                        Log.d("mainSessonCartList>>>", "" + mainSessonCartList.size());
                        for (int i = 0; i < mainSessonCartList.size(); i++) {
                            countValue = countValue + mainSessonCartList.get(i).HalfQty + mainSessonCartList.get(i).MediumQty + mainSessonCartList.get(i).FullQty + mainSessonCartList.get(i).LargeQty;
                        }
                        tvCartCount.setVisibility(View.VISIBLE);
                        tvCartCount.setText("" + countValue);
                    }
                }*/

                Log.d("other", "other");


                CheckGpsStatus();

                if (GpsStatus == true) {
                    if (isPermissionGranted()) {
                        proceedAfterPermission();
                    }
                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Enable location");  // GPS not found
                    builder.setMessage("Do you want to enable location?");
                    builder.setCancelable(false);// Want to enable?
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 1);

                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            onBackPressed();
                        }
                    });
                    builder.create().show();

                    Log.d("GPS Location is dissable", "jhjhjhjhjh");
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("ere", "excep");
        }

        int countValue = 0;
        if (sessionManager.isLoggedIn()) {
            mainSessonCartList = sessionManager.getCartList(mContext);
            if (mainSessonCartList != null) {
                for (int i = 0; i < mainSessonCartList.size(); i++) {
                    countValue = countValue + mainSessonCartList.get(i).HalfQty + mainSessonCartList.get(i).MediumQty + mainSessonCartList.get(i).FullQty + mainSessonCartList.get(i).LargeQty;
                }
                tvCartCount.setVisibility(View.VISIBLE);
                tvCartCount.setText("" + countValue);
            } else {
                tvCartCount.setVisibility(View.GONE);
            }

            // countValue=sessionManager.
        } else {
            SessionNew sessionNew1 = new SessionNew(mContext);
            ArrayList<CartFillPojo> sessionNewCartList1 = sessionNew1.getCartList(mContext);
            if (sessionNewCartList1 != null) {
                for (int i = 0; i < sessionNewCartList1.size(); i++) {
                    countValue = countValue + sessionNewCartList1.get(i).HalfQty + sessionNewCartList1.get(i).MediumQty + sessionNewCartList1.get(i).FullQty + sessionNewCartList1.get(i).LargeQty;
                }
                tvCartCount.setVisibility(View.VISIBLE);
                tvCartCount.setText("" + countValue);
            } else {
                tvCartCount.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("gfg", resultCode + " kk " + requestCode);
        if (requestCode == 1) {
            Intent intent = new Intent(this, RestaurantListActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void CheckGpsStatus() {
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted 23");
                locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
                GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                proceedAfterPermission();
                return true;
            } else {
                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted 22");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(this, RestaurantListActivity.class);
                    startActivity(intent);
                    finish();

                    Log.d("Permission is granted", "Permission is granted");

                } else {

                    //  Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private void proceedAfterPermission() {

        gps = new GPSTracker(RestaurantListActivity.this);
        if (gps.canGetLocation())
        {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            Log.d("Location>>>", "" + latitude + "," + longitude);
            Geocoder geocoder;
            List<Address> addresses = null;
            geocoder = new Geocoder(this, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                Log.d("address>>", address);
                Log.d("address>>", city);
                Log.d("address>>", state);
                Log.d("address>>", country);
                // Log.d("address>>",address);
                //Log.d("address>>",addresses.toString());
                // \n is for new line
                String locationAddress = address + ", " + city + ", " + state + ", " + country;
                        /* Log.d("address>>", locationAddress);*/
                AppGlobal.LocationAddress = locationAddress;
                AppGlobal.Addresses = address;
                AppGlobal.City = city;
                tvCity.setText(AppGlobal.Addresses);
                tvArea.setText(AppGlobal.LocationAddress);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer2, new RestaurentFragment()).addToBackStack(null).commit();

            } catch (IOException e) {

                e.printStackTrace();
            }

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    private void revealShow(View dialogView, boolean b, final Dialog dialog) {
        final View view = dialogView.findViewById(R.id.dialog);
        int w = view.getHeight();
        int h = view.getHeight();

        int endRadius = (int) Math.hypot(w, h);

        int cx = (int) (fab.getX() + (fab.getWidth() / 2));
        int cy = (int) (fab.getY()) + fab.getHeight() + 56;

        if (b) {
            Animator revealAnimator = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                revealAnimator = ViewAnimationUtils.createCircularReveal(view, cx, cy, 20, endRadius);
            }
            view.setVisibility(View.VISIBLE);
            revealAnimator.setDuration(150);
            revealAnimator.start();

        } else {

            Animator anim = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, endRadius, 20);
            }

            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    dialog.dismiss();
                    view.setVisibility(View.INVISIBLE);
                }
            });
            anim.setDuration(150);
            anim.start();
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.layoutCall:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:9017811811"));
                startActivity(intent);
                break;
            case R.id.layoutFav:

                if (sessionManager.isLoggedIn()) {
                    startActivity(new Intent(mContext, FavRestorentActivity.class));
                } else {
                    AppGlobal.FavFrom = "RestaurentList";
                    startActivity(new Intent(mContext, LoginActivity.class));
                }
                break;
            case R.id.layoutCart:
                RestaurentDetailsPojo restaurentDetailsPojo = null;
                AppGlobal.mainCartList.clear();
                AppGlobal.GlobalCartList.clear();
                ArrayList<CartFillPojo> sessionCartList = new ArrayList<>();
                int Quentity = 0, GlobalQuentity = 0;
                if (sessionManager.isLoggedIn()) {
                    if (AppGlobal.mainCartList.isEmpty()) {
                        sessionCartList = sessionManager.getCartList(mContext);
                        try {
                            if (sessionCartList == null) {
                                UtilityMethod.showAlertBox(mContext, "Your Cart is Empty");
                            } else if (sessionCartList.size() != 0) {
                                AppGlobal.mainCartList = sessionCartList;
                                Log.d("sessionCartList>>>", "" + sessionCartList.size());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        int HalfQty = 0;
                        double HalfAmount = 0;
                        double HalfTotalAmount = 0;
                        int MediumQty = 0;
                        double MediumAmt = 0;
                        double MediumTotalAmt = 0;
                        int FullQty = 0;
                        double FullAmt = 0;
                        double FullTotalAmt = 0;
                        int LargeQty = 0;
                        int cartQty = 0;
                        double LargeAmt = 0;
                        double LargeTotalAmt = 0;
                        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                            HalfQty = 0;
                            MediumQty = 0;
                            MediumTotalAmt = 0;
                            FullQty = 0;
                            FullAmt = 0;
                            FullTotalAmt = 0;
                            LargeQty = 0;
                            LargeAmt = 0;
                            LargeTotalAmt = 0;
                            if (AppGlobal.RestaurentProductList.get(i).halfQty != 0) {
                                HalfQty = AppGlobal.RestaurentProductList.get(i).halfQty;
                                HalfAmount = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).half);
                                HalfTotalAmount = HalfQty * HalfAmount;
                                Log.d("HalfTotalAmount>>>", "" + HalfTotalAmount);
                                cartQty = cartQty + HalfQty;

                            }
                            if (AppGlobal.RestaurentProductList.get(i).mediumQty != 0) {
                                MediumQty = AppGlobal.RestaurentProductList.get(i).mediumQty;
                                MediumAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).medium);
                                MediumTotalAmt = MediumQty * MediumAmt;
                                cartQty = cartQty + MediumQty;
                                Log.d("MediumTotalAmt>>>", "" + MediumTotalAmt);
                            }
                            if (AppGlobal.RestaurentProductList.get(i).fullQty != 0) {
                                FullQty = AppGlobal.RestaurentProductList.get(i).fullQty;
                                FullAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).full);
                                FullTotalAmt = FullQty * FullAmt;
                                cartQty = cartQty + FullQty;

                                Log.d("FullTotalAmt>>>", "" + FullTotalAmt);
                            }
                            if (AppGlobal.RestaurentProductList.get(i).largeQty != 0) {
                                LargeQty = AppGlobal.RestaurentProductList.get(i).largeQty;
                                LargeAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).half);
                                LargeTotalAmt = LargeQty * LargeAmt;
                                cartQty = cartQty + LargeQty;

                                Log.d("LargeTotalAmt>>>", "" + LargeTotalAmt);
                            }
                            Quentity = AppGlobal.RestaurentProductList.get(i).getCount();
                            Log.d("Quentity>>>", "" + Quentity);
                            int total = (AppGlobal.RestaurentProductList.get(i).afterDiscountValue) * Quentity;
                            int productPrice = Integer.parseInt((AppGlobal.RestaurentProductList.get(i).Product_price));
                            int totalProductPrice = productPrice * Quentity;
                            String minimum_purchase = AppGlobal.RestaurentProductList.get(i).minimum_purchase;
                            String extra_charge = AppGlobal.RestaurentProductList.get(i).extra_charge;
                            Log.d("minimum_purchase>>", minimum_purchase);
                            Log.d("extra_charge>>", extra_charge);
                            double container = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).container_charge);
                            Log.d("Quentity>>>", "" + Quentity);
                            container = container * Quentity;
                            if (HalfQty != 0 || FullQty != 0 || LargeQty != 0 || MediumQty != 0) {
                                AppGlobal.mainCartList.add(new CartFillPojo(
                                        AppGlobal.RestaurentProductList.get(i).Product_id,
                                        Quentity,
                                        AppGlobal.RestaurentProductList.get(i).Product_name,
                                        AppGlobal.RestaurentProductList.get(i).Product_price,
                                        total,
                                        AppGlobal.RestaurentProductList.get(i).restaurant_id,
                                        sessionManager.getValuesSession(SessionManager.Key_UserID),
                                        AppGlobal.RestaurentProductList.get(i).Product_vegproduct,
                                        AppGlobal.RestaurentProductList.get(i).afterDiscountValue,
                                        totalProductPrice, AppGlobal.RestaurentProductList.get(i).name,
                                        AppGlobal.RestaurentProductList.get(i).discount,
                                        String.valueOf(container),
                                        AppGlobal.RestaurentProductList.get(i).minimum_purchase,
                                        AppGlobal.RestaurentProductList.get(i).extra_charge,
                                        AppGlobal.RestaurentProductList.get(i).container_charge,
                                        HalfQty, HalfAmount, HalfTotalAmount,
                                        FullQty, FullAmt, FullTotalAmt,
                                        LargeQty, LargeAmt, LargeTotalAmt,
                                        MediumQty, MediumAmt, MediumTotalAmt
                                        , AppGlobal.RestaurentProductList.get(i).discountType
                                        , AppGlobal.RestaurentProductList.get(i).half_label
                                        , AppGlobal.RestaurentProductList.get(i).medium_label
                                        , AppGlobal.RestaurentProductList.get(i).full_label
                                        , AppGlobal.RestaurentProductList.get(i).large_label
                                        , AppGlobal.RestaurentProductList.get(i).let_nite_charge
                                        , AppGlobal.RestaurentProductList.get(i).tax_percentage
                                        , AppGlobal.RestaurentProductList.get(i).RestInTime
                                        , AppGlobal.RestaurentProductList.get(i).Restoff_time_1
                                ));
                            }
                        }
                        ArrayList<CartFillPojo> mainSessonCartList;
                        mainSessonCartList = sessionManager.getCartList(mContext);
                        if (!AppGlobal.mainCartList.isEmpty() && mainSessonCartList != null) {
                            sessionManager.clearArrayList(mContext);
                            sessionManager.saveCartList(mContext, AppGlobal.mainCartList);
                            Log.d("CartQty>>>", "" + cartQty);
                            if (cartQty != 0) {
                                tvCartCount.setVisibility(View.VISIBLE);
                                tvCartCount.setText("" + cartQty);
                            } else {

                                tvCartCount.setVisibility(View.GONE);
                                tvCartCount.setText("" + cartQty);
                            }
                        } else {
                            sessionManager.saveCartList(mContext, AppGlobal.mainCartList);
                        }
                    }
                    if (sessionCartList == null) {
                    } else {
                        Log.d("AppGlobal>>>", "" + AppGlobal.mainCartList.size());
                        startActivity(new Intent(mContext, ReviewOrderActivity.class));
                    }
                } else {
                    startActivity(new Intent(mContext, LoginActivity.class));
                }
                // startActivity(new Intent(mContext, ReviewOrderActivity.class));
                break;
            case R.id.layoutOrder:
                break;
            case R.id.imgSlideUp:
              /* *//* new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {*//*
                        slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                                R.anim.slide_up);
                        linearBottom.setVisibility(View.VISIBLE);
                        linearBottom.startAnimation(slide_up);
               *//*     }
                }, 3000);*/
                break;
            case R.id.layoutNotification:
                sessionManager.setKey_NotificationList_count("0");
                startActivity(new Intent(mContext, NotificationActivity.class));
                break;

            case R.id.imgFilter:
                startActivity(new Intent(mContext, FilterActivity.class));
                break;

        }
    }

    public void getLocation() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (permissionHelper.isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION) && permissionHelper.isPermissionGranted(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                gps = new GPSTracker(RestaurantListActivity.this);
                if (gps.canGetLocation()) {

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();
                    Log.d("Location>>>", "" + latitude + "," + longitude);
                    Geocoder geocoder;
                    List<Address> addresses = null;
                    geocoder = new Geocoder(this, Locale.getDefault());
                    try {
                        addresses = geocoder.getFromLocation(latitude, longitude, 1);
                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName();
                        Log.d("address>>", address);
                        Log.d("address>>", city);
                        Log.d("address>>", state);
                        Log.d("address>>", country);
                        // Log.d("address>>",address);
                        //Log.d("address>>",addresses.toString());
                        // \n is for new line
                        String locationAddress = address + ", " + city + ", " + state + ", " + country;
                        /* Log.d("address>>", locationAddress);*/
                        AppGlobal.LocationAddress = locationAddress;
                        AppGlobal.Addresses = address;
                        AppGlobal.City = city;


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                }
            } else {
                permissionHelper
                        .setForceAccepting(false)
                        .request(MULTI_PERMISSIONS);
            }
        } else {
            gps = new GPSTracker(RestaurantListActivity.this);
            if (gps.canGetLocation()) {
                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();
                Log.d("Location>>>", "" + latitude + "," + longitude);
                Geocoder geocoder;
                List<Address> addresses = null;
                geocoder = new Geocoder(this, Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();
                    Log.d("address>>", address);
                    Log.d("address>>", city);
                    Log.d("address>>", state);
                    Log.d("address>>", country);
                    // Log.d("address>>",address);
                    //Log.d("address>>",addresses.toString());

                    // \n is for new line
                    String locationAddress = address + ", " + city + ", " + state + ", " + country;
                    Log.d("address>>", locationAddress);
                    AppGlobal.LocationAddress = locationAddress;
                    AppGlobal.Addresses = address;
                    AppGlobal.City = city;

                } catch (IOException e) {
                    e.printStackTrace();
                }

                // \n is for new line
                // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onPermissionGranted(@NonNull String[] permissionName) {

    }

    @Override
    public void onPermissionDeclined(@NonNull String[] permissionName) {

    }

    @Override
    public void onPermissionPreGranted(@NonNull String permissionsName) {

    }

    @Override
    public void onPermissionNeedExplanation(@NonNull String permissionName) {

    }

    @Override
    public void onPermissionReallyDeclined(@NonNull String permissionName) {

    }

    @Override
    public void onNoPermissionNeeded() {

    }

    @Override
    public void setFragment(String fName) {
       /* if (linearBottom.getVisibility() == View.VISIBLE) {
            slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.anim.slide_down);
            linearBottom.setVisibility(View.GONE);
            linearBottom.startAnimation(slide_down);
        }*/
        Fragment fragment = null;
        String fragmentName = "";
        fragmentName = fName;
        if (fragmentName.equals("RestaurentDetailsFragment")) {
            startActivity(new Intent(mContext, RestaurentDetailsActivity.class));
        } else if (fragmentName.equals("RecommendedFragment")) {
            fragment = new RecommendedFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, fragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void setCartValue(int value) {
        int cartValue = value;
        Log.d("CartValue>>>", "" + cartValue);
        if (cartValue == 0) {
            tvCartCount.setVisibility(View.GONE);
        } else {
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText("" + cartValue);
        }
    }


    @Override
    public void onBackPressed() {
        System.exit(0);
    }
}
