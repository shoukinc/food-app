package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.PickAddressAdapter;
import com.b2infosoft_jaipur.chakhle.fragments.SendData;
import com.b2infosoft_jaipur.chakhle.pojo.SavedAddressPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.util.ArrayList;

public class PickAddressActivity extends AppCompatActivity implements View.OnClickListener, SendData {

    RecyclerView recycler_addressList;
    TextView tvNameTxt, tvName, tvMobileTxt, tvMobile, tvEmailTxt, tvEmail, tvPickAddress;
    LinearLayout layoutAddAddress;
    Context mContext;
    ImageView locIcon, iconGoNext;
    Button btnAddAddress;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_address);
        mContext = PickAddressActivity.this;
        initView();
        sessionManager = new SessionManager(mContext);
        if (sessionManager.isLoggedIn()) {
            SavedAddressPojo.getAddressList(mContext, sessionManager.getValuesSession(SessionManager.Key_UserID), "Activity", "", "PickAddressActivity");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       // UtilityMethod.goNextClass(mContext, ReviewOrderActivity.class);
        Intent intent = new Intent(mContext, RestaurantListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle();
        bundle.putString("ClassName", "RestaurentDetailsActivity");
        bundle.putString("AddToFav", "true");

        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void initView() {
        tvNameTxt =   findViewById(R.id.tvNameTxt);
        tvName =  findViewById(R.id.tvName);
        tvMobileTxt = (TextView) findViewById(R.id.tvMobileTxt);
        tvMobile = (TextView) findViewById(R.id.tvMobile);
        tvEmailTxt = (TextView) findViewById(R.id.tvEmailTxt);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        tvPickAddress = (TextView) findViewById(R.id.tvPickAddress);
        layoutAddAddress = (LinearLayout) findViewById(R.id.layoutAddAddress);
        btnAddAddress = (Button) findViewById(R.id.btnAddAddress);
        iconGoNext = (ImageView) findViewById(R.id.iconGoNext);
        locIcon = (ImageView) findViewById(R.id.locIcon);
        layoutAddAddress.setOnClickListener(this);
        locIcon.setOnClickListener(this);
        iconGoNext.setOnClickListener(this);

        tvNameTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvName.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvMobileTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        // tvMobile.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvEmailTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvEmail.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        btnAddAddress.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvPickAddress.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_bold.ttf"));
        btnAddAddress.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.layoutAddAddress:
                Intent intent = new Intent(mContext, ManageAddressActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("FromWhere", "PickAddressActivity");
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.locIcon:
               // UtilityMethod.goNextClass(mContext, ReviewOrderActivity.class);
                onBackPressed();
                break;
            case R.id.btnAddAddress:
                Intent intent2 = new Intent(mContext, ManageAddressActivity.class);
                Bundle bundle2 = new Bundle();
                bundle2.putString("FromWhere", "PickAddressActivity");
                intent2.putExtras(bundle2);
                startActivity(intent2);
                break;

            case R.id.iconGoNext:
                if (!tvMobile.getText().toString().equals("") && !tvName.getText().toString().equals("")) {
                    startActivity(new Intent(mContext, PaymentActivity.class));
                } else {
                    UtilityMethod.showAlertBox(mContext, "Please select delivery location");
                }
                break;

        }
    }
    public void setAddressList(ArrayList<SavedAddressPojo> list, String addressID) {
        recycler_addressList = findViewById(R.id.recycler_addressList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recycler_addressList.setLayoutManager(linearLayoutManager);
        recycler_addressList.setHasFixedSize(true);
        PickAddressAdapter adapter = new PickAddressAdapter(mContext, list);
        recycler_addressList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void sendDataToActivity(String name, String mobile, String email) {
        tvName.setText(name);
        tvMobile.setText(mobile);
        tvEmail.setText(email);


    }
}
