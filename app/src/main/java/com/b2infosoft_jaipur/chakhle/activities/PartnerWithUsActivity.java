package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.pojo.CityPojo;
import com.b2infosoft_jaipur.chakhle.pojo.StatePojo;
import com.b2infosoft_jaipur.chakhle.useful.ConnectionDetector;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.useful.Validations;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PartnerWithUsActivity extends AppCompatActivity implements View.OnClickListener {


    EditText edtPhoneNumber, edtFullName, edtEmail, edtWebsite, edtRestaurantName, edtRestaurantAddress, edtLandmark, edtPincode, edtAddress;
    Spinner spinState, spinCity;
    TextView tvSignOrReg, tvTC, tvAgree, tvDone;
    CheckBox chkTC;
    Context mContext;
    ConnectionDetector connectionDetector;
    ImageView locIcon;

    String StateID = "";
    String CityID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partner_with_us);
        mContext = PartnerWithUsActivity.this;
        initView();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       // UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);
    }

    private void initView() {
        connectionDetector = new ConnectionDetector(mContext);
        locIcon = (ImageView) findViewById(R.id.locIcon);
        edtPhoneNumber = (EditText) findViewById(R.id.edtPhoneNumber);
        edtFullName = (EditText) findViewById(R.id.edtFullName);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtWebsite = (EditText) findViewById(R.id.edtWebsite);
        edtRestaurantName = (EditText) findViewById(R.id.edtRestaurantName);
        edtRestaurantAddress = (EditText) findViewById(R.id.edtRestaurantAddress);
        edtLandmark = (EditText) findViewById(R.id.edtLandmark);
        edtPincode = (EditText) findViewById(R.id.edtPincode);
        edtAddress = (EditText) findViewById(R.id.edtAddress);
        spinState = (Spinner) findViewById(R.id.spinState);
        spinCity = (Spinner) findViewById(R.id.spinCity);
        tvSignOrReg = (TextView) findViewById(R.id.tvSignOrReg);
        tvTC = (TextView) findViewById(R.id.tvTC);
        tvTC.setPaintFlags(tvTC.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvTC.setText("T&C's");
        tvAgree = (TextView) findViewById(R.id.tvAgree);
        tvDone = (TextView) findViewById(R.id.tvDone);
        chkTC = (CheckBox) findViewById(R.id.chkTC);

        tvDone.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_light.ttf"));
        edtFullName.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtEmail.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtRestaurantName.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtRestaurantAddress.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtAddress.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtLandmark.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        tvSignOrReg.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvAgree.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        // StatePojo.getStateList(mContext, "partnerWithUs");

        tvDone.setOnClickListener(this);
        locIcon.setOnClickListener(this);
    }

   /* public void setStateAdapter(final ArrayList<StatePojo> statePojoList) {
        List<String> stateList = new ArrayList<String>();
        stateList.add("Select State");
        for (int i = 0; i < statePojoList.size(); i++) {
            stateList.add(statePojoList.get(i).StateName);
        }
        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stateList);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinState.setAdapter(stateAdapter);
        try {
            for (int j = 0; j < statePojoList.size(); j++) {

                spinState.setSelection(stateAdapter.getPosition(statePojoList.get(j).StateName));
                StateID = statePojoList.get(j).StateID;


            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        spinState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String txt = adapterView.getItemAtPosition(i).toString();
                if (!txt.equals("Select State")) {
                    StateID = statePojoList.get(i - 1).StateID;
                    Log.d("StateID>>>", StateID);
                    CityPojo.getCityList(mContext, StateID, "partnerWithUs");

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setCityAdapter(final ArrayList<CityPojo> cityPojoList) {
        List<String> cityList = new ArrayList<String>();
        cityList.add("Select City");
        for (int i = 0; i < cityPojoList.size(); i++) {
            cityList.add(cityPojoList.get(i).CityName);
        }
        ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cityList);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinCity.setAdapter(cityAdapter);
        try {
            for (int j = 0; j < cityPojoList.size(); j++) {

                spinCity.setSelection(cityAdapter.getPosition(cityPojoList.get(j).CityName));
                CityID = cityPojoList.get(j).CityID;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        spinCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String txt = adapterView.getItemAtPosition(i).toString();
                if (!txt.equals("Select City")) {
                    CityID = cityPojoList.get(i - 1).CityID;
                    Log.d("CityID>>>", CityID);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }*/

    /****************************************State And City Adapter Set ont Spinner****************************************************/
    public void setStateAdapter(final ArrayList<StatePojo> stateList) {
        Log.d("StateList>>>", "" + stateList.size());
        final ArrayList<String> list = new ArrayList<>();
        list.add("Select State");

        for (int i = 0; i < stateList.size(); i++) {

            list.add(stateList.get(i).StateName);

        }


        ArrayAdapter<String> stateSpinnAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, list);
        stateSpinnAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spinState.setAdapter(stateSpinnAdapter);


        spinState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                String selectedState = parent.getItemAtPosition(position).toString();
                if (!selectedState.equals("Select State")) {
                    StateID = stateList.get(position - 1).StateID;
                    Log.d("StateID>>>", StateID);
                    CityPojo.getCityList(mContext, StateID, "partnerWithUs");
                } else {
                    StateID = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void setCityAdapter(final ArrayList<CityPojo> cityList) {
        Log.d("CityList>>>", "" + cityList.size());
        ArrayList<String> list = new ArrayList<>();
        list.add("select City");

        for (int i = 0; i < cityList.size(); i++) {

            list.add(cityList.get(i).CityName);

        }

        ArrayAdapter<String> stateSpinnAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, list);
        stateSpinnAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spinCity.setAdapter(stateSpinnAdapter);
        stateSpinnAdapter.notifyDataSetChanged();


        spinCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String selectedCity = parent.getItemAtPosition(position).toString();
                if (!selectedCity.equals("select City")) {

                    CityID = cityList.get(position - 1).CityID;
                    Log.d("CityID>>>", CityID);
                } else {
                    CityID = "";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    /****************************************State And City Adapter Set ont Spinner***********************************************/
    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tvDone:
                if (connectionDetector.isConnectingToInternet()) {
                    if (!edtFullName.getText().toString().equals("")
                            && !edtRestaurantName.getText().toString().equals("")
                            && !edtRestaurantAddress.getText().toString().equals("")
                            && !edtPhoneNumber.getText().toString().equals("")
                            && !edtPincode.getText().toString().equals("")
                            //   && !StateID.equals("")
                            //   && !CityID.equals("")
                            && !edtAddress.getText().toString().equals("")
                            && chkTC.isChecked()) {

                        if (Validations.isValidMobile(edtPhoneNumber, "Please Enter valid Mobile Number") &&
                                Validations.isValidPin(edtPincode)) {
                            PartenerWithUs();
                        }

                    } else {
                        UtilityMethod.showAlertBox(mContext, "Please fill all fields");
                    }
                } else {
                    connectionDetector.showAlertDialog(mContext, "No Internet Connection", "You don't have internet connection.", true);

                }
                break;
            case R.id.locIcon:
               // startActivity(new Intent(mContext, RestaurantListActivity.class));
                onBackPressed();
                break;

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    private void PartenerWithUs() {

        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, AppGlobal.ApiCallWaitMsg, true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    //{"message":"Request has been submited","success":true,"status":200}
                    if (mainObj.getString("success").equals("true")) {
                        UtilityMethod.showAlertBoxwithIntent(mContext, "Congratulations! Now you are in CHAKLE Family", RestaurantListActivity.class);
                    } else {
                        UtilityMethod.showAlertBox(mContext, "Failed! Please try Again");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        serviceCaller.addNameValuePair("mobile", edtPhoneNumber.getText().toString().trim());
        serviceCaller.addNameValuePair("name", edtFullName.getText().toString().trim());
        serviceCaller.addNameValuePair("email", edtEmail.getText().toString().trim());
        serviceCaller.addNameValuePair("restaurant_name", edtRestaurantName.getText().toString().trim());
        serviceCaller.addNameValuePair("address", edtRestaurantAddress.getText().toString().trim());
        serviceCaller.addNameValuePair("website_optional", edtWebsite.getText().toString().trim());
        serviceCaller.addNameValuePair("landmark", edtLandmark.getText().toString().trim());
        serviceCaller.addNameValuePair("pincode", edtPincode.getText().toString().trim());
        //serviceCaller.addNameValuePair("state_id", StateID);
        // serviceCaller.addNameValuePair("city_id", CityID);
        serviceCaller.addNameValuePair("city", edtAddress.getText().toString());
        String url = AppGlobal.PartnerWithUs;
        Log.d("url>>", url);
        serviceCaller.execute(url);

    }
}
