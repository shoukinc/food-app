package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.services.Config;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.ConnectionDetector;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONException;
import org.json.JSONObject;

public class OtpActivity extends AppCompatActivity {

    TextView tvOtpVerification, tvEnterOTP, tvVerify,resend_otp;
    EditText edtOTP;
    Context mContext;
    ConnectionDetector connectionDetector;
    SessionManager sessionManager;
    ImageView locIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        mContext = OtpActivity.this;
        initView();

    }

    private void initView() {

        connectionDetector = new ConnectionDetector(mContext);
        sessionManager = new SessionManager(mContext);
        locIcon = (ImageView) findViewById(R.id.locIcon);
        tvOtpVerification = (TextView) findViewById(R.id.tvOtpVerification);
        tvEnterOTP = (TextView) findViewById(R.id.tvEnterOTP);
        tvVerify = (TextView) findViewById(R.id.tvVerify);
        resend_otp = (TextView) findViewById(R.id.resend_otp);
        edtOTP = (EditText) findViewById(R.id.edtOTP);
        tvOtpVerification.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        //edtOTP.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        Log.d("userID>>>", AppGlobal.UserID);

        tvVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, AppGlobal.ApiCallWaitMsg, true) {
                    @Override
                    public void handleResponse(String response) {
                        Log.d("response", response);
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            //{"data":[],"message":"Register successfull.","success":true,"status":200}
                            if (mainObj.getString("success").equals("true")) {
                                // UtilityMethod.showAlertBox(mContext, "Please verify OTP");
                                JSONObject dataObject = mainObj.getJSONObject("data");
                                JSONObject userObject = dataObject.getJSONObject("User");
                                sessionManager.createLoginSession(userObject.getString("id"), userObject.getString("firstname"), userObject.getString("lastname"), userObject.getString("mobile"), userObject.getString("email"), "", "", "", "", userObject.getString("referral_code"), "", "");
                                if (AppGlobal.FavFrom.equals("RestaurentDetails")) {
                                    Bundle bundle = new Bundle();
                                    Intent intent = new Intent(mContext, RestaurentDetailsActivity.class);
                                    bundle.putString("FromWhere", "LogingActivity");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                } else if (AppGlobal.FavFrom.equals("RestaurentList")) {
                                    Intent intent = new Intent(mContext, RestaurentDetailsActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                } else {
                                    UtilityMethod.showAlertBoxwithIntentClearHistory(mContext, "Registration successfull", RestaurantListActivity.class);

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                serviceCaller.addNameValuePair("mobile", AppGlobal.MobileNo);
                serviceCaller.addNameValuePair("user_id", AppGlobal.UserID);
                serviceCaller.addNameValuePair("otp", edtOTP.getText().toString());
                String url = AppGlobal.OtpVerification;
                Log.d("url>>", url);
                serviceCaller.execute(url);
            }
        });

        resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, AppGlobal.ApiCallWaitMsg, true) {
                    @Override
                    public void handleResponse(String response) {
                        Log.d("response", response);
                       
                    }
                };

                serviceCaller.addNameValuePair("user_id", AppGlobal.UserID);
                String url = AppGlobal.OtpResend;
                Log.d("url>>", url);
                serviceCaller.execute(url);
            }
        });

        locIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtilityMethod.goNextClass(mContext, RestaurentDetailsActivity.class);

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        UtilityMethod.goNextClass(mContext, RestaurentDetailsActivity.class);
    }
}
