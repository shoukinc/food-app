package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.RestaurentListAdapter;
import com.b2infosoft_jaipur.chakhle.adapter.SearchListAdapter;
import com.b2infosoft_jaipur.chakhle.fragments.CallFragment;
import com.b2infosoft_jaipur.chakhle.fragments.CartValueInterface;
import com.b2infosoft_jaipur.chakhle.fragments.NewCartDataFill;
import com.b2infosoft_jaipur.chakhle.pojo.CartFillPojo;
import com.b2infosoft_jaipur.chakhle.pojo.RestaurentDetailsPojo;
import com.b2infosoft_jaipur.chakhle.pojo.ResturentListPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.sessions.SessionNew;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.util.ArrayList;
import java.util.Locale;

public class SearchActivity extends AppCompatActivity implements CartValueInterface, CallFragment, NewCartDataFill {

    EditText edtSearchBox;
    ImageView locIcon, fabCart;
    RecyclerView recycler_searchList;
    Context mContext;
    TextView tvCartCount;
    SessionManager sessionManager;
    SessionNew sessionManagerNew;
    SearchListAdapter adapter;
    Intent intent;
    String fromWhere = "";
    View v;
    RestaurentListAdapter restaurentListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        mContext = SearchActivity.this;
        initView();
    }

    private void initView() {
        sessionManager = new SessionManager(mContext);
        sessionManagerNew = new SessionNew(mContext);
        edtSearchBox =   findViewById(R.id.edtSearchBox);
        locIcon =   findViewById(R.id.locIcon);
        fabCart =  findViewById(R.id.fabCart);
        tvCartCount =  findViewById(R.id.tvCartCount);
        edtSearchBox.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        intent = getIntent();
        if (intent != null) {
            fromWhere = intent.getStringExtra("FromWhere");
        }
        if (fromWhere.equals("RestaurentDetail")) {

            if (AppGlobal.CartValue == 0) {
                fabCart.setVisibility(View.GONE);
                tvCartCount.setVisibility(View.GONE);
            } else {
                fabCart.setVisibility(View.VISIBLE);
                tvCartCount.setVisibility(View.VISIBLE);
                tvCartCount.setText("" + AppGlobal.CartValue);
            }

            // }
            Log.d("RestaurantCategoryList>>>", "" + AppGlobal.RestaurentProductList.size());
            recycler_searchList = (RecyclerView) findViewById(R.id.recycler_searchList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            recycler_searchList.setLayoutManager(linearLayoutManager);
            recycler_searchList.setHasFixedSize(true);
            adapter = new SearchListAdapter(mContext, AppGlobal.RestaurentProductList);
            recycler_searchList.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                Log.d("mediumQty", AppGlobal.RestaurentProductList.get(i).mediumQty + " quenty");
                Log.d("halfQty", AppGlobal.RestaurentProductList.get(i).halfQty + " quenty");
                Log.d("largeQty", AppGlobal.RestaurentProductList.get(i).largeQty + " quenty");
                Log.d("fullQty", AppGlobal.RestaurentProductList.get(i).fullQty + " quenty");
            }

            edtSearchBox.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                    //  String text = edt_searchBox.getText().toString().toLowerCase(Locale.getDefault());
                    //  vidhayakListAdapter.filter(text);
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1,
                                              int arg2, int arg3) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onTextChanged(CharSequence query, int arg1, int arg2,
                                          int arg3) {
                    query = edtSearchBox.getText().toString().toLowerCase(Locale.getDefault());

                    ArrayList<RestaurentDetailsPojo> filteredList = new ArrayList<>();
                    int count = 0;

                    for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {

                        final String text = AppGlobal.RestaurentProductList.get(i).Product_name.toLowerCase();
                        if (text.contains(query)) {
                            //String videoId, String memberId, String videoTitleEn, String videoTitleHi, String video, String addedDate, String isActive, int videoNo
                            filteredList.add(new RestaurentDetailsPojo(
                                    AppGlobal.RestaurentProductList.get(i).name,
                                    AppGlobal.RestaurentProductList.get(i).is_favorite,
                                    AppGlobal.RestaurentProductList.get(i).restaurant_id,
                                    AppGlobal.RestaurentProductList.get(i).Product_id,
                                    AppGlobal.RestaurentProductList.get(i).Product_name,
                                    AppGlobal.RestaurentProductList.get(i).Product_description,
                                    AppGlobal.RestaurentProductList.get(i).Product_image1,
                                    AppGlobal.RestaurentProductList.get(i).Product_vegproduct,
                                    AppGlobal.RestaurentProductList.get(i).Product_slug,
                                    AppGlobal.RestaurentProductList.get(i).Product_status,
                                    AppGlobal.RestaurentProductList.get(i).Product_price,
                                    AppGlobal.RestaurentProductList.get(i).Product_category_id,
                                    AppGlobal.RestaurentProductList.get(i).count, 0, 0,
                                    AppGlobal.RestaurentProductList.get(i).discount,
                                    AppGlobal.RestaurentProductList.get(i).half,
                                    AppGlobal.RestaurentProductList.get(i).medium,
                                    AppGlobal.RestaurentProductList.get(i).full,
                                    AppGlobal.RestaurentProductList.get(i).large,
                                    AppGlobal.RestaurentProductList.get(i).half_label,
                                    AppGlobal.RestaurentProductList.get(i).medium_label,
                                    AppGlobal.RestaurentProductList.get(i).full_label,
                                    AppGlobal.RestaurentProductList.get(i).large_label,
                                    AppGlobal.RestaurentProductList.get(i).in_time,
                                    AppGlobal.RestaurentProductList.get(i).out_time,
                                    AppGlobal.RestaurentProductList.get(i).minimum_purchase,
                                    AppGlobal.RestaurentProductList.get(i).extra_charge,
                                    AppGlobal.RestaurentProductList.get(i).container_charge,
                                    AppGlobal.RestaurentProductList.get(i).halfQty,
                                    AppGlobal.RestaurentProductList.get(i).mediumQty,
                                    AppGlobal.RestaurentProductList.get(i).fullQty,
                                    AppGlobal.RestaurentProductList.get(i).largeQty,
                                    AppGlobal.RestaurentProductList.get(i).discountType
                                    , AppGlobal.RestaurentProductList.get(i).Restaddress
                                    , AppGlobal.RestaurentProductList.get(i).let_nite_charge
                                    , AppGlobal.RestaurentProductList.get(i).tax_percentage
                                    , AppGlobal.RestaurentProductList.get(i).RestInTime
                                    , AppGlobal.RestaurentProductList.get(i).Restoff_time_1

                            ));

                            Log.d("mediumQty", AppGlobal.RestaurentProductList.get(i).mediumQty + " quenty");
                            Log.d("halfQty", AppGlobal.RestaurentProductList.get(i).halfQty + " quenty");
                            Log.d("largeQty", AppGlobal.RestaurentProductList.get(i).largeQty + " quenty");
                            Log.d("fullQty", AppGlobal.RestaurentProductList.get(i).fullQty + " quenty");
                        }
                    }
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    recycler_searchList.setLayoutManager(layoutManager);
                    adapter = new SearchListAdapter(mContext, filteredList);
                    recycler_searchList.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            });
        } else {
           // ResturentListPojo.getResturentList(v, mContext, AppGlobal.LocationAddress, "SearchActivity", "0");
        }
        fabCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RestaurentDetailsPojo restaurentDetailsPojo = null;
                AppGlobal.mainCartList.clear();
                AppGlobal.GlobalCartList.clear();
                if (sessionManager.isLoggedIn()) {
                    for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                        int Quentity = 0, GlobalQuentity = 0;
                        int HalfQty = 0;
                        double HalfAmount = 0;
                        double HalfTotalAmount = 0;
                        int MediumQty = 0;
                        double MediumAmt = 0;
                        double MediumTotalAmt = 0;
                        int FullQty = 0;
                        double FullAmt = 0;
                        double FullTotalAmt = 0;
                        int LargeQty = 0;
                        double LargeAmt = 0;
                        double LargeTotalAmt = 0;

                        if (AppGlobal.RestaurentProductList.get(i).halfQty != 0) {
                            HalfQty = AppGlobal.RestaurentProductList.get(i).halfQty;
                            HalfAmount = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).half);
                            HalfTotalAmount = HalfQty * HalfAmount;
                            Log.d("HalfTotalAmount>>>", "" + HalfTotalAmount);

                        }
                        if (AppGlobal.RestaurentProductList.get(i).mediumQty != 0) {
                            MediumQty = AppGlobal.RestaurentProductList.get(i).mediumQty;
                            MediumAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).medium);
                            MediumTotalAmt = MediumQty * MediumAmt;
                            Log.d("MediumTotalAmt>>>", "" + MediumTotalAmt);
                        }
                        if (AppGlobal.RestaurentProductList.get(i).fullQty != 0) {
                            FullQty = AppGlobal.RestaurentProductList.get(i).fullQty;
                            FullAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).full);
                            FullTotalAmt = FullQty * FullAmt;
                            Log.d("FullTotalAmt>>>", "" + FullTotalAmt);
                        }
                        if (AppGlobal.RestaurentProductList.get(i).largeQty != 0) {
                            LargeQty = AppGlobal.RestaurentProductList.get(i).largeQty;
                            LargeAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).half);
                            LargeTotalAmt = LargeQty * LargeAmt;
                            Log.d("LargeTotalAmt>>>", "" + LargeTotalAmt);
                        }
                        Quentity = AppGlobal.RestaurentProductList.get(i).getCount();
                        Log.d("Quentity>>>", "" + Quentity);
                        int total = (AppGlobal.RestaurentProductList.get(i).afterDiscountValue) * Quentity;
                        int productPrice = Integer.parseInt((AppGlobal.RestaurentProductList.get(i).Product_price));
                        int totalProductPrice = productPrice * Quentity;
                        String minimum_purchase = AppGlobal.RestaurentProductList.get(i).minimum_purchase;
                        String extra_charge = AppGlobal.RestaurentProductList.get(i).extra_charge;
                        Log.d("minimum_purchase>>", minimum_purchase);
                        Log.d("extra_charge>>", extra_charge);
                        double container = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).container_charge);
                        Log.d("Quentity>>>", "" + Quentity);
                        container = container * Quentity;
                        if (HalfQty != 0 || MediumQty != 0 || LargeQty != 0 || FullQty != 0) {
                            AppGlobal.mainCartList.add(new CartFillPojo(
                                    AppGlobal.RestaurentProductList.get(i).Product_id,
                                    Quentity,
                                    AppGlobal.RestaurentProductList.get(i).Product_name,
                                    AppGlobal.RestaurentProductList.get(i).Product_price,
                                    total,
                                    AppGlobal.RestaurentProductList.get(i).restaurant_id,
                                    sessionManager.getValuesSession(SessionManager.Key_UserID),
                                    AppGlobal.RestaurentProductList.get(i).Product_vegproduct,
                                    AppGlobal.RestaurentProductList.get(i).afterDiscountValue,
                                    totalProductPrice, AppGlobal.RestaurentProductList.get(i).name,
                                    AppGlobal.RestaurentProductList.get(i).discount,
                                    String.valueOf(container),
                                    AppGlobal.RestaurentProductList.get(i).minimum_purchase,
                                    AppGlobal.RestaurentProductList.get(i).extra_charge,
                                    AppGlobal.RestaurentProductList.get(i).container_charge,
                                    HalfQty, HalfAmount, HalfTotalAmount,
                                    FullQty, FullAmt, FullTotalAmt,
                                    LargeQty, LargeAmt, LargeTotalAmt,
                                    MediumQty, MediumAmt, MediumTotalAmt,
                                    AppGlobal.RestaurentProductList.get(i).discountType
                                    , AppGlobal.RestaurentProductList.get(i).half_label
                                    , AppGlobal.RestaurentProductList.get(i).medium_label
                                    , AppGlobal.RestaurentProductList.get(i).full_label
                                    , AppGlobal.RestaurentProductList.get(i).large_label
                                    , AppGlobal.RestaurentProductList.get(i).let_nite_charge
                                    , AppGlobal.RestaurentProductList.get(i).tax_percentage
                                    , AppGlobal.RestaurentProductList.get(i).RestInTime
                                    , AppGlobal.RestaurentProductList.get(i).Restoff_time_1
                            ));
                        }
                    }
                    if (!AppGlobal.mainCartList.isEmpty()) {
                        sessionManager.clearArrayList(mContext);
                        sessionManager.saveCartList(mContext, AppGlobal.mainCartList);
                        sessionManagerNew.clearArrayList(mContext);
                        sessionManagerNew.saveCartList(mContext, AppGlobal.mainCartList);
                    } else {
                        sessionManagerNew.clearArrayList(mContext);
                        sessionManager.clearArrayList(mContext);
                    }
                    Log.d("MainCartList>>>", "" + AppGlobal.mainCartList.size());

                    startActivity(new Intent(mContext, ReviewOrderActivity.class));
                } else {
                    startActivity(new Intent(mContext, LoginActivity.class));
                }
            }
        });

        locIcon.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        RestaurentDetailsPojo restaurentDetailsPojo = null;
        AppGlobal.mainCartList.clear();
        AppGlobal.GlobalCartList.clear();
        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            int Quentity = 0, GlobalQuentity = 0;
            int HalfQty = 0;
            double HalfAmount = 0;
            double HalfTotalAmount = 0;
            int MediumQty = 0;
            double MediumAmt = 0;
            double MediumTotalAmt = 0;
            int FullQty = 0;
            double FullAmt = 0;
            double FullTotalAmt = 0;
            int LargeQty = 0;
            double LargeAmt = 0;
            double LargeTotalAmt = 0;

            if (AppGlobal.RestaurentProductList.get(i).halfQty != 0) {
                HalfQty = AppGlobal.RestaurentProductList.get(i).halfQty;
                HalfAmount = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).half);
                HalfTotalAmount = HalfQty * HalfAmount;
                Log.d("HalfTotalAmount>>>", "" + HalfTotalAmount);

            }
            if (AppGlobal.RestaurentProductList.get(i).mediumQty != 0) {
                MediumQty = AppGlobal.RestaurentProductList.get(i).mediumQty;
                MediumAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).medium);
                MediumTotalAmt = MediumQty * MediumAmt;
                Log.d("MediumTotalAmt>>>", "" + MediumTotalAmt);
            }
            if (AppGlobal.RestaurentProductList.get(i).fullQty != 0) {
                FullQty = AppGlobal.RestaurentProductList.get(i).fullQty;
                FullAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).full);
                FullTotalAmt = FullQty * FullAmt;
                Log.d("FullTotalAmt>>>", "" + FullTotalAmt);
            }
            if (AppGlobal.RestaurentProductList.get(i).largeQty != 0) {
                LargeQty = AppGlobal.RestaurentProductList.get(i).largeQty;
                LargeAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).large);
                LargeTotalAmt = LargeQty * LargeAmt;
                Log.d("LargeTotalAmt>>>", "" + LargeTotalAmt);
            }
            Quentity = AppGlobal.RestaurentProductList.get(i).getCount();
            Log.d("Quentity>>>", "" + Quentity);
            int total = (AppGlobal.RestaurentProductList.get(i).afterDiscountValue) * Quentity;
            int productPrice = Integer.parseInt((AppGlobal.RestaurentProductList.get(i).Product_price));
            int totalProductPrice = productPrice * Quentity;
            String minimum_purchase = AppGlobal.RestaurentProductList.get(i).minimum_purchase;
            String extra_charge = AppGlobal.RestaurentProductList.get(i).extra_charge;
            Log.d("minimum_purchase>>", minimum_purchase);
            Log.d("extra_charge>>", extra_charge);
            double container = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).container_charge);
            Log.d("Quentity>>>", "" + Quentity);
            container = container * Quentity;
            if (HalfQty != 0 || MediumQty != 0 || LargeQty != 0 || FullQty != 0) {
                AppGlobal.mainCartList.add(new CartFillPojo(
                        AppGlobal.RestaurentProductList.get(i).Product_id,
                        Quentity,
                        AppGlobal.RestaurentProductList.get(i).Product_name,
                        AppGlobal.RestaurentProductList.get(i).Product_price,
                        total,
                        AppGlobal.RestaurentProductList.get(i).restaurant_id,
                        sessionManager.getValuesSession(SessionManager.Key_UserID),
                        AppGlobal.RestaurentProductList.get(i).Product_vegproduct,
                        AppGlobal.RestaurentProductList.get(i).afterDiscountValue,
                        totalProductPrice, AppGlobal.RestaurentProductList.get(i).name,
                        AppGlobal.RestaurentProductList.get(i).discount,
                        String.valueOf(container),
                        AppGlobal.RestaurentProductList.get(i).minimum_purchase,
                        AppGlobal.RestaurentProductList.get(i).extra_charge,
                        AppGlobal.RestaurentProductList.get(i).container_charge,
                        HalfQty, HalfAmount, HalfTotalAmount,
                        FullQty, FullAmt, FullTotalAmt,
                        LargeQty, LargeAmt, LargeTotalAmt,
                        MediumQty, MediumAmt, MediumTotalAmt,
                        AppGlobal.RestaurentProductList.get(i).discountType
                        , AppGlobal.RestaurentProductList.get(i).half_label
                        , AppGlobal.RestaurentProductList.get(i).medium_label
                        , AppGlobal.RestaurentProductList.get(i).full_label
                        , AppGlobal.RestaurentProductList.get(i).large_label
                        , AppGlobal.RestaurentProductList.get(i).let_nite_charge
                        , AppGlobal.RestaurentProductList.get(i).tax_percentage
                        , AppGlobal.RestaurentProductList.get(i).RestInTime
                        , AppGlobal.RestaurentProductList.get(i).Restoff_time_1
                ));
            }
        }

        if (!AppGlobal.mainCartList.isEmpty()) {
            sessionManager.clearArrayList(mContext);
            sessionManager.saveCartList(mContext, AppGlobal.mainCartList);
            sessionManagerNew.clearArrayList(mContext);
            sessionManagerNew.saveCartList(mContext, AppGlobal.mainCartList);
        } else {
            sessionManagerNew.clearArrayList(mContext);
            sessionManager.clearArrayList(mContext);
        }
        Log.d("MainCartList>>>", "" + AppGlobal.mainCartList.size());

        if (fromWhere.equals("RestaurentDetail")) {
            UtilityMethod.goNextClass(mContext, RestaurentDetailsActivity.class);
        } else {
            // UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);

            Intent intent = new Intent(mContext, RestaurantListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle bundle = new Bundle();
            bundle.putString("ClassName", "RestaurentDetailsActivity");
            bundle.putString("AddToFav", "true");
            intent.putExtras(bundle);
            startActivity(intent);
        }

    }


    @Override
    public void setCartData(int cartData, String operator, String productID) {
        Log.d("cartValue>>>", "" + cartData);
        int tvValue = 0;
        int cart = 0;
        AppGlobal.ProductIDList.add(productID);

        if (operator.equals("+")) {
            if (!tvCartCount.getText().toString().equals("")) {
                tvValue = Integer.parseInt(tvCartCount.getText().toString());
            }
            tvValue = tvValue + 1;
            fabCart.setVisibility(View.VISIBLE);
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText("" + tvValue);
        } else if (operator.equals("-")) {
            if (!tvCartCount.getText().toString().equals("")) {
                AppGlobal.ProductIDList.remove(productID);
                tvValue = Integer.parseInt(tvCartCount.getText().toString());
                tvValue = tvValue - 1;
                fabCart.setVisibility(View.VISIBLE);
                tvCartCount.setVisibility(View.VISIBLE);
                tvCartCount.setText("" + tvValue);
            }
        }
        try {
            if (operator.equals("check") && Integer.parseInt(tvCartCount.getText().toString()) == 0) {
                AppGlobal.ProductIDList.clear();
                fabCart.setVisibility(View.GONE);
                tvCartCount.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        AppGlobal.searchCartValue = Integer.parseInt(tvCartCount.getText().toString());

    }

    public void setRestaurentList(final ArrayList<ResturentListPojo> list, final ArrayList<ResturentListPojo> discountList) {
        Log.d("list>>>", "" + list.size());
        recycler_searchList = (RecyclerView) findViewById(R.id.recycler_searchList);
        final RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        //ScrollingLinearLayoutManager scrollingLinearLayoutManager = new ScrollingLinearLayoutManager(mContext);
        recycler_searchList.setLayoutManager(linearLayoutManager);
        restaurentListAdapter = new RestaurentListAdapter(mContext, list, discountList);
        recycler_searchList.setAdapter(restaurentListAdapter);
        recycler_searchList.setNestedScrollingEnabled(false);
        restaurentListAdapter.notifyDataSetChanged();
        edtSearchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                //  String text = edt_searchBox.getText().toString().toLowerCase(Locale.getDefault());
                //  vidhayakListAdapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence query, int arg1, int arg2,
                                      int arg3) {
                query = edtSearchBox.getText().toString().toLowerCase(Locale.getDefault());

                ArrayList<ResturentListPojo> filteredList = new ArrayList<>();

                for (int i = 0; i < list.size(); i++) {

                    final String text = list.get(i).name.toLowerCase();
                    Log.d("txt>>>", text);
                    if (text.contains(query)) {
                        filteredList.add(new ResturentListPojo(
                                list.get(i).id,
                                list.get(i).name,
                                list.get(i).slug,
                                list.get(i).moblie,
                                list.get(i).email,
                                list.get(i).logo_image,
                                list.get(i).description,
                                list.get(i).address,
                                list.get(i).minimum_purchase,
                                list.get(i).delivery_time,
                                list.get(i).chakhle_assured,
                                list.get(i).pure_veg_dishes,
                                list.get(i).status,
                                list.get(i).distance,
                                list.get(i).cuisine,
                                list.get(i).TotalRating,
                                list.get(i).TotalReview,
                                list.get(i).banner_image,
                                list.get(i).open_time,
                                list.get(i).close_time
                        ));
                    }
                }
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                recycler_searchList.setLayoutManager(layoutManager);
                restaurentListAdapter = new RestaurentListAdapter(mContext, filteredList, discountList);
                recycler_searchList.setAdapter(restaurentListAdapter);
                recycler_searchList.setNestedScrollingEnabled(false);
                restaurentListAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void setFragment(String fragmentName) {
        if (fragmentName.equals("RestaurentDetailsFragment")) {
            AppGlobal.IamFrom = "SearchRestorent";
            Intent intent = new Intent(mContext, RestaurentDetailsActivity.class);
            //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void setCartValue(int value) {

    }


    @Override
    public void newCartFillData(ArrayList<RestaurentDetailsPojo> RestaurentProductList) {
        int HalfQty = 0;
        double HalfAmount = 0;
        double HalfTotalAmount = 0;
        int MediumQty = 0;
        double MediumAmt = 0;
        double MediumTotalAmt = 0;
        int FullQty = 0;
        double FullAmt = 0;
        double FullTotalAmt = 0;
        int LargeQty = 0;
        int cartQty = 0;
        double LargeAmt = 0;
        double LargeTotalAmt = 0;
        for (int i = 0; i < RestaurentProductList.size(); i++) {
            HalfQty = 0;
            MediumQty = 0;
            MediumTotalAmt = 0;
            FullQty = 0;
            FullAmt = 0;
            FullTotalAmt = 0;
            LargeQty = 0;
            LargeAmt = 0;
            LargeTotalAmt = 0;

            if (RestaurentProductList.get(i).halfQty != 0) {
                HalfQty = RestaurentProductList.get(i).halfQty;
                HalfAmount = Double.parseDouble(RestaurentProductList.get(i).half);
                HalfTotalAmount = HalfQty * HalfAmount;
                Log.d("HalfTotalAmount>>>", "" + HalfTotalAmount);
                cartQty = cartQty + HalfQty;
            }
            if (RestaurentProductList.get(i).mediumQty != 0) {
                MediumQty = RestaurentProductList.get(i).mediumQty;
                MediumAmt = Double.parseDouble(RestaurentProductList.get(i).medium);
                MediumTotalAmt = MediumQty * MediumAmt;
                cartQty = cartQty + MediumQty;
                Log.d("MediumTotalAmt>>>", "" + MediumTotalAmt);
            }
            if (RestaurentProductList.get(i).fullQty != 0) {
                FullQty = RestaurentProductList.get(i).fullQty;
                FullAmt = Double.parseDouble(RestaurentProductList.get(i).full);
                FullTotalAmt = FullQty * FullAmt;
                cartQty = cartQty + FullQty;

                Log.d("FullTotalAmt>>>", "" + FullTotalAmt);
            }
            if (RestaurentProductList.get(i).largeQty != 0) {
                LargeQty = RestaurentProductList.get(i).largeQty;
                LargeAmt = Double.parseDouble(RestaurentProductList.get(i).half);
                LargeTotalAmt = LargeQty * LargeAmt;
                cartQty = cartQty + LargeQty;
                Log.d("LargeTotalAmt>>>", "" + LargeTotalAmt);
            }

           /* int Quentity = 0, GlobalQuentity = 0;


            Quentity = AppGlobal.RestaurentProductList.get(i).getCount();
            Log.d("Quentity>>>", "" + Quentity);
            int total = (AppGlobal.RestaurentProductList.get(i).afterDiscountValue) * Quentity;
            int productPrice = Integer.parseInt((AppGlobal.RestaurentProductList.get(i).Product_price));
            int totalProductPrice = productPrice * Quentity;
            String minimum_purchase = AppGlobal.RestaurentProductList.get(i).minimum_purchase;
            String extra_charge = AppGlobal.RestaurentProductList.get(i).extra_charge;
            Log.d("minimum_purchase>>", minimum_purchase);
            Log.d("extra_charge>>", extra_charge);
            double container = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).container_charge);
            Log.d("Quentity>>>", "" + Quentity);
            container = container * Quentity;
            if (HalfQty != 0 || FullQty != 0 || LargeQty != 0 || MediumQty != 0) {
                AppGlobal.mainCartList.add(new CartFillPojo(
                        AppGlobal.RestaurentProductList.get(i).Product_id,
                        Quentity,
                        AppGlobal.RestaurentProductList.get(i).Product_name,
                        AppGlobal.RestaurentProductList.get(i).Product_price,
                        total,
                        AppGlobal.RestaurentProductList.get(i).restaurant_id,
                        sessionManager.getValuesSession(SessionManager.Key_UserID),
                        AppGlobal.RestaurentProductList.get(i).Product_vegproduct,
                        AppGlobal.RestaurentProductList.get(i).afterDiscountValue,
                        totalProductPrice, AppGlobal.RestaurentProductList.get(i).name,
                        AppGlobal.RestaurentProductList.get(i).discount,
                        String.valueOf(container),
                        AppGlobal.RestaurentProductList.get(i).minimum_purchase,
                        AppGlobal.RestaurentProductList.get(i).extra_charge,
                        AppGlobal.RestaurentProductList.get(i).container_charge,
                        HalfQty, HalfAmount, HalfTotalAmount,
                        FullQty, FullAmt, FullTotalAmt,
                        LargeQty, LargeAmt, LargeTotalAmt,
                        MediumQty, MediumAmt, MediumTotalAmt));
            }*/
        }


        if (cartQty != 0) {
            fabCart.setVisibility(View.VISIBLE);
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText("" + cartQty);
        } else {
            fabCart.setVisibility(View.GONE);
            tvCartCount.setVisibility(View.GONE);
            tvCartCount.setText("" + cartQty);
        }
        /*ArrayList<CartFillPojo> mainSessonCartList;
        mainSessonCartList = sessionManager.getCartList(mContext);
        if (!AppGlobal.mainCartList.isEmpty() && mainSessonCartList != null) {
            sessionManager.clearArrayList(mContext);
          //  sessionManager.saveCartList(mContext, AppGlobal.mainCartList);
            Log.d("CartQty>>>", "" + cartQty);
            if (cartQty != 0) {
                fabCart.setVisibility(View.VISIBLE);
                tvCartCount.setVisibility(View.VISIBLE);
                tvCartCount.setText("" + cartQty);
            } else {
                fabCart.setVisibility(View.GONE);
                tvCartCount.setVisibility(View.GONE);
                tvCartCount.setText("" + cartQty);
            }
        } else {
            sessionManager.saveCartList(mContext, AppGlobal.mainCartList);
            if (cartQty != 0) {
                fabCart.setVisibility(View.VISIBLE);
                tvCartCount.setVisibility(View.VISIBLE);
                tvCartCount.setText("" + cartQty);
            } else {
                fabCart.setVisibility(View.GONE);
                tvCartCount.setVisibility(View.GONE);
                tvCartCount.setText("" + cartQty);
            }
        }*/
    }


}
