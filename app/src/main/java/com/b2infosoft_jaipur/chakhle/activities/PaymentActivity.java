package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.sessions.SessionNew;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvPayment, tvCOD, tvPaytmTxt, tvPayOnline, tvYumCash, tvOnlineNetbanking, tvAddMoney, tvTotalCost, tvIncTax;
    ImageView locIcon, imgRightClick;
    Context mContext;
    LinearLayout layoutCOD, layoutPaytm, layoutPayOnline, layoutYumCash, layoutYumCashTxt;
    String paymentMode = "", tax_percentage = "";
    SessionManager sessionManager;
    SessionNew sessionManagerNew;

    TextView cashClick, onlineClick;

    RadioButton radioButtonOnline, radioButtonCash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        mContext = PaymentActivity.this;
        initView();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        UtilityMethod.goNextClass(mContext, PickAddressActivity.class);
    }

    private void initView() {
        sessionManager = new SessionManager(mContext);
        sessionManagerNew = new SessionNew(mContext);
        tvPayment = (TextView) findViewById(R.id.tvPayment);
        tvIncTax = (TextView) findViewById(R.id.tvIncTax);
        tvTotalCost = (TextView) findViewById(R.id.tvTotalCost);
        cashClick = (TextView) findViewById(R.id.cashClick);
        onlineClick = (TextView) findViewById(R.id.onlineClick);


        tvCOD = (TextView) findViewById(R.id.tvCOD);
        tvPaytmTxt = (TextView) findViewById(R.id.tvPaytmTxt);
        tvPayOnline = (TextView) findViewById(R.id.tvPayOnline);
        tvYumCash = (TextView) findViewById(R.id.tvYumCash);
        tvOnlineNetbanking = (TextView) findViewById(R.id.tvOnlineNetbanking);
        tvAddMoney = (TextView) findViewById(R.id.tvAddMoney);
        locIcon = (ImageView) findViewById(R.id.locIcon);
        layoutCOD = (LinearLayout) findViewById(R.id.layoutCOD);
        layoutPaytm = (LinearLayout) findViewById(R.id.layoutPaytm);
        layoutPayOnline = (LinearLayout) findViewById(R.id.layoutPayOnline);
        layoutYumCash = (LinearLayout) findViewById(R.id.layoutYumCash);
        layoutYumCashTxt = (LinearLayout) findViewById(R.id.layoutYumCashTxt);

        radioButtonOnline = (RadioButton) findViewById(R.id.radioButtonOnline);
        radioButtonCash = (RadioButton) findViewById(R.id.radioButtonCash);

        layoutCOD.setBackgroundColor(getResources().getColor(R.color.color_white));
        layoutPayOnline.setBackgroundColor(getResources().getColor(R.color.color_white));

        tvPayment.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvCOD.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvPaytmTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvPayOnline.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvYumCash.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvOnlineNetbanking.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvAddMoney.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_bold.ttf"));
        tvIncTax.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvTotalCost.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        imgRightClick = (ImageView) findViewById(R.id.imgRightClick);

        imgRightClick.setOnClickListener(this);
        /*layoutCOD.setOnClickListener(this);
        layoutPaytm.setOnClickListener(this);
        layoutPayOnline.setOnClickListener(this);
        layoutYumCash.setOnClickListener(this);
        layoutYumCashTxt.setOnClickListener(this);
        tvAddMoney.setOnClickListener(this);
        locIcon.setOnClickListener(this);*/
        cashClick.setOnClickListener(this);
        onlineClick.setOnClickListener(this);
        tvTotalCost.setText("Total Cost: " + AppGlobal.TotalCost + "/-");
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.locIcon:
                UtilityMethod.goNextClass(mContext, PickAddressActivity.class);
                break;
            case R.id.imgRightClick:

                if (paymentMode.equals("")) {

                    UtilityMethod.showAlertBox(mContext, "Please select payment mode.");

                } else {
                    bookOrder();
                }

                break;
            case R.id.cashClick:
                paymentMode = "cod";
                radioButtonCash.setChecked(true);
                radioButtonOnline.setChecked(false);

                break;
            case R.id.onlineClick:
                paymentMode = "paid";
                radioButtonCash.setChecked(false);
                radioButtonOnline.setChecked(true);
                break;
                /*
            case R.id.layoutCOD:
                paymentMode = "cod";
                layoutCOD.setBackgroundColor(getResources().getColor(R.color.color_white));
                layoutPaytm.setBackgroundColor(getResources().getColor(R.color.color_background));
                layoutPayOnline.setBackgroundColor(getResources().getColor(R.color.color_background));
                layoutYumCash.setBackgroundColor(getResources().getColor(R.color.color_background));
                radioButtonCash.setEnabled(true);
                radioButtonOnline.setEnabled(false);
                break;
            case R.id.layoutPaytm:
                paymentMode = "cod";
                layoutPaytm.setBackgroundColor(getResources().getColor(R.color.color_white));
                layoutCOD.setBackgroundColor(getResources().getColor(R.color.color_background));
                layoutPayOnline.setBackgroundColor(getResources().getColor(R.color.color_background));
                layoutYumCash.setBackgroundColor(getResources().getColor(R.color.color_background));
                break;
            case R.id.layoutPayOnline:
                paymentMode = "cod";
                layoutPayOnline.setBackgroundColor(getResources().getColor(R.color.color_white));
                layoutPaytm.setBackgroundColor(getResources().getColor(R.color.color_background));
                layoutCOD.setBackgroundColor(getResources().getColor(R.color.color_background));
                layoutYumCash.setBackgroundColor(getResources().getColor(R.color.color_background));
                radioButtonCash.setEnabled(false);
                radioButtonOnline.setEnabled(true);
            case R.id.radioButtonCash:
                paymentMode = "cod";
                layoutCOD.setBackgroundColor(getResources().getColor(R.color.color_white));
                layoutPaytm.setBackgroundColor(getResources().getColor(R.color.color_background));
                layoutPayOnline.setBackgroundColor(getResources().getColor(R.color.color_background));
                layoutYumCash.setBackgroundColor(getResources().getColor(R.color.color_background));
                radioButtonCash.setEnabled(true);
                radioButtonOnline.setEnabled(false);
                break;
            case R.id.cashClick:
                paymentMode = "cod";
                layoutPayOnline.setBackgroundColor(getResources().getColor(R.color.color_white));
                layoutPaytm.setBackgroundColor(getResources().getColor(R.color.color_background));
                layoutCOD.setBackgroundColor(getResources().getColor(R.color.color_background));
                layoutYumCash.setBackgroundColor(getResources().getColor(R.color.color_background));
                radioButtonCash.setEnabled(false);
                radioButtonOnline.setEnabled(true);
                break;
            case R.id.layoutYumCash:
                paymentMode = "cod";
                layoutYumCash.setBackgroundColor(getResources().getColor(R.color.color_white));
                layoutPayOnline.setBackgroundColor(getResources().getColor(R.color.color_background));
                layoutPaytm.setBackgroundColor(getResources().getColor(R.color.color_background));
                layoutCOD.setBackgroundColor(getResources().getColor(R.color.color_background));
                break;
            case R.id.layoutYumCashTxt:
                paymentMode = "cod";
                layoutYumCash.setBackgroundColor(getResources().getColor(R.color.color_white));
                layoutPayOnline.setBackgroundColor(getResources().getColor(R.color.color_background));
                layoutPaytm.setBackgroundColor(getResources().getColor(R.color.color_background));
                layoutCOD.setBackgroundColor(getResources().getColor(R.color.color_background));
                break;
            case R.id.tvAddMoney:
                break;*/
        }


    }

    private void bookOrder() {

        Log.d("ItemsAmount>>", "" + AppGlobal.ItemsAmount);
        Log.d("DiscountPrice>>", "" + AppGlobal.discountPrice);
        Log.d("AfterDiscountTotal>>", "" + AppGlobal.AfterDiscountTotal);
        Log.d("gstPrice>>", "" + AppGlobal.gstPrice);
        Log.d("AfterGstAmt>>", "" + AppGlobal.AfterGstAmt);

        String totalCost = AppGlobal.TotalCost;
        totalCost = totalCost.substring(2, totalCost.length()).trim();
        String taxAmt = AppGlobal.TaxAmt;
        //    taxAmt = taxAmt.substring(2, taxAmt.length()).trim();
        Log.d("txtAmt>>>", AppGlobal.TotalCost);

        WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    //{"success":true,"data":"333"}
                    JSONObject mainObject = new JSONObject(response);
                    if (mainObject.getString("success").equals("true")) {
                        AppGlobal.OrderBooked = "Booked";
                        AppGlobal.SuggestionText = "";
                        AppGlobal.MinimumPurchase = "";
                        AppGlobal.ExtraCharge = "";
                        AppGlobal.OrderID = mainObject.getString("data");
                        AppGlobal.OrderStatus = "OrderStatus";
                        sessionManager.clearArrayList(mContext);
                        AppGlobal.mainCartList.clear();

                        sessionManagerNew.clearArrayList(mContext);

                        UtilityMethod.showAlertBoxwithIntentClearHistory(mContext, "Order Booked", OrderStatusActivity.class);
                    } else {
                        UtilityMethod.showAlertBox(mContext, "Order Booking Fail");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        JSONObject mainObject = new JSONObject();
        JSONObject dataObject = new JSONObject();
        String discountType = "", discount_amount = "";
        double item_total_amount = 0;
        try {
            Log.d("Address>>>", AppGlobal.OrderDeliverAddress);
            dataObject.put("user_id", sessionManager.getValuesSession(SessionManager.Key_UserID));
            dataObject.put("fname", sessionManager.getValuesSession(SessionManager.Key_UserFirstName));
            dataObject.put("lname", "");
            dataObject.put("email", sessionManager.getValuesSession(SessionManager.Key_UserEmail));
            dataObject.put("cel", AppGlobal.OrderMobileNumber);

            for (int i = 0; i < AppGlobal.mainCartList.size(); i++) {

                //  Log.d("fgf",AppGlobal.mainCartList.get(i).quantity+" gggg");

                AppGlobal.RestaurentCategoryID = AppGlobal.mainCartList.get(i).restaurent_id;
                Log.d("restID", AppGlobal.mainCartList.get(i).restaurent_id);
                discountType = AppGlobal.mainCartList.get(i).DiscountType;
                discount_amount = AppGlobal.mainCartList.get(i).Discount;
                AppGlobal.MinimumPurchase = AppGlobal.mainCartList.get(i).MinimumPurchase;
                AppGlobal.ExtraCharge = AppGlobal.mainCartList.get(i).ExtraCharge;
                tax_percentage = AppGlobal.mainCartList.get(i).tax_percentage;
                Log.d("tax_percentage>>>", tax_percentage);
                Log.d("discountType>>>", discountType);
                Log.d("discount_amount>>>", discount_amount);
                /*item_total_amount = item_total_amount + AppGlobal.mainCartList.get(i).HalfTotalPrice + AppGlobal.mainCartList.get(i).MediumTotalPrice +
                        AppGlobal.mainCartList.get(i).FullTotalPrice + AppGlobal.mainCartList.get(i).LargeTotalPrice;

                item_total_amount = item_total_amount + Double.parseDouble(AppGlobal.mainCartList.get(i).TotalContainerCharge);*/
            }
            if (Double.parseDouble(AppGlobal.MinimumPurchase) < Double.parseDouble(totalCost)) {
                AppGlobal.ExtraCharge = "0";
            }
            Log.d("item_total_amount>>>", "" + AppGlobal.ItemsAmount);
            dataObject.put("restaurant_id", AppGlobal.RestaurentCategoryID);
            Log.d("restIDMain>>>", AppGlobal.RestaurentCategoryID);
            dataObject.put("delivery_zone_id", "");
            dataObject.put("delivery_option", "pickup");
            dataObject.put("location_id", AppGlobal.OrderLocatioID);
            dataObject.put("delivery_address", AppGlobal.OrderDeliverAddress);
            dataObject.put("restaurant_delivery_id", AppGlobal.RestaurentCategoryID);
            dataObject.put("delivery_amount", "");
            dataObject.put("item_total", AppGlobal.mainCartList.size());
            dataObject.put("item_total_amount", AppGlobal.ItemsAmount);
            dataObject.put("tax_amount", "" + AppGlobal.gstPrice);
            dataObject.put("discount", "0");
            dataObject.put("discount_code_id", "");
            dataObject.put("discount_code", "");
            dataObject.put("cart_total", totalCost);
            dataObject.put("pay_mode", paymentMode);
            dataObject.put("note", AppGlobal.SuggestionText);
            Log.d("ExtraCharge>>", AppGlobal.ExtraCharge);
            Log.d("minimum_purchase>>", AppGlobal.MinimumPurchase);
            dataObject.put("delivery_amount", AppGlobal.ExtraCharge);
            dataObject.put("extra_charge", AppGlobal.ExtraCharge);
            dataObject.put("minimum_purchase", AppGlobal.MinimumPurchase);
            dataObject.put("discount_type", "" + discountType);
            dataObject.put("discount_amount", "" + AppGlobal.discountPrice);
            dataObject.put("after_dis_total", "" + AppGlobal.AfterDiscountTotal);
            dataObject.put("after_gst_total", "" + AppGlobal.AfterGstAmt);
            JSONObject productitem = new JSONObject();
            for (int i = 0; i < AppGlobal.mainCartList.size(); i++) {


                JSONObject productData = new JSONObject();
                Log.d("restID", AppGlobal.mainCartList.get(i).restaurent_id);
                productData.put("product_id", AppGlobal.mainCartList.get(i).Product_id);
                productData.put("quantity", AppGlobal.mainCartList.get(i).quantity);
                productData.put("product_name", AppGlobal.mainCartList.get(i).Product_name);
                productData.put("unit_price", AppGlobal.mainCartList.get(i).Product_price);
                productData.put("product_total", AppGlobal.mainCartList.get(i).afterDiscountValue);
                productData.put("restaurant_id", AppGlobal.RestaurentCategoryID);
                productData.put("user_id", sessionManager.getValuesSession(SessionManager.Key_UserID));
                double halfPrice = 0;
                double mediumPrice = 0;
                double fullPrice = 0;
                double largePrice = 0;
                halfPrice = AppGlobal.mainCartList.get(i).HalfPrice * AppGlobal.mainCartList.get(i).HalfQty;
                mediumPrice = AppGlobal.mainCartList.get(i).MediumPrice * AppGlobal.mainCartList.get(i).MediumQty;
                fullPrice = AppGlobal.mainCartList.get(i).FullPrice * AppGlobal.mainCartList.get(i).FullQty;
                largePrice = AppGlobal.mainCartList.get(i).LargePrice * AppGlobal.mainCartList.get(i).LargeQty;

                Log.d("halfPrice>>", "" + halfPrice);
                Log.d("mediumPrice>>", "" + mediumPrice);
                Log.d("fullPrice>>", "" + fullPrice);
                Log.d("largePrice>>", "" + largePrice);
                Log.d("largePrice>>", "" + AppGlobal.mainCartList.get(i).HalfPrice);

                productData.put("half_label", AppGlobal.mainCartList.get(i).HalfLabel);
                productData.put("half_qty", AppGlobal.mainCartList.get(i).HalfQty);
                productData.put("half_per_product_price", AppGlobal.mainCartList.get(i).HalfPrice);
                productData.put("half_price", "" + halfPrice);
                productData.put("medium_label", AppGlobal.mainCartList.get(i).MediumLabel);
                productData.put("medium_qty", AppGlobal.mainCartList.get(i).MediumQty);
                productData.put("medium_per_product_price", AppGlobal.mainCartList.get(i).MediumPrice);
                productData.put("medium_price", "" + mediumPrice);
                productData.put("full_label", AppGlobal.mainCartList.get(i).FullLabel);
                productData.put("full_qty", AppGlobal.mainCartList.get(i).FullQty);
                productData.put("full_per_product_price", AppGlobal.mainCartList.get(i).FullPrice);
                productData.put("full_price", "" + fullPrice);
                productData.put("large_label", AppGlobal.mainCartList.get(i).LargeLabel);
                productData.put("large_qty", AppGlobal.mainCartList.get(i).LargeQty);
                productData.put("large_per_product_price", AppGlobal.mainCartList.get(i).LargePrice);
                productData.put("large_price", "" + largePrice);
                Log.d("container_charge>>", AppGlobal.mainCartList.get(i).TotalContainerCharge);
                productData.put("container_charge", AppGlobal.mainCartList.get(i).TotalContainerCharge);
                productData.put("each_container_charge", AppGlobal.mainCartList.get(i).PerProductContainerCharge);
                //productData.put("container_charge", "" + AppGlobal.mainCartList.get(i).TotalContainerCharge);
                productitem.put("" + i, productData);

            }
            dataObject.put("productitem", productitem);
            mainObject.put("data", dataObject);
            Log.d("Data>>>", mainObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StringEntity se = null;
        try {
            se = new StringEntity(mainObject.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        webServiceCaller.addEntity(se);
        String url = AppGlobal.AddOrder;
        Log.d("url>>", url);
        webServiceCaller.execute(url);
    }
}

