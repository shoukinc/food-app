package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.RestaruentDetailViewPager;
import com.b2infosoft_jaipur.chakhle.fragments.CallFragment;
import com.b2infosoft_jaipur.chakhle.fragments.CartValueInterface;
import com.b2infosoft_jaipur.chakhle.fragments.NewCartDataFill;
import com.b2infosoft_jaipur.chakhle.fragments.NotifyAdapter;
import com.b2infosoft_jaipur.chakhle.pojo.CartFillPojo;
import com.b2infosoft_jaipur.chakhle.pojo.RestaurentDetailsPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.sessions.SessionNew;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class RestaurentDetailsActivity extends AppCompatActivity implements View.OnClickListener, CartValueInterface, CallFragment, NotifyAdapter, NewCartDataFill {
    Toolbar toolbar;
    ViewPager viewPager;
    TabLayout tabLayout;
    CollapsingToolbarLayout collapsingToolbarLayout;
    Context mContext;
    ImageView htab_header;
    private static final String TAG = RestaurentDetailsActivity.class.getSimpleName();
    TextView tvDiscount, tvAddress, tvCartCount;
    ImageView imgRestLogo, imgFav, imgShare, imgSearch, imgDiscount;
    EditText autoCompView;
    ImageView fabCart;
    View view;
    RelativeLayout logoShadow;
    Bundle bundle;
    SessionManager sessionManager;
    SessionNew sessionManagerNew;
    String isFavroite = "", discountType = "", discountPrice = "";
    ArrayList<RestaurentDetailsPojo> arrayList = new ArrayList<>();
    RelativeLayout discountLayout;
    RestaruentDetailViewPager adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurent_details);
        mContext = RestaurentDetailsActivity.this;
        sessionManager = new SessionManager(mContext);
        sessionManagerNew=new SessionNew(mContext);
        initView();
        Log.d("CategoryID>>>", AppGlobal.RestaurentCategoryID);
        if (sessionManager.isLoggedIn()) {
            RestaurentDetailsPojo.getRestaurentDetails(view, mContext, AppGlobal.RestaurentCategoryID, sessionManager.getValuesSession(SessionManager.Key_UserID));
        } else {
            RestaurentDetailsPojo.getRestaurentDetails(view, mContext, AppGlobal.RestaurentCategoryID, "0");
        }
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.htab_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(AppGlobal.RestaurentName);
        discountLayout = (RelativeLayout) findViewById(R.id.discountLayout);
        logoShadow = (RelativeLayout) findViewById(R.id.logoShadow);
        viewPager = (ViewPager) findViewById(R.id.htab_viewpager);
        htab_header = (ImageView) findViewById(R.id.htab_header);
        imgRestLogo = (ImageView) findViewById(R.id.imgRestLogo);
        imgSearch = (ImageView) findViewById(R.id.imgSearch);
        imgShare = (ImageView) findViewById(R.id.imgShare);
        imgShare.setOnClickListener(this);
        tvDiscount = (TextView) findViewById(R.id.tvDiscount);
        tvAddress = (TextView) findViewById(R.id.tvAddress);

        //tvAddress.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        //tvDiscount.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_bold.ttf"));

        Log.d("AppGlobal.RestBanner",AppGlobal.RestBanner+" hhhhh");

        Glide.with(mContext)
                .load(AppGlobal.RestBanner)
                .into(htab_header);

       /* if (!AppGlobal.RestarentImage.equals("")) {
            logoShadow.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(AppGlobal.RestarentImage).into(imgRestLogo);
        }*/

        autoCompView = (EditText) findViewById(R.id.autoCompleteTextView);
        autoCompView.setCursorVisible(false);
        autoCompView.setClickable(false);
        autoCompView.setEnabled(false);
        autoCompView.setText(AppGlobal.RestaurentName);
        autoCompView.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvCartCount = (TextView) findViewById(R.id.tvCartCount);
        //tvCartCount.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        imgFav = (ImageView) findViewById(R.id.imgFav);
        imgDiscount = (ImageView) findViewById(R.id.imgDiscount);
        fabCart = (ImageView) findViewById(R.id.fabCart);
        fabCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RestaurentDetailsPojo restaurentDetailsPojo = null;
                AppGlobal.mainCartList.clear();
                AppGlobal.GlobalCartList.clear();

                for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                    int Quentity = 0, GlobalQuentity = 0;
                    int HalfQty = 0;
                    double HalfAmount = 0;
                    double HalfTotalAmount = 0;
                    int MediumQty = 0;
                    double MediumAmt = 0;
                    double MediumTotalAmt = 0;
                    int FullQty = 0;
                    double FullAmt = 0;
                    double FullTotalAmt = 0;
                    int LargeQty = 0;
                    double LargeAmt = 0;
                    double LargeTotalAmt = 0;

                    if (AppGlobal.RestaurentProductList.get(i).halfQty != 0) {
                        HalfQty = AppGlobal.RestaurentProductList.get(i).halfQty;
                        HalfAmount = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).half);
                        HalfTotalAmount = HalfQty * HalfAmount;
                        Log.d("HalfTotalAmount>>>", "" + HalfTotalAmount);
                    }
                    if (AppGlobal.RestaurentProductList.get(i).mediumQty != 0) {
                        MediumQty = AppGlobal.RestaurentProductList.get(i).mediumQty;
                        MediumAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).medium);
                        MediumTotalAmt = MediumQty * MediumAmt;
                        Log.d("MediumTotalAmt>>>", "" + MediumTotalAmt);
                    }
                    if (AppGlobal.RestaurentProductList.get(i).fullQty != 0) {
                        FullQty = AppGlobal.RestaurentProductList.get(i).fullQty;
                        FullAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).full);
                        FullTotalAmt = FullQty * FullAmt;
                        Log.d("FullTotalAmt>>>", "" + FullTotalAmt);
                    }
                    if (AppGlobal.RestaurentProductList.get(i).largeQty != 0) {
                        LargeQty = AppGlobal.RestaurentProductList.get(i).largeQty;
                        LargeAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).large);
                        LargeTotalAmt = LargeQty * LargeAmt;
                        Log.d("LargeTotalAmt>>>", "" + LargeTotalAmt);
                    }
                    Quentity = AppGlobal.RestaurentProductList.get(i).getCount();
                    Log.d("Quentity>>>", "" + Quentity);
                    double total = HalfTotalAmount + MediumTotalAmt + FullTotalAmt + LargeTotalAmt;
                    int productPrice = Integer.parseInt((AppGlobal.RestaurentProductList.get(i).Product_price));
                    int totalProductPrice = productPrice * Quentity;
                    String minimum_purchase = AppGlobal.RestaurentProductList.get(i).minimum_purchase;
                    String extra_charge = AppGlobal.RestaurentProductList.get(i).extra_charge;
                    Log.d("minimum_purchase>>", minimum_purchase);
                    Log.d("extra_charge>>", extra_charge);
                    double container = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).container_charge);
                    Log.d("Quentity>>>", "" + Quentity);
                    container = container * Quentity;
                    if (HalfQty != 0 || MediumQty != 0 || LargeQty != 0 || FullQty != 0) {
                        AppGlobal.mainCartList.add(new CartFillPojo(
                                AppGlobal.RestaurentProductList.get(i).Product_id,
                                Quentity,
                                AppGlobal.RestaurentProductList.get(i).Product_name,
                                AppGlobal.RestaurentProductList.get(i).Product_price,
                                total,
                                AppGlobal.RestaurentProductList.get(i).restaurant_id,
                                sessionManager.getValuesSession(SessionManager.Key_UserID),
                                AppGlobal.RestaurentProductList.get(i).Product_vegproduct,
                                AppGlobal.RestaurentProductList.get(i).afterDiscountValue,
                                totalProductPrice, AppGlobal.RestaurentProductList.get(i).name,
                                AppGlobal.RestaurentProductList.get(i).discount,
                                String.valueOf(container),
                                AppGlobal.RestaurentProductList.get(i).minimum_purchase,
                                AppGlobal.RestaurentProductList.get(i).extra_charge,
                                AppGlobal.RestaurentProductList.get(i).container_charge,
                                HalfQty, HalfAmount, HalfTotalAmount,
                                FullQty, FullAmt, FullTotalAmt,
                                LargeQty, LargeAmt, LargeTotalAmt,
                                MediumQty, MediumAmt, MediumTotalAmt,
                                AppGlobal.RestaurentProductList.get(i).discountType
                                , AppGlobal.RestaurentProductList.get(i).half_label
                                , AppGlobal.RestaurentProductList.get(i).medium_label
                                , AppGlobal.RestaurentProductList.get(i).full_label
                                , AppGlobal.RestaurentProductList.get(i).large_label
                                , AppGlobal.RestaurentProductList.get(i).let_nite_charge
                                , AppGlobal.RestaurentProductList.get(i).tax_percentage
                                , AppGlobal.RestaurentProductList.get(i).RestInTime
                                , AppGlobal.RestaurentProductList.get(i).Restoff_time_1
                        ));
                    }
                }

                if (sessionManager.isLoggedIn()) {
                    if (!AppGlobal.mainCartList.isEmpty()) {
                        sessionManager.clearArrayList(mContext);
                        sessionManager.saveCartList(mContext, AppGlobal.mainCartList);
                    }
                    Log.d("MainCartList>>>", "" + AppGlobal.mainCartList.size());
                    startActivity(new Intent(mContext, ReviewOrderActivity.class));
                } else {
                    SessionNew sessionNew=new SessionNew(mContext);
                    sessionNew.saveCartList(mContext, AppGlobal.mainCartList);
                    startActivity(new Intent(mContext, LoginActivity.class));
                }
            }
        });
        imgSearch.setOnClickListener(this);
        imgFav.setOnClickListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RestaurentDetailsPojo restaurentDetailsPojo = null;
                AppGlobal.mainCartList.clear();
                AppGlobal.GlobalCartList.clear();
                if (sessionManager.isLoggedIn()) {
                    for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                        int Quentity = 0, GlobalQuentity = 0;
                        int HalfQty = 0;
                        double HalfAmount = 0;
                        double HalfTotalAmount = 0;
                        int MediumQty = 0;
                        double MediumAmt = 0;
                        double MediumTotalAmt = 0;
                        int FullQty = 0;
                        double FullAmt = 0;
                        double FullTotalAmt = 0;
                        int LargeQty = 0;
                        double LargeAmt = 0;
                        double LargeTotalAmt = 0;
                        if (AppGlobal.RestaurentProductList.get(i).halfQty != 0) {
                            HalfQty = AppGlobal.RestaurentProductList.get(i).halfQty;
                            HalfAmount = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).half);
                            HalfTotalAmount = HalfQty * HalfAmount;
                            Log.d("HalfTotalAmount>>>", "" + HalfTotalAmount);
                        }
                        if (AppGlobal.RestaurentProductList.get(i).mediumQty != 0) {
                            MediumQty = AppGlobal.RestaurentProductList.get(i).mediumQty;
                            MediumAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).medium);
                            MediumTotalAmt = MediumQty * MediumAmt;
                            Log.d("MediumTotalAmt>>>", "" + MediumTotalAmt);
                        }
                        if (AppGlobal.RestaurentProductList.get(i).fullQty != 0) {
                            FullQty = AppGlobal.RestaurentProductList.get(i).fullQty;
                            FullAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).full);
                            FullTotalAmt = FullQty * FullAmt;
                            Log.d("FullTotalAmt>>>", "" + FullTotalAmt);
                        }
                        if (AppGlobal.RestaurentProductList.get(i).largeQty != 0) {
                            LargeQty = AppGlobal.RestaurentProductList.get(i).largeQty;
                            LargeAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).large);
                            LargeTotalAmt = LargeQty * LargeAmt;
                            Log.d("LargeTotalAmt>>>", "" + LargeTotalAmt);
                        }
                        Quentity = AppGlobal.RestaurentProductList.get(i).getCount();
                        Log.d("Quentity>>>", "" + Quentity);
                        int total = (AppGlobal.RestaurentProductList.get(i).afterDiscountValue) * Quentity;
                        int productPrice = Integer.parseInt((AppGlobal.RestaurentProductList.get(i).Product_price));
                        int totalProductPrice = productPrice * Quentity;
                        String minimum_purchase = AppGlobal.RestaurentProductList.get(i).minimum_purchase;
                        String extra_charge = AppGlobal.RestaurentProductList.get(i).extra_charge;
                        Log.d("minimum_purchase>>", minimum_purchase);
                        Log.d("extra_charge>>", extra_charge);
                        double container = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).container_charge);
                        Log.d("Quentity>>>", "" + Quentity);
                        container = container * Quentity;
                        if (HalfQty != 0 || MediumQty != 0 || LargeQty != 0 || FullQty != 0) {
                            AppGlobal.mainCartList.add(new CartFillPojo(
                                    AppGlobal.RestaurentProductList.get(i).Product_id,
                                    Quentity,
                                    AppGlobal.RestaurentProductList.get(i).Product_name,
                                    AppGlobal.RestaurentProductList.get(i).Product_price,
                                    total,
                                    AppGlobal.RestaurentProductList.get(i).restaurant_id,
                                    sessionManager.getValuesSession(SessionManager.Key_UserID),
                                    AppGlobal.RestaurentProductList.get(i).Product_vegproduct,
                                    AppGlobal.RestaurentProductList.get(i).afterDiscountValue,
                                    totalProductPrice, AppGlobal.RestaurentProductList.get(i).name,
                                    AppGlobal.RestaurentProductList.get(i).discount,
                                    String.valueOf(container),
                                    AppGlobal.RestaurentProductList.get(i).minimum_purchase,
                                    AppGlobal.RestaurentProductList.get(i).extra_charge,
                                    AppGlobal.RestaurentProductList.get(i).container_charge,
                                    HalfQty, HalfAmount, HalfTotalAmount,
                                    FullQty, FullAmt, FullTotalAmt,
                                    LargeQty, LargeAmt, LargeTotalAmt,
                                    MediumQty, MediumAmt, MediumTotalAmt,
                                    AppGlobal.RestaurentProductList.get(i).discountType
                                    , AppGlobal.RestaurentProductList.get(i).half_label
                                    , AppGlobal.RestaurentProductList.get(i).medium_label
                                    , AppGlobal.RestaurentProductList.get(i).full_label
                                    , AppGlobal.RestaurentProductList.get(i).large_label
                                    , AppGlobal.RestaurentProductList.get(i).let_nite_charge
                                    , AppGlobal.RestaurentProductList.get(i).tax_percentage
                                    , AppGlobal.RestaurentProductList.get(i).RestInTime
                                    , AppGlobal.RestaurentProductList.get(i).Restoff_time_1
                            ));
                        }

                    }

                    if (!AppGlobal.mainCartList.isEmpty()) {
                        sessionManager.clearArrayList(mContext);
                        sessionManager.saveCartList(mContext, AppGlobal.mainCartList);
                    }
                    Log.d("MainCartList>>>", "" + AppGlobal.mainCartList.size());
                    //  startActivity(new Intent(mContext, ReviewOrderActivity.class));
                }
                if (AppGlobal.IamFrom.equals("SearchRestorent")) {
                    Intent intent = new Intent(mContext, SearchActivity.class);
                    intent.putExtra("FromWhere", "SearchActivity");
                    AppGlobal.IamFrom = "";
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(mContext, RestaurantListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle bundle = new Bundle();
                    bundle.putString("ClassName", "RestaurentDetailsActivity");
                    bundle.putString("AddToFav", "true");
                    bundle.putString("CartValue", tvCartCount.getText().toString());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });
        if (!AppGlobal.RestDiscount.equals("")) {
            imgDiscount.setVisibility(View.GONE);
            discountLayout.setVisibility(View.VISIBLE);
            tvDiscount.setVisibility(View.VISIBLE);
            tvDiscount.setText(AppGlobal.RestDiscount + "%" + " OFF");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        RestaurentDetailsPojo restaurentDetailsPojo = null;
        AppGlobal.mainCartList.clear();
        AppGlobal.GlobalCartList.clear();

            for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
                int Quentity = 0, GlobalQuentity = 0;
                int HalfQty = 0;
                double HalfAmount = 0;
                double HalfTotalAmount = 0;
                int MediumQty = 0;
                double MediumAmt = 0;
                double MediumTotalAmt = 0;
                int FullQty = 0;
                double FullAmt = 0;
                double FullTotalAmt = 0;
                int LargeQty = 0;
                double LargeAmt = 0;
                double LargeTotalAmt = 0;

                if (AppGlobal.RestaurentProductList.get(i).halfQty != 0) {
                    HalfQty = AppGlobal.RestaurentProductList.get(i).halfQty;
                    HalfAmount = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).half);
                    HalfTotalAmount = HalfQty * HalfAmount;
                    Log.d("HalfTotalAmount>>>", "" + HalfTotalAmount);

                }
                if (AppGlobal.RestaurentProductList.get(i).mediumQty != 0) {
                    MediumQty = AppGlobal.RestaurentProductList.get(i).mediumQty;
                    MediumAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).medium);
                    MediumTotalAmt = MediumQty * MediumAmt;
                    Log.d("MediumTotalAmt>>>", "" + MediumTotalAmt);
                }
                if (AppGlobal.RestaurentProductList.get(i).fullQty != 0) {
                    FullQty = AppGlobal.RestaurentProductList.get(i).fullQty;
                    FullAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).full);
                    FullTotalAmt = FullQty * FullAmt;
                    Log.d("FullTotalAmt>>>", "" + FullTotalAmt);
                }
                if (AppGlobal.RestaurentProductList.get(i).largeQty != 0) {
                    LargeQty = AppGlobal.RestaurentProductList.get(i).largeQty;
                    LargeAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).large);
                    LargeTotalAmt = LargeQty * LargeAmt;
                    Log.d("LargeTotalAmt>>>", "" + LargeTotalAmt);
                }
                Quentity = AppGlobal.RestaurentProductList.get(i).getCount();
                Log.d("Quentity>>>", "" + Quentity);
                int total = (AppGlobal.RestaurentProductList.get(i).afterDiscountValue) * Quentity;
                int productPrice = Integer.parseInt((AppGlobal.RestaurentProductList.get(i).Product_price));
                int totalProductPrice = productPrice * Quentity;
                String minimum_purchase = AppGlobal.RestaurentProductList.get(i).minimum_purchase;
                String extra_charge = AppGlobal.RestaurentProductList.get(i).extra_charge;
                Log.d("minimum_purchase>>", minimum_purchase);
                Log.d("extra_charge>>", extra_charge);
                double container = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).container_charge);
                Log.d("Quentity>>>", "" + Quentity);
                container = container * Quentity;
                if (HalfQty != 0 || MediumQty != 0 || LargeQty != 0 || FullQty != 0) {
                    AppGlobal.mainCartList.add(new CartFillPojo(
                            AppGlobal.RestaurentProductList.get(i).Product_id,
                            Quentity,
                            AppGlobal.RestaurentProductList.get(i).Product_name,
                            AppGlobal.RestaurentProductList.get(i).Product_price,
                            total,
                            AppGlobal.RestaurentProductList.get(i).restaurant_id,
                            sessionManager.getValuesSession(SessionManager.Key_UserID),
                            AppGlobal.RestaurentProductList.get(i).Product_vegproduct,
                            AppGlobal.RestaurentProductList.get(i).afterDiscountValue,
                            totalProductPrice, AppGlobal.RestaurentProductList.get(i).name,
                            AppGlobal.RestaurentProductList.get(i).discount,
                            String.valueOf(container),
                            AppGlobal.RestaurentProductList.get(i).minimum_purchase,
                            AppGlobal.RestaurentProductList.get(i).extra_charge,
                            AppGlobal.RestaurentProductList.get(i).container_charge,
                            HalfQty, HalfAmount, HalfTotalAmount,
                            FullQty, FullAmt, FullTotalAmt,
                            LargeQty, LargeAmt, LargeTotalAmt,
                            MediumQty, MediumAmt, MediumTotalAmt,
                            AppGlobal.RestaurentProductList.get(i).discountType
                            , AppGlobal.RestaurentProductList.get(i).half_label
                            , AppGlobal.RestaurentProductList.get(i).medium_label
                            , AppGlobal.RestaurentProductList.get(i).full_label
                            , AppGlobal.RestaurentProductList.get(i).large_label
                            , AppGlobal.RestaurentProductList.get(i).let_nite_charge
                            , AppGlobal.RestaurentProductList.get(i).tax_percentage
                            , AppGlobal.RestaurentProductList.get(i).RestInTime
                            , AppGlobal.RestaurentProductList.get(i).Restoff_time_1
                    ));
                }
            }

        if (!AppGlobal.mainCartList.isEmpty()) {
            sessionManager.clearArrayList(mContext);
            sessionManager.saveCartList(mContext, AppGlobal.mainCartList);
            sessionManagerNew.clearArrayList(mContext);
            sessionManagerNew.saveCartList(mContext, AppGlobal.mainCartList);
        }else{
            sessionManagerNew.clearArrayList(mContext);
            sessionManager.clearArrayList(mContext);
        }
            Log.d("MainCartList>>>", "" + AppGlobal.mainCartList.size());
            //startActivity(new Intent(mContext, ReviewOrderActivity.class));

        if (AppGlobal.IamFrom.equals("FavRestoActivity")) {
            Intent intent = new Intent(mContext, FavRestorentActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            AppGlobal.IamFrom = "";
            startActivity(intent);

        } else if (AppGlobal.IamFrom.equals("SearchRestorent")) {
            Intent intent = new Intent(mContext, SearchActivity.class);
            intent.putExtra("FromWhere", "SearchActivity");
            AppGlobal.IamFrom = "";
            startActivity(intent);
        } else {
            Intent intent = new Intent(mContext, RestaurantListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle bundle = new Bundle();
            bundle.putString("ClassName", "RestaurentDetailsActivity");
            bundle.putString("AddToFav", "true");
            String tvcartValue = tvCartCount.getText().toString();
            int cart = 0;
            int cartValue = 0;
            if (!tvCartCount.getText().toString().equals("")) {
                cart = Integer.parseInt(tvcartValue);
                cartValue = AppGlobal.searchCartValue + cart;
            }
            //tvcartValue = tvcartValue.substring(2, tvcartValue.length()).trim();
            bundle.putString("CartValue", "" + cart);
            AppGlobal.searchCartValue = 0;
            intent.putExtras(bundle);
            startActivity(intent);
        }

    }


    public void setProductList(final ArrayList<RestaurentDetailsPojo> restaurantCategoryList, final ArrayList<RestaurentDetailsPojo> restaurentProductList) {
        AppGlobal.RestaurentProductList.clear();
        AppGlobal.RestaurentProductList = restaurentProductList;
        for (int i = 0; i < restaurentProductList.size(); i++) {
            isFavroite = restaurentProductList.get(i).is_favorite;
            discountPrice = restaurentProductList.get(i).discount;
            discountType = restaurentProductList.get(i).discountType;
            Log.d("RestAddress>>>", restaurentProductList.get(i).Restaddress);
            tvAddress.setText(restaurentProductList.get(i).Restaddress);
        }
        if (isFavroite.equals("1")) {
            imgFav.setImageResource(R.drawable.fav_fill);
        } else {
            imgFav.setImageResource(R.drawable.fav_unfill);
        }
        if (!discountType.equals("")) {
            if (discountType.equals("price")) {
                imgDiscount.setVisibility(View.GONE);
                discountLayout.setVisibility(View.VISIBLE);
                tvDiscount.setVisibility(View.VISIBLE);
                tvDiscount.setText(discountPrice + " Rs." + " OFF");
            } else {
                imgDiscount.setVisibility(View.GONE);
                discountLayout.setVisibility(View.VISIBLE);
                tvDiscount.setVisibility(View.VISIBLE);
                tvDiscount.setText(discountPrice + " %" + " OFF");
            }
        }
        int HalfQty = 0;
        double HalfAmount = 0;
        double HalfTotalAmount = 0;
        int MediumQty = 0;
        double MediumAmt = 0;
        double MediumTotalAmt = 0;
        int FullQty = 0;
        double FullAmt = 0;
        double FullTotalAmt = 0;
        int LargeQty = 0;
        int cartQty = 0;
        double LargeAmt = 0;
        double LargeTotalAmt = 0;
        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            HalfQty = 0;
            MediumQty = 0;
            MediumTotalAmt = 0;
            FullQty = 0;
            FullAmt = 0;
            FullTotalAmt = 0;
            LargeQty = 0;
            LargeAmt = 0;
            LargeTotalAmt = 0;

            if (AppGlobal.RestaurentProductList.get(i).halfQty != 0) {
                HalfQty = AppGlobal.RestaurentProductList.get(i).halfQty;
                HalfAmount = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).half);
                HalfTotalAmount = HalfQty * HalfAmount;
                Log.d("HalfTotalAmount>>>", "" + HalfTotalAmount);
                cartQty = cartQty + HalfQty;
            }
            if (AppGlobal.RestaurentProductList.get(i).mediumQty != 0) {
                MediumQty = AppGlobal.RestaurentProductList.get(i).mediumQty;
                MediumAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).medium);
                MediumTotalAmt = MediumQty * MediumAmt;
                cartQty = cartQty + MediumQty;
                Log.d("MediumTotalAmt>>>", "" + MediumTotalAmt);
            }
            if (AppGlobal.RestaurentProductList.get(i).fullQty != 0) {
                FullQty = AppGlobal.RestaurentProductList.get(i).fullQty;
                FullAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).full);
                FullTotalAmt = FullQty * FullAmt;
                cartQty = cartQty + FullQty;
                Log.d("FullTotalAmt>>>", "" + FullTotalAmt);
            }
            if (AppGlobal.RestaurentProductList.get(i).largeQty != 0) {
                LargeQty = AppGlobal.RestaurentProductList.get(i).largeQty;
                LargeAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).large);
                LargeTotalAmt = LargeQty * LargeAmt;
                cartQty = cartQty + LargeQty;
                Log.d("LargeTotalAmt>>>", "" + LargeTotalAmt);
            }
        }
        if (cartQty != 0) {
            fabCart.setVisibility(View.VISIBLE);
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText("" + cartQty);
        } else {
            fabCart.setVisibility(View.GONE);
            tvCartCount.setVisibility(View.GONE);
            tvCartCount.setText("" + cartQty);
        }
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.htab_collapse_toolbar);
        collapsingToolbarLayout.setContentScrimColor(getResources().getColor(R.color.colorAccent));
        collapsingToolbarLayout.setStatusBarScrimColor(getResources().getColor(R.color.color_black));
        viewPager = (ViewPager) findViewById(R.id.htab_viewpager);
        tabLayout = (TabLayout) findViewById(R.id.htab_tabs);

        for (int i = 0; i < restaurantCategoryList.size(); i++) {
            tabLayout.addTab(tabLayout.newTab().setText("" + restaurantCategoryList.get(i).Category_name));
        }
        //tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        adapter = new RestaruentDetailViewPager(getSupportFragmentManager(), tabLayout.getTabCount(), restaurantCategoryList, restaurentProductList);
        // int limit = (adapter.getCount() > 1 ? adapter.getCount() - 1 : 1);
        viewPager.setAdapter(adapter);
        // viewPager.setOffscreenPageLimit(limit);
        //viewPager.setOnPageChangeListener(adapter);
        viewPager.getAdapter().notifyDataSetChanged();
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Fragment f = adapter.getItem(position);
                f.onResume();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                //AppGlobal.CategoryID = restaurantCategoryList.get(tab.getPosition()).category_id;
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgSearch:
                /*toolbar.setTitle("");
                // rlll.setVisibility(View.VISIBLE);
                autoCompView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
                autoCompView.getBackground().setColorFilter(getResources().getColor(R.color.color_white), PorterDuff.Mode.SRC_ATOP);
                autoCompView.setVisibility(View.VISIBLE);
                autoCompView.setCursorVisible(true);
                autoCompView.setClickable(true);
                autoCompView.setEnabled(true);
                autoCompView.setHint("");
                autoCompView.setText("");
                autoCompView.requestFocus();
                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(autoCompView, InputMethodManager.SHOW_IMPLICIT);*/
                if (fabCart.getVisibility() == View.VISIBLE) {
                    AppGlobal.CartValue = Integer.parseInt(tvCartCount.getText().toString());
                } else {
                    AppGlobal.CartValue = 0;
                }

                Intent intent = new Intent(mContext, SearchActivity.class);
                intent.putExtra("FromWhere", "RestaurentDetail");
                startActivity(intent);
                // startActivity(new Intent(mContext, SearchActivity.class));


                break;
            case R.id.imgFav:
                Object tag = imgFav.getTag();
                int backgroundId = R.drawable.fav_unfill;
                int backgroundId2 = R.drawable.fav_fill;
                /*if (tag != null && ((Integer) tag).intValue() == backgroundId)
                {
                    //backgroundId = R.drawable.bg_image_1;
                    if (sessionManager.isLoggedIn()) {
                        imgFav.setBackgroundResource(backgroundId2);
                        imgFav.setTag(backgroundId2);
                        //Toast.makeText(mContext, "From Session", Toast.LENGTH_SHORT).show();
                        doFavruiteRestorent();

                    } else {
                        AppGlobal.FavFrom = "RestaurentDetails";
                        startActivity(new Intent(mContext, LoginActivity.class));
                    }

                } else {
                    if (sessionManager.isLoggedIn()) {
                        imgFav.setBackgroundResource(backgroundId);
                        imgFav.setTag(backgroundId);
                        //Toast.makeText(mContext, "From Session", Toast.LENGTH_SHORT).show();
                        removeFavruiteRestorent();
                    } else {
                        AppGlobal.FavFrom = "RestaurentDetails";
                        startActivity(new Intent(mContext, LoginActivity.class));
                    }
                }*/


                if (isFavroite.equals("0")) {
                    if (sessionManager.isLoggedIn()) {
                        //Toast.makeText(mContext, "From Session", Toast.LENGTH_SHORT).show();
                        doFavruiteRestorent();

                    } else {
                        AppGlobal.FavFrom = "RestaurentDetails";
                        startActivity(new Intent(mContext, LoginActivity.class));
                    }
                } else {
                    if (sessionManager.isLoggedIn()) {
                        //Toast.makeText(mContext, "From Session", Toast.LENGTH_SHORT).show();
                        removeFavruiteRestorent();
                    } else {
                        AppGlobal.FavFrom = "RestaurentDetails";
                        startActivity(new Intent(mContext, LoginActivity.class));
                    }
                }
                break;
            case R.id.imgShare:

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = AppGlobal.RestaurentName + AppGlobal.RestarentAddress + "\n" + "https://play.google.com/store/apps/details?id=com.b2infosoft_jaipur.chakhle" + AppGlobal.RestDiscount;
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Super Restaurant");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
        }
    }

    private void removeFavruiteRestorent() {
        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait....", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getBoolean("success") == true) {
                        imgFav.setImageResource(R.drawable.fav_unfill);
                        AppGlobal.AddedToFav = "false";
                        UtilityMethod.showAlertBoxwithIntentClearHistory(mContext, "Remove Successfully", RestaurentDetailsActivity.class);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        //serviceCaller.addNameValuePair("city", "jaipur");
        serviceCaller.addNameValuePair("restaurant_id", AppGlobal.RestaurentCategoryID);
        serviceCaller.addNameValuePair("user_id", sessionManager.getValuesSession(SessionManager.Key_UserID));
        String url = AppGlobal.RemoveFavoriteRest;
        Log.d("url>>", url);
        serviceCaller.execute(url);

    }

    private void doFavruiteRestorent() {
        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait....", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getBoolean("success") == true) {
                        imgFav.setImageResource(R.drawable.fav_fill);
                        AppGlobal.AddedToFav = "true";
                        UtilityMethod.showAlertBoxwithIntentClearHistory(mContext, "Added Successfully", RestaurentDetailsActivity.class);

                        // UtilityMethod.showAlertBox(mContext, "Added Successfully");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        //  serviceCaller.addNameValuePair("city", "jaipur");
        serviceCaller.addNameValuePair("restaurant_id", AppGlobal.RestaurentCategoryID);
        serviceCaller.addNameValuePair("user_id", sessionManager.getValuesSession(SessionManager.Key_UserID));
        String url = AppGlobal.AddtoFavoriteRest;
        Log.d("url>>", url);
        serviceCaller.execute(url);
    }

    @Override
    public void setCartData(int cartData, String operator, String productID) {
        /*Log.d("cartValue>>>", "" + cartData);
        int tvValue = 0;
        int cart = 0;
        AppGlobal.ProductIDList.add(productID);

        if (operator.equals("+")) {
            if (!tvCartCount.getText().toString().equals("")) {
                tvValue = Integer.parseInt(tvCartCount.getText().toString());
            }
            tvValue = tvValue + 1;
            fabCart.setVisibility(View.VISIBLE);
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText("" + tvValue);
        } else if (operator.equals("-")) {
            if (!tvCartCount.getText().toString().equals("")) {
                AppGlobal.ProductIDList.remove(productID);
                tvValue = Integer.parseInt(tvCartCount.getText().toString());
                tvValue = tvValue - 1;
                fabCart.setVisibility(View.VISIBLE);
                tvCartCount.setVisibility(View.VISIBLE);
                tvCartCount.setText("" + tvValue);
            }
        } else if (operator.equals("visibleCart") && cartData != 0) {
            fabCart.setVisibility(View.VISIBLE);
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText("" + cartData);
        }
        try {
            if (operator.equals("check") && Integer.parseInt(tvCartCount.getText().toString()) == 0) {
                AppGlobal.ProductIDList.clear();

                fabCart.setVisibility(View.GONE);
                tvCartCount.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/


    }

    @Override
    public void setFragment(String fragmentName) {

        String fname = fragmentName;
        Log.d("fname>>>", fname);


    }

    @Override
    public void setCartValue(int value) {

    }

    @Override
    public void notifyData(String cateID) {
        adapter.notifyDataSetChanged();


    }

    @Override
    public void newCartFillData(ArrayList<RestaurentDetailsPojo> RestaurentProductList) {
        int HalfQty = 0;
        double HalfAmount = 0;
        double HalfTotalAmount = 0;
        int MediumQty = 0;
        double MediumAmt = 0;
        double MediumTotalAmt = 0;
        int FullQty = 0;
        double FullAmt = 0;
        double FullTotalAmt = 0;
        int LargeQty = 0;
        int cartQty = 0;
        double LargeAmt = 0;
        double LargeTotalAmt = 0;
        for (int i = 0; i < RestaurentProductList.size(); i++) {
            HalfQty = 0;
            MediumQty = 0;
            MediumTotalAmt = 0;
            FullQty = 0;
            FullAmt = 0;
            FullTotalAmt = 0;
            LargeQty = 0;
            LargeAmt = 0;
            LargeTotalAmt = 0;

            if (RestaurentProductList.get(i).halfQty != 0) {
                HalfQty = RestaurentProductList.get(i).halfQty;
                HalfAmount = Double.parseDouble(RestaurentProductList.get(i).half);
                HalfTotalAmount = HalfQty * HalfAmount;
                Log.d("HalfTotalAmount122>>>", "" + HalfTotalAmount);
                cartQty = cartQty + HalfQty;

            }
            if (RestaurentProductList.get(i).mediumQty != 0) {
                MediumQty = RestaurentProductList.get(i).mediumQty;
                MediumAmt = Double.parseDouble(RestaurentProductList.get(i).medium);
                MediumTotalAmt = MediumQty * MediumAmt;
                cartQty = cartQty + MediumQty;
                Log.d("MediumTotalAmt122>>>", "" + MediumTotalAmt);
            }
            if (RestaurentProductList.get(i).fullQty != 0) {
                FullQty = RestaurentProductList.get(i).fullQty;
                FullAmt = Double.parseDouble(RestaurentProductList.get(i).full);
                FullTotalAmt = FullQty * FullAmt;
                cartQty = cartQty + FullQty;

                Log.d("FullTotalAmt122>>>", "" + FullTotalAmt);
            }
            if (RestaurentProductList.get(i).largeQty != 0) {
                LargeQty = RestaurentProductList.get(i).largeQty;
                LargeAmt = Double.parseDouble(RestaurentProductList.get(i).large);
                LargeTotalAmt = LargeQty * LargeAmt;
                cartQty = cartQty + LargeQty;
                Log.d("LargeTotalAmt122>>>", "" + LargeTotalAmt);
            }

           /* int Quentity = 0, GlobalQuentity = 0;


            Quentity = AppGlobal.RestaurentProductList.get(i).getCount();
            Log.d("Quentity>>>", "" + Quentity);
            int total = (AppGlobal.RestaurentProductList.get(i).afterDiscountValue) * Quentity;
            int productPrice = Integer.parseInt((AppGlobal.RestaurentProductList.get(i).Product_price));
            int totalProductPrice = productPrice * Quentity;
            String minimum_purchase = AppGlobal.RestaurentProductList.get(i).minimum_purchase;
            String extra_charge = AppGlobal.RestaurentProductList.get(i).extra_charge;
            Log.d("minimum_purchase>>", minimum_purchase);
            Log.d("extra_charge>>", extra_charge);
            double container = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).container_charge);
            Log.d("Quentity>>>", "" + Quentity);
            container = container * Quentity;
            if (HalfQty != 0 || FullQty != 0 || LargeQty != 0 || MediumQty != 0) {
                AppGlobal.mainCartList.add(new CartFillPojo(
                        AppGlobal.RestaurentProductList.get(i).Product_id,
                        Quentity,
                        AppGlobal.RestaurentProductList.get(i).Product_name,
                        AppGlobal.RestaurentProductList.get(i).Product_price,
                        total,
                        AppGlobal.RestaurentProductList.get(i).restaurant_id,
                        sessionManager.getValuesSession(SessionManager.Key_UserID),
                        AppGlobal.RestaurentProductList.get(i).Product_vegproduct,
                        AppGlobal.RestaurentProductList.get(i).afterDiscountValue,
                        totalProductPrice, AppGlobal.RestaurentProductList.get(i).name,
                        AppGlobal.RestaurentProductList.get(i).discount,
                        String.valueOf(container),
                        AppGlobal.RestaurentProductList.get(i).minimum_purchase,
                        AppGlobal.RestaurentProductList.get(i).extra_charge,
                        AppGlobal.RestaurentProductList.get(i).container_charge,
                        HalfQty, HalfAmount, HalfTotalAmount,
                        FullQty, FullAmt, FullTotalAmt,
                        LargeQty, LargeAmt, LargeTotalAmt,
                        MediumQty, MediumAmt, MediumTotalAmt));
            }*/
        }
        if (cartQty != 0) {
            fabCart.setVisibility(View.VISIBLE);
            tvCartCount.setVisibility(View.VISIBLE);
            tvCartCount.setText("" + cartQty);
        } else {
            fabCart.setVisibility(View.GONE);
            tvCartCount.setVisibility(View.GONE);
            tvCartCount.setText("" + cartQty);
        }
        /*ArrayList<CartFillPojo> mainSessonCartList;
        mainSessonCartList = sessionManager.getCartList(mContext);
        if (!AppGlobal.mainCartList.isEmpty() && mainSessonCartList != null) {
            sessionManager.clearArrayList(mContext);
          //  sessionManager.saveCartList(mContext, AppGlobal.mainCartList);
            Log.d("CartQty>>>", "" + cartQty);
            if (cartQty != 0) {
                fabCart.setVisibility(View.VISIBLE);
                tvCartCount.setVisibility(View.VISIBLE);
                tvCartCount.setText("" + cartQty);
            } else {
                fabCart.setVisibility(View.GONE);
                tvCartCount.setVisibility(View.GONE);
                tvCartCount.setText("" + cartQty);
            }
        } else {
            sessionManager.saveCartList(mContext, AppGlobal.mainCartList);
            if (cartQty != 0) {
                fabCart.setVisibility(View.VISIBLE);
                tvCartCount.setVisibility(View.VISIBLE);
                tvCartCount.setText("" + cartQty);
            } else {
                fabCart.setVisibility(View.GONE);
                tvCartCount.setVisibility(View.GONE);
                tvCartCount.setText("" + cartQty);
            }
        }*/
    }
}
