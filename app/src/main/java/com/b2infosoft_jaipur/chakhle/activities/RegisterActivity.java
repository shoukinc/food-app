package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.services.Config;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.ConnectionDetector;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.useful.Validations;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvTC, tvPhoneNumber, tvSignUp, tvAgree, tvOtpText, tvSignOrReg;
    EditText edtFullName, edtLastName, edtEmail, edtPassword;
    CheckBox chkTC;
    Context mContext;
    ConnectionDetector connectionDetector;
    SessionManager sessionManager;
    ImageView locIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mContext = RegisterActivity.this;
        initView();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("layoutSecond", "login2");
        startActivity(intent);
    }

    private void initView() {
        connectionDetector = new ConnectionDetector(mContext);
        sessionManager = new SessionManager(mContext);

        locIcon = (ImageView) findViewById(R.id.locIcon);
        tvPhoneNumber = (TextView) findViewById(R.id.tvPhoneNumber);
        tvAgree = (TextView) findViewById(R.id.tvAgree);
        tvOtpText = (TextView) findViewById(R.id.tvOtpText);
        tvSignUp = (TextView) findViewById(R.id.tvSignUp);
        edtFullName = (EditText) findViewById(R.id.edtFullName);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtLastName = (EditText) findViewById(R.id.edtLastName);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        tvTC = (TextView) findViewById(R.id.tvTC);
        tvSignOrReg = (TextView) findViewById(R.id.tvSignOrReg);
        tvPhoneNumber.setText(AppGlobal.MobileNo);
        tvTC.setPaintFlags(tvTC.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvTC.setText("T&C's");
        tvSignUp.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_light.ttf"));
        edtFullName.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtEmail.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtLastName.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtPassword.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        tvSignOrReg.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvAgree.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        tvSignUp.setOnClickListener(this);
        locIcon.setOnClickListener(this);


    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tvSignUp:
                if (connectionDetector.isConnectingToInternet()) {
                    if (!edtFullName.getText().toString().equals("")
                            && !edtPassword.getText().toString().equals("")) {
                        if (!edtEmail.getText().toString().equals("")) {
                            if (Validations.isValidEmail(edtEmail, "Please enter valid email")) {
                                signUpUser();
                            }
                        } else {
                            signUpUser();
                        }
                    } else {
                        UtilityMethod.showAlertBox(mContext, "Please fill all fields");
                    }
                } else {
                    connectionDetector.showAlertDialog(mContext, "No Internet Connection", "You don't have internet connection.", true);

                }
                break;
            case R.id.locIcon:
                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("layoutSecond", "login2");
                startActivity(intent);
                break;

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    private void signUpUser() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        //  DeviceID= pref.getString("regId", null);
        Log.e("FirebaseRegIDLogin>>>", "" + regId);
        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, AppGlobal.ApiCallWaitMsg, true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    //{"data":[],"message":"Register successfull.","success":true,"status":200}
                    if (mainObj.getString("success").equals("true")) {
                        //{"data":{"User":{"id":"118","role_id":"3","parent_id":"0","location_id":"0","city_id":"0","state_id":"0","country_id":"0","address_line1":"","address_line2":"","pincode":"","firstname":"asdff","lastname":"asddd","lastname2":null,"email":"aaa@g.com","password":"a92cb1efc6b301bb426383b11b138225c68411f4","cp":"","phone":null,"mobile":"9633333333","job":null,"config":"","fbid":null,"googleid":"","twid":null,"findfrom":"","isimg":"","desire_url":null,"fav_feature":null,"i_am":null,"business_plan":null,"api":null,"extra_point_added_by_super_admin":"0","language_id":"0","smscode":null,"status":"inactive","created":"2017-05-15 23:56:27","modified":"2017-05-15 23:56:27","referral_code":"asdffdATb1","otp":"688651"}},"message":"Register successfull.","success":true,"status":200}
                        JSONObject dataObject = mainObj.getJSONObject("data");
                        JSONObject userObject = dataObject.getJSONObject("User");
                        AppGlobal.UserID = userObject.getString("id");
                        Log.d("UserID>>>", AppGlobal.UserID);
                        Log.d("otp>>>", userObject.getString("otp"));
                        UtilityMethod.showAlertBoxwithIntentClearHistory(mContext, "Please Verify OTP", OtpActivity.class);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        serviceCaller.addNameValuePair("token", regId);
        serviceCaller.addNameValuePair("mobile", AppGlobal.MobileNo);
        serviceCaller.addNameValuePair("firstname", edtFullName.getText().toString());
        serviceCaller.addNameValuePair("lastname", "");
        serviceCaller.addNameValuePair("email", edtEmail.getText().toString());
        serviceCaller.addNameValuePair("password", edtPassword.getText().toString());
        serviceCaller.addNameValuePair("referrer_code", "");
        String url = AppGlobal.SignUpUser;
        Log.d("url>>", url);
        serviceCaller.execute(url);

    }
}
