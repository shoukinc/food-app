package com.b2infosoft_jaipur.chakhle.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.RecentSerchesAdapter;
import com.b2infosoft_jaipur.chakhle.adapter.SavedAddressAdapter;
import com.b2infosoft_jaipur.chakhle.fragments.CallFragment;
import com.b2infosoft_jaipur.chakhle.pojo.SavedAddressPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.GPSTracker;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.fastaccess.permission.base.PermissionHelper;
import com.fastaccess.permission.base.callback.OnPermissionCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class SearchRestaurentActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, OnPermissionCallback, CallFragment {

    EditText edtSearch;
    AutoCompleteTextView autoCompView;
    LinearLayout layoutUseGPS;
    RecyclerView recycler_savedAddress, recycler_recentSearch;
    Context mContext;
    GPSTracker gps;
    Location mLocation;
    RecentSerchesAdapter recentSerchesAdapter;
    SavedAddressAdapter savedAddressAdapter;
    private static final String API_KEY = "AIzaSyCWSJNo4sQfVonDPjn0CVhWmK07aypSebA";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private final static String[] MULTI_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    PermissionHelper permissionHelper;
    ImageView iconSearch, backArrow;
    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_restaurent);
        mContext = SearchRestaurentActivity.this;
        sessionManager = new SessionManager(mContext);
        initView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       // UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);

        Intent intent = new Intent(mContext, RestaurantListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle();
        bundle.putString("ClassName", "RestaurentDetailsActivity");
        bundle.putString("AddToFav", "true");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void initView() {


       /* edtSearch = (EditText) findViewById(R.id.edtSearch);
        edtSearch.setText("Select delivery location");*/
        permissionHelper = PermissionHelper.getInstance(this);
        autoCompView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        autoCompView.setVisibility(View.VISIBLE);
        iconSearch = (ImageView) findViewById(R.id.iconSearch);
        backArrow = (ImageView) findViewById(R.id.backArrow);
        layoutUseGPS = (LinearLayout) findViewById(R.id.layoutUseGPS);

        recycler_savedAddress = (RecyclerView) findViewById(R.id.recycler_savedAddress);
        layoutUseGPS.setOnClickListener(this);
        iconSearch.setOnClickListener(this);
        backArrow.setOnClickListener(this);
        autoCompView.setCursorVisible(false);
        autoCompView.setClickable(false);
        autoCompView.setEnabled(false);
        if (sessionManager.isLoggedIn()) {
            SavedAddressPojo.getAddressList(mContext, sessionManager.getValuesSession(SessionManager.Key_UserID), "Activity", "", "SearchResturentActivity");
        }
        if (sessionManager.getValuesSession(SessionManager.Key_RecentPlaces) != null && !sessionManager.getValuesSession(SessionManager.Key_RecentPlaces).equals("")) {
            Log.d("address2222>>>", sessionManager.getValuesSession(SessionManager.Key_RecentPlaces));

            List<String> getLocationList = Arrays.asList(sessionManager.getValuesSession(SessionManager.Key_RecentPlaces).split(",,"));
            for (int i = 0; i < getLocationList.size(); i++) {
                Log.d("address222>>", "" + getLocationList.get(i));
            }
            if (getLocationList != null) {
                recycler_recentSearch = (RecyclerView) findViewById(R.id.recycler_recentSearch);
                recycler_recentSearch.setHasFixedSize(true);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
                recycler_recentSearch.setLayoutManager(layoutManager);
                recentSerchesAdapter = new RecentSerchesAdapter(mContext, getLocationList);
                recycler_recentSearch.setAdapter(recentSerchesAdapter);
            }
        }
    }

    public void setAddressList(ArrayList<SavedAddressPojo> savedAddressList) {
        recycler_savedAddress.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(mContext);
        recycler_savedAddress.setLayoutManager(layoutManager2);
        savedAddressAdapter = new SavedAddressAdapter(mContext, savedAddressList, "SearchRestaurent");
        recycler_savedAddress.setAdapter(savedAddressAdapter);
        recycler_savedAddress.setNestedScrollingEnabled(true);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        String str = (String) adapterView.getItemAtPosition(position);
        //Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
        //  sessionManager.clearArrayLocationList(mContext);
        //  locationList.add(str);
        String kept = str.substring(0, str.indexOf(","));
        String remainder = str.substring(str.indexOf(",") + 1, str.length());
        Log.d("reminder>>>", remainder);
        Log.d("kept>>>", kept);
        AppGlobal.City = kept;

        if (sessionManager.isLoggedIn()) {
            sessionManager.saveLocationList(mContext, str);
        }
        Intent intent = new Intent(mContext, RestaurantListActivity.class);
        //intent.putExtra("locationAddress", str);
        //intent.putExtra("address", address);
        Bundle bundle = new Bundle();
        bundle.putString("ClassName", "SearchRestaurentActivity");
        //Add your data from getFactualResults method to bundle

        AppGlobal.Addresses=str;
        AppGlobal.LocationAddress = str;
        AppGlobal.City = str;
        bundle.putString("locationAddress", str);
        bundle.putString("address", str);
        bundle.putString("city", kept);

        intent.putExtras(bundle);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.iconSearch:
               /* edtSearch.setText("");
                edtSearch.setClickable(true);
                edtSearch.setCursorVisible(true);*/
                autoCompView.setCursorVisible(true);
                autoCompView.setClickable(true);
                autoCompView.setEnabled(true);
                autoCompView.setHint("");
                //autoCompView.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(autoCompView, InputMethodManager.SHOW_IMPLICIT);
                autoCompView.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.list_item));
                autoCompView.setOnItemClickListener(this);
                //Toast.makeText(mContext,"clicked",Toast.LENGTH_SHORT).show();

                break;
            case R.id.layoutUseGPS:
                if (Build.VERSION.SDK_INT >= 23) {
                    if (permissionHelper.isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION) && permissionHelper.isPermissionGranted(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        gps = new GPSTracker(SearchRestaurentActivity.this);
                        if (gps.canGetLocation()) {
                            String locationAddress = "";
                            String address = "";
                            String city = "";
                            String state = "";
                            String country = "";
                            double latitude = gps.getLatitude();
                            double longitude = gps.getLongitude();
                            Log.d("Location>>>", "" + latitude + "," + longitude);
                            Geocoder geocoder;
                            List<Address> addresses = null;
                            geocoder = new Geocoder(SearchRestaurentActivity.this, Locale.getDefault());
                            try {
                                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                                address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                city = addresses.get(0).getLocality();
                                state = addresses.get(0).getAdminArea();
                                country = addresses.get(0).getCountryName();
                                String postalCode = addresses.get(0).getPostalCode();
                                String knownName = addresses.get(0).getFeatureName();
                                Log.d("address>>", address);
                                Log.d("address>>", city);
                                Log.d("address>>", state);
                                Log.d("address>>", country);
                                // Log.d("address>>",address);
                                //Log.d("address>>",addresses.toString());

                                // \n is for new line
                                locationAddress = address + ", " + city + ", " + state + ", " + country;// Here 1 represent max location result to returned, by documents it recommended 1 to 5
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                            Log.d("address>>", locationAddress);
                            Intent intent = new Intent(mContext, RestaurantListActivity.class);
                     /*  /*//* intent.putExtra("locationAddress", locationAddress);
                        intent.putExtra("address", address);*/
                            Bundle bundle = new Bundle();
                            bundle.putString("ClassName", "SearchRestaurentActivity");

                            /*AppGlobal.LocationAddress = locationAddress;
                            AppGlobal.City = city;*/
                            //Add your data from getFactualResults method to bundle
                            bundle.putString("locationAddress", locationAddress);
                            bundle.putString("address", address);
                            bundle.putString("city", city);

                            //Add the bundle to the intent
                            intent.putExtras(bundle);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            //Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        permissionHelper
                                .setForceAccepting(false)
                                .request(MULTI_PERMISSIONS);
                    }
                } else {
                    gps = new GPSTracker(SearchRestaurentActivity.this);
                    if (gps.canGetLocation()) {
                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();
                        Log.d("Location>>>", "" + latitude + "," + longitude);
                        Geocoder geocoder;
                        List<Address> addresses = null;
                        try {
                            geocoder = new Geocoder(SearchRestaurentActivity.this, Locale.getDefault());
                            addresses = geocoder.getFromLocation(latitude, longitude, 1);
                            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String country = addresses.get(0).getCountryName();
                            String postalCode = addresses.get(0).getPostalCode();
                            String knownName = addresses.get(0).getFeatureName();
                            Log.d("address>>", address);
                            Log.d("address>>", city);
                            Log.d("address>>", state);
                            Log.d("address>>", country);
                            // Log.d("address>>",address);
                            //Log.d("address>>",addresses.toString());

                            // \n is for new line
                            String locationAddress = address + ", " + city + ", " + state + ", " + country;
                            Log.d("address>>", locationAddress);
                            Intent intent = new Intent(mContext, RestaurantListActivity.class);
                     /*  /*//* intent.putExtra("locationAddress", locationAddress);
                        intent.putExtra("address", address);*/
                            Bundle bundle = new Bundle();
                            bundle.putString("ClassName", "SearchRestaurentActivity");
                            //Add your data from getFactualResults method to bundle
                            bundle.putString("locationAddress", locationAddress);
                            bundle.putString("address", address);
                            bundle.putString("city", city);

                            //Add the bundle to the intent
                            intent.putExtras(bundle);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);// Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                        // \n is for new line
                        // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    } else {
                        // can't get location
                        // GPS or Network is not enabled
                        // Ask user to enable GPS/network in settings
                        gps.showSettingsAlert();
                    }
                }

                break;
            case R.id.backArrow:
               // startActivity(new Intent(mContext, RestaurantListActivity.class));
                onBackPressed();
                break;
        }

    }

    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:IN");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            //Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            //Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            //Log.e(LOG_TAG, "Cannot process JSON results", e);
            e.printStackTrace();
        }

        return resultList;
    }

    @Override
    public void setFragment(String fragmentName) {

    }

    @Override
    public void setCartValue(int value) {

    }


    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    @Override
    public void onPermissionGranted(@NonNull String[] permissionName) {
        Log.d("onPermissionGranted", "Permission(s) " + Arrays.toString(permissionName) + " Granted");


    }

    @Override
    public void onPermissionDeclined(@NonNull String[] permissionName) {
        Log.d("onPermissionDeclined", "Permission(s) " + Arrays.toString(permissionName) + " Declined");

    }

    @Override
    public void onPermissionPreGranted(@NonNull String permissionsName) {
        Log.d("onPermissionPreGranted", "Permission( " + permissionsName + " ) preGranted");

    }

    @Override
    public void onPermissionNeedExplanation(@NonNull String permissionName) {
        Log.i("NeedExplanation", "Permission( " + permissionName + " ) needs Explanation");

      /*  neededPermission = PermissionFragmentHelper.declinedPermissions(this, MULTI_PERMISSIONS);
        StringBuilder builder = new StringBuilder(neededPermission.length);
        if (neededPermission.length > 0) {
            for (String permission : neededPermission) {
                builder.append(permission).append("\n");
            }
        }
        AlertDialog alert = getAlertDialog(neededPermission, builder.toString());
        if (!alert.isShowing()) {
            alert.show();
        }*/
    }

    @Override
    public void onPermissionReallyDeclined(@NonNull String permissionName) {
        Log.d("ReallyDeclined", "Permission " + permissionName + " can only be granted from settingsScreen");
        /** you can call  {@link PermissionFragmentHelper#openSettingsScreen(Fragment)} to open the settings screen */
    }

    @Override
    public void onNoPermissionNeeded() {
        Log.d("onNoPermissionNee" +
                "ded", "Permission(s) not needed");

    }

}
