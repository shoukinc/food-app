package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.MyOrderAdapter;
import com.b2infosoft_jaipur.chakhle.pojo.MyOrderPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.util.ArrayList;

public class MyOrdersActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recyclerMyOrder;
    Context mContext;
    ImageView locIcon;
    TextView tvOrder;
    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        mContext = MyOrdersActivity.this;
        sessionManager = new SessionManager(mContext);
        locIcon = (ImageView) findViewById(R.id.locIcon);
        tvOrder = (TextView) findViewById(R.id.tvOrder);
        tvOrder.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        recyclerMyOrder = (RecyclerView) findViewById(R.id.recyclerMyOrder);
        locIcon.setOnClickListener(this);
        MyOrderPojo.getOrderList(mContext, sessionManager.getValuesSession(SessionManager.Key_UserID), "MyOrder");
    }

    public void setOrderAdapter(ArrayList<MyOrderPojo> orderList, ArrayList<MyOrderPojo> productList) {
        AppGlobal.OrderproductList.clear();
        AppGlobal.UserOrderList.clear();
        AppGlobal.OrderproductList = productList;
        AppGlobal.UserOrderList = orderList;
        final RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recyclerMyOrder.setLayoutManager(linearLayoutManager);
        recyclerMyOrder.setAdapter(new MyOrderAdapter(mContext, orderList));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       // UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);
        Intent intent = new Intent(mContext, RestaurantListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle();
        bundle.putString("ClassName", "RestaurentDetailsActivity");
        bundle.putString("AddToFav", "true");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.locIcon:
               // UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);
                onBackPressed();
                break;

        }
    }
}
