package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.FavRestorentAdapter;
import com.b2infosoft_jaipur.chakhle.pojo.ResturentListPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.util.ArrayList;

public class FavRestorentActivity extends AppCompatActivity {

    TextView tvFav;
    ImageView locIcon;
    RecyclerView recycler_favRestro;
    Context mContext;
    FavRestorentAdapter favRestorentAdapter;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav_resto);
        mContext = FavRestorentActivity.this;
        sessionManager = new SessionManager(mContext);
        tvFav = (TextView) findViewById(R.id.tvFav);
        locIcon = (ImageView) findViewById(R.id.locIcon);
        recycler_favRestro = (RecyclerView) findViewById(R.id.recycler_favRestro);
        tvFav.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        if (sessionManager.isLoggedIn()) {
            ResturentListPojo.getFavRestaurant(mContext, sessionManager.getValuesSession(SessionManager.Key_UserID));
        }

        locIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);
                onBackPressed();
            }
        });
    }

    public void setFavRestorantList(ArrayList<ResturentListPojo> list, ArrayList<ResturentListPojo> discountList) {
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recycler_favRestro.setLayoutManager(linearLayoutManager);
        favRestorentAdapter = new FavRestorentAdapter(mContext, list, discountList);
        recycler_favRestro.setAdapter(favRestorentAdapter);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(mContext, RestaurantListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle();
        bundle.putString("ClassName", "RestaurentDetailsActivity");
        bundle.putString("AddToFav", "true");
        intent.putExtras(bundle);
        startActivity(intent);

    }
}
