package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.CuisineListAdapter;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.util.ArrayList;

public class FilterActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView locIcon, iconRefresh, imgRating, imgTime;
    Context mContext;
    TextView tvFilter, tvCost, tvAffordable, tvAverage, tvCostly, tvPremium, tvRating, tvPriceRange, tvTimeRange;
    TextView tvRestaurentWith, tvChakhleAssured, tvPureVeg, tvOffers, tvCusinies, tvSortBy;
    CheckBox chk1, chk2, chk3, chk4;
    SwitchCompat switch_ChakhleAssured, switch_PureVeg, switch_Offers;
    RecyclerView recycler_cuisinesList;
    CuisineListAdapter cuisineListAdapter;
    Button btnApplyFilter;
    RelativeLayout layoutDeliveryTime, layoutRating;
    int fill_iconID;
    int blank_iconID;
    String chakhle_assured = "0", pure_veg_dishes = "0",Rating="0",offers="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        mContext = FilterActivity.this;
        initView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(mContext, RestaurantListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle();
        bundle.putString("ClassName", "RestaurentDetailsActivity");
        bundle.putString("AddToFav", "true");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void initView() {
        fill_iconID = R.drawable.fill_star;
        blank_iconID = R.drawable.blank_star;
        // layoutDeliveryTime = (RelativeLayout) findViewById(R.id.layoutDeliveryTime);
        layoutRating = (RelativeLayout) findViewById(R.id.layoutRating);

        btnApplyFilter = (Button) findViewById(R.id.btnApplyFilter);
        locIcon = (ImageView) findViewById(R.id.locIcon);
        //  imgTime = (ImageView) findViewById(R.id.imgTime);
        imgRating = (ImageView) findViewById(R.id.imgRating);
        iconRefresh = (ImageView) findViewById(R.id.iconRefresh);
        tvFilter = (TextView) findViewById(R.id.tvFilter);
        tvCost = (TextView) findViewById(R.id.tvHalfPrice);
        tvAffordable = (TextView) findViewById(R.id.tvAffordable);
        tvAverage = (TextView) findViewById(R.id.tvAverage);
        tvCostly = (TextView) findViewById(R.id.tvCostly);
        tvPremium = (TextView) findViewById(R.id.tvPremium);
        tvRating = (TextView) findViewById(R.id.tvRating);
        tvPriceRange = (TextView) findViewById(R.id.tvPriceRange);
        // tvDeliveryTime = (TextView) findViewById(R.id.tvDeliveryTime);
        //  tvTimeRange = (TextView) findViewById(R.id.tvTimeRange);
        tvRestaurentWith = (TextView) findViewById(R.id.tvRestaurentWith);
        tvChakhleAssured = (TextView) findViewById(R.id.tvChakhleAssured);
        tvPureVeg = (TextView) findViewById(R.id.tvPureVeg);
        tvOffers = (TextView) findViewById(R.id.tvOffers);
        tvCusinies = (TextView) findViewById(R.id.tvCusinies);
        tvSortBy = (TextView) findViewById(R.id.tvSortBy);
        chk1 = (CheckBox) findViewById(R.id.chk1);
        chk2 = (CheckBox) findViewById(R.id.chk2);
        chk3 = (CheckBox) findViewById(R.id.chk3);
        chk4 = (CheckBox) findViewById(R.id.chk4);
        switch_ChakhleAssured = (SwitchCompat) findViewById(R.id.switch_ChakhleAssured);
        switch_PureVeg = (SwitchCompat) findViewById(R.id.switch_PureVeg);
        switch_Offers = (SwitchCompat) findViewById(R.id.switch_Offers);
        recycler_cuisinesList = (RecyclerView) findViewById(R.id.recycler_cuisinesList);
        tvFilter.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvCost.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvAffordable.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvAverage.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvCostly.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvPremium.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvPriceRange.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvRating.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvCusinies.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvSortBy.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));

        tvRestaurentWith.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvChakhleAssured.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvPureVeg.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvOffers.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        btnApplyFilter.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));

        locIcon.setOnClickListener(this);
        layoutRating.setOnClickListener(this);
        btnApplyFilter.setOnClickListener(this);

        switch_ChakhleAssured.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (switch_ChakhleAssured.isChecked()) {
                    chakhle_assured = "1";
                } else {
                    chakhle_assured = "0";
                }
            }
        });
        switch_PureVeg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (switch_PureVeg.isChecked()) {
                    pure_veg_dishes = "1";
                } else {
                    pure_veg_dishes = "0";
                }
            }
        });
        switch_Offers.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (switch_Offers.isChecked()) {
                    offers = "1";
                } else {
                    offers = "0";
                }
            }
        });

        ArrayList<String> stringsList = new ArrayList<>();
        stringsList.add("American");
        stringsList.add("Arbian");
        stringsList.add("Asian");
        stringsList.add("Bakery");
        stringsList.add("Bengali");
        stringsList.add("Bihari");
        stringsList.add("Cafe");
        stringsList.add("Chiness");
        stringsList.add("Dessert");
        stringsList.add("French");
        stringsList.add("Healthy Food");
        stringsList.add("Punjanbi");
        stringsList.add("Rajasthani");

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recycler_cuisinesList.setLayoutManager(layoutManager);
        cuisineListAdapter = new CuisineListAdapter(mContext, stringsList);
        recycler_cuisinesList.setAdapter(cuisineListAdapter);
        recycler_cuisinesList.setNestedScrollingEnabled(false);

    }

    @Override
    public void onClick(View view) {
        Object tag;
        switch (view.getId()) {
            case R.id.layoutRating:
                tag = imgRating.getTag();
                if (tag != null && ((Integer) tag).intValue() == blank_iconID) {
                    imgRating.setBackgroundResource(fill_iconID);
                    imgRating.setTag(fill_iconID);

                    Rating="1";

                } else {
                    imgRating.setBackgroundResource(blank_iconID);
                    imgRating.setTag(blank_iconID);
                   Rating="0";
                }

                break;
            case R.id.locIcon:
                //  UtilityMethod.goNextClass(mContext,RestaurantListActivity.class);
                onBackPressed();
                break;
            case R.id.btnApplyFilter:

                if (chakhle_assured.equals("0") && pure_veg_dishes.equals("0")&& offers.equals("0")) {

                   if (Rating.equals("0")){
                       UtilityMethod.showAlertBox(mContext, "Please select option for filter");
                   }else{
                       Intent intent = new Intent(mContext, RestaurantListActivity.class);
                       intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                       intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                       Bundle bundle = new Bundle();
                       bundle.putString("ClassName", "RestaurentFilterActivity");
                       bundle.putString("AddToFav", "true");

                       AppGlobal.Chakhle_assured = chakhle_assured;
                       AppGlobal.Pure_veg_dishes = pure_veg_dishes;
                       AppGlobal.Rating = Rating;
                       AppGlobal.Offers = offers;

                       intent.putExtras(bundle);
                       startActivity(intent);
                   }

                } else {

                    Intent intent = new Intent(mContext, RestaurantListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle bundle = new Bundle();
                    bundle.putString("ClassName", "RestaurentFilterActivity");
                    bundle.putString("AddToFav", "true");

                    AppGlobal.Chakhle_assured = chakhle_assured;
                    AppGlobal.Pure_veg_dishes = pure_veg_dishes;
                    AppGlobal.Rating = Rating;
                    AppGlobal.Offers = offers;

                    intent.putExtras(bundle);
                    startActivity(intent);
                }

                break;
        }

    }
}
