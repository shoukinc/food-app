package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.OfferListAdapter;
import com.b2infosoft_jaipur.chakhle.pojo.OfferListPojo;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;

import java.util.ArrayList;

public class OfferActivity extends AppCompatActivity {

    RecyclerView recycler_offerList;
    TextView tvOffers;
    ImageView locIcon;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);
        mContext = OfferActivity.this;
        initView();


    }

    private void initView() {

        OfferListPojo.getOfferList(mContext);


       /* ArrayList<OfferListPojo> offerList = new ArrayList<>();
        offerList.add(new OfferListPojo("Get 50% Discount On Chakhle Assured", "CHAKHLEASS50"));
        offerList.add(new OfferListPojo("Get 10% Discount On Delivery", "CHAKHLEDEL10"));
        offerList.add(new OfferListPojo("Get 50% Discount On Chakhle", "CHAKHLE50"));
        offerList.add(new OfferListPojo("Get 15% Discount On Chakhle Assian Food", "CHAKHLEASSF15"));
        offerList.add(new OfferListPojo("Get Rs 50 cashback on first order + Rs 70 discount on next order", "CHAKHLE FIRST"));
        offerList.add(new OfferListPojo("Get 5% Discount On Chakhle Assured", "CHAKHLEASS5"));
*/

        tvOffers = (TextView) findViewById(R.id.tvOffers);
        locIcon = (ImageView) findViewById(R.id.locIcon);
        recycler_offerList = (RecyclerView) findViewById(R.id.recycler_offerList);
        tvOffers.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));

        locIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);
            }
        });
    }

    public void setOfferListAdapter(ArrayList<OfferListPojo> offerListPojos) {
        final RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recycler_offerList.setLayoutManager(linearLayoutManager);
        recycler_offerList.setAdapter(new OfferListAdapter(mContext, offerListPojos));
        recycler_offerList.setNestedScrollingEnabled(false);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);
    }

}
