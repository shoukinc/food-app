package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.OrderSummaryAdapter;
import com.b2infosoft_jaipur.chakhle.pojo.MyOrderPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.ConnectionDetector;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

public class OrderStatusActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvOrderID, tvOrderStatus, tvRecieved, tvInProcess, tvOut, tvCompletd, tvContainerChargeTxt, tvContainerCharge;
    ImageView locIcon, imgRecieved, imgInProcess, imgOutToDeli, imgCompleted;
    Context mContext;
    ConnectionDetector connectionDetector;
    SessionManager sessionManager;
    TextView tvAmountPay, tvAmountPayTxt, tvTotalCost, tvTotalCostTxt, tvTax, tvTaxTxt, tvConvenice, tvConvenFee, tvDeliveryCharge, tvDeliveryChargeTxt, tvCost, tvQuantity, tvMealName, tvOrderSummry, tvAddress, tvAddressTxt, tvPlacedAt, tvPlacedAtTxt, tvScheduleTime, tvScheduleTimeTxt, tvOrderDate, tvOrderDateTxt;
    RecyclerView recycler_orderList;
    private Timer mTimer1;
    private TimerTask mTt1;
    TextView tvNextOrder, tvVarient;
    private Handler mTimerHandler = new Handler();
    private final static int INTERVAL = 1000 * 60 * 1; //1 minutes
    View divContainer, divDiscount;
    LinearLayout layoutContainer, layoutDiscount;
    TextView tvItemTot, tvItemTotTxt;
    TextView tvAfterDiscountTot, tvAfterDiscountTotTxt, tvAfterGstTotTxt, tvAfterGstTot, tvGstTxt, tvGstAmt;

    LinearLayout layoutContainer_bg,layoutDelivery_bg,tvGst_bg,layoutDiscount_bg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);
        mContext = OrderStatusActivity.this;
        initView();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startTimer();
            }
        }, INTERVAL);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopTimer();
    }

    private void stopTimer() {
        if (mTimer1 != null) {
            mTimer1.cancel();
            mTimer1.purge();
        }
    }

    private void startTimer() {
        mTimer1 = new Timer();
        mTt1 = new TimerTask() {
            public void run() {
                mTimerHandler.post(new Runnable() {
                    public void run() {
                        //TODO
                        //Toast.makeText(mContext, "hi", Toast.LENGTH_SHORT).show();
                        MyOrderPojo.getOrderList(mContext, sessionManager.getValuesSession(SessionManager.Key_UserID), "OrderStatus");
                    }
                });
            }
        };

        mTimer1.schedule(mTt1, 1, INTERVAL);
    }

    private void initView() {
        connectionDetector = new ConnectionDetector(mContext);
        sessionManager = new SessionManager(mContext);
        tvAfterGstTotTxt = (TextView) findViewById(R.id.tvAfterGstTotTxt);
        tvAfterGstTot = (TextView) findViewById(R.id.tvAfterGstTot);
        tvAfterDiscountTot = (TextView) findViewById(R.id.tvAfterDiscountTot);
        tvAfterDiscountTotTxt = (TextView) findViewById(R.id.tvAfterDiscountTotTxt);

        layoutDiscount_bg=(LinearLayout)findViewById(R.id.layoutDiscount_bg);
        tvGst_bg=(LinearLayout)findViewById(R.id.tvGst_bg);
        layoutDelivery_bg=(LinearLayout)findViewById(R.id.layoutDelivery_bg);
        layoutContainer_bg=(LinearLayout)findViewById(R.id.layoutContainer_bg);


        tvItemTot = (TextView) findViewById(R.id.tvItemTot);
        tvItemTotTxt = (TextView) findViewById(R.id.tvItemTotTxt);
        tvOrderID = (TextView) findViewById(R.id.tvOrderID);
        tvOrderStatus = (TextView) findViewById(R.id.tvOrderStatus);
        tvVarient = (TextView) findViewById(R.id.tvHalfLabel);
        tvRecieved = (TextView) findViewById(R.id.tvRecieved);
        tvInProcess = (TextView) findViewById(R.id.tvInProcess);
        tvOut = (TextView) findViewById(R.id.tvOut);
        tvCompletd = (TextView) findViewById(R.id.tvCompletd);
        locIcon = (ImageView) findViewById(R.id.locIcon);
        imgRecieved = (ImageView) findViewById(R.id.imgRecieved);
        imgInProcess = (ImageView) findViewById(R.id.imgInProcess);
        imgOutToDeli = (ImageView) findViewById(R.id.imgOutToDeli);
        imgCompleted = (ImageView) findViewById(R.id.imgCompleted);
        tvAmountPay = (TextView) findViewById(R.id.tvAmountPay);
        tvAmountPayTxt = (TextView) findViewById(R.id.tvAmountPayTxt);
        tvTotalCost = (TextView) findViewById(R.id.tvTotalCost);
        tvTotalCostTxt = (TextView) findViewById(R.id.tvTotalCostTxt);
        tvTax = (TextView) findViewById(R.id.tvTax);
        tvTaxTxt = (TextView) findViewById(R.id.tvTaxTxt);
        tvConvenice = (TextView) findViewById(R.id.tvConvenice);
        tvDeliveryCharge = (TextView) findViewById(R.id.tvDeliveryCharge);
        tvDeliveryChargeTxt = (TextView) findViewById(R.id.tvDeliveryChargeTxt);
        tvCost = (TextView) findViewById(R.id.tvHalfPrice);
        tvQuantity = (TextView) findViewById(R.id.tvHalfQty);
        tvMealName = (TextView) findViewById(R.id.tvMealName);
        tvOrderSummry = (TextView) findViewById(R.id.tvOrderSummry);
        tvAddress = (TextView) findViewById(R.id.tvAddress);
        tvAddressTxt = (TextView) findViewById(R.id.tvAddressTxt);
        tvPlacedAt = (TextView) findViewById(R.id.tvPlacedAt);
        tvPlacedAtTxt = (TextView) findViewById(R.id.tvPlacedAtTxt);
        tvScheduleTime = (TextView) findViewById(R.id.tvScheduleTime);
        tvScheduleTimeTxt = (TextView) findViewById(R.id.tvScheduleTimeTxt);
        tvOrderDate = (TextView) findViewById(R.id.tvOrderDate);
        tvOrderDateTxt = (TextView) findViewById(R.id.tvOrderDateTxt);
        tvConvenFee = (TextView) findViewById(R.id.tvConvenFee);
        tvNextOrder = (TextView) findViewById(R.id.tvNextOrder);
        tvContainerChargeTxt = (TextView) findViewById(R.id.tvContainerChargeTxt);
        tvContainerCharge = (TextView) findViewById(R.id.tvContainerCharge);
        layoutContainer = (LinearLayout) findViewById(R.id.layoutContainer);
        layoutDiscount = (LinearLayout) findViewById(R.id.layoutDiscount);
        divDiscount = (View) findViewById(R.id.divDiscount);
        divContainer = (View) findViewById(R.id.divContainer);

        tvGstTxt = (TextView) findViewById(R.id.tvGstTxt);
        tvGstAmt = (TextView) findViewById(R.id.tvGstAmt);
        recycler_orderList = (RecyclerView) findViewById(R.id.recycler_orderList);
        locIcon.setOnClickListener(this);


        tvGstTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvAfterGstTotTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvAfterDiscountTotTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvContainerChargeTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvItemTotTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvRecieved.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvInProcess.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvOut.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvCompletd.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        // tvOrderID.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvOrderStatus.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvNextOrder.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        //tvAmountPay.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvAmountPayTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        //tvTotalCost.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvTotalCostTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvVarient.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        //  tvTax.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvTaxTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        //  tvConvenice.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvConvenFee.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        //tvDeliveryCharge.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvDeliveryChargeTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvCost.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvQuantity.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvMealName.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvOrderSummry.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvAddress.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvAddressTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvPlacedAt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvPlacedAtTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        //tvScheduleTime.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvScheduleTimeTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        //tvOrderDate.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvOrderDateTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        MyOrderPojo.getOrderList(mContext, sessionManager.getValuesSession(SessionManager.Key_UserID), "OrderStatus");
        tvOrderID.setText("#" + AppGlobal.OrderID);
        tvNextOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                //UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);
            }
        });
    }

    public void setOrderAdapter(ArrayList<MyOrderPojo> orderList, ArrayList<MyOrderPojo> productList) {
        /*for (int i = 0; i < AppGlobal.UserOrderList.size(); i++) {
            if (AppGlobal.OrderID.equals(AppGlobal.UserOrderList.get(i).OrderID)) {
                tvScheduleTime.setText(AppGlobal.UserOrderList.get(i).Mobileno);
                tvPlacedAt.setText(AppGlobal.UserOrderList.get(i).Restaurant_name);
                tvAddress.setText(AppGlobal.UserOrderList.get(i).delivery_address);
                tvDeliveryCharge.setText("Rs " + AppGlobal.UserOrderList.get(i).delivery_amount + " /-");
                if (AppGlobal.UserOrderList.get(i).discount_type.equals("price")) {
                    tvConvenice.setText("Rs " + AppGlobal.UserOrderList.get(i).discount + " /-");
                } else {
                    tvConvenice.setText(AppGlobal.UserOrderList.get(i).discount + " %");
                }
                tvTax.setText("Rs " + AppGlobal.UserOrderList.get(i).tax_amount + " /-");
                tvTotalCost.setText("Rs " + AppGlobal.UserOrderList.get(i).item_total_amount + " /-");
                //tvAmountPay.setText(AppGlobal.OrderproductList.get(i).am);
            }
        }*/
        /*ArrayList<MyOrderPojo> orderDetailList = new ArrayList<>();
        for (int i = 0; i < AppGlobal.OrderproductList.size(); i++) {
            if (AppGlobal.OrderID.equals(AppGlobal.OrderproductList.get(i).order_id)) {
                orderDetailList.add(new MyOrderPojo(
                        AppGlobal.OrderproductList.get(i).OrderItem_id,
                        AppGlobal.OrderproductList.get(i).order_id,
                        AppGlobal.OrderproductList.get(i).product_id,
                        AppGlobal.OrderproductList.get(i).quantity,
                        AppGlobal.OrderproductList.get(i).name,
                        AppGlobal.OrderproductList.get(i).unit_price,
                        AppGlobal.OrderproductList.get(i).container_charge,
                        AppGlobal.OrderproductList.get(i).product_total,
                        AppGlobal.OrderproductList.get(i).restaurant_id,
                        AppGlobal.OrderproductList.get(i).half_label,
                        AppGlobal.OrderproductList.get(i).half_qty,
                        AppGlobal.OrderproductList.get(i).half_price,
                        AppGlobal.OrderproductList.get(i).medium_label,
                        AppGlobal.OrderproductList.get(i).medium_qty,
                        AppGlobal.OrderproductList.get(i).medium_price,
                        AppGlobal.OrderproductList.get(i).full_label,
                        AppGlobal.OrderproductList.get(i).full_qty,
                        AppGlobal.OrderproductList.get(i).full_price,
                        AppGlobal.OrderproductList.get(i).large_label,
                        AppGlobal.OrderproductList.get(i).large_qty,
                        AppGlobal.OrderproductList.get(i).large_price
                ));
            }
        }*//*
        if (!orderDetailList.isEmpty()) {
            final RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            recycler_orderList.setLayoutManager(linearLayoutManager);
            recycler_orderList.setAdapter(new OrderSummaryAdapter(mContext, orderDetailList));
          //  recycler_orderList.setNestedScrollingEnabled(false);

        }*/
        AppGlobal.OrderproductList.clear();
        AppGlobal.UserOrderList.clear();
        AppGlobal.OrderproductList = productList;
        AppGlobal.UserOrderList = orderList;
        for (int i = 0; i < AppGlobal.UserOrderList.size(); i++) {
            if (AppGlobal.OrderID.equals(AppGlobal.UserOrderList.get(i).OrderID)) {
                tvAddress.setText(AppGlobal.UserOrderList.get(i).delivery_address);
                tvScheduleTime.setText(AppGlobal.UserOrderList.get(i).Mobileno);
                tvPlacedAt.setText(AppGlobal.UserOrderList.get(i).Restaurant_name);
                tvItemTot.setText("Rs " + String.format("%.02f", Float.parseFloat(AppGlobal.UserOrderList.get(i).item_total_amount)) + " /-");

                tvAfterDiscountTot.setText("Rs " + String.format("%.02f", Float.parseFloat(AppGlobal.UserOrderList.get(i).after_dis_total)) + " /-");
                tvAfterGstTot.setText("Rs " + String.format("%.02f", Float.parseFloat(AppGlobal.UserOrderList.get(i).after_gst_total)) + " /-");
                tvGstAmt.setText("Rs " + String.format("%.02f", Float.parseFloat(AppGlobal.UserOrderList.get(i).tax_amount)) + " /-");

                if (Float.parseFloat(AppGlobal.UserOrderList.get(i).tax_amount)>0){
                    tvGst_bg.setVisibility(View.VISIBLE);
                }else {
                    tvGst_bg.setVisibility(View.GONE);
                }

                int dsamt=Integer.parseInt(AppGlobal.UserOrderList.get(i).delivery_amount);
                if (dsamt>0){
                    layoutDelivery_bg.setVisibility(View.VISIBLE);
                }else{
                    layoutDelivery_bg .setVisibility(View.GONE);
                }

                tvDeliveryCharge.setText("Rs " + AppGlobal.UserOrderList.get(i).delivery_amount + " /-");
                tvTax.setText(String.format("%.02f", Float.parseFloat(AppGlobal.UserOrderList.get(i).tax_amount)) + " %");
                Log.d("CartTotal>>>", AppGlobal.UserOrderList.get(i).cart_total);
                tvTotalCost.setText("Rs " + String.format("%.02f", Float.parseFloat(AppGlobal.UserOrderList.get(i).cart_total)) + " /-");
                if (AppGlobal.UserOrderList.get(i).discount.equals("0")) {
                    layoutDiscount_bg.setVisibility(View.GONE);
                    tvConvenice.setText("Rs " + "00" + " /-");
                } else {
                    // if (AppGlobal.UserOrderList.get(i).discount_type.equals("price")) {
                    layoutDiscount_bg.setVisibility(View.VISIBLE);
                    tvConvenice.setText("Rs " + AppGlobal.UserOrderList.get(i).discount + " /-");
                    tvItemTot.setPaintFlags(tvItemTot.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                    //   } else {
                    //        tvConvenice.setText("" + AppGlobal.UserOrderList.get(i).discount + " %");
                    // }
                    //tvAmountPay.setText(AppGlobal.OrderproductList.get(i).am);
                /*if (AppGlobal.OrderStatus.equals("completed"))*/
                }
            }
        }
        ArrayList<MyOrderPojo> orderDetailList2 = new ArrayList<>();
        double containerCharge = 0;
        for (int i = 0; i < AppGlobal.OrderproductList.size(); i++) {

            if (AppGlobal.OrderID.equals(AppGlobal.OrderproductList.get(i).order_id)) {
                orderDetailList2.add(new MyOrderPojo(
                        AppGlobal.OrderproductList.get(i).OrderItem_id,
                        AppGlobal.OrderproductList.get(i).order_id,
                        AppGlobal.OrderproductList.get(i).product_id,
                        AppGlobal.OrderproductList.get(i).quantity,
                        AppGlobal.OrderproductList.get(i).name,
                        AppGlobal.OrderproductList.get(i).unit_price,
                        AppGlobal.OrderproductList.get(i).container_charge,
                        AppGlobal.OrderproductList.get(i).product_total,
                        AppGlobal.OrderproductList.get(i).restaurant_id,
                        AppGlobal.OrderproductList.get(i).half_label,
                        AppGlobal.OrderproductList.get(i).half_qty,
                        AppGlobal.OrderproductList.get(i).half_price,
                        AppGlobal.OrderproductList.get(i).medium_label,
                        AppGlobal.OrderproductList.get(i).medium_qty,
                        AppGlobal.OrderproductList.get(i).medium_price,
                        AppGlobal.OrderproductList.get(i).full_label,
                        AppGlobal.OrderproductList.get(i).full_qty,
                        AppGlobal.OrderproductList.get(i).full_price,
                        AppGlobal.OrderproductList.get(i).large_label,
                        AppGlobal.OrderproductList.get(i).large_qty,
                        AppGlobal.OrderproductList.get(i).large_price,
                        AppGlobal.OrderproductList.get(i).OrderTime));

                // Get date from string
                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = null;
                try {
                    date = dateFormatter.parse(AppGlobal.OrderproductList.get(i).OrderTime);
                    Log.d("date>>>", date.toString());

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                // Get time from date
                SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
                String displayValue = timeFormatter.format(date);
                Log.d("time>>>", displayValue.toString());

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String sttdate = sdf.format(date);
                Log.d("strDate>>>", sttdate + ", " + displayValue);
                tvOrderDate.setText("" + sttdate + ", " + displayValue);
                containerCharge = containerCharge + Double.parseDouble(AppGlobal.OrderproductList.get(i).container_charge);
            }
        }
        if (containerCharge != 0.0) {
            tvContainerCharge.setText("Rs " + containerCharge);
            layoutContainer_bg.setVisibility(View.VISIBLE);
        } else {
            layoutContainer_bg.setVisibility(View.GONE);
            layoutContainer.setVisibility(View.GONE);
            divContainer.setVisibility(View.GONE);
        }
        Log.d("containerCharge>>>", "" + containerCharge);
        if (!orderDetailList2.isEmpty()) {
            final RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            recycler_orderList.setLayoutManager(linearLayoutManager);
            recycler_orderList.setAdapter(new OrderSummaryAdapter(mContext, orderDetailList2));
            recycler_orderList.setNestedScrollingEnabled(false);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.locIcon:
                UtilityMethod.goNextClass(mContext, MyOrdersActivity.class);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        UtilityMethod.goNextClass(mContext, MyOrdersActivity.class);
    }
}
