package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONException;
import org.json.JSONObject;

public class FeedbackActivity extends AppCompatActivity {

    TextView tvFeedback;
    ImageView locIcon;
    Context mContext;
    EditText edtFeedback;
    Button btnSubmit;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        mContext = FeedbackActivity.this;
        initView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       // UtilityMethod.goNextClass(mContext,RestaurantListActivity.class);
    }

    private void initView() {
        sessionManager = new SessionManager(mContext);
        tvFeedback = (TextView) findViewById(R.id.tvFeedback);
        edtFeedback = (EditText) findViewById(R.id.edtFeedback);
        locIcon = (ImageView) findViewById(R.id.locIcon);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        tvFeedback.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtFeedback.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        btnSubmit.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));

        locIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);
                onBackPressed();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edtFeedback.getText().toString().equals("")) {

                    WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
                        @Override
                        public void handleResponse(String response) {
                            Log.d("response", response);
                            try {
                                // sessionManager.setValuesSession(SessionManager.Key_UserBday, "false");

                                JSONObject mainObject = new JSONObject(response);
                                if (mainObject.getString("success").equals("true")) {

                                    UtilityMethod.showAlertBox(mContext, "Feedback submitted");
                                    edtFeedback.setText("");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    };
                    webServiceCaller.addNameValuePair("user_id", sessionManager.getValuesSession(SessionManager.Key_UserID));
                    webServiceCaller.addNameValuePair("description", edtFeedback.getText().toString());
                    String url = AppGlobal.SaveFeedback;
                    Log.d("url>>", url);
                    webServiceCaller.execute(url);
                } else {
                    UtilityMethod.showAlertBox(mContext, "Please input feedback");
                }
            }
        });

    }
}
