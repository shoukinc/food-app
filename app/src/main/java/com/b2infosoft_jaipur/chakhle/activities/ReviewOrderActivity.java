package com.b2infosoft_jaipur.chakhle.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.ReviewOrderAdapter;
import com.b2infosoft_jaipur.chakhle.fragments.CartFillInterface;
import com.b2infosoft_jaipur.chakhle.fragments.CartValueInterface;
import com.b2infosoft_jaipur.chakhle.pojo.CartFillPojo;
import com.b2infosoft_jaipur.chakhle.pojo.RestaurentDetailsPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.sessions.SessionNew;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ReviewOrderActivity extends AppCompatActivity implements CartValueInterface, CartFillInterface {

    RecyclerView recycler_cartList;
    TextView tvHotelNameTxt, tvPayableTxt, tvPayAmt, tvHotelName, tvInCart, tvOrderReview, tvPriceDetails, tvSubTotalTxt, tvSubTotal, tvTaxTxt, tvTax, tvDiscountTxt, tvDiscount, tvMinimumPurchaseTxt, tvMinimumPurchase, tvAddInfo, tvSave;
    LinearLayout LayoutExtraCharge, layoutShipping, layoutLateNight, layoutDiscount, layoutContainer, bgGst, discount_bg, extra_bg;
    TextView tvExtraCharge, tvExtraChargeFee, tvContainerChargeTxt, tvContainerCharge, tvGstTxt, tvGstAmt, tvLateNightChargeTxt, tvLateNightCharge;
    View viewExtraCharge;
    ImageView iconGoNext, locIcon;
    Context mContext;
    double amount = 0;
    EditText edtSuggestText;
    String suggestionText = "";
    SessionManager sessionManager;
    SessionNew sessionManagerNew;
    String restName = "", Discount = "", DiscountType = "";
    String containerCharge = "";
    String minimumPurchase = "";
    String extraCharge = "", GstAmt = "";
    double discountPrice = 0;
    double discount = 0;
    double containerChargeDouble = 0;
    String RestInTime = "", RestOffTime = "";
    View viewLate, divDis, divContainer;
    TextView tvAfterDiscountTot, tvAfterDiscountTotTxt, tvAfterGstTotTxt, tvAfterGstTot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_order);
        mContext = ReviewOrderActivity.this;
        sessionManager = new SessionManager(mContext);
        sessionManagerNew = new SessionNew(mContext);
        initView();
    }

    @Override
    public void onBackPressed() {


        RestaurentDetailsPojo restaurentDetailsPojo = null;
        AppGlobal.mainCartList.clear();
        AppGlobal.GlobalCartList.clear();

        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            int Quentity = 0, GlobalQuentity = 0;
            int HalfQty = 0;
            double HalfAmount = 0;
            double HalfTotalAmount = 0;
            int MediumQty = 0;
            double MediumAmt = 0;
            double MediumTotalAmt = 0;
            int FullQty = 0;
            double FullAmt = 0;
            double FullTotalAmt = 0;
            int LargeQty = 0;
            double LargeAmt = 0;
            double LargeTotalAmt = 0;

            if (AppGlobal.RestaurentProductList.get(i).halfQty != 0) {
                HalfQty = AppGlobal.RestaurentProductList.get(i).halfQty;
                HalfAmount = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).half);
                HalfTotalAmount = HalfQty * HalfAmount;
                Log.d("HalfTotalAmount>>>", "" + HalfTotalAmount);

            }
            if (AppGlobal.RestaurentProductList.get(i).mediumQty != 0) {
                MediumQty = AppGlobal.RestaurentProductList.get(i).mediumQty;
                MediumAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).medium);
                MediumTotalAmt = MediumQty * MediumAmt;
                Log.d("MediumTotalAmt>>>", "" + MediumTotalAmt);
            }
            if (AppGlobal.RestaurentProductList.get(i).fullQty != 0) {
                FullQty = AppGlobal.RestaurentProductList.get(i).fullQty;
                FullAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).full);
                FullTotalAmt = FullQty * FullAmt;
                Log.d("FullTotalAmt>>>", "" + FullTotalAmt);
            }
            if (AppGlobal.RestaurentProductList.get(i).largeQty != 0) {
                LargeQty = AppGlobal.RestaurentProductList.get(i).largeQty;
                LargeAmt = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).large);
                LargeTotalAmt = LargeQty * LargeAmt;
                Log.d("LargeTotalAmt>>>", "" + LargeTotalAmt);
            }
            Quentity = AppGlobal.RestaurentProductList.get(i).getCount();
            Log.d("Quentity>>>", "" + Quentity);
            int total = (AppGlobal.RestaurentProductList.get(i).afterDiscountValue) * Quentity;
            int productPrice = Integer.parseInt((AppGlobal.RestaurentProductList.get(i).Product_price));
            int totalProductPrice = productPrice * Quentity;
            String minimum_purchase = AppGlobal.RestaurentProductList.get(i).minimum_purchase;
            String extra_charge = AppGlobal.RestaurentProductList.get(i).extra_charge;
            Log.d("minimum_purchase>>", minimum_purchase);
            Log.d("extra_charge>>", extra_charge);
            double container = Double.parseDouble(AppGlobal.RestaurentProductList.get(i).container_charge);
            Log.d("Quentity>>>", "" + Quentity);
            container = container * Quentity;
            if (HalfQty != 0 || MediumQty != 0 || LargeQty != 0 || FullQty != 0) {
                AppGlobal.mainCartList.add(new CartFillPojo(
                        AppGlobal.RestaurentProductList.get(i).Product_id,
                        Quentity,
                        AppGlobal.RestaurentProductList.get(i).Product_name,
                        AppGlobal.RestaurentProductList.get(i).Product_price,
                        total,
                        AppGlobal.RestaurentProductList.get(i).restaurant_id,
                        sessionManager.getValuesSession(SessionManager.Key_UserID),
                        AppGlobal.RestaurentProductList.get(i).Product_vegproduct,
                        AppGlobal.RestaurentProductList.get(i).afterDiscountValue,
                        totalProductPrice, AppGlobal.RestaurentProductList.get(i).name,
                        AppGlobal.RestaurentProductList.get(i).discount,
                        String.valueOf(container),
                        AppGlobal.RestaurentProductList.get(i).minimum_purchase,
                        AppGlobal.RestaurentProductList.get(i).extra_charge,
                        AppGlobal.RestaurentProductList.get(i).container_charge,
                        HalfQty, HalfAmount, HalfTotalAmount,
                        FullQty, FullAmt, FullTotalAmt,
                        LargeQty, LargeAmt, LargeTotalAmt,
                        MediumQty, MediumAmt, MediumTotalAmt,
                        AppGlobal.RestaurentProductList.get(i).discountType
                        , AppGlobal.RestaurentProductList.get(i).half_label
                        , AppGlobal.RestaurentProductList.get(i).medium_label
                        , AppGlobal.RestaurentProductList.get(i).full_label
                        , AppGlobal.RestaurentProductList.get(i).large_label
                        , AppGlobal.RestaurentProductList.get(i).let_nite_charge
                        , AppGlobal.RestaurentProductList.get(i).tax_percentage
                        , AppGlobal.RestaurentProductList.get(i).RestInTime
                        , AppGlobal.RestaurentProductList.get(i).Restoff_time_1
                ));
            }
        }

        if (!AppGlobal.mainCartList.isEmpty()) {
            sessionManager.clearArrayList(mContext);
            sessionManager.saveCartList(mContext, AppGlobal.mainCartList);
            sessionManagerNew.clearArrayList(mContext);
            sessionManagerNew.saveCartList(mContext, AppGlobal.mainCartList);
        }else{
            sessionManagerNew.clearArrayList(mContext);
            sessionManager.clearArrayList(mContext);
        }

        if (AppGlobal.RestaurentCategoryID.equals("")) {
            Intent intent = new Intent(mContext, RestaurantListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle bundle = new Bundle();
            bundle.putString("ClassName", "RestaurentDetailsActivity");
            bundle.putString("AddToFav", "true");
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            UtilityMethod.goNextClass(mContext, RestaurentDetailsActivity.class);
        }

    }

    private void initView() {

        viewLate = (View) findViewById(R.id.viewLate);
        divContainer = (View) findViewById(R.id.divContainer);
        divDis = (View) findViewById(R.id.divDis);

        tvAfterGstTotTxt = (TextView) findViewById(R.id.tvAfterGstTotTxt);
        tvAfterGstTot = (TextView) findViewById(R.id.tvAfterGstTot);
        tvAfterDiscountTot = (TextView) findViewById(R.id.tvAfterDiscountTot);
        tvAfterDiscountTotTxt = (TextView) findViewById(R.id.tvAfterDiscountTotTxt);
        tvLateNightChargeTxt = (TextView) findViewById(R.id.tvLateNightChargeTxt);
        tvLateNightCharge = (TextView) findViewById(R.id.tvLateNightCharge);
        tvContainerChargeTxt = (TextView) findViewById(R.id.tvContainerChargeTxt);
        tvContainerCharge = (TextView) findViewById(R.id.tvContainerCharge);
        tvOrderReview = (TextView) findViewById(R.id.tvOrderReview);
        tvInCart = (TextView) findViewById(R.id.tvInCart);
        tvAddInfo = (TextView) findViewById(R.id.tvAddInfo);
        tvPriceDetails = (TextView) findViewById(R.id.tvPriceDetails);
        tvSubTotalTxt = (TextView) findViewById(R.id.tvSubTotalTxt);
        tvSubTotal = (TextView) findViewById(R.id.tvSubTotal);
        tvTaxTxt = (TextView) findViewById(R.id.tvTaxTxt);
        tvTax = (TextView) findViewById(R.id.tvTax);
        tvPayableTxt = (TextView) findViewById(R.id.tvPayableTxt);
        tvPayAmt = (TextView) findViewById(R.id.tvPayAmt);
        tvHotelNameTxt = (TextView) findViewById(R.id.tvHotelNameTxt);
        tvHotelName = (TextView) findViewById(R.id.tvHotelName);
        tvDiscountTxt = (TextView) findViewById(R.id.tvDiscountTxt);
        tvDiscount = (TextView) findViewById(R.id.tvDiscount);
        tvMinimumPurchaseTxt = (TextView) findViewById(R.id.tvMinimumPurchaseTxt);
        tvMinimumPurchase = (TextView) findViewById(R.id.tvMinimumPurchase);
        LayoutExtraCharge = (LinearLayout) findViewById(R.id.LayoutExtraCharge);
        layoutLateNight = (LinearLayout) findViewById(R.id.layoutLateNight);
        tvExtraCharge = (TextView) findViewById(R.id.tvExtraCharge);
        tvExtraChargeFee = (TextView) findViewById(R.id.tvExtraChargeFee);
        extra_bg = (LinearLayout) findViewById(R.id.extra_bg);
        discount_bg = (LinearLayout) findViewById(R.id.discount_bg);
        bgGst = (LinearLayout) findViewById(R.id.bgGst);

        tvGstTxt = (TextView) findViewById(R.id.tvGstTxt);
        tvGstAmt = (TextView) findViewById(R.id.tvGstAmt);
        viewExtraCharge = (View) findViewById(R.id.viewExtraCharge);
        iconGoNext = (ImageView) findViewById(R.id.iconGoNext);
        locIcon = (ImageView) findViewById(R.id.locIcon);
        layoutShipping = (LinearLayout) findViewById(R.id.layoutShipping);
        layoutDiscount = (LinearLayout) findViewById(R.id.layoutDiscount);
        layoutContainer = (LinearLayout) findViewById(R.id.layoutContainer);
        locIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // UtilityMethod.goNextClass(mContext, RestaurentDetailsActivity.class);
               onBackPressed();
            }
        });
        iconGoNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvPayAmt.getText().toString().equals("Rs 00")) {
                    UtilityMethod.showAlertBox(mContext, "Your Cart is Empty");
                } else {
                    AppGlobal.ExtraCharge = "" + extraCharge;
                    AppGlobal.MinimumPurchase = "" + minimumPurchase;
                    Log.d("extraCharge>>", "" + extraCharge);
                    Log.d("MinimumPurchase>>", "" + AppGlobal.MinimumPurchase);
                    AppGlobal.SuggestionText = tvAddInfo.getText().toString();
                    AppGlobal.TotalCost = tvPayAmt.getText().toString();
                    AppGlobal.TaxAmt = tvTax.getText().toString();
                    startActivity(new Intent(mContext, PickAddressActivity.class));
                }
            }
        });
        tvAddInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_suggestion);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setCancelable(true);
                // set the custom dialog components - text, image and button
                tvSave = (TextView) dialog.findViewById(R.id.tvSave);
                edtSuggestText = (EditText) dialog.findViewById(R.id.edtSuggestText);
                tvSave.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
                edtSuggestText.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
                if (!suggestionText.equals("")) {
                    edtSuggestText.setText(suggestionText);
                }
                // if button is clicked, close the custom dialog
                tvSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        suggestionText = edtSuggestText.getText().toString();
                        Log.d("SuggestionText>>", suggestionText);
                        dialog.dismiss();
                        if (suggestionText.equals("")) {
                            tvAddInfo.setText("Leave a note for the Restaurant");
                            tvAddInfo.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
                        } else {
                            tvAddInfo.setText(suggestionText);
                            tvAddInfo.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
                        }
                    }
                });
                dialog.show();
            }
        });

        tvOrderReview.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_bold.ttf"));
        tvAfterGstTotTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvAfterDiscountTotTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvLateNightChargeTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvInCart.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvGstTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvHotelName.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvHotelNameTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvExtraCharge.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvExtraChargeFee.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvPayableTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvContainerChargeTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        //tvContainerCharge.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        //tvPriceDetails.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvSubTotalTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvMinimumPurchaseTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvAddInfo.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        // tvSubTotal.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        //tvTax.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvMinimumPurchase.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvDiscountTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        //tvDiscount.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        //tvConvenFee.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        //tvConvenice.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvTaxTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        int totalPrice = 0, qty = 0;
        double tot = 0;
        for (int i = 0; i < AppGlobal.mainCartList.size(); i++) {
            qty = 0;
            restName = AppGlobal.mainCartList.get(i).RestaurentName;
            Discount = AppGlobal.mainCartList.get(i).Discount;
            DiscountType = AppGlobal.mainCartList.get(i).DiscountType;
            // tvGstAmt.setText(AppGlobal.mainCartList.get(i).tax_percentage + " %");
            GstAmt = AppGlobal.mainCartList.get(i).tax_percentage;
            RestInTime = AppGlobal.mainCartList.get(i).RestInTime;
            RestOffTime = AppGlobal.mainCartList.get(i).RestOffTime;
            Log.d("RestInTime>>>", "" + RestInTime);
            Log.d("RestOffTime>>>", "" + RestOffTime);
            //amount = amount + AppGlobal.mainCartList.get(i).totalProductPrice;
            //discountPrice = discountPrice + AppGlobal.mainCartList.get(i).ProductTotalPrice;
            tot = tot + AppGlobal.mainCartList.get(i).ProductTotalPrice;
            Log.d("totalProductPrice>>>", "" + tot);
            totalPrice = totalPrice + AppGlobal.mainCartList.get(i).totalProductPrice;
            containerCharge = AppGlobal.mainCartList.get(i).PerProductContainerCharge;
            minimumPurchase = AppGlobal.mainCartList.get(i).MinimumPurchase;
            extraCharge = AppGlobal.mainCartList.get(i).ExtraCharge;
            qty = AppGlobal.mainCartList.get(i).HalfQty + AppGlobal.mainCartList.get(i).MediumQty + AppGlobal.mainCartList.get(i).FullQty + AppGlobal.mainCartList.get(i).LargeQty;
            Log.d("Total_Qty>>", "" + qty);

            amount = amount + ((AppGlobal.mainCartList.get(i).HalfPrice * AppGlobal.mainCartList.get(i).HalfQty) + (AppGlobal.mainCartList.get(i).MediumPrice * AppGlobal.mainCartList.get(i).MediumQty) +
                    (AppGlobal.mainCartList.get(i).FullPrice * AppGlobal.mainCartList.get(i).FullQty) + (AppGlobal.mainCartList.get(i).LargePrice * AppGlobal.mainCartList.get(i).LargeQty));
            containerChargeDouble = containerChargeDouble + (qty * Double.parseDouble(containerCharge));
            Log.d("containerChargeDouble>>", "" + containerChargeDouble);
            AppGlobal.mainCartList.get(i).TotalContainerCharge = "" + (qty * Double.parseDouble(containerCharge));
        }

        Log.d("totalProductAmt::::", "" + amount);
        Log.d("GstAmt>>>", GstAmt);
        Log.d("mainContainerCharge>>", containerCharge);
        Log.d("containerChargeTotal>>", "" + containerChargeDouble);
        Log.d("mainExtraCharge>>", extraCharge);
        Log.d("amount>>", "" + amount);
        Log.d("Discount>>", Discount + ":::" + DiscountType);
        tvSubTotal.setText("Rs " + amount);

        tvMinimumPurchase.setText("Rs. " + minimumPurchase);
        double minimumPurchaseAmt = Double.parseDouble(minimumPurchase);
        //containerChargeDouble = qty * Double.parseDouble(containerCharge);
        if (!Discount.equals("")) {
            discount = Double.parseDouble(Discount);
            if (DiscountType.equals("price")) {
                discountPrice = discount;
                AppGlobal.discountPrice = discountPrice;
                tvDiscount.setText("Rs " + discountPrice);
            } else {
                discountPrice = (discount / 100) * amount;
                tvDiscount.setText("Rs " + String.format("%.02f", discountPrice));
                // tvDiscount.setText("Rs " + discountPrice);
                AppGlobal.discountPrice = discountPrice;
            }
        } else {
            discountPrice = 0;
            AppGlobal.discountPrice = discountPrice;
            tvDiscount.setText("Rs " + String.format("%.02f", discountPrice));
            layoutDiscount.setVisibility(View.VISIBLE);
            divDis.setVisibility(View.VISIBLE);
        }

        if (discountPrice != 0) {
            tvSubTotal.setPaintFlags(tvSubTotal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            discount_bg.setVisibility(View.VISIBLE);
        } else {
            discount_bg.setVisibility(View.GONE);
        }

        AppGlobal.ItemsAmount = amount;
        amount = amount - discountPrice;
        Log.d("afterDiscount>>", "" + amount);
        tvAfterDiscountTot.setText("Rs " + amount);
        AppGlobal.AfterDiscountTotal = amount;

        if (GstAmt.equals("0")) {
            bgGst.setVisibility(View.GONE);
        } else {
            bgGst.setVisibility(View.VISIBLE);
        }

        double gst = Double.parseDouble(GstAmt);
        double gstAmt = amount * (gst / 100);
        AppGlobal.gstPrice = gstAmt;

        tvGstAmt.setText("Rs " + String.format("%.02f", gstAmt));
        Log.d("gstAmt>>", "" + gstAmt);
        amount = amount + gstAmt;
        AppGlobal.AfterGstAmt = amount;
        Log.d("afterGst>>>", "" + amount);
        tvAfterGstTot.setText("Rs " + String.format("%.02f", amount));
        if (containerChargeDouble != 0.0) {
            layoutContainer.setVisibility(View.VISIBLE);
            divContainer.setVisibility(View.VISIBLE);
            amount = amount + containerChargeDouble;
            tvContainerCharge.setText("Rs " + containerChargeDouble);
        } else {
            layoutContainer.setVisibility(View.GONE);
            divContainer.setVisibility(View.GONE);
        }

        if (amount <= minimumPurchaseAmt) {
            layoutShipping.setVisibility(View.VISIBLE);
            LayoutExtraCharge.setVisibility(View.VISIBLE);
            tvExtraChargeFee.setText("Rs. " + extraCharge);
            amount = amount + Double.parseDouble(extraCharge);
            viewExtraCharge.setVisibility(View.VISIBLE);

        } else {
            layoutShipping.setVisibility(View.GONE);
            LayoutExtraCharge.setVisibility(View.GONE);
            viewExtraCharge.setVisibility(View.GONE);
            extraCharge = "0";
            //amount = amount + Double.parseDouble(extraCharge);
        }

        if (extraCharge.equals("0")) {

            extra_bg.setVisibility(View.GONE);
        } else {
            extra_bg.setVisibility(View.VISIBLE);
        }
        tvPayAmt.setText("Rs " + String.format("%.02f", amount));

        /*if (!Discount.equals("")) {
            double diff = amount - discount;
            diff = diff + containerChargeDouble;
            diff = diff + Double.parseDouble(extraCharge);
            double amtNew = diff;
            AppGlobal.ItemsAmount = diff;
            Log.d("ExcludeWithGst>>>", "" + diff);
            double gst = Double.parseDouble(GstAmt);
            double gstAmt = diff * (gst / 100);
            diff = diff + gstAmt;
            Log.d("IncludeWithGst>>>", "" + diff);
            tvPayAmt.setText("Rs " + diff);
            tvSubTotal.setText("Rs " + amtNew);
            tvTax.setText("Rs 0");
        } else {
            amount = amount + containerChargeDouble;
            double amtNew = amount;
            amount = amount + Double.parseDouble(extraCharge);
            AppGlobal.ItemsAmount = amount;
            tvSubTotal.setText("Rs " + amount);

            Log.d("ExcludeWithGst>>>", "" + amount);
            double gst = Double.parseDouble(GstAmt);
            double gstAmt = amount * (gst / 100);
            amount = amount + gstAmt;
            Log.d("IncludeWithGst>>>", "" + amount);
            tvDiscount.setText("Rs 0");

            tvTax.setText("Rs 0");
        }*/
        tvHotelName.setText(restName);
        recycler_cartList = (RecyclerView) findViewById(R.id.recycler_cartList);
        final RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recycler_cartList.setLayoutManager(linearLayoutManager);
        recycler_cartList.setAdapter(new ReviewOrderAdapter(mContext, AppGlobal.mainCartList));
        recycler_cartList.setNestedScrollingEnabled(false);
    }


    @Override
    public void setCartData(int cartData, String operator, String totalAmt) {
        try {
            int payAmt = 0;
            if (!tvPayAmt.getText().toString().equals("0") && !tvSubTotal.getText().toString().equals("0")) {
                String tvPay_Amt = tvPayAmt.getText().toString();
                String tvSubTotal_Amt = tvSubTotal.getText().toString();
                tvPay_Amt = tvPay_Amt.substring(2, tvPay_Amt.length()).trim();
                tvSubTotal_Amt = tvSubTotal_Amt.substring(2, tvSubTotal_Amt.length()).trim();
                Log.d("tvValue>>", tvPay_Amt);
                Log.d("tvValue>>", tvSubTotal_Amt);

                if (operator.equals("+")) {
                    //int actual = cartData;
                    payAmt = Integer.parseInt(tvPay_Amt);
                    int subTotalAmt = Integer.parseInt(tvSubTotal_Amt);
                    payAmt = payAmt + cartData;
                    subTotalAmt = subTotalAmt + Integer.parseInt(totalAmt);
                    int diff = subTotalAmt - payAmt;
                    if (Discount.equals("")) {
                        tvDiscount.setText("Rs 0");
                    } else {
                        tvDiscount.setText("Rs " + diff);
                    }
                    tvPayAmt.setText("Rs " + payAmt);
                    tvSubTotal.setText("Rs " + subTotalAmt);
                } else if (operator.equals("-")) {
                    payAmt = Integer.parseInt(tvPay_Amt);
                    int subTotalAmt = Integer.parseInt(tvSubTotal_Amt);
                    payAmt = payAmt - cartData;
                    subTotalAmt = subTotalAmt - Integer.parseInt(totalAmt);
                    int diff = subTotalAmt - payAmt;
                    if (Discount.equals("") && payAmt == 0) {
                        tvDiscount.setText("Rs 0");
                    } else if (!Discount.equals("") && payAmt == 0) {
                        tvDiscount.setText("Rs 0");
                    } else if (!Discount.equals("") && payAmt != 0) {
                        tvDiscount.setText("Rs " + diff);
                    }
                    tvPayAmt.setText("Rs " + payAmt);
                    tvSubTotal.setText("Rs " + subTotalAmt);
                }
            } else {
                try {
                    if (operator.equals("check") && Integer.parseInt(tvSubTotal.getText().toString()) == 0) {
                        tvDiscount.setText("Rs 0");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            double minimumPurchaseAmt = Double.parseDouble(minimumPurchase);
            if (payAmt < minimumPurchaseAmt) {
                layoutShipping.setVisibility(View.VISIBLE);
                LayoutExtraCharge.setVisibility(View.VISIBLE);
                viewExtraCharge.setVisibility(View.VISIBLE);
                tvExtraChargeFee.setText("Rs. " + extraCharge);
            } else {
                layoutShipping.setVisibility(View.GONE);
                LayoutExtraCharge.setVisibility(View.GONE);
                viewExtraCharge.setVisibility(View.GONE);
                extraCharge = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void newCartFillData(ArrayList<CartFillPojo> cartFillPojos) {


        //   AppGlobal.mainCartList = cartFillPojos;

        int totalPrice = 0, qty = 0;
        amount = 0;
        discount = 0;
        discountPrice = 0;
        double tot = 0;
        containerChargeDouble = 0;
        for (int i = 0; i < cartFillPojos.size(); i++) {
            qty = 0;
            restName = cartFillPojos.get(i).RestaurentName;
            Discount = cartFillPojos.get(i).Discount;
            DiscountType = cartFillPojos.get(i).DiscountType;
            RestInTime = AppGlobal.mainCartList.get(i).RestInTime;
            RestOffTime = AppGlobal.mainCartList.get(i).RestOffTime;
            Log.d("RestInTime>>>", "" + RestInTime);
            Log.d("RestOffTime>>>", "" + RestOffTime);
            //amount = amount + AppGlobal.mainCartList.get(i).totalProductPrice;
            //discountPrice = discountPrice + AppGlobal.mainCartList.get(i).ProductTotalPrice;
            tot = tot + cartFillPojos.get(i).ProductTotalPrice;
            Log.d("produtTotalPrice>>>", "" + tot);
            totalPrice = totalPrice + cartFillPojos.get(i).totalProductPrice;
            containerCharge = cartFillPojos.get(i).PerProductContainerCharge;
            minimumPurchase = cartFillPojos.get(i).MinimumPurchase;
            extraCharge = cartFillPojos.get(i).ExtraCharge;
            qty = cartFillPojos.get(i).HalfQty + cartFillPojos.get(i).MediumQty + cartFillPojos.get(i).FullQty + cartFillPojos.get(i).LargeQty;
            Log.d("Total_Qty>>", "" + qty);
            amount = amount + ((cartFillPojos.get(i).HalfPrice * cartFillPojos.get(i).HalfQty) + (cartFillPojos.get(i).MediumPrice * cartFillPojos.get(i).MediumQty) +
                    (cartFillPojos.get(i).FullPrice * cartFillPojos.get(i).FullQty) + (cartFillPojos.get(i).LargePrice * cartFillPojos.get(i).LargeQty));
            containerChargeDouble = containerChargeDouble + (qty * Double.parseDouble(containerCharge));
            cartFillPojos.get(i).TotalContainerCharge = "" + (qty * Double.parseDouble(containerCharge));
        }
        Log.d("mainContainerCharge>>", containerCharge);
        Log.d("containerChargeDouble>>", "" + containerChargeDouble);
        Log.d("mainExtraCharge>>", extraCharge);
        Log.d("minimumPurchase>>", minimumPurchase);
        Log.d("Total_Qty>>", "" + qty);
        Log.d("amount>>", "" + amount);
        Log.d("Discount>>", Discount + ":::" + DiscountType);
        Log.d("totalProductAmt::::", "" + amount);
        Log.d("GstAmt>>>", GstAmt);
        Log.d("mainContainerCharge>>", containerCharge);
        Log.d("containerChargeTotal>>", "" + containerChargeDouble);
        Log.d("mainExtraCharge>>", extraCharge);
        Log.d("amount>>", "" + amount);
        Log.d("Discount>>", Discount + ":::" + DiscountType);

        if (discountPrice != 0) {
            tvSubTotal.setPaintFlags(tvSubTotal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            discount_bg.setVisibility(View.VISIBLE);
        } else {
            discount_bg.setVisibility(View.GONE);
        }

        tvSubTotal.setText("Rs " + amount);

        // tvSubTotal.setPaintFlags(tvSubTotal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tvMinimumPurchase.setText("Rs. " + minimumPurchase);
        double minimumPurchaseAmt = Double.parseDouble(minimumPurchase);
        //containerChargeDouble = qty * Double.parseDouble(containerCharge);
        if (!Discount.equals("")) {
            discount = Double.parseDouble(Discount);
            if (DiscountType.equals("price")) {
                discountPrice = discount;
                AppGlobal.discountPrice = discountPrice;
                tvDiscount.setText("Rs " + String.format("%.02f", discountPrice));
            } else {
                discountPrice = (discount / 100) * amount;
                AppGlobal.discountPrice = discountPrice;
                tvDiscount.setText("Rs " + String.format("%.02f", discountPrice));
            }
        } else {
            discountPrice = 0;
            AppGlobal.discountPrice = discountPrice;
            layoutDiscount.setVisibility(View.GONE);
            divDis.setVisibility(View.GONE);
        }
        AppGlobal.ItemsAmount = amount;
        amount = amount - discountPrice;
        AppGlobal.AfterDiscountTotal = amount;
        Log.d("afterDiscount>>", "" + amount);

        tvAfterDiscountTot.setText("Rs " + String.format("%.02f", amount));
        double gst = Double.parseDouble(GstAmt);
        double gstAmt = amount * (gst / 100);
        AppGlobal.gstPrice = gstAmt;
        tvGstAmt.setText("Rs " + String.format("%.02f", gstAmt));
        Log.d("gstAmt>>", "" + gstAmt);
        amount = amount + gstAmt;
        AppGlobal.AfterGstAmt = amount;
        tvAfterGstTot.setText("Rs " + String.format("%.02f", amount));

        Log.d("afterGst>>>", "" + amount);
        if (containerChargeDouble != 0.0) {
            layoutContainer.setVisibility(View.VISIBLE);
            divContainer.setVisibility(View.VISIBLE);
            amount = amount + containerChargeDouble;
            tvContainerCharge.setText("Rs " + containerChargeDouble);
        } else {
            layoutContainer.setVisibility(View.GONE);
            divContainer.setVisibility(View.GONE);
        }
        if (amount <= minimumPurchaseAmt) {
            layoutShipping.setVisibility(View.VISIBLE);
            LayoutExtraCharge.setVisibility(View.VISIBLE);
            tvExtraChargeFee.setText("Rs. " + extraCharge);
            amount = amount + Double.parseDouble(extraCharge);
            viewExtraCharge.setVisibility(View.VISIBLE);

        } else {
            layoutShipping.setVisibility(View.GONE);
            LayoutExtraCharge.setVisibility(View.GONE);
            viewExtraCharge.setVisibility(View.GONE);
            extraCharge = "0";
            //amount = amount + Double.parseDouble(extraCharge);
        }
        tvPayAmt.setText("Rs " + String.format("%.02f", amount));
        if (tvSubTotal.getText().toString().equals("Rs 0.0")) {
            tvDiscount.setText("Rs 00");
            tvPayAmt.setText("Rs 00");
            tvSubTotal.setText("Rs 00");
            tvTax.setText("Rs 00");
        }
        AppGlobal.mainCartList = cartFillPojos;


        /********************************************/


        if (!AppGlobal.mainCartList.isEmpty()) {
            int count = 0;
            for (int i = 0; i < AppGlobal.mainCartList.size(); i++) {
                for (int j = 0; j < AppGlobal.RestaurentProductList.size(); j++) {
                    if (AppGlobal.mainCartList.get(i).restaurent_id.equals(AppGlobal.RestaurentProductList.get(j).restaurant_id)) {
                        if (AppGlobal.mainCartList.get(i).Product_id.equals(AppGlobal.RestaurentProductList.get(j).Product_id)) {
                            //   count = mainSessonCartList.get(i).quantity;
                            //   restaurentProductList.get(j).setCount(count);
                            AppGlobal.RestaurentProductList.get(j).halfQty = AppGlobal.mainCartList.get(i).HalfQty;
                            AppGlobal.RestaurentProductList.get(j).fullQty = AppGlobal.mainCartList.get(i).FullQty;
                            AppGlobal.RestaurentProductList.get(j).largeQty = AppGlobal.mainCartList.get(i).LargeQty;
                            AppGlobal.RestaurentProductList.get(j).mediumQty = AppGlobal.mainCartList.get(i).MediumQty;
                            AppGlobal.ProductIDList.add(AppGlobal.mainCartList.get(i).restaurent_id);
                        }
                    }
                }
            }
        }

    }
}
