package com.b2infosoft_jaipur.chakhle.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.NotificationAdapter;
import com.b2infosoft_jaipur.chakhle.pojo.NotificationPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

public class NotificationActivity extends AppCompatActivity {

    TextView tvNotificationText;
    ImageView locIcon;
    RecyclerView recycler_Notification;
    Context mContext;
    TextView tvNotificationDetails;
    private static final String TAG = NotificationActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    ArrayList<NotificationPojo> notificationList = new ArrayList<>();
    ArrayList<NotificationPojo> sessionList = new ArrayList<>();

    SessionManager sessionManager;
    String title = "", body = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        mContext = NotificationActivity.this;
        sessionManager = new SessionManager(mContext);
        tvNotificationText =  findViewById(R.id.tvNotificationText);
        locIcon = findViewById(R.id.locIcon);
        tvNotificationText.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        sessionList = sessionManager.getNotificationList(mContext);

        String data_list = sessionManager.getKey_NotificationList_();

        if (data_list != null) {
            Log.d("data_list", data_list);
            if (!data_list.equals("")) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(data_list);
                    JSONArray jsonArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        notificationList.add(new NotificationPojo(jsonObject1.getString("title"), jsonObject1.getString("messageBody"), jsonObject1.getString("time"), jsonObject1.getString("order_id")));
                    }

                    Collections.reverse(notificationList);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }

       /* if (sessionList != null) {
            for (int i = 0; i < sessionList.size(); i++) {
                notificationList.add(new NotificationPojo(sessionList.get(i).NotificationTitle, sessionList.get(i).NotificationDesc, sessionList.get(i).NotificationTiming));
          Log.d("dd",sessionList.get(i).NotificationTiming);
            }
            Collections.sort(notificationList, new OutcomeAscComparator());
        }*/
/*
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                String value = getIntent().getExtras().getString(key);
                Log.d("key", "" + key);
                Log.d("value", "" + value);
                if (key != null && key.equals("messageTitle")) {
                    title = value;
                }
                if (key != null && key.equals("messageBody")) {
                    body = value;
                }
                // notificationList.add(new NotificationPojo(title, body,""));
            }

            if (!title.equals("") && !body.equals("")) {
                AppGlobal.NotificationCount++;
                Log.d("notificationDetail>>>>", "title:" + title + "  body:" + body);
                sessionManager.clearArrayNotificationList(mContext);
                notificationList.add(new NotificationPojo(title, body, ""));
                sessionManager.saveNotificationList(mContext, notificationList);
                //showCloudDilogOne(this, AppGlobal.NotificationTitle, AppGlobal.NotificationBody);
            }
        }
        sessionList = sessionManager.getNotificationList(mContext);*/

        if (notificationList != null) {
            Log.d("notificationList>>>", "" + notificationList.size());
            recycler_Notification = (RecyclerView) findViewById(R.id.recycler_Notification);
            final RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            recycler_Notification.setLayoutManager(linearLayoutManager);
            recycler_Notification.setAdapter(new NotificationAdapter(mContext, notificationList));
        }
        locIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                // UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Log.d("AppGlobal.Addresses", AppGlobal.Addresses + " ggg");

        if (!AppGlobal.Addresses.equals("")) {

            Intent intent = new Intent(mContext, RestaurantListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle bundle = new Bundle();
            bundle.putString("ClassName", "RestaurentDetailsActivity");
            bundle.putString("AddToFav", "true");
            intent.putExtras(bundle);
            startActivity(intent);
        } else {

            Intent intent = new Intent(mContext, RestaurantListActivity.class);
            startActivity(intent);
        }

        // UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);
    }
}
