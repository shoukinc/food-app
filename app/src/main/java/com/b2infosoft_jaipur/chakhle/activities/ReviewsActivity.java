package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONException;
import org.json.JSONObject;

public class ReviewsActivity extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    ImageView locIcon;
    TextView tvFeedback, edtFeedback;
    TextView tvRating;
    ImageView rat1, rat2, rat3, rat4, rat5;
    RatingBar simpleRatingBar;
    Button btnSubmit;
    SessionManager sessionManager;
    String totalStars;
    String rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);
        mContext = ReviewsActivity.this;
        init();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        UtilityMethod.goNextClass(mContext, MyOrdersActivity.class);
    }

    private void init() {
        sessionManager = new SessionManager(mContext);
        locIcon = (ImageView) findViewById(R.id.locIcon);
        tvFeedback = (TextView) findViewById(R.id.tvFeedback);
        edtFeedback = (TextView) findViewById(R.id.edtFeedback);
        tvRating = (TextView) findViewById(R.id.tvRating);
        rat1 = (ImageView) findViewById(R.id.rat1);
        rat2 = (ImageView) findViewById(R.id.rat2);
        rat3 = (ImageView) findViewById(R.id.rat3);
        rat4 = (ImageView) findViewById(R.id.rat4);
        rat5 = (ImageView) findViewById(R.id.rat5);
       /* simpleRatingBar = (RatingBar) findViewById(R.id.simpleRatingBar);*/
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        tvFeedback.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtFeedback.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvRating.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        btnSubmit.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));

        locIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtilityMethod.goNextClass(mContext, MyOrdersActivity.class);
            }
        });

        rat1.setOnClickListener(this);
        rat2.setOnClickListener(this);
        rat3.setOnClickListener(this);
        rat4.setOnClickListener(this);
        rat5.setOnClickListener(this);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edtFeedback.getText().toString().equals("") && !tvRating.getText().toString().equals("0")) {

                    WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
                        @Override
                        public void handleResponse(String response) {
                            Log.d("response", response);
                            try {
                                JSONObject mainObject = new JSONObject(response);
                                if (mainObject.getString("success").equals("true")) {
                                    edtFeedback.setText("");
                                    rat1.setImageResource(R.drawable.blank_star);
                                    rat2.setImageResource(R.drawable.blank_star);
                                    rat3.setImageResource(R.drawable.blank_star);
                                    rat4.setImageResource(R.drawable.blank_star);
                                    rat5.setImageResource(R.drawable.blank_star);
                                    tvRating.setText("0");
                                    UtilityMethod.showAlertBoxwithIntentNew(mContext, "Review submitted", MyOrdersActivity.class);
                                } else {
                                    UtilityMethod.showAlertBox(mContext, "Can't submitted your Review");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    webServiceCaller.addNameValuePair("user_id", sessionManager.getValuesSession(SessionManager.Key_UserID));
                    webServiceCaller.addNameValuePair("restaurant_id", AppGlobal.Restaurant_id);
                    webServiceCaller.addNameValuePair("order_id", AppGlobal.OrderID);
                    webServiceCaller.addNameValuePair("comment", edtFeedback.getText().toString());
                    webServiceCaller.addNameValuePair("rating", tvRating.getText().toString());
                    String url = AppGlobal.SaveReview;
                    Log.d("url>>", url);
                    webServiceCaller.execute(url);
                } else {
                    UtilityMethod.showAlertBox(mContext, "Please input feedback and Give Rating! ");
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rat1:
                rat1.setImageResource(R.drawable.fill_star);
                rat2.setImageResource(R.drawable.blank_star);
                rat3.setImageResource(R.drawable.blank_star);
                rat4.setImageResource(R.drawable.blank_star);
                rat5.setImageResource(R.drawable.blank_star);
                tvRating.setText("1");
                break;

            case R.id.rat2:
                rat1.setImageResource(R.drawable.fill_star);
                rat2.setImageResource(R.drawable.fill_star);
                rat3.setImageResource(R.drawable.blank_star);
                rat4.setImageResource(R.drawable.blank_star);
                rat5.setImageResource(R.drawable.blank_star);
                tvRating.setText("2");
                break;
            case R.id.rat3:
                rat1.setImageResource(R.drawable.fill_star);
                rat2.setImageResource(R.drawable.fill_star);
                rat3.setImageResource(R.drawable.fill_star);
                rat4.setImageResource(R.drawable.blank_star);
                rat5.setImageResource(R.drawable.blank_star);
                tvRating.setText("3");
                break;
            case R.id.rat4:
                rat1.setImageResource(R.drawable.fill_star);
                rat2.setImageResource(R.drawable.fill_star);
                rat3.setImageResource(R.drawable.fill_star);
                rat4.setImageResource(R.drawable.fill_star);
                rat5.setImageResource(R.drawable.blank_star);
                tvRating.setText("4");
                break;
            case R.id.rat5:
                rat1.setImageResource(R.drawable.fill_star);
                rat2.setImageResource(R.drawable.fill_star);
                rat3.setImageResource(R.drawable.fill_star);
                rat4.setImageResource(R.drawable.fill_star);
                rat5.setImageResource(R.drawable.fill_star);
                tvRating.setText("5");
                break;
        }
    }
}
