package com.b2infosoft_jaipur.chakhle.activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.b2infosoft_jaipur.chakhle.webservice.WebServiceCaller;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class UserProfileActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnLogout;
    TextView tvProfile, tvPersonalDetails, tvName, tvEmail, tvPassword, tvDob, tvManageAddressTxt, tvManageAddress, tvWalletTxt, tvBalance, tvRS, tvAddMoney, tvAdd;
    Context mContext;
    SessionManager sessionManager;
    EditText edtName, edtEmail, edtDob;
    ImageView iconEditName, iconEditEmail, iconEditPassword, iconEditDob, iconArrow;
    TextView tvChangePassword, tvSkip, tvUpdate;
    EditText edtCurrentPass, edtNewPass, edtConfirmPass;
    int mYear, mMonth, mDay;
    String formattedDate = "";
    int edit_iconID;
    int check_iconID;
    ImageView locIcon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_setting);
        mContext = UserProfileActivity.this;
        sessionManager = new SessionManager(mContext);

        locIcon = (ImageView) findViewById(R.id.locIcon);
        locIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);

                onBackPressed();
            }
        });
        initView();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    /*    Intent intent2 = new Intent(mContext, ManageAddressActivity.class);
        Bundle bundle2 = new Bundle();
        bundle2.putString("FromWhere", "UserProfile");
        intent2.putExtras(bundle2);
        startActivity(intent2);*/
      //  UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);
    }

    private void initView() {
        edit_iconID = R.drawable.edit_icon;
        check_iconID = R.drawable.check_icon;
        tvProfile = (TextView) findViewById(R.id.tvProfile);
        tvPassword = (TextView) findViewById(R.id.tvPassword);
        tvDob = (TextView) findViewById(R.id.tvDob);
        tvManageAddressTxt = (TextView) findViewById(R.id.tvManageAddressTxt);
        tvManageAddress = (TextView) findViewById(R.id.tvManageAddress);
        tvPersonalDetails = (TextView) findViewById(R.id.tvPersonalDetails);
        tvName = (TextView) findViewById(R.id.tvName);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        tvWalletTxt = (TextView) findViewById(R.id.tvWalletTxt);
        //tvRS,tvAddMoney,tvAdd;
        tvBalance = (TextView) findViewById(R.id.tvBalance);
        tvRS = (TextView) findViewById(R.id.tvRS);
        tvAddMoney = (TextView) findViewById(R.id.tvAddMoney);
        tvAdd = (TextView) findViewById(R.id.tvAdd);
        iconEditName = (ImageView) findViewById(R.id.iconEditName);
        iconEditEmail = (ImageView) findViewById(R.id.iconEditEmail);
        iconEditPassword = (ImageView) findViewById(R.id.iconEditPassword);
        iconEditDob = (ImageView) findViewById(R.id.iconEditDob);
        iconArrow = (ImageView) findViewById(R.id.iconArrow);
        iconEditName.setOnClickListener(this);
        iconEditEmail.setOnClickListener(this);
        iconEditPassword.setOnClickListener(this);
        iconEditDob.setOnClickListener(this);
        iconArrow.setOnClickListener(this);

        tvWalletTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        tvPersonalDetails.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        tvName.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvEmail.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        btnLogout = (Button) findViewById(R.id.btnLogout);
        btnLogout.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        tvPassword.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvDob.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvManageAddressTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        tvManageAddress.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvBalance.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvRS.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvAddMoney.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));

        edtName = (EditText) findViewById(R.id.edtName);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtDob = (EditText) findViewById(R.id.edtDob);
        edtName.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        //edtDob.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtEmail.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));


        edtEmail.setCursorVisible(false);
        edtEmail.setClickable(false);
        edtEmail.setEnabled(false);
        edtName.setCursorVisible(false);
        edtName.setClickable(false);
        edtName.setEnabled(false);
        btnLogout.setOnClickListener(this);

        tvProfile.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));

        edtName.setText(sessionManager.getValuesSession(SessionManager.Key_UserFirstName));
        edtEmail.setText(sessionManager.getValuesSession(SessionManager.Key_UserEmail));
        edtDob.setText(sessionManager.getValuesSession(SessionManager.Key_UserDOB));

        iconEditName.setBackgroundResource(edit_iconID);
        iconEditName.setTag(edit_iconID);
        iconEditEmail.setBackgroundResource(edit_iconID);
        iconEditEmail.setTag(edit_iconID);
        iconEditDob.setBackgroundResource(edit_iconID);
        iconEditDob.setTag(edit_iconID);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View v) {
        edit_iconID = R.drawable.edit_icon;
        check_iconID = R.drawable.check_icon;
        Object tag;
        switch (v.getId()) {

            case R.id.iconEditName:
                tag = iconEditName.getTag();
                if (tag != null && ((Integer) tag).intValue() == edit_iconID) {
                    iconEditName.setBackgroundResource(check_iconID);
                    iconEditName.setTag(check_iconID);
                    edtName.setCursorVisible(true);
                    edtName.setClickable(true);
                    edtName.setEnabled(true);
                    edtName.requestFocus();
                    edtName.setBackgroundTintList(ColorStateList.valueOf(R.color.color_gray_light));
                } else {
                    iconEditName.setBackgroundResource(edit_iconID);
                    iconEditName.setTag(edit_iconID);
                    updateUserInfo();


                }
                /*if (iconEditName.getDrawable() == getResources().getDrawable(R.drawable.edit_icon)) {
                    iconEditName.setBackgroundResource(R.drawable.check_icon);
                    edtName.setCursorVisible(true);
                    edtName.setClickable(true);
                    edtName.setEnabled(true);
                    edtName.requestFocus();
                    edtName.setBackgroundTintList(ColorStateList.valueOf(R.color.color_gray_light));
                    updateUserInfo();
                } else {
                    iconEditName.setBackgroundResource(R.drawable.edit_icon);

                }*/


                //updateUserInfo();

                break;
            case R.id.iconEditEmail:
                tag = iconEditEmail.getTag();
                if (tag != null && ((Integer) tag).intValue() == edit_iconID) {
                    iconEditEmail.setBackgroundResource(check_iconID);
                    iconEditEmail.setTag(check_iconID);
                    edtEmail.setCursorVisible(true);
                    edtEmail.setClickable(true);
                    edtEmail.setEnabled(true);
                    edtEmail.setBackgroundTintList(ColorStateList.valueOf(R.color.color_gray_light));
                    edtEmail.requestFocus();
                } else {
                    iconEditEmail.setBackgroundResource(edit_iconID);
                    iconEditEmail.setTag(edit_iconID);
                    updateUserInfo();

                }

                // updateUserInfo();
                break;
            case R.id.iconEditPassword:
                openPasswordDialog();
                break;
            case R.id.iconEditDob:
                tag = iconEditDob.getTag();
                if (tag != null && ((Integer) tag).intValue() == edit_iconID) {
                    iconEditDob.setBackgroundResource(check_iconID);
                    iconEditDob.setTag(check_iconID);
                    edtDob.setVisibility(View.VISIBLE);
                    edtDob.setCursorVisible(false);
                    edtDob.setClickable(false);
                    edtDob.setEnabled(true);
                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    ArrayList<String> monthList = new ArrayList<>();
                                    monthList.add("Jan");
                                    monthList.add("Feb");
                                    monthList.add("Mar");
                                    monthList.add("Apr");
                                    monthList.add("May");
                                    monthList.add("Jun");
                                    monthList.add("Jul");
                                    monthList.add("Aug");
                                    monthList.add("Sep");
                                    monthList.add("Oct");
                                    monthList.add("Nov");
                                    monthList.add("Dec");
                                    String month = "";
                                    for (int i = 0; i < monthList.size(); i++) {
                                        if (monthOfYear == i) {
                                            month = monthList.get(i);
                                        }
                                    }
                                    formattedDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                                    edtDob.setText(dayOfMonth + "-" + (month) + "-" + year);
                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                } else {
                    iconEditDob.setBackgroundResource(edit_iconID);
                    iconEditDob.setTag(edit_iconID);
                    updateUserInfo();
                }

                //updateUserInfo();
                break;
            case R.id.iconArrow:
                Intent intent2 = new Intent(mContext, ManageAddressActivity.class);
                Bundle bundle2 = new Bundle();
                bundle2.putString("FromWhere", "UserProfile");
                intent2.putExtras(bundle2);
                startActivity(intent2);
                //startActivity(new Intent(mContext, ManageAddressActivity.class));
                break;

            case R.id.btnLogout:
                sessionManager.logoutUser();
                startActivity(new Intent(mContext, RestaurantListActivity.class));
                break;
        }

    }

    private void updateUserInfo() {
        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, AppGlobal.ApiCallWaitMsg, true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    //{"data":[],"message":"Register successfull.","success":true,"status":200}
                    if (mainObj.getString("success").equals("true")) {
                        JSONObject dataObject = mainObj.getJSONObject("data");
                        JSONObject userObject = dataObject.getJSONObject("User");
                        sessionManager.createLoginSession(userObject.getString("id"), userObject.getString("firstname"), userObject.getString("lastname"), userObject.getString("mobile"), userObject.getString("email"), "", "", "", "", "", userObject.getString("dob"),"");
                        UtilityMethod.showAlertBoxwithIntentClearHistory(mContext, "Updated successfull", UserProfileActivity.class);
                        // UtilityMethod.showAlertBox(mContext, "Password Updated successfull");
                    } else {
                        UtilityMethod.showAlertBox(mContext, "Updating Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        serviceCaller.addNameValuePair("name", edtName.getText().toString());
        serviceCaller.addNameValuePair("email", edtEmail.getText().toString());
        if (!formattedDate.equals("")) {
            serviceCaller.addNameValuePair("dob", formattedDate);
            Log.d("formattedDate>>>>", formattedDate);
        }
        serviceCaller.addNameValuePair("user_id", sessionManager.getValuesSession(SessionManager.Key_UserID));
        String url = AppGlobal.EditProfile;
        Log.d("url>>", url);
        serviceCaller.execute(url);


    }

    private void openPasswordDialog() {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_custom_password);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        //tvChangePassword, tvSkip, tvUpdate;
        //edtCurrentPass, edtNewPass, edtConfirmPass;
        tvChangePassword = (TextView) dialog.findViewById(R.id.tvChangePassword);
        tvSkip = (TextView) dialog.findViewById(R.id.tvSkip);
        tvUpdate = (TextView) dialog.findViewById(R.id.tvUpdate);
        edtCurrentPass = (EditText) dialog.findViewById(R.id.edtCurrentPass);
        edtNewPass = (EditText) dialog.findViewById(R.id.edtNewPass);
        edtConfirmPass = (EditText) dialog.findViewById(R.id.edtConfirmPass);
        tvChangePassword.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_bold.ttf"));
        tvUpdate.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_bold.ttf"));
        tvSkip.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        edtCurrentPass.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtConfirmPass.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        edtNewPass.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        dialog.show();
        dialog.setCancelable(false);
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!edtNewPass.getText().toString().equals("") && !edtConfirmPass.getText().toString().equals("") && !edtCurrentPass.getText().toString().equals("")) {
                    WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, AppGlobal.ApiCallWaitMsg, true) {
                        @Override
                        public void handleResponse(String response) {
                            Log.d("response", response);
                            try {
                                JSONObject mainObj = new JSONObject(response);
                                //{"data":[],"message":"Register successfull.","success":true,"status":200}
                                if (mainObj.getString("success").equals("true")) {
                                    dialog.dismiss();
                                    //UtilityMethod.showAlertBoxwithIntentClearHistory(mContext, "Password Updated successfull", UserProfileActivity.class);
                                    UtilityMethod.showAlertBox(mContext, "Password Updated successfull");
                                } else {
                                    UtilityMethod.showAlertBox(mContext, "Password Updating Failed");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    serviceCaller.addNameValuePair("current_password", edtCurrentPass.getText().toString());
                    serviceCaller.addNameValuePair("new_password", edtConfirmPass.getText().toString());
                    serviceCaller.addNameValuePair("user_id", sessionManager.getValuesSession(SessionManager.Key_UserID));
                    String url = AppGlobal.ChangePassword;
                    Log.d("url>>", url);
                    serviceCaller.execute(url);
                } else {
                    UtilityMethod.showAlertBox(mContext, "Please fill all fields");
                }
            }
        });
    }
}
