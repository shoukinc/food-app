package com.b2infosoft_jaipur.chakhle.activities;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;

public class ManageMobileActivity extends AppCompatActivity {

    LinearLayout layoutContainer;
    TextView tvManageMobile;
    Button btnAddNew;
    Context mContext;
    ImageView locIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_mobile);
        mContext = ManageMobileActivity.this;
        initView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);
    }

    private void initView() {
        locIcon = (ImageView) findViewById(R.id.locIcon);
        layoutContainer = (LinearLayout) findViewById(R.id.layoutContainer);
        tvManageMobile = (TextView) findViewById(R.id.tvManageMobile);
        btnAddNew = (Button) findViewById(R.id.btnAddNew);
        tvManageMobile.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        btnAddNew.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
        btnAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editText = new EditText(mContext);
                // Add an ID to it
                editText.setId(View.generateViewId());
                // Get the Hint text for EditText field which will be presented to the
                // user in the TextInputLayout
                editText.setHint("Add Mobile Number");
                // Set color of the hint text inside the EditText field
                editText.setHintTextColor(getResources().getColor(R.color.color_red));
                // Set the font size of the text that the user will enter
                editText.setTextSize(16);
                editText.setInputType(InputType.TYPE_CLASS_PHONE);
                // Set the color of the text inside the EditText field
                editText.setTextColor(getResources().getColor(android.R.color.black));
                editText.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_medium.ttf"));
                InputFilter[] filterArray = new InputFilter[1];
                filterArray[0] = new InputFilter.LengthFilter(10);
                editText.setFilters(filterArray);
                // Define layout params for the EditTExt field
                int left = R.dimen.layout_left;
                int right = R.dimen.layout_right;
                int top = R.dimen.layout_top;
                LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                //LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(100, 100);
                layoutParams2.setMargins(80, 50, 80, 50);
                editText.setLayoutParams(layoutParams2);
                TextInputLayout newAnswer = new TextInputLayout(mContext);
                newAnswer.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                newAnswer.addView(editText, layoutParams2);
                layoutContainer.addView(newAnswer);
            }
        });
        locIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtilityMethod.goNextClass(mContext, RestaurantListActivity.class);
            }
        });

    }
}
