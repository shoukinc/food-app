package com.b2infosoft_jaipur.chakhle.extra;

import android.content.Context;
import android.content.res.TypedArray;

import com.b2infosoft_jaipur.chakhle.R;

/**
 * Created by Microsoft on 5/19/2017.
 */

public class Utils {
    public static int getToolbarHeight(Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});
        int toolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        return toolbarHeight;
    }

    public static int getTabsHeight(Context context) {
        return (int) context.getResources().getDimension(R.dimen.tabsHeight);
    }
}
