package com.b2infosoft_jaipur.chakhle.sessions;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.b2infosoft_jaipur.chakhle.pojo.CartFillPojo;
import com.b2infosoft_jaipur.chakhle.pojo.NotificationPojo;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SessionManager {

    public static final String Key_UserID = "UserID";
    public static final String Key_UserMobile = "UserMobile";
    public static final String Key_UserFirstName = "UserFirstName";
    public static final String Key_UserLastName = "UserLastName";
    public static final String Key_UserEmail = "UserEmail";
    public static final String Key_UserCountryName = "UserCountryName";
    public static final String Key_UserStateName = "UserStateName";
    public static final String Key_UserCityName = "UserCityName";
    public static final String Key_UserLocation = "UserLocation";
    public static final String Key_ReferalCode = "UserReferal";
    public static final String Key_UserDOB = "DOB";
    public static final String Key_CartList = "Cart_list";
    public static final String Key_NotificationList = "Notification_list";
    public static final String Key_NotificationList_ = "";
    public static final String Key_NotificationList_count = "0";
    public static final String Key_UserBday = "b_day";
    public static final String Key_RecentPlaces = "aa";
    // User Details after logoin

    private static final String PREF_NAME = "ChakhlePref";
    private static final String IS_User = "IsUserIn";
//    private static final String IS_Online = "IsOnline";


    SharedPreferences pref;
    Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void createLoginSession(String UserId, String UserName, String lastName, String Mobile, String email, String userCountryName, String userStateName, String cserCityName, String userLocation, String referal, String DOB, String RecentPlaces) {
        // Storing login value as TRUE
        editor.putString(Key_UserID, UserId);
        editor.putString(Key_UserFirstName, UserName);
        editor.putString(Key_UserLastName, lastName);
        editor.putString(Key_UserMobile, Mobile);
        editor.putString(Key_UserEmail, email);
        editor.putString(Key_UserCountryName, userCountryName);
        editor.putString(Key_UserStateName, userStateName);
        editor.putString(Key_UserCityName, cserCityName);
        editor.putString(Key_UserLocation, userLocation);
        editor.putString(Key_ReferalCode, referal);
        editor.putString(Key_UserDOB, DOB);
        editor.putString(Key_RecentPlaces, RecentPlaces);
        editor.putBoolean(IS_User, true);
        editor.apply();
    }


    public void setValuesSession(String setKey, String setValue) {
        // Storing email in pref
        editor.putString(setKey, setValue);
        // commit changes
        editor.apply();
    }

    public void setKey_NotificationList_(String setValue) {
        // Storing email in pref
        editor.putString(Key_NotificationList_, setValue);
        // commit changes
        editor.apply();
    }



    public String getKey_NotificationList_() {

        return pref.getString(Key_NotificationList_, null);
    }

    public String getKey_NotificationList_count() {

        return pref.getString(Key_NotificationList_count, null);
    }
    public void setKey_NotificationList_count(String setValue) {
        // Storing email in pref
        editor.putString(Key_NotificationList_count, setValue);
        // commit changes
        editor.apply();
    }



    public String getValuesSession(String getKey) {

        return pref.getString(getKey, null);
    }

    public boolean getValuesSessionBoolean(String getKey) {

        return pref.getBoolean(getKey, false);
    }


    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
           /* Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);*/
        }

    }


   /* *//**
     * Get stored session data
     *//*
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        // user email id
        user.put(KEY_UserName, pref.getString(KEY_UserName, null));

        // return user
        return user;
    }*/

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        /*Intent i = new Intent(_context, SignUpActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
         
        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
         
        // Staring Login Activity
        _context.startActivity(i);*/
    }

    /**
     * Quick check for login
     **/
    // Get Login StatePojo
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_User, false);
    }

    public void saveCartList(Context context, ArrayList<CartFillPojo> cartList) {
        pref = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        editor = pref.edit();

        Gson gson = new Gson();
        String jsonCartList = gson.toJson(cartList);
        editor.putString(Key_CartList, jsonCartList);
        editor.apply();
    }

    public void saveLocationList(Context context, String location) {
        StringBuilder stringBuilder;
        String rct = getValuesSession(SessionManager.Key_RecentPlaces);

        if (rct.equals("")) {
            rct = location;
        } else {
            stringBuilder = new StringBuilder(rct);
            stringBuilder.append(",," + location);
            rct = stringBuilder.toString();
        }
        editor.putString(Key_RecentPlaces, rct);
        editor.apply();
        Log.d("Key_RecentPlaces>>>", rct);
    }

    public ArrayList<String> getLocationList(Context context) {
        List<String> locationList;
        pref = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        if (pref.contains(Key_RecentPlaces)) {
            String jsonFavorites = pref.getString(Key_RecentPlaces, null);
            Gson gson = new Gson();
            String[] favoriteItems = gson.fromJson(jsonFavorites, String[].class);
            locationList = Arrays.asList(favoriteItems);
            locationList = new ArrayList(locationList);
            Log.d("LocationList>>>", "" + locationList.size());
        } else
            return null;
        return (ArrayList<String>) locationList;
    }

    public void saveNotificationList(Context context, ArrayList<NotificationPojo> notificationList) {
        pref = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        editor = pref.edit();

        Gson gson = new Gson();
        String jsonNotificationList = gson.toJson(notificationList);
        editor.putString(Key_NotificationList, jsonNotificationList);
        editor.apply();
    }

    public ArrayList<NotificationPojo> getNotificationList(Context context) {
        List<NotificationPojo> notificationList;
        pref = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        if (pref.contains(Key_NotificationList)) {
            String jsonFavorites = pref.getString(Key_NotificationList, null);
            Gson gson = new Gson();
            NotificationPojo[] favoriteItems = gson.fromJson(jsonFavorites, NotificationPojo[].class);
            notificationList = Arrays.asList(favoriteItems);
            notificationList = new ArrayList(notificationList);
            Log.d("CartList>>>", "" + notificationList.size());
        } else
            return null;
        return (ArrayList<NotificationPojo>) notificationList;
    }

    public void clearArrayLocationList(Context context) {
        editor.remove(Key_RecentPlaces).commit();
    }

    public void clearArrayNotificationList(Context context) {
        editor.remove(Key_NotificationList).commit();
    }


    public void clearArrayList(Context context) {
        editor.remove(Key_CartList).commit();
    }

    public ArrayList<CartFillPojo> getCartList(Context context) {
        List<CartFillPojo> cartList;
        pref = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        if (pref.contains(Key_CartList)) {
            String jsonFavorites = pref.getString(Key_CartList, null);
            Gson gson = new Gson();
            CartFillPojo[] favoriteItems = gson.fromJson(jsonFavorites, CartFillPojo[].class);
            cartList = Arrays.asList(favoriteItems);
            cartList = new ArrayList(cartList);
            Log.d("CartList>>>", "" + cartList.size());
        } else
            return null;
        return (ArrayList<CartFillPojo>) cartList;
    }
}
