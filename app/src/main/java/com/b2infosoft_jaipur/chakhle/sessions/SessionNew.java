package com.b2infosoft_jaipur.chakhle.sessions;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.b2infosoft_jaipur.chakhle.pojo.CartFillPojo;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by b2andro on 11/24/2017.
 */

public class SessionNew {

    public static final String Key_CartList_New = "Cart_list_new";

    private static final String PREF_NAME_NEW = "CartLIstSession";

    SharedPreferences prefNew;
    SharedPreferences.Editor editorNew;
    Context _contextNew;
    int PRIVATE_MODE = 0;

    @SuppressLint("CommitPrefEdits")
    public SessionNew(Context context) {
        this._contextNew = context;
        prefNew = _contextNew.getSharedPreferences(PREF_NAME_NEW, PRIVATE_MODE);
        editorNew = prefNew.edit();
    }

    public void saveCartList(Context context, ArrayList<CartFillPojo> cartList) {
        prefNew = context.getSharedPreferences(PREF_NAME_NEW,
                Context.MODE_PRIVATE);
        editorNew = prefNew.edit();

        Gson gson = new Gson();
        String jsonCartList = gson.toJson(cartList);
        editorNew.putString(Key_CartList_New, jsonCartList);
        editorNew.apply();
    }

    public ArrayList<CartFillPojo> getCartList(Context context) {
        List<CartFillPojo> cartList;
        prefNew = context.getSharedPreferences(PREF_NAME_NEW,
                Context.MODE_PRIVATE);
        if (prefNew.contains(Key_CartList_New)) {
            String jsonFavorites = prefNew.getString(Key_CartList_New, null);
            Gson gson = new Gson();
            CartFillPojo[] favoriteItems = gson.fromJson(jsonFavorites, CartFillPojo[].class);
            cartList = Arrays.asList(favoriteItems);
            cartList = new ArrayList(cartList);
            Log.d("CartList>>>", "" + cartList.size());
        } else
            return null;
        return (ArrayList<CartFillPojo>) cartList;
    }

    public void clearArrayList(Context context) {
        editorNew.remove(Key_CartList_New).commit();
    }

}
