package com.b2infosoft_jaipur.chakhle.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.FragmentProductListAdapter;
import com.b2infosoft_jaipur.chakhle.pojo.RestaurentDetailsPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.util.ArrayList;

/**
 * Created by Microsoft on 5/22/2017.
 */

public class ProductListFragment extends Fragment {
    RecyclerView recycler_productList;
    Context mContext;
    SessionManager sessionManager;
    ArrayList<RestaurentDetailsPojo> arrayList = new ArrayList<>();
    String cateID = "";
    View view;

    public ProductListFragment() {
    }

    public ProductListFragment(String cateID) {
        this.cateID = cateID;
        AppGlobal.cateID = cateID;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product_list, container, false);
        mContext = getActivity();
        sessionManager = new SessionManager(mContext);
        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            if (cateID.equals(AppGlobal.RestaurentProductList.get(i).Product_category_id)) {
                arrayList.add(new RestaurentDetailsPojo(
                        AppGlobal.RestaurentProductList.get(i).name, "",
                        AppGlobal.RestaurentProductList.get(i).restaurant_id,
                        AppGlobal.RestaurentProductList.get(i).Product_id,
                        AppGlobal.RestaurentProductList.get(i).Product_name,
                        AppGlobal.RestaurentProductList.get(i).Product_description,
                        AppGlobal.RestaurentProductList.get(i).Product_image1,
                        AppGlobal.RestaurentProductList.get(i).Product_vegproduct,
                        AppGlobal.RestaurentProductList.get(i).Product_slug,
                        AppGlobal.RestaurentProductList.get(i).Product_status,
                        AppGlobal.RestaurentProductList.get(i).Product_price,
                        AppGlobal.RestaurentProductList.get(i).Product_category_id,
                        AppGlobal.RestaurentProductList.get(i).count,
                        AppGlobal.RestaurentProductList.get(i).afterDiscountValue, 0,
                        AppGlobal.RestaurentProductList.get(i).discount,
                        AppGlobal.RestaurentProductList.get(i).half,
                        AppGlobal.RestaurentProductList.get(i).medium,
                        AppGlobal.RestaurentProductList.get(i).full,
                        AppGlobal.RestaurentProductList.get(i).large,
                        AppGlobal.RestaurentProductList.get(i).half_label,
                        AppGlobal.RestaurentProductList.get(i).medium_label,
                        AppGlobal.RestaurentProductList.get(i).full_label,
                        AppGlobal.RestaurentProductList.get(i).large_label,
                        AppGlobal.RestaurentProductList.get(i).in_time,
                        AppGlobal.RestaurentProductList.get(i).out_time,
                        AppGlobal.RestaurentProductList.get(i).minimum_purchase,
                        AppGlobal.RestaurentProductList.get(i).extra_charge,
                        AppGlobal.RestaurentProductList.get(i).container_charge,
                        AppGlobal.RestaurentProductList.get(i).halfQty,
                        AppGlobal.RestaurentProductList.get(i).mediumQty,
                        AppGlobal.RestaurentProductList.get(i).fullQty,
                        AppGlobal.RestaurentProductList.get(i).largeQty,
                        AppGlobal.RestaurentProductList.get(i).discountType,
                        AppGlobal.RestaurentProductList.get(i).Restaddress
                        , AppGlobal.RestaurentProductList.get(i).let_nite_charge
                        , AppGlobal.RestaurentProductList.get(i).tax_percentage
                        , AppGlobal.RestaurentProductList.get(i).RestInTime
                        , AppGlobal.RestaurentProductList.get(i).Restoff_time_1
                ));
            }
        }
        recycler_productList = (RecyclerView) view.findViewById(R.id.recycler_productList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recycler_productList.setLayoutManager(linearLayoutManager);
        recycler_productList.setHasFixedSize(true);
        FragmentProductListAdapter adapter = new FragmentProductListAdapter(getActivity(), arrayList);
        recycler_productList.setAdapter(adapter);
        // adapter.notifyDataSetChanged();
        return view;
    }

    public void getProductList() {

        final ArrayList<RestaurentDetailsPojo> restaurentProductList = new ArrayList<>();
        /*WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait....", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("response", response);
                String isFav = "";

                try {
                    String restID = "", CateID = "";
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getBoolean("success") == true) {
                        JSONObject dataJson = mainObj.getJSONObject("data");
                        JSONObject resObj = dataJson.getJSONObject("Restaurant");
                        isFav = resObj.getString("is_favorite");
                        JSONArray mainProductArray = mainObj.getJSONArray("products");
                        JSONArray RestaurantCategoryArray = dataJson.getJSONArray("RestaurantCategory");

                        for (int j = 0; j < RestaurantCategoryArray.length(); j++) {
                            JSONObject RestaurantCategoryObject = RestaurantCategoryArray.getJSONObject(j);
                            restID = RestaurantCategoryObject.getString("restaurant_id");

                        }

                        for (int j = 0; j < mainProductArray.length(); j++) {
                            JSONObject productObject = mainProductArray.getJSONObject(j);
                            JSONObject dataProductObject = productObject.getJSONObject("Product");
                            restaurentProductList.add(new RestaurentDetailsPojo(
                                    isFav,
                                    restID,
                                    dataProductObject.getString("id"),
                                    dataProductObject.getString("name"),
                                    dataProductObject.getString("description"),
                                    dataProductObject.getString("image1"),
                                    dataProductObject.getString("vegproduct"),
                                    dataProductObject.getString("slug"),
                                    dataProductObject.getString("status"),
                                    dataProductObject.getString("price"),
                                    dataProductObject.getString("category_id"),
                                    0, 0, 0
                            ));
                        }
                        if (!restaurentProductList.isEmpty()) {

                            Log.d("RestaurantCategoryList>>>", "" + restaurentProductList.size());
                            arrayList = restaurentProductList;
                            recycler_productList = (RecyclerView) view.findViewById(R.id.recycler_productList);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
                            recycler_productList.setLayoutManager(linearLayoutManager);
                            recycler_productList.setHasFixedSize(true);
                            FragmentProductListAdapter adapter = new FragmentProductListAdapter(getActivity(), arrayList);
                            recycler_productList.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            //((RestaurentDetailsActivity) mContext).setProductList(restaurentProductList);
                        }
                    }
                } catch (JSONException e) {
                    if (!restaurentProductList.isEmpty()) {
                        arrayList = restaurentProductList;
                    }
                    //((AppCompatActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new RestaurentFragment(list, reviewList, discountList)).addToBackStack(null).commit();
                    e.printStackTrace();
                }

            }
        };

        Log.d("restID>>>", AppGlobal.RestaurentCategoryID);
        Log.d("category_id>>>", AppGlobal.CategoryID);
        serviceCaller.addNameValuePair("restaurants_id", AppGlobal.RestaurentCategoryID);
        serviceCaller.addNameValuePair("category_id", cateID);

        if (sessionManager.isLoggedIn()) {
            serviceCaller.addNameValuePair("user_id", sessionManager.getValuesSession(SessionManager.Key_UserID));
        } else {
            serviceCaller.addNameValuePair("user_id", "0");
        }
        String url = AppGlobal.RestaurentDetails;
        Log.d("url>>", url);
        serviceCaller.execute(url);*/


        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            if (AppGlobal.cateID.equals(AppGlobal.RestaurentProductList.get(i).Product_category_id)) {
                arrayList.add(new RestaurentDetailsPojo(
                        AppGlobal.RestaurentProductList.get(i).name,
                        "",
                        AppGlobal.RestaurentProductList.get(i).restaurant_id,
                        AppGlobal.RestaurentProductList.get(i).Product_id,
                        AppGlobal.RestaurentProductList.get(i).Product_name,
                        AppGlobal.RestaurentProductList.get(i).Product_description,
                        AppGlobal.RestaurentProductList.get(i).Product_image1,
                        AppGlobal.RestaurentProductList.get(i).Product_vegproduct,
                        AppGlobal.RestaurentProductList.get(i).Product_slug,
                        AppGlobal.RestaurentProductList.get(i).Product_status,
                        AppGlobal.RestaurentProductList.get(i).Product_price,
                        AppGlobal.RestaurentProductList.get(i).Product_category_id,
                        AppGlobal.RestaurentProductList.get(i).count,
                        AppGlobal.RestaurentProductList.get(i).afterDiscountValue,
                        0,
                        AppGlobal.RestaurentProductList.get(i).discount,
                        AppGlobal.RestaurentProductList.get(i).half,
                        AppGlobal.RestaurentProductList.get(i).medium,
                        AppGlobal.RestaurentProductList.get(i).full,
                        AppGlobal.RestaurentProductList.get(i).large,
                        AppGlobal.RestaurentProductList.get(i).half_label,
                        AppGlobal.RestaurentProductList.get(i).medium_label,
                        AppGlobal.RestaurentProductList.get(i).full_label,
                        AppGlobal.RestaurentProductList.get(i).large_label,
                        AppGlobal.RestaurentProductList.get(i).in_time,
                        AppGlobal.RestaurentProductList.get(i).out_time,
                        AppGlobal.RestaurentProductList.get(i).minimum_purchase,
                        AppGlobal.RestaurentProductList.get(i).extra_charge,
                        AppGlobal.RestaurentProductList.get(i).container_charge, 0, 0, 0, 0,
                        AppGlobal.RestaurentProductList.get(i).discountType
                        , AppGlobal.RestaurentProductList.get(i).Restaddress
                        , AppGlobal.RestaurentProductList.get(i).let_nite_charge
                        , AppGlobal.RestaurentProductList.get(i).tax_percentage
                        , AppGlobal.RestaurentProductList.get(i).RestInTime
                        , AppGlobal.RestaurentProductList.get(i).Restoff_time_1


                ));
            }
        }
      /*  recycler_productList = (RecyclerView) view.findViewById(R.id.recycler_productList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recycler_productList.setLayoutManager(linearLayoutManager);
        recycler_productList.setHasFixedSize(true);
        FragmentProductListAdapter adapter = new FragmentProductListAdapter(getActivity(), arrayList);
        recycler_productList.setAdapter(adapter);
        adapter.notifyDataSetChanged();*/


    }
}
