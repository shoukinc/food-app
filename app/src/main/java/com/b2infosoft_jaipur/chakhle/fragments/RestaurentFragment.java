package com.b2infosoft_jaipur.chakhle.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.activities.RestaurantListActivity;
import com.b2infosoft_jaipur.chakhle.adapter.AdvertiesViewPagerAdapter;
import com.b2infosoft_jaipur.chakhle.adapter.HorizontalOptionsAdapter;
import com.b2infosoft_jaipur.chakhle.adapter.MyAdapter;
import com.b2infosoft_jaipur.chakhle.adapter.RestaurentListAdapter;

import com.b2infosoft_jaipur.chakhle.pojo.AdvertiesPojo;
import com.b2infosoft_jaipur.chakhle.pojo.HorizontalAdvPojo;
import com.b2infosoft_jaipur.chakhle.pojo.ResturentListPojo;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

import me.angeldevil.autoscrollviewpager.AutoScrollViewPager;

import static android.support.v7.widget.RecyclerView.LayoutManager;


/**
 * Created by Microsoft on 5/11/2017.
 */

public class RestaurentFragment extends Fragment {

    View mainView;
    Context mContext;
    //ViewPager viewpager_adverties;
    AutoScrollViewPager viewpager_adverties;
    AdvertiesViewPagerAdapter viewPagerAdapter;
    //ArrayList<AdvertiesPojo> advertiesPojosList = new ArrayList<>();
    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000;
    RecyclerView recycler_horizontalList;
    HorizontalOptionsAdapter horizontalOptionsAdapter;
    RecyclerView recycler_resturentList;
    ArrayList<HorizontalAdvPojo> horizontalAdvPojos = new ArrayList<>();
    RestaurentListAdapter restaurentListAdapter;
    ArrayList<ResturentListPojo> resturentListPojos;
    ArrayList<ResturentListPojo> discountList;
    ArrayList<ResturentListPojo> reviewList;
    private int mToolbarHeight;
    Animation slide_up, slide_down;
    LinearLayout linearLayout;
    String location = "";
    String city = "";

    public RestaurentFragment() {

    }

    public RestaurentFragment(ArrayList<ResturentListPojo> resturentListPojos, ArrayList<ResturentListPojo> discountList) {
        this.resturentListPojos = resturentListPojos;
        this.discountList = discountList;
        Log.d("resturentListPojos", "" + resturentListPojos.size());
        Log.d("discountList", "" + discountList.size());

    }

    public RestaurentFragment(String locationAddress, String city) {
        this.location = locationAddress;
        this.city = city;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_restaurent, container, false);
        mContext = getActivity();

        AdvertiesPojo.getAdvt_List(mainView, mContext, AppGlobal.City);
        HorizontalAdvPojo.getCategoriesWithImageList(mainView, mContext);
        Log.d("LocationAddress>>>>", AppGlobal.LocationAddress);

        if (((RestaurantListActivity) mContext).from.equals("RestaurentFilterActivity")) {
            ResturentListPojo.getResturentList(mainView, mContext, AppGlobal.LocationAddress, "RestaurentFilterActivity", "0");

        } else {
            ResturentListPojo.getResturentList(mainView, mContext, AppGlobal.LocationAddress, "RestFragment", "0");
        }
        return mainView;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void initItem(View mainView) {
        recycler_resturentList = (RecyclerView) mainView.findViewById(R.id.recycler_resturentList);
        viewpager_adverties = (AutoScrollViewPager) mainView.findViewById(R.id.viewpager_adverties);
        viewpager_adverties.setAdapter(new MyAdapter(mContext, AppGlobal.AdvertiseList));
        // viewpager_adverties.setPageTransformer(true, new ZoomOutPageTransformer());
       /* viewpager_adverties.startAutoScroll(2500);
        viewpager_adverties.setCycle(true);
        viewpager_adverties.setDirection(AutoScrollViewPager.DIRECTION_RIGHT);*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            viewpager_adverties.setNestedScrollingEnabled(true);
        }
        viewpager_adverties.setClipToPadding(false);
        viewpager_adverties.setPadding(48, 10, 48, 10);
        viewpager_adverties.setPageMargin(15);

        // }

        int duration = 100;
        final LayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        //ScrollingLinearLayoutManager scrollingLinearLayoutManager = new ScrollingLinearLayoutManager(mContext);
        recycler_resturentList.setLayoutManager(linearLayoutManager);
        restaurentListAdapter = new RestaurentListAdapter(mContext, resturentListPojos, discountList);
        recycler_resturentList.setAdapter(restaurentListAdapter);
        recycler_resturentList.setNestedScrollingEnabled(false);
    }


    public void setAdapterData(View v, final Context mContext, ArrayList<ResturentListPojo> list, ArrayList<ResturentListPojo> discountList) {
        mainView = v;
        this.resturentListPojos = list;
        // AppGlobal.GlobalRestorentList = list;
        this.discountList = discountList;
        Log.d("resturentListPojos", "" + resturentListPojos.size());
        Log.d("discountList", "" + discountList.size());
        recycler_resturentList = (RecyclerView) mainView.findViewById(R.id.recycler_resturentList);
        viewpager_adverties = (AutoScrollViewPager) mainView.findViewById(R.id.viewpager_adverties);
       /* recycler_horizontalList = (RecyclerView) mainView.findViewById(R.id.recycler_horizontalList);*/
      /*  LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        recycler_horizontalList.setLayoutManager(layoutManager);*/
       /* for (int i = 0; i < 5; i++) {
            integerArrayList.add(R.drawable.img_a);
        }*/
        //horizontalAdvPojos.add(new HorizontalAdvPojo(R.drawable.rest, "Suggest"));
       /* horizontalAdvPojos.add(new HorizontalAdvPojo(mContext,String.valueOf(R.drawable.veg), "Veg"));
        horizontalAdvPojos.add(new HorizontalAdvPojo(mContext,String.valueOf(R.drawable.non), "Non Veg"));
        horizontalAdvPojos.add(new HorizontalAdvPojo(mContext,String.valueOf(R.drawable.rest), "Both"));*/
        // horizontalAdvPojos.add(new HorizontalAdvPojo(R.drawable.veg, "Suggest"));
        // horizontalAdvPojos.add(new HorizontalAdvPojo(R.drawable.rest, "Veg Restro"));
       /* horizontalOptionsAdapter = new HorizontalOptionsAdapter(mContext, horizontalAdvPojos,mainView);
        recycler_horizontalList.setAdapter(horizontalOptionsAdapter);*/
        /*viewpager_adverties.setAdapter(new AdvertiesViewPagerAdapter(viewpager_adverties, AppGlobal.AdvertiseList, mContext));
        viewpager_adverties.startAutoScroll(3000);
        viewpager_adverties.setCycle(true);
        //viewpager_adverties.setSlideDuration(5000);
        //viewpager_adverties.setDirection(AutoScrollViewPager.DIRECTION_RIGHT);*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            viewpager_adverties.setNestedScrollingEnabled(true);
        }
        viewpager_adverties.setClipToPadding(false);
        viewpager_adverties.setPadding(48, 10, 48, 10);
        viewpager_adverties.setPageMargin(15);

        viewpager_adverties.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return AppGlobal.AdvertiseList.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object o) {
                return view == o;
            }

            @Override
            public Object instantiateItem(ViewGroup container, final int position) {
                ImageView view = new ImageView(container.getContext());
                view.setScaleType(ImageView.ScaleType.CENTER_CROP);
                Glide.with(mContext)
                        .load(AppGlobal.AdvertiseList.get(position).image)
                        .crossFade()
                        .into(view);
                container.addView(view);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        {
                            CallFragment callFragment;
                            callFragment = (CallFragment) mContext;

                            if (AppGlobal.AdvertiseList.size() > 0) {
                                if (AppGlobal.AdvertiseList.get(position).off_time.equals("") || AppGlobal.AdvertiseList.get(position).off_time.equals("0")) {
                                    UtilityMethod.showAlertBox(mContext, "Time not updated by restaurant admin.");
                                } else {

                                    String in_time = AppGlobal.AdvertiseList.get(position).off_time;
                                    String out_time = AppGlobal.AdvertiseList.get(position).off_time_1;
                                    try {
                                        Date mToday = new Date();
                                        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm:ss");
                                        String curTime = sdf.format(mToday);
                                        Date start = sdf.parse(in_time);
                                        Date end = sdf.parse(out_time);
                                        Date userDate = sdf.parse(curTime);

                                        if (end.before(start)) {
                                            Calendar mCal = Calendar.getInstance();
                                            mCal.setTime(end);
                                            mCal.add(Calendar.DAY_OF_YEAR, 1);
                                            end.setTime(mCal.getTimeInMillis());
                                        }
                                        Log.d("curTime", userDate.toString());
                                        Log.d("start", start.toString());
                                        Log.d("end", end.toString());
                                        if (userDate.after(start) && userDate.before(end)) {
                                            Log.d("currentPage>>>", "" + currentPage);
                                            AppGlobal.RestDiscount = "";
                                            AppGlobal.RestaurentCategoryID = "";
                                            AppGlobal.RestaurentName = "";
                                            AppGlobal.RestarentImage = "";
                                            AppGlobal.RestarentAddress = "";
                                            AppGlobal.RestRating = "";
                                            AppGlobal.RestBanner = "";
                                            AppGlobal.RestaurentCategoryID = AppGlobal.AdvertiseList.get(position).restaurant_id;
                                            AppGlobal.RestaurentName = AppGlobal.AdvertiseList.get(position).name;
                                            AppGlobal.RestarentImage = AppGlobal.AdvertiseList.get(position).image;
                                            AppGlobal.RestBanner = AppGlobal.AdvertiseList.get(position).hotel_banner;
                                            AppGlobal.RestarentAddress = AppGlobal.AdvertiseList.get(position).link;
                                            Log.d("link>>>", AppGlobal.AdvertiseList.get(position).hotel_banner);
                                            //AppGlobal.RestRating = advertiesPojosList.get(position).TotalRating;
                                            // AppGlobal.RestBanner = advertiesPojosList.get(position).banner_image;
                                            Log.d("RestID>>>", AppGlobal.RestaurentCategoryID);
                                            Log.d("Name>>>", AppGlobal.RestaurentName);
                                            callFragment.setFragment("RestaurentDetailsFragment");
                                            //mContext.startActivity(new Intent(mContext, RestaurentDetailsActivity.class));
                                        } else {
                                            java.text.SimpleDateFormat _12HourSDF = new java.text.SimpleDateFormat("hh:mm a");
                                            UtilityMethod.showAlertBox(mContext, "This Restaurent is open only " + _12HourSDF.format(start) + " to " + _12HourSDF.format(end));
                                        }
                                    } catch (ParseException e) {
                                        // Invalid date was entered
                                    }
                                }
                            }
                        }
                    }
                });
                return view;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View) object);
            }
        });

        viewpager_adverties.setScrollFactor(5);
        viewpager_adverties.setOffscreenPageLimit(4);
        viewpager_adverties.startAutoScroll(2500);
        int duration = 100;
        final LayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recycler_resturentList.setLayoutManager(linearLayoutManager);
        restaurentListAdapter = new RestaurentListAdapter(mContext, resturentListPojos, discountList);
        recycler_resturentList.setAdapter(restaurentListAdapter);
        recycler_resturentList.setNestedScrollingEnabled(false);

    }

    public void setCategoryListWithImage(View v, final Context mContext, ArrayList<HorizontalAdvPojo> horizontalAdvPojos) {
        mainView = v;
        this.horizontalAdvPojos = horizontalAdvPojos;
        recycler_horizontalList =  mainView.findViewById(R.id.recycler_horizontalList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        recycler_horizontalList.setLayoutManager(layoutManager);
        horizontalOptionsAdapter = new HorizontalOptionsAdapter(mContext, horizontalAdvPojos, mainView);
        recycler_horizontalList.setAdapter(horizontalOptionsAdapter);
    }
}
