package com.b2infosoft_jaipur.chakhle.fragments;

import com.b2infosoft_jaipur.chakhle.pojo.RestaurentDetailsPojo;

import java.util.ArrayList;

/**
 * Created by u on 14-Oct-17.
 */

public interface NewCartDataFill {

    public void newCartFillData(ArrayList<RestaurentDetailsPojo> RestaurentProductList);

}
