package com.b2infosoft_jaipur.chakhle.fragments;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.pojo.RestaurentDetailsPojo;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by Microsoft on 5/24/2017.
 */

public class RestaurentDetailsFragment extends Fragment implements View.OnClickListener {
    View myView;
    Toolbar toolbar;
    ViewPager viewPager;
    TabLayout tabLayout;
    CollapsingToolbarLayout collapsingToolbarLayout;
    Context mContext;
    ImageView htab_header;
    TextView tvDiscount, tvAddress;
    ImageView imgRestLogo, imgFav, imgShare, imgSearch, imgDiscount;
    EditText autoCompView;
    //ImageView rat1, rat2, rat3, rat4, rat5;
    RelativeLayout rlll;
    CallFragment callFragment;
    Animation slide_up, slide_down;
    LinearLayout linearLayout;

    public RestaurentDetailsFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.activity_restaurent_details, container, false);
        mContext = getActivity();
        initView(myView);
        return myView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        linearLayout = (LinearLayout) getActivity().findViewById(R.id.linearBottom);
        if (linearLayout.getVisibility() == View.VISIBLE) {
            slide_down = AnimationUtils.loadAnimation(getActivity(),
                    R.anim.slide_down);
            linearLayout.startAnimation(slide_down);
            linearLayout.setVisibility(View.GONE);
        }
    }

    private void initView(View myView) {
        linearLayout = (LinearLayout) getActivity().findViewById(R.id.linearBottom);
        toolbar = (Toolbar) myView.findViewById(R.id.htab_toolbar);
        ((AppCompatActivity) mContext).setSupportActionBar(toolbar);
        ((AppCompatActivity) mContext).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (((AppCompatActivity) mContext).getSupportActionBar() != null) {
            ((AppCompatActivity) mContext).getSupportActionBar().setTitle(AppGlobal.RestaurentName);
        }
        viewPager = (ViewPager) myView.findViewById(R.id.htab_viewpager);
        autoCompView = (EditText) myView.findViewById(R.id.autoCompleteTextView);
        autoCompView.setCursorVisible(false);
        autoCompView.setClickable(false);
        autoCompView.setEnabled(false);
        autoCompView.setText(AppGlobal.RestaurentName);
        autoCompView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));

        //rlll = (RelativeLayout) myView.findViewById(R.id.rlll);
        htab_header = (ImageView) myView.findViewById(R.id.htab_header);
        imgRestLogo = (ImageView) myView.findViewById(R.id.imgRestLogo);
        imgShare = (ImageView) myView.findViewById(R.id.imgShare);
        imgSearch = (ImageView) myView.findViewById(R.id.imgSearch);
        imgFav = (ImageView) myView.findViewById(R.id.imgFav);
        imgDiscount = (ImageView) myView.findViewById(R.id.imgDiscount);
        tvDiscount = (TextView) myView.findViewById(R.id.tvDiscount);
        tvAddress = (TextView) myView.findViewById(R.id.tvAddress);
        /*rat1 = (ImageView) myView.findViewById(R.id.rat1);
        rat2 = (ImageView) myView.findViewById(R.id.rat2);
        rat3 = (ImageView) myView.findViewById(R.id.rat3);
        rat4 = (ImageView) myView.findViewById(R.id.rat4);
        rat5 = (ImageView) myView.findViewById(R.id.rat5);*/
        tvAddress.setText(AppGlobal.RestarentAddress);
        tvAddress.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_regular.ttf"));
        tvDiscount.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
        if (!AppGlobal.RestDiscount.equals("")) {
            tvDiscount.setVisibility(View.VISIBLE);
            imgDiscount.setVisibility(View.VISIBLE);
            tvDiscount.setText("OFF " + AppGlobal.RestDiscount + "%");
        }
        Glide.with(mContext).load(AppGlobal.RestarentImage).into(htab_header);
        Glide.with(mContext).load(AppGlobal.RestarentImage).into(imgRestLogo);
        RestaurentDetailsPojo.getRestaurentDetails(myView, mContext, AppGlobal.RestaurentCategoryID,"0");
        imgSearch.setOnClickListener(this);
        imgFav.setOnClickListener(this);


        /*if (AppGlobal.RestRating.equals("1")) {
            rat1.setImageResource(R.drawable.fill_star);


        } else if (AppGlobal.RestRating.equals("2")) {
            rat1.setImageResource(R.drawable.fill_star);
            rat2.setImageResource(R.drawable.fill_star);

        } else if (AppGlobal.RestRating.equals("3")) {
            rat1.setImageResource(R.drawable.fill_star);
            rat2.setImageResource(R.drawable.fill_star);
            rat3.setImageResource(R.drawable.fill_star);

        } else if (AppGlobal.RestRating.equals("4")) {
            rat1.setImageResource(R.drawable.fill_star);
            rat2.setImageResource(R.drawable.fill_star);
            rat3.setImageResource(R.drawable.fill_star);
            rat4.setImageResource(R.drawable.fill_star);

        } else if (AppGlobal.RestRating.equals("5")) {
            rat1.setImageResource(R.drawable.fill_star);
            rat2.setImageResource(R.drawable.fill_star);
            rat3.setImageResource(R.drawable.fill_star);
            rat4.setImageResource(R.drawable.fill_star);
            rat5.setImageResource(R.drawable.fill_star);

        } else {
            rat1.setImageResource(R.drawable.heart_bookmark_outline);
            rat2.setImageResource(R.drawable.heart_bookmark_outline);
            rat3.setImageResource(R.drawable.heart_bookmark_outline);
            rat4.setImageResource(R.drawable.heart_bookmark_outline);
            rat5.setImageResource(R.drawable.heart_bookmark_outline);
        }*/

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (linearLayout.getVisibility() == View.VISIBLE) {
                    slide_down = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.slide_down);
                    linearLayout.startAnimation(slide_down);
                    linearLayout.setVisibility(View.GONE);

                }
                RestaurentDetailsFragment restaurentDetailsFragment = new RestaurentDetailsFragment();
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(restaurentDetailsFragment);
                trans.commit();
                manager.popBackStack();
            }
        });
    }

    public void setProductList(View view, Context ctx, final ArrayList<RestaurentDetailsPojo> restaurantCategoryList, final ArrayList<RestaurentDetailsPojo> restaurentProductList) {

        myView = view;
        mContext = ctx;
        AppGlobal.RestaurentProductList.clear();
        AppGlobal.RestaurentProductList = restaurentProductList;
        viewPager = (ViewPager) myView.findViewById(R.id.htab_viewpager);
        collapsingToolbarLayout = (CollapsingToolbarLayout) myView.findViewById(R.id.htab_collapse_toolbar);
        collapsingToolbarLayout.setContentScrimColor(mContext.getResources().getColor(R.color.colorAccent));
        collapsingToolbarLayout.setStatusBarScrimColor(mContext.getResources().getColor(R.color.color_black));
        tabLayout = (TabLayout) myView.findViewById(R.id.htab_tabs);

        for (int i = 0; i < restaurantCategoryList.size(); i++) {
            tabLayout.addTab(tabLayout.newTab().setText(restaurantCategoryList.get(i).Category_name));
        }
        tabLayout.setupWithViewPager(viewPager);
        /*RestaruentDetailViewPager adapter = new RestaruentDetailViewPager(((AppCompatActivity) mContext).getSupportFragmentManager());
        for (int i = 0; i < restaurantCategoryList.size(); i++) {
            if (restaurantCategoryList.get(i).Category_name.equals("RECOMMENDED")) {
                // AppGlobal.CategoryID = "0";
                adapter.addFrag(new RecommendedFragment(), restaurantCategoryList.get(i).Category_name);
               *//* CallFragment callFragment = (CallFragment) mContext;
                callFragment.setFragment("RecommendedFragment");*//*
            } else {
                // AppGlobal.CategoryID = restaurantCategoryList.get(i).category_id;
                adapter.addFrag(new ProductListFragment(), restaurantCategoryList.get(i).Category_name);

            }

        }*/
        //viewPager.setAdapter(adapter);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                AppGlobal.CategoryID = restaurantCategoryList.get(tab.getPosition()).category_id;
                viewPager.setCurrentItem(tab.getPosition());

                /*Log.d(TAG, "onTabSelected: pos: " + tab.getPosition());
                switch (tab.getPosition()) {

                    case 0:
                        Toast.makeText(getApplicationContext(), "One", Toast.LENGTH_SHORT).show();
                        //  showToast("One");
                        break;
                    case 1:
                        Toast.makeText(getApplicationContext(), "Two", Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        Toast.makeText(getApplicationContext(), "Three", Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        Toast.makeText(getApplicationContext(), "Two", Toast.LENGTH_SHORT).show();
                        break;
                    case 4:
                        Toast.makeText(getApplicationContext(), "Three", Toast.LENGTH_SHORT).show();
                        break;
                    case 5:
                        Toast.makeText(getApplicationContext(), "Two", Toast.LENGTH_SHORT).show();
                        break;
                }*/
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.imgSearch:
                toolbar.setTitle("");
                // rlll.setVisibility(View.VISIBLE);
                autoCompView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "font/raleway_semibold.ttf"));
                autoCompView.getBackground().setColorFilter(getResources().getColor(R.color.color_white), PorterDuff.Mode.SRC_ATOP);
                autoCompView.setVisibility(View.VISIBLE);
                autoCompView.setCursorVisible(true);
                autoCompView.setClickable(true);
                autoCompView.setEnabled(true);
                autoCompView.setHint("");
                autoCompView.setText("");
                autoCompView.requestFocus();
              /*  InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(autoCompView, InputMethodManager.SHOW_IMPLICIT);*/
                break;
            case R.id.imgFav:

                Object tag = imgFav.getTag();
                int backgroundId = R.drawable.fav_unfill;
                int backgroundId2 = R.drawable.fav_fill;

                if (tag != null && ((Integer) tag).intValue() == backgroundId) {
                    //backgroundId = R.drawable.bg_image_1;
                    imgFav.setBackgroundResource(backgroundId2);
                    imgFav.setTag(backgroundId2);

                } else {
                    imgFav.setBackgroundResource(backgroundId);
                    imgFav.setTag(backgroundId);

                }
            /*    textView.setTag(backgroundId);
                textView.setBackgroundResource(backgroundId);*/


                break;
        }

    }


}
