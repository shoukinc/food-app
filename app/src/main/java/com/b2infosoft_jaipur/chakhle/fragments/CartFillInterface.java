package com.b2infosoft_jaipur.chakhle.fragments;

import com.b2infosoft_jaipur.chakhle.pojo.CartFillPojo;

import java.util.ArrayList;

/**
 * Created by u on 24-Oct-17.
 */

public interface CartFillInterface {

    public void newCartFillData(ArrayList<CartFillPojo> cartFillPojos);

}
