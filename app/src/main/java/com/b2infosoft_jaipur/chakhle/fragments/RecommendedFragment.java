package com.b2infosoft_jaipur.chakhle.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.adapter.RecommendedAdapter;
import com.b2infosoft_jaipur.chakhle.pojo.CartFillPojo;
import com.b2infosoft_jaipur.chakhle.pojo.RestaurentDetailsPojo;
import com.b2infosoft_jaipur.chakhle.sessions.SessionManager;
import com.b2infosoft_jaipur.chakhle.webservice.AppGlobal;

import java.util.ArrayList;


/**
 * Created by Microsoft on 5/15/2017.
 */

public class RecommendedFragment extends Fragment {
    RecyclerView recyclerView;
    ArrayList<RestaurentDetailsPojo> restaurentProductList;
    SessionManager sessionManager;
    Context context;
    ArrayList<CartFillPojo> mainSessonCartList = null;

    public RecommendedFragment() {

    }

    public RecommendedFragment(ArrayList<RestaurentDetailsPojo> restaurentProductList) {
        this.restaurentProductList = restaurentProductList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dummy_fragment, container, false);
        context = getActivity();
        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.dummyfrag_bg);
        recyclerView = (RecyclerView) view.findViewById(R.id.dummyfrag_scrollableview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            Log.d("mediumQty",AppGlobal.RestaurentProductList.get(i).mediumQty+" quenty");
            Log.d("halfQty",AppGlobal.RestaurentProductList.get(i).halfQty+" quenty");
            Log.d("largeQty",AppGlobal.RestaurentProductList.get(i).largeQty+" quenty");
            Log.d("fullQty",AppGlobal.RestaurentProductList.get(i).fullQty+" quenty");
        }

        RecommendedAdapter adapter = new RecommendedAdapter(getActivity(), AppGlobal.RestaurentProductList);
        recyclerView.setAdapter(adapter);
       /*  int count = 0;
        for (int i = 0; i < AppGlobal.RestaurentProductList.size(); i++) {
            count = count + AppGlobal.RestaurentProductList.get(i).count;
        }
        CartValueInterface callFragment;
        callFragment = (CartValueInterface) getActivity();
        callFragment.setCartData(count, "visibleCart", AppGlobal.RestaurentCategoryID);*/
        return view;
    }
}

