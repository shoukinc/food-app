package com.b2infosoft_jaipur.chakhle.multipart;

import android.app.Application;

import com.b2infosoft_jaipur.chakhle.crash_handler.CrashErrorReporter;


/**
 * Created by Kushal on 27-May-16.
 */
public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        CrashErrorReporter mCrashErrorReporter = CrashErrorReporter.getInstance();
        // Activate the ErrorReporter
        mCrashErrorReporter.Init(getApplicationContext());
        mCrashErrorReporter.CheckCrashErrorAndSendMail(getApplicationContext());


    }
}
