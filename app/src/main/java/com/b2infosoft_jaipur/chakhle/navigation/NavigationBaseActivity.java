package com.b2infosoft_jaipur.chakhle.navigation;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.b2infosoft_jaipur.chakhle.R;
import com.b2infosoft_jaipur.chakhle.useful.UtilityMethod;


public class NavigationBaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    protected NavigationView navigationView;
    Toolbar toolbar;
    ActionBarDrawerToggle mDrawerToggle;
    DrawerLayout drawerLayout;
    Menu menuNav;
    ImageView user_picture;
    Context mContext;
    TextView user_name;
    View header;
    Activity activity;
    String locationAddress = "", address = "";
    TextView tvCity, tvArea;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_base);
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void setContentView(int view) {
        super.setContentView(view);

        setDrawer();
        findValues(view);
        if (navigationView != null) {
            navigationView.setItemIconTintList(null);
            navigationView.setNavigationItemSelectedListener(this);

        }


    }


    public void setDrawer() {
        drawerLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_navigation_base, null);
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                UtilityMethod.hideKeyboardForFocusedView(activity);

                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                UtilityMethod.hideKeyboardForFocusedView(activity);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();

            }
        };
        drawerLayout.addDrawerListener(mDrawerToggle);

    }

    public void findValues(int view) {

        activity = NavigationBaseActivity.this;
        FrameLayout activityContainer = (FrameLayout) drawerLayout.findViewById(R.id.activity_content);
        getLayoutInflater().inflate(view, activityContainer, true);
        super.setContentView(drawerLayout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        navigationView =  findViewById(R.id.navigationView);

        View headerView = navigationView.inflateHeaderView(R.layout.nav_header);


        user_name =  headerView.findViewById(R.id.tv_userName);
        user_picture = headerView.findViewById(R.id.imgUser);
        user_picture.setImageResource(R.drawable.call_icon_white);
        // user_name.setText(AppGlobal.UserName);
        /*if (AppGlobal.ProfilePic != null) {
            Glide.with(getApplicationContext())
                    .load(AppGlobal.ProfilePic)
                    .centerCrop()
                    .placeholder(R.drawable.animation_loading)
                    .crossFade()
                    .into(user_picture);
        }
*/
        //if (AppGlobal.UserName != null) {
        user_name.setText("Guest User");

        // }


    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        if (item.isChecked())
            item.setChecked(true);
        else
            item.setChecked(false);

        switch (item.getItemId()) {
           /* case R.id.nav_Dashboard:
                drawerLayout.closeDrawers();

                UtilityMethod.goNextClass(NavigationBaseActivity.this, DashboardActivity.class);
                return true;

*/
            default:
                return true;
        }

    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {


            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

